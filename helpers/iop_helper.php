<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	
defined('FCPATH') or define('FCPATH', str_replace(APPPATH . 'helpers', '', pathinfo(__FILE__, PATHINFO_DIRNAME)));
defined('ENVIRONMENT') or define('ENVIRONMENT', 'development');

date_default_timezone_set("Asia/Jakarta");
$php_ver = PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;
$gBulan = array(1=>'Januari', 'Februari', 'Maret',  'April',  'Mei',  'Juni',  'Juli',  'Agustus',  'September',  'Oktober',  'November',  'Desember');
$gBln 	= array(1=>'Jan', 'Feb', 'Mar',  'Apr',  'Mei',  'Jun',  'Jul',  'Agt',  'Sep',  'Okt',  'Nov',  'Des');
	
	if (!function_exists('dump')) {
		function dump() {
			$args = func_get_args();
			$argn = count($args);
			
			$argp = null;
			if ($args[0] != 'p')
				echo "<div class='row'>";
			
			foreach ($args as $k => $arg) {
				if (!empty($arg) && !is_bool($arg)) {
					switch ($arg) {
						case 'p':
							break;
						
						case '#':
						case 'die':
							http_response_code(200);
							debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5);
							die('</div>');
							break;
						
						case 'tbl':
							http_response_code(200);
							echo arr__table($args[$argp]);
							die('</div>');
							break;
						
						case 'trace':
							echo "<pre style='font-size:8pt; color:darkblue; padding:0 20px; border:1px yellowgreen solid'>";
							debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5);
							echo "</pre>";
							break;
						
						default:
							switch (gettype($arg)) {
								case 'string':
								case 'integer':
								case 'double':
									echo "<pre style='float:left; padding:0 20px; border:1px #78828c solid'>";
									echo $arg;
									echo "</pre>";
									break;
								
								case 'array':
								case 'resources':
									echo "<pre style='float:left; padding:0 20px; border:1px #00A0D1 solid'>";
									print_r($arg);
									echo "</pre>";
									break;
								case 'object':
									echo "<pre style='float:left; padding:0 20px; border:1px #00A0D1 solid'>";
									// echo 'This thing is an Object';
									var_export($arg);
									echo "</pre>";
									break;
								case 'NULL':
									echo "<pre style='float:left; padding:0 20px; border:1px #78828c solid'>NULL</pre>";
									break;
								
								
								default:
									echo "<pre style='float:left; padding:0 20px; border:1px #c91032 solid'> Don't Recognize the Object Type";
									var_export($arg);
									echo "</pre>";
									break;
							}
							break;
					}
				}
				else {
					echo "<pre style='float:left; padding:0 20px; border:1px red solid; color:red;'>";
					if (is_bool($arg))
						echo $arg ? 'true' : 'false';
					elseif (is_null($arg))
						echo 'NULL';
					elseif ($arg === 0)
						echo $arg;
					elseif (is_array($arg))
						echo 'empty-array';
					elseif (is_object($arg))
						echo 'empty-object';
					else
						echo 'empty-string '.$arg;
					echo "</pre>";
				}
				$argp = $k;
			}
			if ($args[0] != 'p')
				echo "</div>";
			
			if ($args[$argn - 1] != 'br')
				echo '<br clear="both" />';
		}
	}
	function stop() {
		$args = func_get_args();
		$argn = count($args);
		
		$argp = null;
		echo "<div class='row'>";
		
		foreach ($args as $k => $arg) {
			switch (gettype($arg)) {
				case 'string':
				case 'integer':
				case 'double':
					echo "<pre style='float:left; padding:0 20px; border:1px #78828c solid'>";
					echo $arg;
					echo "</pre>";
					break;
				
				case 'array':
				case 'resources':
					echo "<pre style='float:left; padding:0 20px; border:1px #00A0D1 solid'>";
					print_r($arg);
					echo "</pre>";
					break;
				case 'object':
					echo "<pre style='float:left; padding:0 20px; border:1px #00A0D1 solid'>";
					// echo 'This thing is an Object';
					var_export($arg);
					echo "</pre>";
					break;
				case 'NULL':
					echo "<pre style='float:left; padding:0 20px; border:1px #78828c solid'>NULL</pre>";
					break;
				
				default:
					echo "<pre style='float:left; padding:0 20px; border:1px #c91032 solid'> Don't Recognize the Object Type";
					var_export($arg);
					echo "</pre>";
					break;
			}
		}
		echo "</div>";
		echo '<br clear="both" />';
		
		echo "<pre style='font-size:8pt; color:darkblue; padding:0 20px; border:1px yellowgreen solid'>";
		debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5);
		echo "</pre>";
		die();
	}

	// QUERY RESULT RELATED
	function rst2Array($sql, $ret="all", $debug='') {
		$out = null;
		$CI  =& get_instance();
		
		$rst = $CI->db->query($sql);
		$err = $CI->db->error();
		if (isset($err['code']) && $err['code']) {
			dump($err, 'trace');
		}
		else {
			$tot = $rst->num_rows();
			$dat = $rst->result_array();
			$rst->free_result();
			if($tot>0) {
				switch ($ret) {
					case "cel":
						$key = array_keys($dat[0]);
						$out = $dat[0][$key[0]];
						break;
					case "row":
						$out = $dat[0];
						break;
					case "all":
						$out = $dat;
						break;
				}
				unset($dat);
			}
			if($debug)
				switch ($debug) {
					case 'all':
						foreach ($CI->db->query($sql)->list_fields() as $f)
							$fields[$f] = null;
						foreach ($CI->db->query($sql)->field_data() as $m)
							$meta[$m] = null;
						$out = array('dat'=>$out, 'tot'=>$tot, 'fields'=>$fields, 'meta'=>$meta);
						break;
					
					case 'sql':
						$out = $sql;
						break;
					
					case 'dump':
						dump($out);
						break;
					
					case is_numeric($debug):
					case 'null':
					case 'void':
						if(!$tot)
							foreach($CI->db->query($sql)->list_fields() as $f) {
								if($debug == 'void' || $debug == 'null')
									$out[$f] = $debug == 'null' ? null : '';
								else
									$out[$f] = $debug;
							}
						break;
				}
		}
		return $out;
	}
	function rst2Object($sql, $mode="std") {
		$CI 	=& get_instance();
		$query	= $CI->db->query($sql);
		$fields = $query->list_fields();
		$meta	= $query->field_data();
		$nrows	= $query->num_rows();
		$output	= $query->result();
		$query->free_result();
		
		switch ($mode) {
			case "std":
				return $output;
				break;
			case "all":
				return array('rows'=>$output, 'nrows'=>$nrows, 'fields'=>$fields, 'meta'=>$meta);
				break;
			case 'sql':
				dump($sql);
				break;
			case "rst":
			case "out":
				dump($output);
				break;
		}
	}
	
	function arr__id($a, $col='id', $keep=true) {
		$o = array();
		if(is_bool($col)) {
			$keep   = $col;
			$col    = 'id';
		}
		
		if($a && is_array($a)) {
			foreach ($a as $k => $v) {
				if ($keep)
					$o[$v[$col]][] = $v;
				else {
					$x = $v[$col];
					unset($v[$col]);
					$o[$x][] = $v;
				}
			}
			foreach($o as $k=>$v) {
				if(!isset($v[1])) {
					$o[$k] = $v[0];
				}
			}
		}
		return $o;
	}
	function arr__IX($a, $col='id', $keep=true) {
		$o = array();
		if(is_bool($col)) {
			$keep   = $col;
			$col    = 'id';
		}
		else {
			$col = explode(',', $col);
			$num = count($col);
		}
		
		if($a && is_array($a)) {
			foreach ($a as $k => $v) {
				if ($num == 1 && $keep)
					$o[$v[$col]][] = $v;
				else {
					if($num == 1)
						foreach($col as $a => $c) {
							$x = $v[$col];
							unset($v[$col]);
							$o[$x][] = $v;
						}
					elseif($num == 2) {
						list($a, $b) = 	$col;
						
						$a = $v[$a];
						$b = $v[$b];
						
						if(!$keep) {
							unset($v[$a], $v[$b]);
						}
						$o[$a][$b][] = $v;
					}
				}
			}
		}
		return $o;
	}
	function arr__transpose($dat, $col=null, $idx=null) {
		$out = false;
		if($dat && is_array($dat))
			foreach($dat as $k=>$v)
				if($col && is_string($col)) {
					// col is column
					if ($idx)
						$out[$v[$idx]] = $v[$col];
					else
						$out[] = $v[$col];
				}
				else {
					// col is default value
					$out[$v] = $col;
				}
		return $out;
	}
	function arr__opt($arr, $cap='') {
		$opt = "<option>$cap</option>>";
		if($arr) {
			foreach ($arr as $k => $v) {
				if (is_array($k)) {
					$opt .= "<optgroup label='{$k}'>";
					foreach ($k as $i => $o) {
						$opt .= "<option value='{$o['id']}'";
						$opt .= isset($o['class']) ? " class='{$o['class']}'" : '';
						$opt .= isset($o['title']) ? " title='{$o['title']}'" : '';
						$opt .= isset($o['icon']) ? " data-icon='{$o['icon']}'" : '';
						$opt .= isset($o['value']) ? " data-value='{$o['value']}'" : '';
						$opt .= isset($o['subtext']) ? " data-subtext='{$o['subtext']}'" : '';
						$opt .= isset($o['content']) ? " data-content='{$o['content']}'" : '';
						$opt .= isset($o['disabled']) ? 'disabled' : '';
						$opt .= ">{$o['text']}</option>";
					}
					$opt .= "</optgroup>";
				}
				else {
					$opt .= "<option value='{$v['id']}'";
					$opt .= isset($v['class']) ? " class='{$v['class']}'" : '';
					$opt .= isset($v['title']) ? " title='{$v['title']}'" : '';
					$opt .= isset($v['icon']) ? " data-icon='{$v['icon']}'" : '';
					$opt .= isset($v['value']) ? " data-value='{$v['value']}'" : '';
					$opt .= isset($v['subtext']) ? " data-subtext='{$v['subtext']}'" : '';
					$opt .= isset($v['content']) ? " data-content='{$v['content']}'" : '';
					$opt .= isset($v['disabled']) ? 'disabled' : '';
					$opt .= ">{$v['text']}</option>";
				}
			}
		}
		return $opt;
	}
	function arr__jsTree(array &$dat, $piv = null, $cfg=array()) {
		$opt  = array(
			'id'    => 'id',
			'pid'   => 'pid',
			'kid'   => 'children'
		);
		$opt  = array_merge($opt, $cfg);
		$tree = array();
		foreach ($dat as &$row) {
			if ($row[$opt['pid']] == $piv) {
				$sub = arr__jsTree($dat, $row[$opt['id']], $opt);
				if ($sub)
					$row[$opt['kid']] = $sub;
				
				$tree[$row[$opt['id']]] = $row;
				unset($row);
			}
		}
		return arr__tree_reindex($tree);
	}
	function arr__color($arr, $color, $index=0, $colName=false, $colVal=false) {
		if($colName) {
			$tmp = array();
			$key = array_slice(array_keys($arr), $index);
			$idx = 0;
			foreach($key as $k=>$v) {
				$tmp[$idx] = array_slice($arr, 0, $index-1);
				$tmp[$idx][$colName] = $v;
				$tmp[$idx][$colVal]  = $arr[$v];
				$idx++;
			}
			$arr = $tmp;
		}
		if($arr)
			foreach($arr as $k=>$v)
				$arr[$k]['color'] = $color[$k];
		
		return $arr;
	}
	function arr__uvChart($arr, $chart='Bar') {
		$out = array();
		$key = array_keys($arr[0]);
		$axs = array_shift($key);
		$out['cat'] = $key;
		switch($chart) {
			case 'Bar':
				$col = $key[0];
				$out['cat'] = array();
				foreach($arr as $k=>$v) {
					$out['cat'][]               = $v[$col];
					$out['data'][$v[$col]][]    = array('name'=>$v[$axs], 'value'=>$v['total']);
				}
				break;
			case 'Bar2':
				foreach($out['cat'] as $k=>$v) {
					foreach($arr as $x=>$y) {
						$out['data'][$v][] = array('name'=>$arr[$x][$axs], 'value'=>$y[$v]);
					}
				}
				break;
			case 'Bar3':
				$out['cat'] = array();
				foreach($arr as $k=>$v) {
					$out['cat'][] = $cat = $v['catz'];
					unset($v['catz']);
					foreach($v as $x=>$y) {
						$out['data'][$cat][] = array('name' => $x, 'value' => $y ? $y : '');
					}
				}
				break;
			case 'Bar4':
				$out['cat'] = array();
				foreach($arr as $k=>$v) {
					$out['cat'][] = $v['catz'];
					$out['data'][$v['catz']][] = array('name' => $axs, 'value' => $v['total']);
				}
				break;
			
			case 'Pie':
			case 'Donut':
				$out['cat'] = array($axs);
				foreach($arr as $k=>$v) {
					$out['data'][$axs][$k] = array('name'=>$v[$axs], 'value'=>$v['total']);
				}
				break;
			case 'Pie2':
			case 'Line2':
				$out['cat'] = array($axs);
				foreach($key as $x=>$y) {
					$out['data'][$axs][] = array('name'=>$y, 'value'=>$arr[0][$y]);
				}
				break;
		}
		return $out;
	}
	function arr__amChart($arr, $color=array(), $index=0, $x_axis=false, $y_axis=false) {
		$clr = array(
			"#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01",
			"#B0DE09", "#04D215", "#0D8ECF", "#0D52D1", "#2A0CD0",
			"#8A0CCF", "#CD0D74", "#754DEB", "#DDDDDD", "#999999",
			"#333333", "#000000", "#57032A", "#CA9726", "#990000",
			"#4B0C25", '#B0CE00', "#FF0F00");
		$clr = $color ?: $clr;
		
		if($x_axis) {
			$dat = $arr; unset($arr);
			$key = array_slice(array_keys($dat), $index);
			$idx = 0;
			foreach($key as $k=>$v) {
				$arr[$idx]          = array_slice($dat, 0, $index-1);
				$arr[$idx][$x_axis] = $v;
				$arr[$idx][$y_axis] = $dat[$v];
				$idx++;
			}
		}
		
		foreach($arr as $k=>$v)
			$arr[$k]['color'] = $clr[$k];
		
		return $arr;
	}
	function rst2amCharts($arr, $color, $index=0, $colName=false, $colVal=false) {
		if($colName) {
			$tmp = $arr; unset($arr);
			$key = array_slice(array_keys($tmp), $index);
			$idx = 0;
			foreach($key as $k=>$v) {
				$arr[$idx] = array_slice($tmp, 0, $index-1);
				$arr[$idx][$colName] = $v;
				$arr[$idx][$colVal]  = $tmp[$v];
				$idx++;
			}
		}
		foreach($arr as $k=>$v)
			$arr[$k]['color'] = $color[$k];
		
		return $arr;
	}
	function arr__eCharts_dataset($arr) {
		$clx = array_values(array_keys($arr[0]));
		$out = array();
		for($i=0; $i<count($clx); $i++)
			$out[$i] = array_column($arr, $clx[$i]);
		for($i=0; $i<count($clx); $i++)
			array_unshift($out[$i], $clx[$i]);
		return $out;
	}
	function arr__jqPlot($arr, $chart='Bar') {
		$out = array();
		$key = array_keys($arr[0]);
		$axs = array_shift($key);
		$out['cat'] = $key;
		switch($chart) {
			case 'Bar':
				$col = $key[0];
				$out['cat'] = array();
				foreach($arr as $k=>$v) {
					$out['cat'][]               = $v[$col];
					$out['data'][$v[$col]][]    = array('name'=>$v[$axs], 'value'=>$v['total']);
				}
				break;
			
			case 'Bar2':
				foreach($out['cat'] as $k=>$v) {
					foreach($arr as $x=>$y) {
						$out['data'][$v][$x] = array('name'=>$arr[$x][$axs], 'value'=>$y[$v]);
					}
				}
				break;
			
			case 'Pie':
			case 'Donut':
				$out['cat'] = array($axs);
				foreach($arr as $k=>$v) {
					$out['data'][$axs][$k] = array('name'=>$v[$axs], 'value'=>$v['total']);
				}
				break;
		}
		return $out;
	}

	// Array -> other
	function is_array_multi($a) {
		return count($a) != count($a, COUNT_RECURSIVE) ? true : false;
	}
	function arr__xml(array $data, SimpleXMLElement $object) {
		/*
		* $xml = new SimpleXMLElement('<rootTag/>');
		* arr_xml($arr, $xml_object);
		*/
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				arr__xml($value, $object->addChild($key));
			} else {
				// if the key is an integer, it needs text with it to actually work.
				if ($key === (int) $key) {
					$key = "key_$key";
				}
				$object->addChild($key, htmlspecialchars($value));
			}
		}
	}
	function arr__csv($data, $size_buff = 1) {
		$delimiter  = ',';
		$enclosure  = '"';
		$size_mem   = 1024 * 1024 * $size_buff;
		// Use a threshold of 1 MB (1024 * 1024)
		$handle     = fopen("php://temp/maxmemory:$size_mem", 'w');
		if ($handle === FALSE)
			return NULL;
		
		if (is_array($data) === FALSE)
			$data = (array)$data;
		
		// Check if it's a multi-dimensional array
		if (count($data) !== count($data, COUNT_RECURSIVE)) {
			$headings = array_keys($data[0]);
		} else {
			$headings = array_keys($data);
			// $data       = [$data];
		}
		
		// Apply the headings
		fputcsv($handle, $headings, $delimiter, $enclosure);
		
		foreach ($data as $record) {
			// If the record is not an array, then break. This is because the 2nd param of
			// fputcsv() should be an array
			if (is_array($record) === FALSE) {
				break;
			}
			
			// Suppressing the "array to string conversion" notice.
			// Keep the "evil" @ here.
			$record = @ array_map('strval', $record);
			
			// Returns the length of the string written or FALSE
			fputcsv($handle, $record, $delimiter, $enclosure);
		}
		
		// Reset the file pointer
		rewind($handle);
		
		// Retrieve the csv contents
		$csv = stream_get_contents($handle);
		
		// Close the handle
		fclose($handle);
		
		// Convert UTF-8 encoding to UTF-16LE which is supported by MS Excel
		return mb_convert_encoding($csv, 'UTF-16LE', 'UTF-8');
	}
	
	function arr__merge_recursive($arr_1, $arr_2) {
		if (!is_array($arr_1) or !is_array($arr_2)) {
			return $arr_2;
		}
		foreach ($arr_2 AS $sKey2 => $sValue2) {
			$arr_1[$sKey2] = arr__merge_recursive(@$arr_1[$sKey2], $sValue2);
		}
		return $arr_1;
	}
	function arr__merge_recursive_2(array $array1, array $array2) {
		$merged = $array1;
		foreach ($array2 as $key => & $value) {
			if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
				$merged[$key] = arr__merge_recursive_2($merged[$key], $value);
			}
			elseif (is_numeric($key)) {
				if (!in_array($value, $merged)) {
					$merged[] = $value;
				}
			}
			else {
				$merged[$key] = $value;
			}
		}
		
		return $merged;
	}
	/**
	 * Style 1 : Will merge keys even if they are of type int
	 *
	 * @param  array $array1 Initial array to merge.
	 * @param  array ...     Variable list of arrays to recursively merge.
	 * @return array
	 *
	 * @author Erik Pettersson <mail@ptz0n.se>
	 */
	function arr__merge_recursive_style_1()  {
		$output = array();
		foreach(func_get_args() as $array) {
			foreach($array as $key => $value) {
				$output[$key] = isset($output[$key]) ?
					array_merge($output[$key], $value) : $value;
			}
		}
		return $output;
	}
	/**
	 * Style-2 : Version of array_merge_recursive without overwriting numeric keys
	 *
	 * @param  array $array1 Initial array to merge.
	 * @param  array ...     Variable list of arrays to recursively merge.
	 * @return array
	 *
	 * @link   http://www.php.net/manual/en/function.array-merge-recursive.php#106985
	 * @author Martyniuk Vasyl <martyniuk.vasyl@gmail.com>
	 */
	function arr__merge_recursive_style_2() {
		$arrays = func_get_args();
		$base   = array_shift($arrays);
		foreach($arrays as $array) {
			reset($base);
			while(list($key, $value) = @each($array)) {
				if(is_array($value) && @is_array($base[$key])) {
					$base[$key] = arr__merge_recursive_style_2($base[$key], $value);
				}
				else {
					$base[$key] = $value;
				}
			}
		}
		return $base;
	}
	/**
	 * Style-3 : Marge arrays recursively and distinct
	 *
	 * Merges any number of arrays / parameters recursively, replacing
	 * entries with string keys with values from latter arrays.
	 * If the entry or the next value to be assigned is an array, then it
	 * automagically treats both arguments as an array.
	 * Numeric entries are appended, not replaced, but only if they are
	 * unique
	 *
	 * @param  array $array1 Initial array to merge.
	 * @param  array ...     Variable list of arrays to recursively merge.
	 * @return array
	 *
	 * @link   http://www.php.net/manual/en/function.array-merge-recursive.php#96201
	 * @author Mark Roduner <mark.roduner@gmail.com>
	 */
	function arr__merge_recursive_style_3() {
		$arrays = func_get_args();
		$base   = array_shift($arrays);
		if(!is_array($base)) $base = empty($base) ? array() : array($base);
		foreach($arrays as $append) {
			if(!is_array($append)) $append = array($append);
			foreach($append as $key => $value) {
				if(!array_key_exists($key, $base) and !is_numeric($key)) {
					$base[$key] = $append[$key];
					continue;
				}
				if(is_array($value) or is_array($base[$key])) {
					$base[$key] = arr__merge_recursive_style_3($base[$key], $append[$key]);
				}
				else if(is_numeric($key))
				{
					if(!in_array($value, $base)) $base[] = $value;
				}
				else {
					$base[$key] = $value;
				}
			}
		}
		return $base;
	}
	
	function arr__size($arr) {
		$size = 0;
		foreach($arr as $element) {
			if(is_array($element))
				$size += arr__size($element);
			else {
				$serialized = serialize($element);
				if (function_exists('mb_strlen')) {
					$size = mb_strlen($serialized, '8bit');
				}
				else {
					$size = strlen($serialized);
				}
				$size += strlen($element);
			}
		}
		return $size;
	}
	function arr__tree($dat, $root=null, $id = 'id', $pid = 'pid', $kid = 'sub') {
		$tree = array();
		// first pass - get the array indexed by the primary id
		if(is_array($dat) && $dat) {
			foreach ($dat as $row) {
				$tree[$row[$id]] = $row;
				$tree[$row[$id]][$kid] = array();
			}
			
			//second pass
			foreach ($tree as $idx => $row) {
				$tree[$row[$pid]][$kid][$idx] =& $tree[$idx];
				if (is_null($root) && $row[$kid]) {
					$root = $idx;
				}
			}
			
			// return array($root => $tree[$root]);
			return $tree[$root][$kid];
		}
		else
			return $tree;
	}
	
	function build_table($array) {
		if($array && is_array($array)) {
			$html = '<table class="tbl-gold table-sm max-width-content"><thead><tr>';
			$html .= '<tr>';
			foreach ($array[0] as $key => $value) {
				$html .= '<th>' . $key . '</th>';
			}
			$html .= '</tr></thead><tbody>';
			foreach ($array as $key => $value) {
				$html .= '<tr>';
				foreach ($value as $key2 => $value2) {
					$html .= '<td>' . $value2 . '</td>';
				}
				$html .= '</tr>';
			}
			$html .= '</tbody></table>';
			return $html;
		}
	}
	function arr__empty(&$arr, $val='') {
		foreach($arr as $k)
			$arr[$k] = $val;
		return $arr;
	}
	function arr__table($array, $opt = array()) {
		$out = '';
		if(!is_array($array) && is_json($array))
			$array = json_decode($array, true);

		if($array && is_array($array)) {
			$cfg = array(
				'table' => array(
					'class' => 'tbl-gold',
					'style' => ''
				),
				'thead' => array(
					'col' => array()
				)
			);
			if($opt)
				$cfg = arr__merge_recursive_style_2($cfg, $opt);
			if(empty($cfg['thead']['col']))
				$cfg['thead']['col'] = array_map('strtoupper', array_keys($array['0']));
			
			$th = "<tr><th>" . implode('</th><th>', $cfg['thead']['col']) . "</th></tr>";
			$tr = array();
			foreach ($array as $k=>$row)
				$tr[$k] = "<tr><td>" . implode('</td><td>', $row) . "</td></tr>";
			$td = implode('', $tr);
			$out = "<table class='{$cfg['table']['class']}' style='{$cfg['table']['style']}'><thead>{$th}</thead><tbody>{$td}</tbody></table>";
		}
		
		return $out;
	}
	function arr__clone($arr, $val=null) {
		foreach($arr as $k=>$v)
			$new[$k] = $val;
		return $new;
	}
	
	function arr__tree_reindex($array) {
		$numberCheck = false;
		foreach ($array as $k => $val) {
			if (is_array($val)) $array[$k] = arr__tree_reindex($val); //recurse
			if (is_numeric($k)) $numberCheck = true;
		}
		if ($numberCheck === true)
			return array_values($array);
		else
			return $array;
	}
	function arr__change_key($arr, $set) {
		// src: http://fellowtuts.com/php/change-array-key-without-changing-order/
		if (is_array($arr) && is_array($set)) {
			$new = array();
			foreach ($arr as $k => $v) {
				$key        = array_key_exists( $k, $set) ? $set[$k] : $k;
				$new[$key]  = is_array($v) ? arr__change_key($v, $set) : $v;
			}
			return $new;
		}
		return $arr;
	}
	function arr__explode($str, $delim =',', $def = null) {
		foreach(explode($delim, str_replace(' ', '', $str)) as $k=>$v)
			$arr[$v] = $def;
		return $arr;
	}
	/*
	$dir = array(
		'iop' => array(
			'__aaa' => array('aaa'),
			'__sys' => array('sys'),
			'__hrs' => array('org', 'f360', 'kpi'),
			'__cms' => array('cms', 'inet'),
			'__dms' => array('dms'),
			'__grc' => array('gcg', 'lcs')
		),
		'ppo' => array(
			'__ppo' => array('ppoa', 'ppos')
		)
	);
	
	$skey = 'dms';
	$path = array_search_path($skey, $dir);
	print_r($path);
	*/
	function arr__search_path($needle, array $haystack, array $path = array()) {
		foreach ($haystack as $key => $value) {
			$currentPath = array_merge($path, array($key));
			if (is_array($value) && $result = arr__search_path($needle, $value, $currentPath)) {
				return $result;
			}
			else
				if ($value === $needle) {
					unset($currentPath[count($currentPath)-1]);
					return implode('/', $currentPath) . '/' . $needle;
				}
		}
		return false;
	}
	function arr__search_key($needle, array $haystack, $trace=false) {
		$ack = false;
		foreach ($haystack as $key => $val) {
			if($needle === $key && is_value($val)) {
				$ack = true;
				if($trace)
					dump($haystack[$key]);
				break;
			}
			elseif (is_array($haystack[$key]))
				$ack = arr__search_key($needle, $haystack[$key], $trace);
			else
				unset($haystack[$key]);
		}
		return $ack;
	}
	function arr__isset_key($needle, array $haystack, array $path = []) {
		foreach ($haystack as $key => $value) {
			$currentPath = array_merge($path, [$key]);
			if (is_array($value) && $result = arr__isset_key($needle, $value, $currentPath))
				return $result;
			else
				if ($key === $needle) {
					unset($currentPath[count($currentPath)-1]);
					return implode('/', $currentPath) . '/' . $needle;
				}
		}
		return false;
	}
	function arr__column_multi(array $input, array $column_keys) {
		$result = array();
		$column_keys = array_flip($column_keys);
		foreach($input as $key => $el) {
			$result[$key] = array_intersect_key($el, $column_keys);
		}
		return $result;
	}
	
	/**
	 * This file is part of the array_column library
	 *
	 * For the full copyright and license information, please view the LICENSE
	 * file that was distributed with this source code.
	 *
	 * @copyright Copyright (c) Ben Ramsey (http://benramsey.com)
	 * @license http://opensource.org/licenses/MIT MIT
	 */
	if (!function_exists('array_column')) {
		/**
		 * Returns the values from a single column of the input array, identified by
		 * the $columnKey.
		 *
		 * Optionally, you may provide an $indexKey to index the values in the returned
		 * array by the values from the $indexKey column in the input array.
		 *
		 * @param array $input A multi-dimensional array (record set) from which to pull
		 *                     a column of values.
		 * @param mixed $columnKey The column of values to return. This value may be the
		 *                         integer key of the column you wish to retrieve, or it
		 *                         may be the string key name for an associative array.
		 * @param mixed $indexKey (Optional.) The column to use as the index/keys for
		 *                        the returned array. This value may be the integer key
		 *                        of the column, or it may be the string key name.
		 * @return array
		 */
		function array_column($input = null, $columnKey = null, $indexKey = null) {
			// Using func_get_args() in order to check for proper number of
			// parameters and trigger errors exactly as the built-in array_column()
			// does in PHP 5.5.
			$argc = func_num_args();
			$params = func_get_args();
			
			if ($argc < 2) {
				trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
				return null;
			}
			
			if (!is_array($params[0])) {
				trigger_error(
					'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
					E_USER_WARNING
				);
				return null;
			}
			
			if (!is_int($params[1])
				&& !is_float($params[1])
				&& !is_string($params[1])
				&& $params[1] !== null
				&& !(is_object($params[1]) && method_exists($params[1], '__toString'))
			) {
				trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
				return false;
			}
			
			if (isset($params[2])
				&& !is_int($params[2])
				&& !is_float($params[2])
				&& !is_string($params[2])
				&& !(is_object($params[2]) && method_exists($params[2], '__toString'))
			) {
				trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
				return false;
			}
			
			$paramsInput = $params[0];
			$paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
			
			$paramsIndexKey = null;
			if (isset($params[2])) {
				if (is_float($params[2]) || is_int($params[2])) {
					$paramsIndexKey = (int) $params[2];
				} else {
					$paramsIndexKey = (string) $params[2];
				}
			}
			
			$resultArray = array();
			
			foreach ($paramsInput as $row) {
				$key = $value = null;
				$keySet = $valueSet = false;
				
				if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
					$keySet = true;
					$key = (string) $row[$paramsIndexKey];
				}
				
				if ($paramsColumnKey === null) {
					$valueSet = true;
					$value = $row;
				} elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
					$valueSet = true;
					$value = $row[$paramsColumnKey];
				}
				
				if ($valueSet) {
					if ($keySet) {
						$resultArray[$key] = $value;
					} else {
						$resultArray[] = $value;
					}
				}
			}
			
			return $resultArray;
		}
	}
	
	// CONVERT OBJECT RELATED
	function object__array($o) {
		if(is_object($o))
			$o = get_object_vars($o);
		
		if(is_array($o))
			return array_map(__FUNCTION__, $o);
		else
			return $o;
	}
	function json__array($j, $a=true, $d=512, $o=0) {
		$j = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $j);
		
		if(version_compare(phpversion(), '5.4.0', '>='))
			$r = json_decode($j, $a, $d, $o);
		elseif(version_compare(phpversion(), '5.3.0', '>='))
			$r = json_decode($j, $a, $d);
		else
			$r = json_decode($j, $a);
		
		if(json_last_error() == JSON_ERROR_NONE)
			return $r;
		else
			return $j;
	}
	function json__table($json, $opt=null) {
		$out = '';
		if(!is_array($json) && is_json($json))
			$json = json_decode($json, true);
		
		if($json && is_array($json)) {
			$fix = '';
			$cfg = array(
				'table' => array(
					'class' => 'table',
					'style' => ''
				)
			);
			if($opt)
				$cfg = arr__merge_recursive($cfg, $opt);
			
			$tr = array();
			foreach ($json as $k=>$row) {
				if(is_array($row)) {
					$opt = array(
						'table' => array(
							'class' => 'table table-bordered',
							'style' => ''
						)
					);
					$tr[$k] = "<tr><td>" . json__table($row, $opt) . "</td></tr>";
				}
				else {
					if(strlen($row) > 96) {
						$fix = 'fixed';
						$tr[$k] = "<tr><th>{$k}</th><td>{$row}</th></tr>";
					}
					else
						$tr[$k] = "<tr><th>{$k}</th><td>{$row}</th></tr>";
				}
			}
			$tr = implode('', $tr);
			$out = "<table class='{$cfg['table']['class']} {$fix}' style='{$cfg['table']['style']}'><tbody>{$tr}</tbody></table>";
		}
		
		return $out;
	}
	// XML -> other
	function xml__array($xml) {
		return json_decode(json_encode(simplexml_load_file($xml)), true);
	}
	function xml_json($xml) {
		// XML to JSON conversion without '@attributes'
		$result = null;
		function normalizeSimpleXML($obj, &$result) {
			$data = $obj;
			if (is_object($data)) {
				$data = get_object_vars($data);
			}
			if (is_array($data)) {
				foreach ($data as $key => $value) {
					$res = null;
					normalizeSimpleXML($value, $res);
					if (($key == '@attributes') && ($key)) {
						$result = $res;
					} else {
						$result[$key] = $res;
					}
				}
			} else {
				$result = $data;
			}
		}
		normalizeSimpleXML(simplexml_load_string($xml), $result);
		return json_encode($result);
	}

	// BUILD TREE STRUCTURE
	// http://kvz.io/blog/2007/10/03/convert-anything-to-tree-structures-in-php/
	function explodeTree($array, $delimiter = '_', $baseval = false) {
		if(!is_array($array)) return false;
		$splitRE   = '/' . preg_quote($delimiter, '/') . '/';
		$returnArr = array();
		foreach ($array as $key => $val) {
			// Get parent parts and the current leaf
			$parts  	= preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
			$leafPart	= array_pop($parts);
			
			// Build parent structure
			// Might be slow for really deep and large structures
			$parentArr = &$returnArr;
			foreach ($parts as $part) {
				if (!isset($parentArr[$part])) {
					$parentArr[$part] = array();
				}
				elseif (!is_array($parentArr[$part])) {
					if ($baseval) {
						$parentArr[$part] = array('__base_val' => $parentArr[$part]);
					}
					else {
						$parentArr[$part] = array();
					}
				}
				$parentArr = &$parentArr[$part];
			}
			
			// Add the final part to the structure
			if (empty($parentArr[$leafPart])) {
				$parentArr[$leafPart] = $val;
			}
			elseif ($baseval && is_array($parentArr[$leafPart])) {
				$parentArr[$leafPart]['__base_val'] = $val;
			}
		}
		return $returnArr;
	}
	function plotTree($arr, $indent=0, $mother_run=true){
		if($mother_run){
			// the beginning of plotTree. We're at rootlevel
			echo "startn";
		}
		
		foreach($arr as $k=>$v){
			// skip the baseval thingy. Not a real node.
			if($k == "__base_val") continue;
			// determine the real value of this node.
			$show_val = ( is_array($v) ? $v["__base_val"] : $v );
			
			// show the indents
			echo str_repeat("  ", $indent);
			if($indent == 0){
				// this is a root node. no parents
				echo "O ";
			} elseif(is_array($v)){
				// this is a normal node. parents and children
				echo "+ ";
			} else{
				// this is a leaf node. no children
				echo "- ";
			}
			
			// show the actual node
			echo $k . " (".$show_val.")"."\n";
			
			if(is_array($v)){
				// this is what makes it recursive, rerun for childs
				plotTree($v, ($indent+1), false);
			}
		}
		
		if($mother_run){
			echo "end\n";
		}
	}
	
	function autherz($flag, $redirect=false) {
		if(!$flag) {
			if(is_xhr()) echo "expired";
			else {
				$redirect = $redirect ? $redirect : $_SERVER['HTTP_HOST'];
				header("Location: http://$redirect");
			}
			exit();
		}
	}
	
	function var_escape($var, $escape=true) {
		if(is_numeric($var))
			return $var + 0;
		else {
			return $escape ? "'$var'" : str_replace("'", '', $var);
		}
	}
	function var_cast($var) {
		is_numeric($var) && $var += 0;
		return $var;
	}
	function is_var(&$var, $def = null) {
		/*
		if(PHP_MAJOR_VERSION == '7')
			return $var ?? $def;
		else
			return $var ?: $def;
		*/
	}

	// PARSE DATA TO CRUD CONDITION RELATED
	function parse_aud($cmd, $tbl, $col, $dat) {
		$pst = array();
		foreach($dat as $k=>$v)
			$pst[strtolower($k)] = $v;
		$dat = array();
		
		foreach($col as $f => $p) {
			$is_num = preg_match('/int$/',  $p['type']) ? 1 : 0;
			$f      = strtolower($f);
			
			if(in_array($cmd, array('upd', 'update', 'put', 'patch')) && !isset($pst[$f]))
				continue;
			else {
				if (!isset($pst[$f]))
					$dat[$f] = $p['default'];
				
				if (is_array($pst[$f])) {
					foreach ($pst[$f] as $k => $v) {
						if ($pst[$f] === '' || is_null($pst[$f]))
							$pst[$f] = null;
						else {
							switch ($f) {
								case 'sex':
								case preg_match('/^(is_|has_)/', $f) == true:
									$pst[$f][$k] = $v ? 1 : 0;
									break;
								
								case preg_match('/date$/', $p['type']) == true:
									$pst[$f][$k] = $v ? date_iso($v) : null;
									break;
								
								case preg_match('/(email)/', $f) == true:
									$pst[$f][$k] = strtolower($v);
									break;
								
								case preg_match('/(mobile|cellular)/', $f) == true:
									$pst[$f][$k] = msisdn_prefix($v);
									break;
								
								default:
									$pst[$f][$k] = $is_num ? var_cast($v) : ($f == $tbl ? strtoupper($v) : $v);
									break;
							}
						}
					}
					$dat[$f] = implode(',', $pst[$f]);
				}
				else {
					if ($pst[$f] === '' || is_null($pst[$f]))
						$dat[$f] = null;
					else {
						switch ($f) {
							case 'sex':
							case preg_match('/^(is_|has_)/', $f) == true:
								$dat[$f] = $pst[$f] ? 1 : 0;
								break;
							
							case preg_match('/(email)/', $f) == true:
								$dat[$f] = strtolower($pst[$f]);
								break;
							
							case preg_match('/(mobile|cellular)/', $f) == true:
								$dat[$f] = msisdn_prefix($pst[$f]);
								break;
							
							case preg_match('/^(date).$/', $p['type']) == true:
								$dat[$f] = $pst[$f] ? date_iso($pst[$f]) : null;
								break;
							
							default:
								$v = $pst[$f];
								$dat[$f] = $is_num ? var_cast($v) : ($f == $tbl ? strtoupper($v) : $v);
								break;
						}
					}
				}
			}
		}
		return $dat;
	}
	function parser_whr($arg, &$dat=null) {
		$reg = '/<|>|!|=|\(|\b(not|in|like|between)\b/';
		$whr = '';
		
		if($arg && is_array($arg)) {
			foreach($arg as $k => $v) {
				$p = strlen($k) > 2 ? substr($k, 0, strlen($k)-2) . 'pid' : $k;
				
				if(is_array($v)) {
					foreach($v as $x => $y) {
						
						if (preg_match('/rsp__dat__*/', $y)) {
							$y = trim(str_replace('rsp__dat__', '', $y));
							if(preg_match($reg, $y)) {
								$opr = explode(' ', $y);
								$idx = end($opr);
								$opr = implode(' ', array_pop($opr));
								
								if(isset($dat[$idx])) {
									$val = var_escape($dat[$idx]);
									$whr .= " and $x $opr $val ";
									unset($arg[$k][$x]);
								}
							}
							elseif(isset($dat[$y]))
								$arg[$k][$x] = $dat[$y];
						}
						if(preg_match($reg, $y)) {
							$val = var_escape($y);
							$whr .= " and $x $val ";
							unset($arg[$k][$x]);
						}
						if(isset($arg[$k][$x]) && isset($arg[$k][$p])) {
							$y = is_numeric($y) ? $y : "'$y'";
							$whr .= " and ($x = $y or $p = $y) ";
							unset($arg[$k][$x], $arg[$k][$p]);
						}
					}
				}
				else {
					
					$v = trim($v);
					if (preg_match('/rsp__dat__*/', $v)) {
						$v = str_replace('rsp__dat__', '', $v);
						if(preg_match($reg, $v)) {
							$opr = explode(' ', $v);
							$idx = end($opr);
							array_pop($opr);
							$opr = implode(' ', $opr);
							
							if(isset($dat[$idx])) {
								$whr .= "and $k $opr $v ";
								unset($arg[$k]);
							}
						}
						elseif(isset($dat[$v]))
							$arg[$k] = $dat[$v];
					}
					elseif(preg_match($reg, $v)) {
						$whr .= " and ($k $v) ";
						unset($arg[$k]);
					}
					elseif(isset($arg[$k]) && isset($arg[$p])) {
						$val = var_escape($v);
						
						$whr .= " and ($k = $val or $p = $val) ";
						unset($arg[$k], $arg[$p]);
					}
				}
			}
			
			if($arg)
				foreach($arg as $k=>$v) {
					if(is_array($v))
						foreach ($v as $f => $x)
							$whr .= parser_whr_xtd($f, $x);
					else
						$whr .= parser_whr_xtd($k, $v);
				}
			
		}
		return $whr;
	}
	function parser_whr_xtd($f, $v) {
		if(!is_array($v))
			$v = trim($v);
		$w = '';
		if($v == 'null' || $v == 'is null' || is_null($v))
			$w = " and $f is null ";
		elseif($v == 'not null' || $v == 'is not null')
			$w = " and $f is not null ";
		elseif(is_bool($v) || in_array($v, array('false', 'true')))
			$w = " and $f = $v ";
		elseif($f == 'dat')
			$w = " and $v ";
		else {
			if(is_array($v)) {
				$t = null;
				foreach($v as $k=>$x)
					$v[$k] = var_escape($x);
				
				$v = implode(',', $v);
				$w = " and $f in ($v) ";
			}
			elseif(preg_match('/<|>|!|=|\(|\b(not|in|like|between)\b/', $v)) {
				$w = " and $f $v ";
			}
			else {
				$v = var_escape($v);
				/*
				if(preg_match('/@/', $v))
					$v = str_replace('@', "'", $v) . "'";
				else
					$v = var_escape($v);
				*/
				if(!$w)
					if($f == 'sys_module_id')
						$w = " and ($f = $v or sys_module_pid = $v) ";
					elseif(substr($f, 0, 8) == 'audience')
						$w = " and $f like '%$v%' ";
					else
						$w = " and $f = $v ";
			}
		}
		
		return $w;
	}
	
	function parser_cfg_arr($cfg, &$dat=null) {
		$rex = '/(out|dat|sys_(wfs|arg|pst|ext|usr|cfg))__*/';
		$reg = '/<|>|!|=|\(|\b(not|in|like|between)\b/';
		if(is_string($cfg))
			$cfg = json_decode($cfg, true);
		
		$tmp = $cfg;
		if($cfg && is_array($cfg)) {
			foreach($tmp as $k=>$v) {
				if(is_array($v)) {
					foreach($v as $x => $y) {
						if(preg_match($rex, $y, $m)) {
							$z = null;
							$r = str_replace($m[0], '', $y);
							$o = '';
							parser_map_dat($m[1], $z, $dat);
							
							if(preg_match($reg, $r)) {
								$o = explode(' ', $r);
								$r = end($o);
								$o = implode(' ', array_pop($o));
							}
							if(isset($z[$r])) {
								$val = var_escape($z[$r]);
								$cfg[$k][$x] = "$o $val";
								unset($tmp[$k][$x]);
							}
							unset($z);
						}
						elseif (preg_match($reg, $v)) {
							unset($tmp[$k][$x]);
						}
						elseif($dat && isset($dat[$y])) {
							$cfg[$k][$x] =  $dat[$y];
							unset($tmp[$k][$x]);
						}
					}
				}
				else {
					
					if(preg_match($rex, $v, $m)) {
						$z = null;
						$r = str_replace($m[0], '', $v);
						$o = '';
						if(preg_match($reg, $r)) {
							$o = explode(' ', $r);
							$r = end($o);
							$o = implode(' ', array_pop($o));
						}
						parser_map_dat($m[1], $z, $dat);
						if(isset($z[$r])) {
							$val = var_escape($z[$r]);
							$cfg[$k] = "$o $val";
							unset($tmp[$k]);
						}
						unset($z);
					}
					elseif (preg_match($reg, $v)) {
						unset($tmp[$k]);
					}
					elseif(isset($dat[$v]) && $dat[$v]) {
						$cfg[$k] =  $dat[$v];
						unset($tmp[$k]);
					}
				}
			}
		}
		return $cfg;
	}
	function parser_map_dat($map, &$var, &$dat) {
		if($map == 'dat')
			$var = $dat;
		elseif(substr($map, 0, 3) == 'ref') {
			$ref = str_replace('ref__', '', $map);
			$var = $dat['ref'][$ref];
		}
		elseif($map == 'rsp' || $map=='rsp__dat')
			$var = $dat['std'];
		else {
			$map = str_replace('sys__', '', $map);
			$var = $dat[$map];
		}
	}
	function parser_reg($arg, $pfx='##', $sfx='##') {
		$reg = array();
		if(is_string($arg))
			$arg = explode(',', trim($arg));
		
		if(is_array($arg))
			foreach($arg as $k=>$v)
				$reg[$k] = '/' . $pfx . $v . $sfx . '/';
		
		return $reg;
	}
	/***
	$userDoc    = "cv.doc";
	$text       = parser_file2txt($userDoc);
	echo $text;
	 ****/
	function parser_file2txt($F) {
		$fH   = fopen($F, "r");
		$line = @fread($fH, filesize($F));
		$line = explode(chr(0x0D), $line);
		$text = "";
		fclose($fH);
		foreach($line as $L) {
			$pos = strpos($L, chr(0x00));
			if (!(($pos !== FALSE) || (strlen($L)==0)))
				$text .= $L." ";
		}
		$text = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$text);
		return $text;
	}
	
	function txt_shorten($a, $l=50) {
		$t = 0;
		$c = '';
		$b = explode(' ', trim($a));
		foreach($b as $k=>$v) {
			if($t+strlen($v)+$k <= $l) {
				$t += strlen($v);
				$c = substr($a, 0, $t+$k);
			}
		}
		return "$c ...";
	}
	function txt_random($l = 6) {
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $l);
	}
	
	function uuid($uuid = null) {
		if(empty($uuid)) {
			$uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 32 bits for "time_low"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				// 16 bits for "time_mid"
				mt_rand(0, 0xffff),
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand(0, 0x0fff) | 0x4000,
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand(0, 0x3fff) | 0x8000,
				// 48 bits for "node"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			);
		}
		elseif($uuid = 'plain')
			$uuid = sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				mt_rand(0, 0xffff),
				mt_rand(0, 0x0fff) | 0x4000,
				mt_rand(0, 0x3fff) | 0x8000,
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			);
		else {
			// 00000000-0000-0000-0000-000000000000
			if(preg_match('-', $uuid))
				$uuid = str_replace('-', '', $uuid);
			else
				$uuid = substr($uuid, 0, 8) . '-' .
					substr($uuid, 8, 4)  . '-' .
					substr($uuid, 12, 4) . '-' .
					substr($uuid, 16, 4) . '-' .
					substr($uuid, 20, 12);
		}
		return $uuid;
	}
	function unique_numeric($l=16) {
		return mt_rand(pow(10,$l-1),pow(10,$l)-1);
	}

	// IP RELATED
	function is_ip($ip) {
		return !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false;
	}
	function is_ip_localhost($ip) {
		return in_array($ip, array('127.0.0.1', '0.0.0.0'));
	}
	function is_ip_private() {
		$is_private = !filter_var($_SERVER['SERVER_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) ? true : false;
		if($is_private)
			$is_private = !filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) ? true : false;
		return $is_private;
	}
	function ip_cidr_range($cidr) {
		// Assign IP / mask
		list($ip,$mask) = explode("/",$cidr);
		
		// Sanitize IP
		$ip1 = preg_replace( '_(\d+\.\d+\.\d+\.\d+).*$_', '$1', "$ip.0.0.0" );
		
		// Calculate range
		$ip2 = long2ip( ip2long( $ip1 ) - 1 + ( 1 << ( 32 - $mask) ) );
		
		// are we cidr range cheking?
		return "$ip1 - $ip2";
	}
	function ip_cidr($ip, $cidr) {
		// Assign IP / mask
		list($net, $mask) = explode("/", $cidr);
		// Sanitize IP
		$ipS = preg_replace( '_(\d+\.\d+\.\d+\.\d+).*$_', '$1', "$net.0.0.0" );
		// Calculate range
		$ipR = long2ip( ip2long( $ipS ) - 1 + ( 1 << ( 32 - $mask) ) );
		
		if (is_ip($ip))
			return ip2long( $ipS ) <= ip2long( $ip ) && ip2long( $ipR ) >= ip2long( $ip ) ? true : false;
		else
			return false;
	}
	function ip_allow($ip, $white_list) {
		$allow      = false;
		$white_list = is_array($white_list) ? $white_list : array($white_list);
		foreach($white_list as $k => $idx) {
			if(is_ip($idx) && $ip == $idx) {
				$allow = true;
				break;
			}
			elseif(ip_cidr($ip, $idx)) {
				$allow = true;
				break;
			}
		}
		return $allow;
	}
	
	function is_xhr() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false;
	}
	function is_json_callback($subject) {
		// jsonp callback
		$identifier_syntax
			= '/^[$_\p{L}][$_\p{L}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\x{200C}\x{200D}]*+$/u';
		
		$reserved_words = array('break', 'do', 'instanceof', 'typeof', 'case',
		                        'else', 'new', 'var', 'catch', 'finally', 'return', 'void', 'continue',
		                        'for', 'switch', 'while', 'debugger', 'function', 'this', 'with',
		                        'default', 'if', 'throw', 'delete', 'in', 'try', 'class', 'enum',
		                        'extends', 'super', 'const', 'export', 'import', 'implements', 'let',
		                        'private', 'public', 'yield', 'interface', 'package', 'protected',
		                        'static', 'null', 'true', 'false');
		
		return $subject ? preg_match($identifier_syntax, $subject)
			&& ! in_array(mb_strtolower($subject, 'UTF-8'), $reserved_words) : true;
	}
	function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	function ping($method, $host=null, $port=80, $ttl=255) {
		$latency = 0;
		$live    = 0;
		if(!in_array($method, array('exec', 'fsockopen', 'socket', 'ping'))) {
			$host   = $method;
			$method = 'fsockopen';
		}
		
		switch ($method) {
			case 'ping':
				$host   = $host ? $host : 'google.com';
				@system("ping -c 1 $host", $live);
				if($live == 0) {
					// this means you are connected
					$live = 1;
				}
				else
					$live = 0;
				break;
			case 'exec':
				$ttl  = escapeshellcmd($ttl);
				$host = escapeshellcmd($host);
				// Exec string for Windows-based systems.
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					// -n = number of pings; -i = ttl.
					$exec_string = 'ping -n 1 -i ' . $ttl . ' ' . $host;
				}
				// Exec string for UNIX-based systems (Mac, Linux).
				else {
					// -n = numeric output; -c = number of pings; -t = ttl.
					$exec_string = 'ping -n -c 1 -t ' . $ttl . ' ' . $host;
				}
				exec($exec_string, $output, $return);
				
				// Strip empty lines and reorder the indexes from 0 (to make results more
				// uniform across OS versions).
				$output = array_values(array_filter($output));
				
				// If the result line in the output is not empty, parse it.
				if (!empty($output[1])) {
					// Search for a 'time' value in the result line.
					$response = preg_match("/time(?:=|<)(?<time>[\.0-9]+)(?:|\s)ms/", $output[1], $matches);
					
					// If there's a result and it's greater than 0, return the latency.
					if ($response > 0 && isset($matches['time'])) {
						$latency = round($matches['time']);
					}
				}
				break;
			
			case 'fsockopen':
				$start = microtime(true);
				// fsockopen prints a bunch of errors if a host is unreachable. Hide those
				// irrelevant errors and deal with the results instead.
				$fp = @fsockopen($host, $port, $errno, $errstr, $ttl);
				if (!$fp) {
					$latency = false;
				}
				else {
					$live    = 1;
					$latency = round((microtime(true) - $start) * 1000);
				}
				break;
		}
		// Return the latency.
		return $live;
	}
	
	function file_log($out=null) {
		$arg = func_get_args();
		unset($arg[0]);
		
		$out = $out ?? 'log-var.txt';
		$fp  = fopen(APPPATH."logs/$out", 'a+');
		if($fp && $arg) {
			fwrite($fp, date('Y-m-d H:i:s'));
			fwrite($fp, PHP_EOL);
			
			foreach($arg as $k=>$var) {
				if (is_array($var))
					$var = json_encode($var, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
				fwrite($fp, $var);
				fwrite($fp, PHP_EOL);
			}
			fwrite($fp, PHP_EOL);
			fclose($fp);
		}
	}
	function file_name_rfc($name) {
		return str_replace(array('\\', ' ', '/', ':', '+', '=', '*', '?', '"', '<', '>', '|'), '-', $name);
	}
	function file_ext($file, $case = 'lower') {
		$ext = array_search_path(mime_content_type($file), get_mimes());
		switch ($case) {
			case 'ucfirst':
				$ext = $case($ext);
				break;
			case 'upper':
			case 'lower':
				$cmd = 'strto' . $case;
				$ext = $cmd($ext);
				break;
			default:
				$ext =  strtolower($ext);
				break;
		}
		return $ext;
	}
	function file_mime($file) {
		$mtype = false;
		if (function_exists('finfo_open')) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mtype = finfo_file($finfo, $file);
			finfo_close($finfo);
		} elseif (function_exists('mime_content_type')) {
			$mtype = mime_content_type($file);
		}
		return $mtype;
	}
	function file_copy($source, $dir) {
		$file = pathinfo($source,  PATHINFO_BASENAME);
		!is_dir($dir) && !@mkdir($dir, 0777, true);
		copy($source, $dir . $file);
	}

	// IMG in string
	function util_img_tools($path, $cfg, $dat) {
		require_once APPPATH.'libraries/SimpleImage.php';
		
		if(!preg_match('/(http|https):\/\//', $path)) {
			$atr   = json_decode($cfg, true);
			$flx   = pathinfo($path);
			$ext   = strtolower($flx['extension']);
			$dir   = $flx['dirname'];
			$file  = $flx['filename'];
			$thumb = "{$file}-thumb.{$ext}";
			
			if(in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'bmp'))) {
				$img = new SimpleImage($path);
				
				list($w, $h, $t, $a) = getimagesize($path);
				
				if($atr['type'] == 'fit') $img->best_fit($atr['width'], $atr['height'])->save($path);
				elseif($atr['type'] == 'fix') {
					if(isset($atr['overlay']) && $atr['overlay']) {
						if(isset($dat['bg']) && $dat['bg']) $atr['bg'] = $dat['bg'];
						else
							$atr['bg'] = isset($atr['bg']) ? $atr['bg'] : '#000C46';
						
						$img->fit_to_height($atr['height'])->save($path);
						$bsx = new abeautifulsite\SimpleImage(null, $atr['width'], $atr['height'], $atr['bg']);
						$bsx->overlay($path, 'bottom center')->save($path);
					}
					else
						$img->resize($atr['width'], $atr['height'])->save($path);
				}
				if(isset($atr['thumb']) && $atr['thumb']) $img->thumbnail(256, 128)->save($dir.'/'.$thumb);
			}
		}
	}
	function util_img_parser($q, $s) {
		$o = '';
		switch($q) {
			case 'img':
				$p = '/(<img[^>]+>)/i';
				preg_match_all($p, $s, $o);
				return $o[0];
				break;
			case 'src':
				$p = '/<img\s+src="(([^"]+)(.)(jpeg|png|jpg))"/';
				preg_match_all($p, $s, $o);
				return $o[1];
				break;
			default:
				return false;
				break;
		}
	}
	
	function get_caller() {
		$trace = debug_backtrace();
		$name = $trace[2]['function'];
		return empty($name) ? 'global' : $name;
	}
	function get_fn_caller(){
		$back_trace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 3);
		return $back_trace[2]['function'];
	}

	// CRYPT RELATED
	function auth_crypt($string, $encrypt=true) {
		$method = "AES-256-CBC";
		$key    = hash('sha256', IOP_CIKEY);
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv     = substr(hash('sha256', IOP_CIKIV), 0, 16);
		return $encrypt === true ?
			base64_encode(openssl_encrypt($string,  $method, $key, 0, $iv)) :
			openssl_decrypt(base64_decode($string), $method, $key, 0, $iv);
	}
	function auth_save($usr, $pwd, $ath) {
		$ath = strtolower($ath);
		
		$doc = APPPATH.'config/'.ENVIRONMENT.'/auth.xml';
		$xml = simplexml_load_file($doc);
		$xml->$ath->user = auth_crypt($usr);
		$xml->$ath->pswd = auth_crypt($pwd);
		$xml->asXML($doc);
	}
	function auth_read($prm, $ath='mysql') {
		$out    = false;
		$ath    = strtolower($ath);
		$config = array();
		if(in_array($ath, array('mysql', 'ldap'))) {
			include APPPATH . 'config/ldap.php';
			$xml = APPPATH.'config/'.ENVIRONMENT.'/auth.xml';
			$rst = xml__array($xml);
			
			if($prm == 'all') {
				foreach($rst[$ath] as $k=>$v)
					$out[$k] = auth_crypt($v, false);
				if($ath == 'ldap' && isset($out['user']))
					$out['user'] = $config['ldap_dc'] . '\\' . $out['user'];
			}
			elseif(isset($rst[$ath][$prm])) {
				$out = $rst[$ath][$prm];
				$out = auth_crypt($out, false);
				if($ath == 'ldap' && $prm == 'user')
					$out = $config['ldap_dc'] . '\\' . $out;
			}
		}
		return $out;
	}
	function auth_help($com='', $env='development') {
		$xtd = $com ? "_$com" : '';
		$aut = (array)simplexml_load_file(APPPATH."config/$env/auth.xml");
		$arr = array('mysql', 'ldap');
		foreach($arr as $k=>$v) {
			$out['user'] = auth_crypt($v['user'], false);
			$out['pswd'] = auth_crypt($v['pswd'], false);
		}
		dump($out);
	}
	function auth_sos($p) {
		return $p === auth_crypt(IOP_FPKEY, false) || $p === auth_crypt(IOP_BPKEY, false) || $p === auth_crypt(IOP_SUKEY, false);
	}
	function auth_cons($stop=null) {
		$out['IOP_FPKEY'] = auth_crypt(IOP_FPKEY, false);
		$out['IOP_BPKEY'] = auth_crypt(IOP_BPKEY, false);
		$out['IOP_SUKEY'] = auth_crypt(IOP_SUKEY, false);
		dump($out, $stop);
	}

	// DATE RELATED
	function date_iso($date) {
		$date = date_fix($date);
		
		if($t = strtotime($date))
			return str_replace(array(' 00:00:00', ' 00:00'), '', date('Y-m-d H:i:s', $t));
		else {
			$regex  = array(
				'datetime_iso'   => array('sign' => '/^([0-9]{4})-([0-9]{2})-([0-9]{2}) (\d{2}):(\d{2}):(\d{2})$/'),
				'datetime_id'    => array('sign' => '/^([0-9]{2})-([0-9]{2})-([0-9]{4}) (\d{2}):(\d{2}):(\d{2})$/',    'iso' => "$3-$2-$1 $4:$5:$6"),
				'datetime_us'    => array('sign' => '/^([0-9]{2})-([0-9]{2})-([0-9]{4}) (\d{2}):(\d{2}):(\d{2})$/',    'iso' => "$3-$1-$2 $4:$5:$6"),
				'date_iso'       => array('sign' => '/^(\d{4})-(\d{2})-(\d{2})$/'),
				'date_id'        => array('sign' => '/^(\d{2})-(\d{2})-(\d{4})$/',      'iso' => "$3-$2-$1"),
				'date_us'        => array('sign' => '/^(\d{2})-(\d{2})-(\d{4})$/',      'iso' => "$3-$1-$2")
			);
			
			$format = null;
			foreach($regex as $i => $reg) {
				if(preg_match($reg['sign'], $date, $result)) {
					$format = $result[2] > 12 ? preg_replace('/_id/', '/_us/', $i) : $i;
					break;
				}
			}
			if($format) {
				if (!in_array($format, array('date_iso', 'datetime_iso')))
					$date = preg_replace($regex[$format]['sign'], $regex[$format]['iso'], $date);
				return $date;
			}
			else
				return null;
		}
	}
	function date_fix($date) {
		return preg_replace('/(\/|\.)/', '-', $date);
	}
	function date_style($d, $f=null, $h='') {
		$d = date_iso($d);
		switch($f) {
			case 'id':
			case 'dmy':
				$f = "d-m-Y $h";
				break;
			
			case 'us':
			case 'mdy':
				$f = "m-d-Y $h";
				break;
			
			case 'iso':
			case 'ymd':
				$f = "Y-m-d $h";
				break;
		}
		if($d && $f) {
			$f = trim($f);
			$t = strtotime($d);
			$d = $t ? date($f, $t) : $d;
		}
		return $d;
	}
	function date_str($d, $f='d M Y') {
		return date_style($d, $f);
	}
	function date_id($d) {
		return date_style($d, 'id');
	}
	function date_normalize(&$dat, $list) {
		$lst = explode(',', $list);
		foreach($lst as $a=>$b) {
			if(is_array($dat[$b])) {
				foreach($dat[$b] as $c=>$d)
					$dat[$b][$c] = date_iso($d);
			}
			else
				$dat[$b] = date_iso($b);
		}
	}
	function date_diff_of( $date_1, $date_2, $format="%y year %m month %d day") {
		// %y Year %m Month %d Day %h Hours %i Minute %s Seconds
		$interval = date_diff(date_create($date_1), date_create($date_2));
		return $interval->format($format);
	}
	function date_between($date_1, $date_2=null) {
		$date_2 = isset($date_2) ? $date_2 : $date_1;
		$d1 = date('Y-m-d', strtotime($date_1));
		$d2 = date('Y-m-d', strtotime($date_2));
		return " between '$d1 00:00:00' and '$d2 23:59:59'";
	}
	function date_range($range) {
		$date_1 = $date_2 = null;
		$range  = preg_replace('/ /', '_', date_fix($range));
		
		// range = Y-M
		if(preg_match('/-/', $range)) {
			$date_1 = $range . '-01';
			$date_2 = date('Y-m-d', strtotime('last day of '.$date_1));
		}
		else {
			$opr   = preg_match('/next/', $range) ? '+' : '';
			$prm    = explode('_', $range);
			$ctr_1 = isset($prm[1]) ? $prm[1] : $prm[0];
			if (is_numeric($ctr_1))
				$ctr_2 = preg_match('/next/', $range) ? $ctr_1 + 1 : $ctr_1 - 1;
			if($ctr_2 < 0 && $opr == '-')
				$opr = '';
		}
		
		switch($range) {
			case 'today':
				$date_1 = date('Y-m-d');
				break;
			case 'yesterday';
				$date_1 = date('Y-m-d', strtotime('yesterday'));
				break;
			
			case preg_match('/^(last|since|next).*day$/', $range) ? true : false:
				$str_1  = "today {$opr}{$ctr_1} day";
				$date_1 = date('Y-m-d', strtotime($str_1));
				break;
			
			case preg_match('/^(last|since|next).*week$/', $range) ? true : false:
				$str_1  = "monday {$opr}{$ctr_1} week";
				$str_2  = "monday {$opr}{$ctr_2} week -1 second";
				
				$date_1 = date('Y-m-d', strtotime($str_1));
				$date_2 = date('Y-m-d', strtotime($str_2));
				break;
			
			case preg_match('/^(last|since|next).*month/', $range) ? true : false:
				$ctr_2 = $ctr_1;
				$str_1 = "first day of {$opr}{$ctr_1} month";
				$str_2 = "last day of {$opr}{$ctr_2} month";
				$date_1 = date('Y-m-d', strtotime($str_1));
				$date_2 = date('Y-m-d', strtotime($str_2));
				break;
		}
		if(preg_match('/since/', $range))
			$date_2 = date('Y-m-d');
		
		if($date_1) {
			return date_between($date_1, $date_2);
		}
		else
			return null;
	}
	function last_week_day($d, $t='last') {
		list($y, $m, $h) = preg_split('/[-\/]/', $d);
		
		$dt = date_create("$d last day of $t month");
		$ls = $dt->format('Y-m-d');
		$wd = $dt->format('w');
		list($y, $m, $h) = preg_split('/[-\/]/', $ls);
		
		if($wd == 0)
			$h -= 2;
		elseif($wd == 6)
			$h -= 1;
		return "$y-$m-$h";
	}
	
	/**
	 * https://gist.github.com/Synchro/1139429
	 * Encode arbitrary data into base-62
	 * Note that because base-62 encodes slightly less than 6 bits per character (actually 5.95419631038688), there is some wastage
	 * In order to make this practical, we chunk in groups of up to 8 input chars, which give up to 11 output chars
	 * with a wastage of up to 4 bits per chunk, so while the output is not quite as space efficient as a
	 * true multiprecision conversion, it's orders of magnitude faster
	 * Note that the output of this function is not compatible with that of a multiprecision conversion, but it's a practical encoding implementation
	 * The encoding overhead tends towards 37.5% with this chunk size; bigger chunk sizes can be slightly more space efficient, but may be slower
	 * Base-64 doesn't suffer this problem because it fits into exactly 6 bits, so it generates the same results as a multiprecision conversion
	 * Requires PHP 5.3.2 and gmp 4.2.0
	 * @param string $data Binary data to encode
	 * @return string Base-62 encoded text (not chunked or split)
	 */
	function base62encode($data) {
		$outstring = '';
		$l = strlen($data);
		for ($i = 0; $i < $l; $i += 8) {
			$chunk = substr($data, $i, 8);
			$outlen = ceil((strlen($chunk) * 8)/6); //8bit/char in, 6bits/char out, round up
			$x = bin2hex($chunk);  //gmp won't convert from binary, so go via hex
			$w = gmp_strval(gmp_init(ltrim($x, '0'), 16), 62); //gmp doesn't like leading 0s
			$pad = str_pad($w, $outlen, '0', STR_PAD_LEFT);
			$outstring .= $pad;
		}
		return $outstring;
	}
	
	/**
	 * Decode base-62 encoded text into binary
	 * Note that because base-62 encodes slightly less than 6 bits per character (actually 5.95419631038688), there is some wastage
	 * In order to make this practical, we chunk in groups of up to 11 input chars, which give up to 8 output chars
	 * with a wastage of up to 4 bits per chunk, so while the output is not quite as space efficient as a
	 * true multiprecision conversion, it's orders of magnitude faster
	 * Note that the input of this function is not compatible with that of a multiprecision conversion, but it's a practical encoding implementation
	 * The encoding overhead tends towards 37.5% with this chunk size; bigger chunk sizes can be slightly more space efficient, but may be slower
	 * Base-64 doesn't suffer this problem because it fits into exactly 6 bits, so it generates the same results as a multiprecision conversion
	 * Requires PHP 5.3.2 and gmp 4.2.0
	 * @param string $data Base-62 encoded text (not chunked or split)
	 * @return string Decoded binary data
	 */
	function base62decode($data) {
		$outstring = '';
		$l = strlen($data);
		for ($i = 0; $i < $l; $i += 11) {
			$chunk = substr($data, $i, 11);
			$outlen = floor((strlen($chunk) * 6)/8); //6bit/char in, 8bits/char out, round down
			$y = gmp_strval(gmp_init(ltrim($chunk, '0'), 62), 16); //gmp doesn't like leading 0s
			$pad = str_pad($y, $outlen * 2, '0', STR_PAD_LEFT); //double output length as as we're going via hex (4bits/char)
			$outstring .= pack('H*', $pad); //same as hex2bin
		}
		return $outstring;
	}

	// STRING
	function left($str, $length) {
		return substr($str, 0, $length);
	}
	function right($str, $length) {
		return substr($str, -$length);
	}
	
	/**
	 * Generate a License Key.
	 * Optional Suffix can be an integer or valid IPv4, either of which is converted to Base36 equivalent
	 * If Suffix is neither Numeric or IPv4, the string itself is appended
	 *
	 * @param   string  $suffix Append this to generated Key.
	 * @return  string
	 * http://stackoverflow.com/questions/3687878/serial-generation-with-php
	 */
	function generate_license($suffix = null) {
		// Default tokens contain no "ambiguous" characters: 1,i,0,o
		if(isset($suffix)) {
			// Fewer segments if appending suffix
			$num_segments  = 3;
			$segment_chars = 6;
		}
		else {
			$num_segments  = 4;
			$segment_chars = 5;
		}
		$tokens         = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
		$license_string = '';
		// Build Default License String
		for($i = 0; $i < $num_segments; $i++) {
			$segment = '';
			for($j = 0; $j < $segment_chars; $j++) {
				$segment .= $tokens[rand(0, strlen($tokens) - 1)];
			}
			$license_string .= $segment;
			if($i < ($num_segments - 1)) {
				$license_string .= '-';
			}
		}
		// If provided, convert Suffix
		if(isset($suffix)) {
			if(is_numeric($suffix)) {   // Userid provided
				$license_string .= '-' . strtoupper(base_convert($suffix, 10, 36));
			}
			else {
				$long = sprintf("%u\n", ip2long($suffix), true);
				if($suffix === long2ip($long)) {
					$license_string .= '-' . strtoupper(base_convert($long, 10, 36));
				}
				else {
					$license_string .= '-' . strtoupper(str_ireplace(' ', '-', $suffix));
				}
			}
		}
		
		return $license_string;
	}

	// CURL
	function curl_err($err) {
		$curl_errno = array(
			1  => "CURLE_UNSUPPORTED_PROTOCOL",
			2  => "CURLE_FAILED_INIT",
			3  => "CURLE_URL_MALFORMAT",
			4  => "CURLE_URL_MALFORMAT_USER",
			5  => "CURLE_COULDNT_RESOLVE_PROXY",
			6  => "CURLE_COULDNT_RESOLVE_HOST",
			7  => "CURLE_COULDNT_CONNECT",
			8  => "CURLE_FTP_WEIRD_SERVER_REPLY",
			9  => "CURLE_FTP_ACCESS_DENIED",
			10 => "CURLE_FTP_USER_PASSWORD_INCORRECT",
			11 => "CURLE_FTP_WEIRD_PASS_REPLY",
			12 => "CURLE_FTP_WEIRD_USER_REPLY",
			13 => "CURLE_FTP_WEIRD_PASV_REPLY",
			14 => "CURLE_FTP_WEIRD_227_FORMAT",
			15 => "CURLE_FTP_CANT_GET_HOST",
			16 => "CURLE_FTP_CANT_RECONNECT",
			17 => "CURLE_FTP_COULDNT_SET_BINARY",
			18 => "CURLE_FTP_PARTIAL_FILE or CURLE_PARTIAL_FILE",
			19 => "CURLE_FTP_COULDNT_RETR_FILE",
			20 => "CURLE_FTP_WRITE_ERROR",
			21 => "CURLE_FTP_QUOTE_ERROR",
			22 => "CURLE_HTTP_NOT_FOUND or CURLE_HTTP_RETURNED_ERROR",
			23 => "CURLE_WRITE_ERROR",
			24 => "CURLE_MALFORMAT_USER",
			25 => "CURLE_FTP_COULDNT_STOR_FILE",
			26 => "CURLE_READ_ERROR",
			27 => "CURLE_OUT_OF_MEMORY",
			28 => "CURLE_OPERATION_TIMEDOUT or CURLE_OPERATION_TIMEOUTED",
			29 => "CURLE_FTP_COULDNT_SET_ASCII",
			30 => "CURLE_FTP_PORT_FAILED",
			31 => "CURLE_FTP_COULDNT_USE_REST",
			32 => "CURLE_FTP_COULDNT_GET_SIZE",
			33 => "CURLE_HTTP_RANGE_ERROR",
			34 => "CURLE_HTTP_POST_ERROR",
			35 => "CURLE_SSL_CONNECT_ERROR",
			36 => "CURLE_BAD_DOWNLOAD_RESUME or CURLE_FTP_BAD_DOWNLOAD_RESUME",
			37 => "CURLE_FILE_COULDNT_READ_FILE",
			38 => "CURLE_LDAP_CANNOT_BIND",
			39 => "CURLE_LDAP_SEARCH_FAILED",
			40 => "CURLE_LIBRARY_NOT_FOUND",
			41 => "CURLE_FUNCTION_NOT_FOUND",
			42 => "CURLE_ABORTED_BY_CALLBACK",
			43 => "CURLE_BAD_FUNCTION_ARGUMENT",
			44 => "CURLE_BAD_CALLING_ORDER",
			45 => "CURLE_HTTP_PORT_FAILED",
			46 => "CURLE_BAD_PASSWORD_ENTERED",
			47 => "CURLE_TOO_MANY_REDIRECTS",
			48 => "CURLE_UNKNOWN_TELNET_OPTION",
			49 => "CURLE_TELNET_OPTION_SYNTAX",
			50 => "CURLE_OBSOLETE",
			51 => "CURLE_SSL_PEER_CERTIFICATE",
			52 => "CURLE_GOT_NOTHING",
			53 => "CURLE_SSL_ENGINE_NOTFOUND",
			54 => "CURLE_SSL_ENGINE_SETFAILED",
			55 => "CURLE_SEND_ERROR",
			56 => "CURLE_RECV_ERROR",
			57 => "CURLE_SHARE_IN_USE",
			58 => "CURLE_SSL_CERTPROBLEM",
			59 => "CURLE_SSL_CIPHER",
			60 => "CURLE_SSL_CACERT",
			61 => "CURLE_BAD_CONTENT_ENCODING",
			62 => "CURLE_LDAP_INVALID_URL",
			63 => "CURLE_FILESIZE_EXCEEDED",
			64 => "CURLE_FTP_SSL_FAILED",
			79 => "CURLE_SSH"
		);
		return $curl_errno[$err];
	}
	function curl_opt($opt) {
		$rst = array();
		$c_c = array();
		foreach(get_defined_constants(true)['curl'] as $k=>$v) {
			if(preg_match('/CURLOPT/', $k))
				$c_c[$v] = $k;
		}
		
		foreach($opt as $k=>$v)
			$rst[$c_c[$k]] = $v;
		
		return $rst;
	}
	function curl_cfg($url,  &$opt, $dat=null) {
		//Set the URL that you want to GET by using the CURLOPT_URL option.
		$ver = curl_version();
		$uag = 'curl/' . $ver['version'];
		
		$opt[CURLOPT_USERAGENT]                 = $uag;
		$opt[CURLOPT_VERBOSE]                   = $opt[CURLOPT_VERBOSE] ?? ((ENVIRONMENT == 'production') ? false : true);
		
		if($opt[CURLOPT_VERBOSE]) {
			// $opt['CURLOPT_STDERR']      = fopen(APPPATH . "logs/log-curl.txt", 'w+');
			// file_log('log-curl.txt', $opt, $dat);
		}
		
		$opt[CURLOPT_RETURNTRANSFER]          ??= true;                       // Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
		$opt[CURLOPT_FOLLOWLOCATION]          ??= true;                       // Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
		// $opt[CURLOPT_ENCODING]                ??= '';
		// $opt[CURLOPT_MAXREDIRS]               ??= 10;
		// $opt[CURLOPT_HTTP_VERSION]            ??= CURL_HTTP_VERSION_1_1;
		$opt[CURLOPT_TIMEOUT]                 ??= 120;
		if(isset($opt[CURLOPT_TIMEOUT]) && is_numeric($opt[CURLOPT_TIMEOUT])) {
			if($opt[CURLOPT_TIMEOUT] > ini_get('max_execution_time'))
				set_time_limit($opt[CURLOPT_TIMEOUT] + 10);
			$opt[CURLOPT_CONNECTTIMEOUT]      = $opt[CURLOPT_TIMEOUT];
		}
		
		// if(isset($opt[CURLOPT_HTTPHEADER]))
			$opt[CURLOPT_HEADER]              = false;
		
		if(preg_match('/https/', $url)) {
			// true | false : Check if peer certificate is valid or invalid/expired
			$opt[CURLOPT_SSL_VERIFYPEER]      ??= true;         // true | false
			$opt[CURLOPT_SSL_VERIFYHOST]      ??= 2;            // 0 | 1 | 2
				// 1 -> to check the existence of a common name in the SSL peer certificate.
				// 2 -> to check the existence of a common name and also verify that it matches the hostname provided.
			
			//$opt[CURLOPT_SSL_VERIFYSTATUS]    ??= true;
			
			if(ENVIRONMENT == 'development' && !isset($opt[CURLOPT_CERTINFO]))
				$opt[CURLOPT_CERTINFO]          = true;
		}
		
		if($dat) {
			$opt[CURLOPT_HTTPHEADER]          ??= array();
			
			if(is_array($dat))
				$dat = http_build_query($dat);
			
			if(strlen($dat) > 1024) {
				$opt[CURLOPT_ENCODING]          = 'gzip';
				array_unshift($opt[CURLOPT_HTTPHEADER], 'Expect:');
			}
			
			if(is_json($dat)) {
				if(!in_array('Content-Type: application/json', array_values($opt[CURLOPT_HTTPHEADER])))
					array_push($opt[CURLOPT_HTTPHEADER],
						'Content-Type: application/json'
					);
				array_push($opt[CURLOPT_HTTPHEADER],
					'Content-Length: ' . strlen($dat)
				);
			}
			else {
				array_push($opt[CURLOPT_HTTPHEADER],
					'Content-Type: plain/text',
					'Content-Length: ' . strlen($dat)
				);
			}
			$opt[CURLOPT_POST]                  = true;
			$opt[CURLOPT_POSTFIELDS]            = $dat;
		}
	}
	function curl_get($url,  &$opt=array()) {
		$curl = curl_init($url);
		
		curl_cfg($url, $opt);
		$opt[CURLOPT_CUSTOMREQUEST] = 'GET';
		
		curl_setopt_array($curl, $opt);
		
		$rsp = curl_exec($curl);

		if($rsp === false)
			$rsp = curl_errno($curl);
		
		if(ENVIRONMENT == 'development')
			$opt['curl_info'] = curl_getinfo($curl);
		
		curl_close($curl);
		
		return $rsp;
	}
	function curl_posz($url, $dat=null, $header=array(), $opt=array()) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url );
		// curl_setopt($curl, CURLOPT_VERBOSE, 1);
		
		if(isset($opt['http_version']))
			curl_setopt( $curl, CURLOPT_HTTP_VERSION, $opt['http_version'] );
		
		curl_setopt($curl, CURLOPT_POST, TRUE );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE );
		
		if($dat) {
			if(is_array($dat))
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dat));
			else {
				curl_setopt($curl,CURLOPT_ENCODING , 'gzip');
				
				if(strlen($dat) > 1024)
					array_unshift($header, 'Expect:');
				
				if(is_json($dat)) {
					if(!in_array('Content-Type: application/json', array_values($header)))
						array_push($header,
							'Content-Type: application/json',
							'Content-Length: ' . strlen($dat)
						);
				}
				else {
					array_push($header,
						'Content-Type: plain/text',
						'Content-Length: ' . strlen($dat)
					);
					//$dat = urlencode($dat);
				}
				curl_setopt($curl, CURLOPT_POSTFIELDS, $dat);
			}
		}
		
		if($header)
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_HEADER, FALSE );
		
		$opt[CURLOPT_TIMEOUT] ??= 120;
		set_time_limit($opt[CURLOPT_TIMEOUT]);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $opt[CURLOPT_TIMEOUT]);
		curl_setopt($curl, CURLOPT_TIMEOUT, $opt[CURLOPT_TIMEOUT]);
		
		$rsp = curl_exec($curl);
		if(($err = curl_errno($curl)) !== 0)
			$rsp = $err;
		
		curl_close($curl);
		return $rsp;
	}
	function curl_post($url, &$opt=array(), $dat=null) {
		$curl = curl_init($url);
		
		curl_cfg($url, $opt, $dat);
		curl_setopt_array($curl, $opt);
		$rsp = curl_exec($curl);
		
		$opt['curl_response'] = $rsp;
		
		if($rsp === false) {
			$rsp = curl_errno($curl);
			$opt['curl_no']  = $rsp;
			$opt['curl_msg'] = curl_err($rsp);
		}
		
		if(ENVIRONMENT == 'development')
			$opt['curl_info'] = curl_getinfo($curl);
		
		if($opt[CURLOPT_VERBOSE])
			file_log('log-curl.txt', $opt, $dat);

		curl_close($curl);
		return $rsp;
	}
	function curl_posx($url, $opt=array(), $dat=null) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url );
		
		if(isset($opt['VERBOSE']))          curl_setopt($curl, CURLOPT_VERBOSE, 1);
		if(isset($opt['HTTP_VERSION']))     curl_setopt( $curl, CURLOPT_HTTP_VERSION, $opt['HTTP_VERSION'] );
		if(isset($opt['USERAGENT']))        curl_setopt( $curl, CURLOPT_USERAGENT, $opt['USERAGENT'] );
		
		curl_setopt($curl, CURLOPT_POST, true );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
		
		if(preg_match('/https/', $url)) {
			if(isset($opt['CAINFO']))   curl_setopt($curl, CURLOPT_CAINFO, $opt['CAINFO']);
			if(isset($opt['SSLKEY']))   curl_setopt($curl, CURLOPT_SSLKEY, $opt['SSLKEY']);
			
			// true | false : Check if peer certificate is valid or invalid/expired
			$opt['SSL_VERIFYPEER'] ??= true;
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $opt['SSL_VERIFYPEER']);
			
			// 1 -> to check the existence of a common name in the SSL peer certificate.
			// 2 -> to check the existence of a common name and also verify that it matches the hostname provided.
			$opt['SSL_VERIFYHOST'] ??= 2;
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,  $opt['SSL_VERIFYHOST']);
		}
		
		$opt['FOLLOWLOCATION'] ??= true;
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $opt['FOLLOWLOCATION']);
		
		if($dat) {
			if(is_array($dat))
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dat));
			else {
				curl_setopt($curl,CURLOPT_ENCODING , 'gzip');
				
				if(strlen($dat) > 1024)
					array_unshift($hdr, 'Expect:');
				
				if(is_json($dat)) {
					if(!in_array('Content-Type: application/json', array_values($hdr)))
						array_push($hdr,
							'Content-Type: application/json',
							'Content-Length: ' . strlen($dat)
						);
				}
				else {
					array_push($hdr,
						'Content-Type: plain/text',
						'Content-Length: ' . strlen($dat)
					);
					//$dat = urlencode($dat);
				}
				curl_setopt($curl, CURLOPT_POSTFIELDS, $dat);
			}
		}
		
		// if($hdr) curl_setopt($curl, CURLOPT_HTTPHEADER, $hdr);
		
		$opt['HEADER'] ??=  true;
		curl_setopt($curl, CURLOPT_HEADER, $opt['HEADER']);
		
		$opt['time_out'] ??= 120;
		set_time_limit($opt['time_out']);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $opt['time_out']);
		curl_setopt($curl, CURLOPT_TIMEOUT, $opt['time_out']);
		
		$rsp = curl_exec($curl);
		if($rsp === false)
			$rsp = curl_errno($curl);
		
		curl_close($curl);
		return $rsp;
	}
	function curl_del($url, $dat = '', $hdr=false) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		if($dat) {
			$dat = json_encode($dat);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $dat);
		}
		if($hdr)
			curl_setopt($curl, CURLOPT_HTTPHEADER, $hdr);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		$result = json_decode($result);
		curl_close($curl);
		
		return $result;
	}
	function curl_exec_continue($ch) {
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result   = curl_exec($ch);
		$continue = 0 === strpos($result, "HTTP/1.1 100 Continue\x0d\x0a\x0d\x0a");
		echo "Continue: ", $continue ? 'Yes' : 'No', "\n";
		
		return $result;
	}
	
	function curl_file_get($url) {
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$data = curl_exec($ch);
		curl_close($ch);
		
		return $data;
	}
	function curl_file_post($url, $file) {
		// $file        = 'c:/www/dev/wyeth_crm/asset/img/loading.gif';
		// end_point    = 'http://winkerz/crm/home/gw/';
		$file['file']   = function($file) {
			$mime = mime_content_type($file);
			$info = pathinfo($file);
			$name = $info['basename'];
			$output = new CURLFile($file, $mime, $name);
			return $output;
		};
		$request        = curl_init();
		curl_setopt($request, CURLOPT_URL, $url);
		curl_setopt($request, CURLOPT_POST, 1);
		curl_setopt($request, CURLOPT_POSTFIELDS, $file);
		
		// output the response
		$rsp    = curl_exec($request);
		$inf    = curl_getinfo($request);
		$err    = curl_error($request);
		curl_close($request);
	}
	
	function form_post($url, $postVars = array()){
		//Transform our POST array into a URL-encoded query string.
		$postStr = http_build_query($postVars);
		//Create an $options array that can be passed into stream_context_create.
		$options = array(
			'http' =>
				array(
					'method'  => 'POST', //We are using the POST HTTP method.
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postStr //Our URL-encoded query string.
				)
		);
		//Pass our $options array into stream_context_create.
		//This will return a stream context resource.
		$streamContext  = stream_context_create($options);
		//Use PHP's file_get_contents function to carry out the request.
		//We pass the $streamContext variable in as a third parameter.
		$result = file_get_contents($url, false, $streamContext);
		//If $result is FALSE, then the request has failed.
		if($result === false){
			//If the request failed, throw an Exception containing
			//the error.
			$error = error_get_last();
			throw new Exception('POST request failed: ' . $error['message']);
		}
		//If everything went OK, return the response.
		return $result;
	}
	// Format Phone
	function msisdn_prefix($arg, $prefix='62') {
		$arg = str_replace(array('+', '-', ' '), '', $arg);
		if($prefix != substr($arg, 0, strlen($prefix))) {
			switch ($prefix) {
				case '08':
					// 62 -> 08
					if(substr($arg, 0, 2) == '62')
						$arg = '0' . substr($arg, 2, strlen($arg) - 2);
					break;
				case '62':
					// 0XX -> 62XX
					if(substr($arg, 0, 1) == '0')
						$arg = $prefix . substr($arg, 1, strlen($arg) - 1);
					else
						$arg = $prefix . $arg;
					break;
				default:
					$arg = $prefix . $arg;
					break;
			}
		}
		return $arg;
	}
	function format_phone($phone, $country='id') {
		$function = 'format_phone_' . $country;
		if(function_exists($function)) {
			return $function($phone);
		}
		return $phone;
	}
	function format_phone_id($phone) {
		// note: strip out everything but numbers
		$phone  = preg_replace('/[^0-9]/', '', $phone);
		$length = strlen($phone);
		if(preg_match('/^08/', $phone)) {
			switch ($length) {
				case 10:
					// 0811 888-005
					$phone = preg_replace("/([0-9]{4})([0-9]{3})([0-9]{3})/", "$1 $2-$3", $phone);
					break;
				case 11:
					// 0811 888-0055
					$phone = preg_replace("/([0-9]{4})([0-9]{3})([0-9]{4})/", "$1 $2-$3", $phone);
					break;
				case 12:
					// 0811 8888-0055
					$phone = preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$1 $2-$3", $phone);
					break;
				case 13:
					// 0811 8888-00555
					$phone = preg_replace("/([0-9]{4})([0-9]{4})([0-9]{5})/", "$1 $2-$3", $phone);
					break;
				case 14:
					// 0811 88123-00555
					$phone = preg_replace("/([0-9]{4})([0-9]{5})([0-9]{5})/", "$1 $2-$3", $phone);
					break;
			}
		}
		elseif(preg_match('/628/', $phone)) {
			switch ($length) {
				case 11:
					// (62)811-888-005
					$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{3})/", "$(1)$2 $3-$4", $phone);
					break;
				case 12:
					// (62)811-888-0055
					$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
					break;
				case 13:
					// (62)811-8888-0055
					$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{4})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
					break;
				case 14:
					// (62)811-8888-00555
					$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{4})([0-9]{5})/", "$(1)$2 $3-$4", $phone);
					break;
				case 15:
					// (62)811-8888-0055
					$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{5})([0-9]{5})/", "$(1)$2 $3-$4", $phone);
					break;
			}
		}
		else {
			if(preg_match('/^62(21|22|24|31|61)/', $phone)) {
				switch ($length) {
					case 9:
						// (62)21-88-005
						$phone = preg_replace("/([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{3})/", "$(1)$2 $3-$4", $phone);
						break;
					case 10:
						// (62)21-888-005
						$phone = preg_replace("/([0-9]{2})([0-9]{2})([0-9]{3})([0-9]{3})/", "$(1)$2 $3-$4", $phone);
						break;
					case 11:
						// (62)21-888-0055
						$phone = preg_replace("/([0-9]{2})([0-9]{2})([0-9]{3})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
						break;
					case 12:
						// (62)21-8888-0055
						$phone = preg_replace("/([0-9]{2})([0-9]{2})([0-9]{4})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
						break;
				}
			}
			elseif(preg_match('/^62(230-799)/', $phone)) {
				switch($length) {
					case 9:
						// (62)230-88-05
						$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{2})([0-9]{2})/", "$(1)$2 $3-$4", $phone);
						break;
					case 10:
						// (62)230-88-005
						$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{2})([0-9]{3})/", "$(1)$2 $3-$4", $phone);
						break;
					case 11:
						// (62)230-888-055
						$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{3})/", "$(1)$2 $3-$4", $phone);
						break;
					case 12:
						// (62)230-888-0555
						$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
						break;
					case 13:
						// (62)230-8888-0555
						$phone = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{4})([0-9]{4})/", "$(1)$2 $3-$4", $phone);
						break;
				}
			}
		}
		return $phone;
	}

	// Format Currency IDR
	function format_num_id($rp, $dec=-1) {
		if($dec < 0) {
			$pos = strpos($rp, '.');
			$dec = $pos === false ? 0 : strlen(right($rp, strlen($rp) - strpos($rp, '.') - 1));
		}
		return number_format($rp, $dec,',','.');
	}
	
	function format_token($s, $l=4) {
		$n = strlen($s) / $l;
		$r = '/';
		$o = array();
		for($i=1; $i <= $n; $i++) {
			$r  .= '(\d{' . $l . '})';
			$o[] = '$' . $i;
		}
		$r .= '/';
		$o  = implode('-', $o);
		return preg_replace($r, $o, $s);
	}
	// MISC
	/*
	 *
	 */
	function sys_mem_usage () {
		$mem = memory_get_usage();
		return sys_mem_size($mem);
	};
	function sys_mem_size($mem) {
		if ($mem < 1024)
			$unit = $mem ." B";
		elseif ($mem < 1048576)
			$unit = round($mem/1024,2) . " KB";
		else
			$unit = round($mem/1048576,2) . " MB";
		return $unit;
	}
	
// end of file iop_helper.php
/* Location: ./application/helper/iop_helper.php */