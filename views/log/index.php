<?php defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Weblink - Logs View</title>
	<link rel="stylesheet" type="text/css" href="/assets/thirdparty/bootstrap/css/1.css" />
	<link rel="shortcut icon" href="https://kaspro.id/style/images/favicon.png" />
	<style type="text/css">
		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }
	
		
		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}
	
		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}
		h5 {
			color: orange!important;
			font-weight: bold!important;
			padding: 14px 15px 10px 15px!important;
			display: inline-block;
		}
		h5 small {
			color: grey!important;
			font-style: italic!important;
		}
		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: inline-block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}
	
		.body {
			margin: 0 15px 0 15px;
		}
	
		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}
	
		.box-container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
		
		table.fixed  {
			border-collapse:collapse;
			table-layout:fixed;
			word-wrap:break-word;
		}
		table.fixed td {
			width: 300px;
			word-wrap: break-word;
		}
		table.table td {
			max-width: 600px;
		}
	</style>
	<script src="<?php echo $app; ?>/assets/thirdparty/js/jquery-1.10.2.js"></script>
</head>
<body>
	<div class="box-container">
		<h1>Weblink - Log View
			<p style="display:inline; float:right; margin-top:-8px;">
				<a href="/log/out" class="btn btn-small"> <?php echo $usr; ?> <i class="glyphicon glyphicon-off"></i></a>
			</p>
		</h1>
		<h5 style="float:right">Certificate Expired Date<br/><?php echo $exp; ?></h5>
		<div class="body" style="width: 800px">
			<table class="table">
				<tr><th>Last 10 Request-ID</th>
					<th><input id="date_trx" type="date" name="date_trx" value="<?php echo $tgl; ?>" /></th>
					<th style="width:16px;vertical-align: middle">
						<a id="tgl" href='javascript:;' class='btn btn-sm' title="search by date"><i class='glyphicon glyphicon-search'></i></a>
					</th>
				</tr>
				<?php
					foreach($lst as $k => $v) {
						echo "<tr><th>{$v['request_name']}</th>
								  <td>{$v['request_id']}</td>
								  <td><a href='javascript:;' class='btn btn-sm lgx' title='view log'><i class='glyphicon glyphicon-refresh'></i></a></td>
							  </tr>";
					}
				?>
			</table>
		</div>
		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
	</div>
	<div class="box-container">
		<h1>Weblink - Transaction Log</h1>
		<div class="body">
			<div class="form-group row">
				<div class="col-md-3">
					<input class="form-control" id="request_id" placeholder="Request-ID" />
				</div>
				<div class="col-md-3">
					<button id="btn" class="btn"><i class='glyphicon glyphicon-refresh'></i> View</button>
				</div>
			</div>
			<div id="rpt" style="display:flex; flex-flow: wrap"></div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('.lgx').click(function() {
				$('#request_id').val($(this).parent().prev().html());
				$('#btn').click();
			});
			$('#btn').click(function() {
				$('#rpt').load('/log/rpt/' + $('#request_id').val());
			});
			$('#tgl').click(function() {
				let val = $('#date_trx').val();
				window.location.href = '/log/index/'+val;
			})
		});
	</script>
</body>
</html>