<link rel="stylesheet" type="text/css" href="<?php echo $app; ?>/assets/thirdparty/bootstrap/css/1.css" />
<div id="form_container">
	<form id="form_102043" class="appnitro" method="post" action="<?php echo $app; ?>/test/payment">
		<div class="form_description">
			<h2>TELCO POSTPAID</h2>
		</div>
		<ul><li><label class="description">fromaccount </label>
				<div><input name="fromaccount" class="element text medium" type="text" maxlength="255" value="595821123456"></div>
			</li>
			<li><label class="description">amount </label>
				<div><input name="amount" class="element text medium" type="text" maxlength="255" value="13000"></div>
			</li>
			<li><label class="description">sku </label>
				<div><input name="sku" class="element text medium" type="text" maxlength="255" value="SUB-TELKOMSEL"></div>
			</li>
			<li><label class="description">branchid </label>
				<div><input name="branchid" class="element text medium" type="text" maxlength="255" value="CSD"></div>
			</li>
			<li><label class="description">brand </label>
				<div><input name="brand" class="element text medium" type="text" maxlength="255" value="TELKOMSEL"></div>
			</li>
			<li><label class="description">language </label>
				<div><input name="language" class="element text medium" type="text" maxlength="255" value="EN"></div>
			</li>
			<li><label class="description">terminalid </label>
				<div><input name="terminalid" class="element text medium" type="text" maxlength="255" value="2F643DE5851620D5"></div>
			</li>
			<li><label class="description">msisdn </label>
				<div><input name="msisdn" class="element text medium" type="text" maxlength="255" value="081218397347"></div>
			</li>
			<li><label class="description">cashierid </label>
				<div><input name="cashierid" class="element text medium" type="text" maxlength="255" value="EMP001"></div>
			</li>
			<li><label class="description">servicefee </label>
				<div><input name="servicefee" class="element text medium" type="text" maxlength="255" value="2500"></div>
			</li>
			<li><label class="description">number-of-fees </label>
				<div><input name="number-of-fees" class="element text medium" type="text" maxlength="255" value="1"></div>
			</li>
			<li><label class="description">transid </label>
				<div><input name="transid" class="element text medium" type="text" maxlength="255" value="2462222"></div>
			</li>
			<li><label class="description">partner-reference </label>
				<div><input name="partner-reference" class="element text medium" type="text" maxlength="255" value="244846164"></div>
			</li>
			<li><label class="description">type </label>
				<div><input name="type" class="element text medium" type="text" maxlength="255" value="POSTPAID"></div>
			</li>
			<li><label class="description">user-id </label>
				<div><input name="user-id" class="element text medium" type="text" maxlength="255" value="081218397347"></div>
			</li>
			<li><label class="description">redirect_url </label>
				<div><input name="redirect_url" class="element text medium" type="text" maxlength="255" value="www.example.com"></div>
			</li>
			
			<li class="buttons">
				<button id="btn" class="button_text" type="submit">Submit</button>
			</li>
		</ul>
	</form>
</div>