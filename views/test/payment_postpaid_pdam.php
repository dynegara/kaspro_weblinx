<link rel="stylesheet" type="text/css" href="<?php echo $app; ?>/assets/thirdparty/bootstrap/css/1.css" />
<div id="form_container">
	<form id="form_102043" class="appnitro" method="post" action="<?php echo $app; ?>/test/payment">
		<div class="form_description">
			<h2>PDAM POSTPAID</h2>
		</div>
		<ul><li><label class="description" for="element_1">fromaccount </label>
				<div><input id="element_1" name="fromaccount" class="element text medium" type="text" maxlength="255" value="595821123456"></div>
			</li>
			<li><label class="description" for="element_2">amount </label>
				<div><input id="element_2" name="amount" class="element text medium" type="text" maxlength="255" value="61,500"></div>
			</li>
			<li><label class="description" for="element_2">sku </label>
				<div><input id="element_2" name="sku" class="element text medium" type="text" maxlength="255" value="SUB-PDAMSY0005"></div>
			</li>
			<li><label class="description" for="element_2">branchid </label>
				<div><input id="element_2" name="branchid" class="element text medium" type="text" maxlength="255" value=""></div>
			</li>
			<li><label class="description" for="element_2">brand </label>
				<div><input id="element_2" name="brand" class="element text medium" type="text" maxlength="255" value=""></div>
			</li>
			<li><label class="description" for="element_2">language </label>
				<div><input id="element_2" name="language" class="element text medium" type="text" maxlength="255" value=""></div>
			</li>
			<li><label class="description" for="element_2">terminalid </label>
				<div><input id="element_2" name="terminalid" class="element text medium" type="text" maxlength="255" value=""></div>
			</li>
			<li><label class="description" for="element_2">msisdn </label>
				<div><input id="element_2" name="msisdn" class="element text medium" type="text" maxlength="255" value="9990312309"></div>
			</li>
			<li><label class="description" for="element_2">cashierid </label>
				<div><input id="element_2" name="cashierid" class="element text medium" type="text" maxlength="255" value=""></div>
			</li>
			<li><label class="description" for="element_2">servicefee </label>
				<div><input id="element_2" name="servicefee" class="element text medium" type="text" maxlength="255" value="2500"></div>
			</li>
			<li><label class="description" for="element_2">transid </label>
				<div><input id="element_2" name="transid" class="element text medium" type="text" maxlength="255" value="259681111039"></div>
			</li>
			<li><label class="description" for="element_2">partner-reference </label>
				<div><input id="element_2" name="partner-reference" class="element text medium" type="text" maxlength="255" value="0000056575"></div>
			</li>
			<li><label class="description" for="element_2">number-of-fees </label>
				<div><input id="element_2" name="number-of-fees" class="element text medium" type="text" maxlength="255" value="1"></div>
			</li>
			<li><label class="description" for="element_2">type </label>
				<div><input id="element_2" name="type" class="element text medium" type="text" maxlength="255" value="PDAM"></div>
			</li>
			<li><label class="description" for="element_2">user-id </label>
				<div><input id="element_2" name="user-id" class="element text medium" type="text" maxlength="255" value="0857202020"></div>
			</li>
			<li><label class="description" for="element_2">Inquire-token </label>
				<div><textarea id="element_2" name="inquire-token" class="element text medium" rows="5" cols="80">MzEzMjExMDk5OTAzMTIzMDkgICAgICAgICAgMTUxMTIwMTgxMDM0MTZQRUxBTkdHQU4gUERBTSBERVBPSyAgICAgICAgICAwMDAwMDAwNTkwMDAwMDAwMDAwMDI1MDAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFNZMDAwNSAgICAgICAgICAgICAgICAgICAgICAgICAgMDAwMDA1NjU3NQ==</textarea> </div>
			</li>
			<li><label class="description" for="element_2">redirect_url </label>
				<div><input id="element_2" name="redirect_url" class="element text medium" type="text" maxlength="255" value="www.example.com"></div>
			</li>
			
			<li class="buttons">
				<button id="btn" class="button_text" type="submit">Submit</button>
			</li>
		</ul>
	</form>
</div>