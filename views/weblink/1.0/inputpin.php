<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Script Tutorials" />
	<title>KasPro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/images/indosat/icon_tab.png" />

	<!-- attach CSS styles -->
	<link href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/css/1.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/font-awesome-4.6.3/css/font-awesome.min.css" />
	<!--<link href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/css/style.css" rel="stylesheet" />-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/country-select-js-master/build/css/countrySelect.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/text-security/text-security.css" />
</head>

<body class="" cz-shortcut-listen="true">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/plugins/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-ui.js"></script>
	<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/messagedialog.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/inputvalidation.js"></script>
	<div id="wrapper">
		<div id="content">
			<div class="container" style="padding: 50px">
			    <span id="data" hidden></span>
				<p class="description">
					Silahkan masukkan 6 digit PIN kamu
				</p>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<div class="input-container">
								<input type="number" id="pin1" class="input input-secure" oninput="onInputEvent(1, event)" onkeyup="onKeyUpEvent(1, event)" maxlength="1" required autofocus />
								<input type="number" id="pin2" class="input input-secure" oninput="onInputEvent(2, event)" onkeyup="onKeyUpEvent(2, event)" maxlength="1" required />
								<input type="number" id="pin3" class="input input-secure" oninput="onInputEvent(3, event)" onkeyup="onKeyUpEvent(3, event)" maxlength="1" required />
								<input type="number" id="pin4" class="input input-secure" oninput="onInputEvent(4, event)" onkeyup="onKeyUpEvent(4, event)" maxlength="1" required />
								<input type="number" id="pin5" class="input input-secure" oninput="onInputEvent(5, event)" onkeyup="onKeyUpEvent(5, event)" maxlength="1" required />
								<input type="number" id="pin6" class="input input-secure" oninput="onInputEvent(6, event)" onkeyup="onKeyUpEvent(6, event)" maxlength="1" required />
								<button class="btn-eye" onclick="showPassword()"><i id="eye" class="fa fa-eye-slash" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>
				</div>
				<form id="inputPinForm" method="post" action="<?php echo "$svr/$res"; ?>" hidden="">
					<input type="hidden" name="request-id" value="<?php echo $transid; ?>" />
					<input type="hidden" id="pin" name="pin" value="" />
				</form>
				<form id="forgotPinForm" name="forgotPinForm" method="post" action="http://dev.kaspro.id/kaspro/inputphonenumber/" hidden="">
					<input type="hidden" name="data" value="<?php echo $ucb; ?>" />
				</form>
				<p class="forgotPin">
					<a href="javascript:" id="forgotPin">Lupa PIN?</a>
				</p>
			</div>
		</div>
	</div>
	<div id="footer" style="background-color: rgba(252,252,252,5);
                     width:100%;
                     /*height:50px;*/
                     position:static;
                     bottom:0;
                     left:0;
                     margin-bottom:0px; padding: 15px">
		<div class="row">
			<div class="col-md-12" style="text-align: center">
				<h6>©KasPro. <a href="http://www.kaspro.id" target="_blank">www.kaspro.id</a></h6>
			</div>
		</div>
	</div>
	<style>
		span {
			font-size: 15px;
		}
		
		span.error {
			color: red;
		}
		
		span.icon-white {
			background: url(<?php echo base_url(); ?>assets/local/images/right-arrow.svg) no-repeat top left;
			background-size: contain;
			cursor: pointer;
			display: inline-block;
			height: 20px;
			width: 20px;
		}
		
		span.icon-dark {
			background: url(<?php echo base_url(); ?>assets/local/images/right-arrow-dark.svg) no-repeat top left;
			background-size: contain;
			cursor: pointer;
			display: inline-block;
			height: 20px;
			width: 20px;
		}
		
		span {
			font-size: 15px;
		}
		
		span.error {
			color: red;
		}
		
		input[type="number"]::-webkit-inner-spin-button,
		input[type="number"]::-webkit-outer-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}
		
		input[type="number"] {
			-moz-appearance: textfield;
		}
		
		.input-container {
			display: flex;
			justify-content: center;
		}
		
		.input-secure {
			font-family: "text-security-disc";
			background-image: radial-gradient(circle at center,
			#ddd 4px,
			transparent 5px) !important;
			background-repeat: no-repeat;
			background-size: 100%;
			background-position: center;
		}
		
		.input {
			text-align: center;
			width: 15%;
			display: inline-block;
			padding: 5px;
			color: transparent;
			text-shadow: 0 0 0 #333;
			line-height: 1px;
			
			padding-bottom: 10px;
			
			border: none;
			border-bottom: 1px solid #b7b7b7;
			outline: 0;
			font-size: 30px;
			background-image: none;
		}
		
		.input:not(:valid) {
			background-image: radial-gradient(circle at center,
			#ddd 4px,
			transparent 5px) !important;
			background-repeat: no-repeat;
			background-size: 100%;
			background-position: center;
		}
		
		.input.input-secure:focus {
			background-image: radial-gradient(circle at center,
			#8b2787 4px,
			transparent 5px) !important;
			text-shadow: 0 0 0 transparent;
			outline: none;
		}
		
		.input:focus {
			text-shadow: 0 0 10px #8b2787;
			background-image: radial-gradient(circle at center,
			rgba(200, 200, 200, 0.5) 1px,
			transparent 15px) !important;
		}
		
		.button {
			width: 100%;
			display: inline-block;
			cursor: pointer;
			padding: 20px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			color: rgba(255, 255, 255, 0.9);
			-o-text-overflow: clip;
			text-overflow: clip;
			background: rgba(255, 187, 0, 1);
			outline: 0;
			border: none;
			font-size: 15px;
		}
		
		.button:active {
			display: inline-block;
			cursor: pointer;
			padding: 20px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			color: rgba(255, 255, 255, 0.9);
			-o-text-overflow: clip;
			text-overflow: clip;
			background: rgba(234, 173, 4, 1);
			outline: 0;
			border: none;
		}
		
		.button-disabled {
			width: 100%;
			display: inline-block;
			cursor: pointer;
			padding: 20px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			border: 1px solid rgb(134, 134, 134);
			color: rgba(73, 73, 73, 0.9);
			-o-text-overflow: clip;
			text-overflow: clip;
			background: #d6d6d6;
			outline: 0;
			font-size: 15px;
		}
		
		.description {
			text-align: center;
			margin-bottom: 20px;
		}
		
		.control-label {
			width: 100%;
			text-align: center;
		}
		
		.btn-eye {
			border-radius: 0;
			border: none;
			border-bottom: 1px solid #b7b7b7;
			background-color: transparent;
			font-size: 20px;
			transition: all 0.1s;
			color: #aaa;
		}
		
		.btn-eye:hover {
			cursor: pointer;
			color: darkorange;
		}
		
		.btn-eye:focus {
			outline: none;
			color: #333;
		}
		
		.forgotPin {
			margin-top: 70px;
			text-align: center;
			font-weight: 800;
		}
	</style>
	<style>
		@font-face {
			font-family: "SF Pro Display";
			src: url("<?php echo base_url(); ?>assets/local/fonts/SFProDisplay-Regular.ttf");
		}
		
		@font-face {
			font-family: "SF Pro Display Bold";
			src: url("<?php echo base_url(); ?>assets/local/fonts/SFProDisplay-Bold.ttf");
		}
		
		body {
			font-family: "SF Pro Display", serif;
		}
		
		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		button,
		input {
			font-family: "SF Pro Display Bold", serif;
		}
	
	</style>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/plugins.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/actions.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/messagedialog.js"></script>
	<script type="text/javascript">
		function getPinElement(index) {
			return document.getElementById("pin" + index);
		}
	
		$(document).on("keypress", "#pin1, #pin2, #pin3, #pin4, #pin5, #pin6", function(event) {
			// 0 for null values
			// 8 for backspace
			// 48-57 for 0-9 numbers
			let eventCode = event.which || event.keyCode
			if (eventCode != 8 && eventCode != 0 && eventCode < 48 || eventCode > 57) {
				event.preventDefault();
			}
		});
	
		function onInputEvent(index, event) {
			getPinElement(index).value = event.data;
			if (
				$("#pin1").val() == "" ||
				$("#pin2").val() == "" ||
				$("#pin3").val() == "" ||
				$("#pin4").val() == "" ||
				$("#pin5").val() == "" ||
				$("#pin6").val() == ""
			) {
				$("#btnContinue").removeClass("button");
				$("#btnContinue").addClass("button-disabled");
				$("#btnContinue").attr("disabled", true);
			} else {
				$("#btnContinue").removeClass("button-disabled");
				$("#btnContinue").addClass("button");
				$("#btnContinue").attr("disabled", false);
			}
			if (getPinElement(index).value.length === 1) {
				if (index !== 6) {
					getPinElement(index + 1).focus();
				}
				else {
					getPinElement(index).blur();
					submitPIN();
					// $('#submitPin').submit();
					//alert("6th pin trigerred, action down below this code")
					// $('#bayadpocloadingmodal').modal('show');
					// check(window.data, 2);
				}
			}
		}
	
		function onKeyUpEvent(index, event) {
			const eventCode = event.which || event.keyCode;
	
			if (eventCode === 37 && index !== 1) {
				getPinElement(index - 1).focus();
			}
			if (eventCode === 39 && index !== 6) {
				getPinElement(index + 1).focus();
			}
			if (eventCode === 8 && index !== 1) {
				getPinElement(index - 1).focus();
				getPinElement(index).value = "";
			} else if (eventCode === 8 && index == 1) {
				getPinElement(index).value = "";
			}
		}
	
		var isHidden = true;
	
		function showPassword() {
			if (isHidden) {
				$(".input").removeClass("input-secure");
				$("#eye").removeClass("fa-eye-slash");
				$("#eye").addClass("fa-eye");
				isHidden = false;
			} else {
				$(".input").addClass("input-secure");
				$("#eye").removeClass("fa-eye");
				$("#eye").addClass("fa-eye-slash");
				isHidden = true;
			}
		}
	
		$(document).ready(function() {
			/**
			$('#bayadpocloadingmodal').modal('show');
			var res = $("#data").text();
			res = decodeURIComponent(res.replace("data=", ""));
			console.log(decodeURIComponent(res.replace("data=", "")));
			if (res != "" || res != null) {
				window.data = res;
				// check(window.data, 1);
			} else {
				$('#bayadpocloadingmodal').modal('hide');
				messageDialogModal("Server Message", "Something went wrong, please try again later");
				$("#btnmessagedialogmodal").click(function() {
					window.location.reload();
				});
			}
			**/
			$('#forgotPin').click(function () {
				$('#forgotPinForm').submit();
			})
		});
		function submitPIN() {
		    let pin = $("#pin1").val() + $("#pin2").val() + $("#pin3").val() + $("#pin4").val() + $("#pin5").val() + $("#pin6").val();
		    console.info(pin);
		    if(pin) {
			    $('#pin').val(pin);
			    $('#inputPinForm').submit();
		    }
		}
		function checkout(val) {
			$.ajax({
				url: '<?php echo base_url(); ?>inputpin/form/inputpin/purchase',
				type: 'POST',
				data: {
					"ACCOUNTNUMBER": val.Message.fromaccount,
					"PAYLOAD": JSON.stringify(val),
					"PIN": $("#pin1").val() + $("#pin2").val() + $("#pin3").val() + $("#pin4").val() + $("#pin5").val()
				},
				dataType: 'json',
				success: function(result) {
					$('#bayadpocloadingmodal').modal('hide');
					if (result.Code == "00") {
						console.log(result);
						messageDialogModal("Server Message",result.Message);
					} else {
						console.log(result);
						messageDialogModal("Server Message", result.Message);
					}
				},
				error: function(e) {
					$('#bayadpocloadingmodal').modal('hide');
					console.log(e);
				}
			});
		}
	
		function check(val, type) {
			$.ajax({
				url: '<?php echo base_url(); ?>setuppin/form/setuppin/check',
				type: 'POST',
				data: {
					"VALUE": val
				},
				dataType: 'json',
				success: function(result) {
					console.log(result);
					if (result.Code == "00") {
						validateMsisdn(result, type);
					} else {
						$('#bayadpocloadingmodal').modal('hide');
						messageDialogModal("Server Message", result.Message);
						$("#btnmessagedialogmodal").click(function() {
							window.location.reload();
						});
	
					}
				},
				error: function(e) {
					$('#bayadpocloadingmodal').modal('hide');
					console.log(e);
				}
			});
		}
	
		function validateMsisdn(val, type) {
			$.ajax({
				url: '<?php echo base_url(); ?>setuppin/form/setuppin/validate',
				type: 'POST',
				data: {
					"MSISDN": val.Message.msisdn
				},
				dataType: 'json',
				success: function(result) {
					$('#bayadpocloadingmodal').modal('hide');
					if (result.Code == "00") {
						window.ret = true;
						switch (type) {
							case 1:
								console.log(val);
								$("#transaction").text(val.Message.brand);
								$("#dateTime").text(moment().format("DD-MM-YYYY HH:ss"));
								$("#amount").text(val.Message.amount);
								break;
							case 2:
								console.log(result);
								checkout(val);
								break;
						}
					} else {
						messageDialogModal("Server Message", result.Message);
						$("#btnmessagedialogmodal").click(function() {
							window.location.reload();
						});
	
					}
				},
				error: function(e) {
					console.log(e);
				}
			});
		}
		
		//$("#forgotpin").click(function(){
		//	document.forgotPinForm.submit();
		//});
	</script>
</body>
</html>


