<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Script Tutorials" />
	<title>KasPro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/images/indosat/icon_tab.png" />
	
	<!-- attach CSS styles -->
	<link href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/css/1.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/font-awesome-4.6.3/css/font-awesome.min.css" />
	<!--<link href="<?php echo base_url(); ?>assets/thirdparty/bootstrap/css/style.css" rel="stylesheet" />-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/country-select-js-master/build/css/countrySelect.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/thirdparty/text-security/text-security.css" />
	<style type="text/css">
		/*
		 * contextMenu.js v 1.4.0
		 * Author: Sudhanshu Yadav
		 * s-yadav.github.com
		 * Copyright (c) 2013 Sudhanshu Yadav.
		 * Dual licensed under the MIT and GPL licenses
		**/
		
		.iw-contextMenu {
			box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.10) !important;
			border: 1px solid #c8c7cc !important;
			border-radius: 11px !important;
			display: none;
			z-index: 1000000132;
			max-width: 300px !important;
			width: auto !important;
		}
		
		.dark-mode .iw-contextMenu,
		.TnITTtw-dark-mode.iw-contextMenu,
		.TnITTtw-dark-mode .iw-contextMenu {
			border-color: #747473 !important;
		}
		
		.iw-cm-menu {
			background: #fff !important;
			color: #000 !important;
			margin: 0px !important;
			padding: 0px !important;
			overflow: visible !important;
		}
		
		.dark-mode .iw-cm-menu,
		.TnITTtw-dark-mode.iw-cm-menu,
		.TnITTtw-dark-mode .iw-cm-menu {
			background: #525251 !important;
			color: #FFF !important;
		}
		
		.iw-curMenu {
		}
		
		.iw-cm-menu li {
			font-family: -apple-system, BlinkMacSystemFont, "Helvetica Neue", Helvetica, Arial, Ubuntu, sans-serif !important;
			list-style: none !important;
			padding: 10px !important;
			padding-right: 20px !important;
			border-bottom: 1px solid #c8c7cc !important;
			font-weight: 400 !important;
			cursor: pointer !important;
			position: relative !important;
			font-size: 14px !important;
			margin: 0 !important;
			line-height: inherit !important;
			border-radius: 0 !important;
			display: block !important;
		}
		
		.dark-mode .iw-cm-menu li, .TnITTtw-dark-mode .iw-cm-menu li {
			border-bottom-color: #747473 !important;
		}
		
		.iw-cm-menu li:first-child {
			border-top-left-radius: 11px !important;
			border-top-right-radius: 11px !important;
		}
		
		.iw-cm-menu li:last-child {
			border-bottom-left-radius: 11px !important;
			border-bottom-right-radius: 11px !important;
			border-bottom: none !important;
		}
		
		.iw-mOverlay {
			position: absolute !important;
			width: 100% !important;
			height: 100% !important;
			top: 0px !important;
			left: 0px !important;
			background: #FFF !important;
			opacity: .5 !important;
		}
		
		.iw-contextMenu li.iw-mDisable {
			opacity: 0.3 !important;
			cursor: default !important;
		}
		
		.iw-mSelected {
			background-color: #F6F6F6 !important;
		}
		
		.dark-mode .iw-mSelected, .TnITTtw-dark-mode .iw-mSelected {
			background-color: #676766 !important;
		}
		
		.iw-cm-arrow-right {
			width: 0 !important;
			height: 0 !important;
			border-top: 5px solid transparent !important;
			border-bottom: 5px solid transparent !important;
			border-left: 5px solid #000 !important;
			position: absolute !important;
			right: 5px !important;
			top: 50% !important;
			margin-top: -5px !important;
		}
		
		.dark-mode .iw-cm-arrow-right, .TnITTtw-dark-mode .iw-cm-arrow-right {
			border-left-color: #FFF !important;
		}
		
		.iw-mSelected > .iw-cm-arrow-right {
		}
		
		/*context menu css end */
	</style>
	<style type="text/css">
		@-webkit-keyframes load4 {
			0%,
			100% {
				box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
			}
			12.5% {
				box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
			}
			25% {
				box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
			}
			37.5% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
			}
			50% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
			}
			62.5% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
			}
			75% {
				box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
			}
			87.5% {
				box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
			}
		}
		
		@keyframes load4 {
			0%,
			100% {
				box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
			}
			12.5% {
				box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
			}
			25% {
				box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
			}
			37.5% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
			}
			50% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
			}
			62.5% {
				box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
			}
			75% {
				box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
			}
			87.5% {
				box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
			}
		}
	</style>
</head>

<body class="" cz-shortcut-listen="true">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/plugins/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/jquery-ui.js"></script>
<script defer type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/messagedialog.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/inputvalidation.js"></script>
<div id="wrapper">
	<div id="content">
		<div class="container" style="padding: 50px">
			<span id="data" hidden><?php echo 'data='.$ucb; ?></span>
			<p class="description">
				Silahkan masukkan 6 digit PIN kamu
			</p>
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<div class="input-container">
							<form id="xyz" method="post" action="<?php echo "$svr/$res"; ?>">
								<input type="hidden" name="request-id" value="<?php echo $transid; ?>" />
								<input type="number" id="pin1" name="pin[1]" class="input input-secure" oninput="onInputEvent(1, event)" onkeyup="onKeyUpEvent(1, event)" maxlength="1" required autofocus />
								<input type="number" id="pin2" name="pin[2]" class="input input-secure" oninput="onInputEvent(2, event)" onkeyup="onKeyUpEvent(2, event)" maxlength="1" required />
								<input type="number" id="pin3" name="pin[3]" class="input input-secure" oninput="onInputEvent(3, event)" onkeyup="onKeyUpEvent(3, event)" maxlength="1" required />
								<input type="number" id="pin4" name="pin[4]" class="input input-secure" oninput="onInputEvent(4, event)" onkeyup="onKeyUpEvent(4, event)" maxlength="1" required />
								<input type="number" id="pin5" name="pin[5]" class="input input-secure" oninput="onInputEvent(5, event)" onkeyup="onKeyUpEvent(5, event)" maxlength="1" required />
								<input type="number" id="pin6" name="pin[6]" class="input input-secure" oninput="onInputEvent(6, event)" onkeyup="onKeyUpEvent(6, event)" maxlength="1" required />
								<button class="btn-eye" onclick="showPassword()"><i id="eye" class="fa fa-eye-slash" aria-hidden="true"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<form id="forgotPinForm" name="forgotPinForm" method="post" action="http://dev.kaspro.id/kaspro/inputphonenumber/" hidden="">
				<input type="hidden" name="data" value="<?php echo $ucb; ?>" />
				<a id="a_ucb" href="#">Lupa PIN?</a>
			</form>
			<p class="forgotPin">
				<a href="#" id="forgotpin">Lupa PIN?</a>
			</p>
		</div>
	</div>
	<div class="modal fade bs-example-modal-sm" id="messagedialogmodal" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content ">
				<div class="modal-header">
					<b>Server Message</b>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<span id="messagealertmodal"></span>
				</div>
				<div class="modal-footer">
					<input type="button" id="btnmessagedialogmodal" class="btn btn-danger" style="width:110px;"
					       data-dismiss="modal" onclick="$('#messagedialogmodal').modal('hide');" value="Ok">
				</div>
			</div>
		</div>
	</div>
	
	<!--  <div class="col-md-12" align="center" style="padding-top: 30px">
		  <br> -->
	<span class="hidden-xs hidden-sm"><br><br><br><br><br></span>
	<!-- <h6 style="color: white;margin-bottom: 0">&copy;Copyright 2017 PayU | powered by iBayad Online Ventures | All Rights Reserved.</h6> -->
	<br>
	<div id="footer" style="text-align: center; border-top: 5px solid #a0a0a0;">
		<h6 style="color: #ffffff;margin-bottom: 0">&copy;<?php echo("Copyright") ?> 2017 PayU
		                                            | <?php echo("powered by") ?> PT Solusi Pasti Indonesia
		                                            | <?php echo("All Rights Reserved.") ?></h6>
		<br>
	</div>
	<!--  </div> -->
</div>
<style>
	span {
		font-size: 15px;
	}
	
	span.error {
		color: red;
	}
	
	span.icon-white {
		background: url(<?php echo base_url(); ?>assets/local/images/right-arrow.svg) no-repeat top left;
		background-size: contain;
		cursor: pointer;
		display: inline-block;
		height: 20px;
		width: 20px;
	}
	
	span.icon-dark {
		background: url(<?php echo base_url(); ?>assets/local/images/right-arrow-dark.svg) no-repeat top left;
		background-size: contain;
		cursor: pointer;
		display: inline-block;
		height: 20px;
		width: 20px;
	}
	
	span {
		font-size: 15px;
	}
	
	span.error {
		color: red;
	}
	
	input[type="number"]::-webkit-inner-spin-button,
	input[type="number"]::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	
	input[type="number"] {
		-moz-appearance: textfield;
	}
	
	.input-container {
		display: flex;
		justify-content: center;
	}
	
	.input-secure {
		font-family: "text-security-disc";
		background-image: radial-gradient(circle at center,
		#ddd 4px,
		transparent 5px) !important;
		background-repeat: no-repeat;
		background-size: 100%;
		background-position: center;
	}
	
	.input {
		text-align: center;
		width: 15%;
		display: inline-block;
		padding: 5px;
		color: transparent;
		text-shadow: 0 0 0 #333;
		line-height: 1px;
		
		padding-bottom: 10px;
		
		border: none;
		border-bottom: 1px solid #b7b7b7;
		outline: 0;
		font-size: 30px;
		background-image: none;
	}
	
	.input:not(:valid) {
		background-image: radial-gradient(circle at center,
		#ddd 4px,
		transparent 5px) !important;
		background-repeat: no-repeat;
		background-size: 100%;
		background-position: center;
	}
	
	.input.input-secure:focus {
		background-image: radial-gradient(circle at center,
		#8b2787 4px,
		transparent 5px) !important;
		text-shadow: 0 0 0 transparent;
		outline: none;
	}
	
	.input:focus {
		text-shadow: 0 0 10px #8b2787;
		background-image: radial-gradient(circle at center,
		rgba(200, 200, 200, 0.5) 1px,
		transparent 15px) !important;
	}
	
	.button {
		width: 100%;
		display: inline-block;
		cursor: pointer;
		padding: 20px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		color: rgba(255, 255, 255, 0.9);
		-o-text-overflow: clip;
		text-overflow: clip;
		background: rgba(255, 187, 0, 1);
		outline: 0;
		border: none;
		font-size: 15px;
	}
	
	.button:active {
		display: inline-block;
		cursor: pointer;
		padding: 20px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		color: rgba(255, 255, 255, 0.9);
		-o-text-overflow: clip;
		text-overflow: clip;
		background: rgba(234, 173, 4, 1);
		outline: 0;
		border: none;
	}
	
	.button-disabled {
		width: 100%;
		display: inline-block;
		cursor: pointer;
		padding: 20px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		border: 1px solid rgb(134, 134, 134);
		color: rgba(73, 73, 73, 0.9);
		-o-text-overflow: clip;
		text-overflow: clip;
		background: #d6d6d6;
		outline: 0;
		font-size: 15px;
	}
	
	.description {
		text-align: center;
		margin-bottom: 20px;
	}
	
	.control-label {
		width: 100%;
		text-align: center;
	}
	
	.btn-eye {
		border-radius: 0;
		border: none;
		border-bottom: 1px solid #b7b7b7;
		background-color: transparent;
		font-size: 20px;
		transition: all 0.1s;
		color: #aaa;
	}
	
	.btn-eye:hover {
		cursor: pointer;
		color: darkorange;
	}
	
	.btn-eye:focus {
		outline: none;
		color: #333;
	}
	
	.forgotPin {
		margin-top: 70px;
		text-align: center;
		font-weight: 800;
	}
</style>
<style>
	@font-face {
		font-family: "SF Pro Display";
		src: url("<?php echo base_url(); ?>assets/local/fonts/SFProDisplay-Regular.ttf");
	}
	
	@font-face {
		font-family: "SF Pro Display Bold";
		src: url("<?php echo base_url(); ?>assets/local/fonts/SFProDisplay-Bold.ttf");
	}
	
	body {
		font-family: "SF Pro Display", serif;
	}
	
	h1,
	h2,
	h3,
	h4,
	h5,
	h6,
	button,
	input {
		font-family: "SF Pro Display Bold", serif;
	}

</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/thirdparty/js/admin/actions.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/local/js/messagedialog.js"></script>
<script type="text/javascript">
	function getPinElement(index) {
		return document.getElementById("pin" + index);
	}

	$(document).on("keypress", "#pin1, #pin2, #pin3, #pin4, #pin5, #pin6", function(event) {
		// 0 for null values
		// 8 for backspace
		// 48-57 for 0-9 numbers
		let eventCode = event.which || event.keyCode
		if (eventCode != 8 && eventCode != 0 && eventCode < 48 || eventCode > 57) {
			event.preventDefault();
		}
	});

	function onInputEvent(index, event) {
		getPinElement(index).value = event.data;
		if (
			$("#pin1").val() == "" ||
			$("#pin2").val() == "" ||
			$("#pin3").val() == "" ||
			$("#pin4").val() == "" ||
			$("#pin5").val() == "" ||
			$("#pin6").val() == ""
		) {
			$("#btnContinue").removeClass("button");
			$("#btnContinue").addClass("button-disabled");
			$("#btnContinue").attr("disabled", true);
		} else {
			$("#btnContinue").removeClass("button-disabled");
			$("#btnContinue").addClass("button");
			$("#btnContinue").attr("disabled", false);
		}
		if (getPinElement(index).value.length === 1) {
			if (index !== 6) {
				getPinElement(index + 1).focus();
			}
			else {
				getPinElement(index).blur();
				$('#xyz').submit();
				//alert("6th pin trigerred, action down below this code")
				// $('#bayadpocloadingmodal').modal('show');
				// check(window.data, 2);
			}
		}
	}

	function onKeyUpEvent(index, event) {
		const eventCode = event.which || event.keyCode;

		if (eventCode === 37 && index !== 1) {
			getPinElement(index - 1).focus();
		}
		if (eventCode === 39 && index !== 6) {
			getPinElement(index + 1).focus();
		}
		if (eventCode === 8 && index !== 1) {
			getPinElement(index - 1).focus();
			getPinElement(index).value = "";
		} else if (eventCode === 8 && index == 1) {
			getPinElement(index).value = "";
		}
	}

	var isHidden = true;

	function showPassword() {
		if (isHidden) {
			$(".input").removeClass("input-secure");
			$("#eye").removeClass("fa-eye-slash");
			$("#eye").addClass("fa-eye");
			isHidden = false;
		} else {
			$(".input").addClass("input-secure");
			$("#eye").removeClass("fa-eye");
			$("#eye").addClass("fa-eye-slash");
			isHidden = true;
		}
	}

	$(document).ready(function() {
		$('#bayadpocloadingmodal').modal('show');
		var res = $("#data").text();
		res = decodeURIComponent(res.replace("data=", ""));
		console.log(decodeURIComponent(res.replace("data=", "")));
		if (res != "" || res != null) {
			window.data = res;
			// check(window.data, 1);
		} else {
			$('#bayadpocloadingmodal').modal('hide');
			messageDialogModal("Server Message", "Something went wrong, please try again later");
			$("#btnmessagedialogmodal").click(function() {
				window.location.reload();
			});
		}

		$('#a_ucb').click(function () {
			$('#f_ucb').submit();
		})
	});

	function checkout(val) {
		$.ajax({
			url: '<?php echo base_url(); ?>inputpin/form/inputpin/purchase',
			type: 'POST',
			data: {
				"ACCOUNTNUMBER": val.Message.fromaccount,
				"PAYLOAD": JSON.stringify(val),
				"PIN": $("#pin1").val() + $("#pin2").val() + $("#pin3").val() + $("#pin4").val() + $("#pin5").val()
			},
			dataType: 'json',
			success: function(result) {
				$('#bayadpocloadingmodal').modal('hide');
				if (result.Code == "00") {
					console.log(result);
					messageDialogModal("Server Message",result.Message);
				} else {
					console.log(result);
					messageDialogModal("Server Message", result.Message);
				}
			},
			error: function(e) {
				$('#bayadpocloadingmodal').modal('hide');
				console.log(e);
			}
		});
	}

	function check(val, type) {
		$.ajax({
			url: '<?php echo base_url(); ?>setuppin/form/setuppin/check',
			type: 'POST',
			data: {
				"VALUE": val
			},
			dataType: 'json',
			success: function(result) {
				console.log(result);
				if (result.Code == "00") {
					validateMsisdn(result, type);
				} else {
					$('#bayadpocloadingmodal').modal('hide');
					messageDialogModal("Server Message", result.Message);
					$("#btnmessagedialogmodal").click(function() {
						window.location.reload();
					});

				}
			},
			error: function(e) {
				$('#bayadpocloadingmodal').modal('hide');
				console.log(e);
			}
		});
	}

	function validateMsisdn(val, type) {
		$.ajax({
			url: '<?php echo base_url(); ?>setuppin/form/setuppin/validate',
			type: 'POST',
			data: {
				"MSISDN": val.Message.msisdn
			},
			dataType: 'json',
			success: function(result) {
				$('#bayadpocloadingmodal').modal('hide');
				if (result.Code == "00") {
					window.ret = true;
					switch (type) {
						case 1:
							console.log(val);
							$("#transaction").text(val.Message.brand);
							$("#dateTime").text(moment().format("DD-MM-YYYY HH:ss"));
							$("#amount").text(val.Message.amount);
							break;
						case 2:
							console.log(result);
							checkout(val);
							break;
					}
				} else {
					messageDialogModal("Server Message", result.Message);
					$("#btnmessagedialogmodal").click(function() {
						window.location.reload();
					});

				}
			},
			error: function(e) {
				console.log(e);
			}
		});
	}
</script>
</body>
</html>


