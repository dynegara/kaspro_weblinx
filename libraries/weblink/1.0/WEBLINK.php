<?php defined('BASEPATH') OR exit('No direct script access allowed');

class WEBLINK {
	protected   $key = 'KbPeShVmYq3t6v9y$B&E)H@McQfTjWnZ';
	
	public      function __construct() {
		$this->CI =& get_instance();
	}
	
	public      function sync(&$sys, $FRM) {
		$this->sys =& $sys;
		$this->FRM =& $FRM;
		
		$this->sys['req']['dbg'] = 1;
		$this->sys['app']['id']  = 7410;
	}
	
	public      function payment__post() {
		$this->sys['req']['out'] = 'echo';
		$pst = $this->sys['req']['pst'];
		$CFG = $this->sys['cfg']['ws_svr'];
		
		if(!isset($pst['request-id'])) {
			// 1.a-1 App submit data
			include APPPATH . 'libraries/Cipher.php';
			$key           = $CFG['weblink']['key'];
			$cipher        = new Cipher($key, "AES-256-CBC");
			$pst           = $pst['data'] ?? $pst;
			$decrypt       = $cipher->decrypt($pst);
			$dat           = json_decode($decrypt, true);
			
			if(is_array($dat)) {
				// 1.a-2 simpan data payload
				$this->trx_log('client_put', $dat);
				
				// 1.a-3 flag error jika data mandatory tidak lengkap
				$err = !$this->trx_man('payment', $dat);
			}
			else {
				// 1.a-4 flag error -> data payload kosong
				$err = true;
				$this->trx_err('API__147', $pst);
			}
			
			if($err) {
				// 1.a-5 throw error (data empty atau data mandatory tidak lengkap)
				$this->sys['req']['out'] = 'json';
			}
			else {
				// 1.a-6 request input pin -> response with weblink input pin
				$dat['svr'] = $CFG['weblink']['api'];
				$dat['res'] = $this->sys['req']['res'];
				$dat['ucb'] = json_encode(array('redirect_url' => $dat['redirect_url']));
				$dat['ucb'] = $cipher->encrypt($dat['ucb']);
				$this->trx_pin($dat);
			}
		}
		else {
			// 1.b-1 App submit pin
			$tmp = $this->trx_log('client_get', $pst['request-id']);;
			$tmp = json_decode($tmp['client_pst'], true);
			$pst = array_merge($pst, $tmp);
			$ack = 'NOT_OK';
			
			$mapping = array(
				'POSTPAID'     => 'payment__postpaid__telco',
				'POSTPAID_PLN' => 'payment__postpaid__pln',
				'PREPAID_PLN'  => 'payment__prepaid__pln',
				'BPJS'         => 'payment__postpaid__bpjs',
				'PDAM'         => 'payment__postpaid__pdam'
			);
			
			$type = strtoupper($pst['type']);
			$cmd  = $mapping[$type];
			$j_p  = $this->$cmd('kaspro', $pst);
			
			if($type == 'PREPAID_PLN')
				$url  = "{$CFG['kaspro']['api']}/{$pst['fromaccount']}/purchases";
			else
				$url  = "{$CFG['kaspro']['api']}/{$pst['fromaccount']}/payments";
			
			// 2.a post to kaspro api
			$opt = $CFG['kaspro']['opt'];
			
			$xtd['api_pst_on'] = date('Y-m-d H:i:s', time());
			$rsp = curl_post($url, $opt, $j_p);
			
			$xtd['api_pst_ack_on']  = date('Y-m-d H:i:s', time());
			
			// 2.b check kaspro api response
			$j_r = array();
			if(is_json($rsp)) {
				$j_r = json_decode($rsp, true);
				if (isset($j_r['code']) && !$j_r['code'] || $j_r['code'] == 0) {
					// 2.b-1 response is OK
					$ack = 'OK';
				}
			}
			else {
				// 2.b-2 response is NOT-OK
				$j_r['rsp'] = $rsp;
			}
			
			// 2.b-3 simpan response
			$j_r['status'] = $ack;
			$this->trx_log('api_pst', $j_p, $j_r, $xtd);;
			
			// 3. callback to app / client
			$j_p  = $this->$cmd('callback', $j_r, $pst);
			
			$url = $CFG['client']['svr'];
			$opt = array(
				CURLOPT_HTTPHEADER    => array(
					'Authorization: Basic '. base64_encode("{$CFG['client']['usr']}:{$CFG['client']['pwd']}")
				)
			);
			$xtd['client_callback_on'] = date('Y-m-d H:i:s', time());
			
			// 3.a post callback to app / client
			$rsp = curl_post($url, $opt, $j_p);
			$xtd['client_callback_ack_on'] = date('Y-m-d H:i:s', time());
			
			if(is_json($rsp))
				$j_r = json_decode($rsp, true);
			else {
				$j_r        = array();
				$j_r['rsp'] = $rsp;
			}
			
			// 3.b save response-callback from app / client
			$this->trx_log('client_callback', $j_p, $j_r, $xtd);
			
			// 4 redirect
			if(!preg_match('/http/', $pst['redirect_url']))
				$pst['redirect_url'] = 'http://'. $pst['redirect_url'];
			
			header('HTTP/1.1 307 Temporary Redirect');
			header("Location: {$pst['redirect_url']}?status=$ack");
		}
	}
	private     function payment__postpaid__telco($map, $pst, $rsp=null) {
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		if(!is_array($rsp) && is_json($rsp))
			$rsp = json_decode($rsp, true);
		
		if($rsp)
			$pst = arr__merge_recursive($pst, $rsp);
		
		switch($map) {
			case 'kaspro':
				$pin = $pst['pin'];
				$dat = array(
					'auth' => array('password' => $pin),
					'payment-data' => array(
						'biller'            => $pst['sku'],
						'paid-amount'       => $pst['amount'],
						'bill-amount'       => $pst['amount'],
						'bill-month'        => '',
						'customer-name'     => '',
						'first-name'        => '',
						'middle-name'       => '',
						'last-name'         => '',
						'service-reference' => $pst['msisdn'],
						'branch-id'         => $pst['branchid'],
						'terminal-id'       => $pst['terminalid'],
						'number-of-fees'    => $pst['number-of-fees'],
						'partner-reference' => $pst['partner-reference'],
						'cashier-id'        => $pst['cashierid'],
						'service-fee'       => $pst['servicefee']
					),
					'request-id' => $pst['transid']
				);
				break;
				
			case 'callback':
				$amt = $pst['payment-data']['amount'] ?? null;
				if($amt)
					$amt = str_replace(',', '', $amt);
				$fee = $pst['payment-data']['service-fee'] ?? null;
				if($fee)
					$fee = str_replace(',', '', $fee);
				$tot = $amt && $fee ? $amt + $fee : null;
				$dat = array (
					'status'          => $pst['status'],
					'transId'         => $pst['request-id'],
					'type'            => 'POSTPAID',
					'responseId'      => $pst['response-id'],
					'service'         => 'postpaid',
					'updateBalance'   => $pst['wallet-transactions'][0]['balance-after'] ?? null,
					'referenceId'     => $pst['payment-data']['response-data']['reference1'] ?? null,
					'paymentMethod'   => 'Home Credit Pay',
					'provider'        => $pst['payment-data']['response-data']['provider-name'] ?? null,
					'transactionTime' => $pst['date'],
					'phoneNumber'     => $pst['payment-data']['account-number'] ?? null,
					'amount'          => $amt,
					'serviceFee'      => $fee,
					'totalAmount'     => $tot,
					'userId'          => $pst['user-id']
				);
				break;
		}
		return json_encode($dat, JSON_PRETTY_PRINT);
	}
	private     function payment__postpaid__pln($map, $pst, $rsp=null) {
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		if(!is_array($rsp) && is_json($rsp))
			$rsp = json_decode($rsp, true);
		
		if($rsp)
			$pst = arr__merge_recursive($pst, $rsp);
		
		switch($map) {
			case 'kaspro':
				$pin = $pst['pin'];
				$dat = array(
					'auth' => array('password' => $pin),
					'payment-data' => array(
						'biller'            => $pst['sku'],
						'paid-amount'       => $pst['amount'],
						'bill-amount'       => $pst['amount'],
						'bill-month'        => '',
						'service-reference' => $pst['msisdn'],
						'branch-id'         => $pst['branchid'],
						'terminal-id'       => $pst['terminalid'],
						'number-of-fees'    => $pst['number-of-fees'],
						'customer-name'     => '',
						'first-name'        => '',
						'middle-name'       => '',
						'last-name'         => '',
						'partner-reference' => $pst['partner-reference'],
						'cashier-id'        => $pst['cashierid'],
						'service-fee'       => $pst['servicefee'],
						'inquire-token'     => $pst['inquire-token']
					),
					'request-id' => $pst['transid']
				);
				break;
			
			case 'callback':
				$dat = array (
					'status'          => $pst['status'],
					'transId'         => $pst['request-id'],
					'type'            => 'POSTPAID_PLN',
					'responseId'      => $pst['response-id'],
					'paymentMethod'   => 'Home Credit Pay',
					'transactionTime' => $pst['date'],
					'customerId'      => $pst['payment-data']['account-number'],
					'customerName'    => $pst['payment-data']['response-data']['name'] ?? null,
					'amount'          => $pst['payment-data']['amount'] ?? null,
					'serviceFee'      => $pst['payment-data']['service-fee'] ?? null,
					'totalAmount'     => $pst['payment-data']['total-amount'] ?? null,
					'userId'          => $pst['user-id']
				);
				break;
		}
		return json_encode($dat, JSON_PRETTY_PRINT);
	}
	private     function payment__prepaid__pln($map, $pst, $rsp=null) {
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		if(!is_array($rsp) && is_json($rsp))
			$rsp = json_decode($rsp, true);
		
		if($rsp)
			$pst = arr__merge_recursive($pst, $rsp);
		
		switch($map) {
			case 'kaspro':
				$pin = $pst['pin'];
				$dat = array(
					'auth' => array('password' => $pin),
					'payment-data' => array(
						'sku'               => $pst['sku'],
						'amount'            => $pst['amount'],
						'from-account'      => $pst['fromaccount'],
						'language'          => $pst['language'],
						'msisdn'            => $pst['msisdn'],
						'branch-id'         => $pst['branchid'],
						'brand'             => $pst['brand'],
						'terminal-id'       => $pst['terminalid'],
						'customer-name'     => '',
						'partner-reference' => $pst['partner-reference'],
						'cashier-id'        => $pst['cashierid'],
						'service-fee'       => $pst['servicefee'],
						'inquire-token'     => $pst['inquire-token']
					),
					'request-id' => $pst['transid']
				);
				break;
			
			case 'callback':
				$tok = $pst['payment-data']['response-data']['token-number'] ?? null;
				$ftc = $tok ? format_token($tok) : null;
				$dat = array (
					'status'          => $pst['status'],
					'transId'         => $pst['request-id'],
					'type'            => 'PREPAID_PLN',
					'responseId'      => $pst['response-id'],
					'paymentMethod'   => 'Home Credit Pay',
					'transactionTime' => $pst['date'],
					'plnNumber'       => $pst['payment-data']['msisdn'] ?? null,
					'customerId'      => $pst['payment-data']['msisdn'] ?? null,
					'customerName'    => $pst['payment-data']['response-data']['name'] ?? null,
					'nominalPower'    => $pst['payment-data']['response-data']['segmentation'] ?? null,
					'amount'          => $pst['payment-data']['amount'] ?? null,
					'serviceFee'      => $pst['payment-data']['service-fee'] ?? null,
					'totalAmount'     => $pst['payment-data']['total-amount'] ?? null,
					'tokenCode'       => $ftc,
					'userId'          => $pst['user-id']
				);
				break;
		}
		return json_encode($dat, JSON_PRETTY_PRINT);
	}
	private     function payment__postpaid__bpjs($map, $pst, $rsp=null) {
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		if(!is_array($rsp) && is_json($rsp))
			$rsp = json_decode($rsp, true);
		
		if($rsp)
			$pst = arr__merge_recursive($pst, $rsp);
		
		switch($map) {
			case 'kaspro':
				$pin = $pst['pin'];
				$dat = array(
					'auth' => array('password' => $pin),
					'payment-data' => array(
						'biller'            => $pst['sku'],
						'paid-amount'       => $pst['amount'],
						'bill-amount'       => $pst['amount'],
						'bill-month'        => '',
						'service-reference' => $pst['msisdn'],
						'branch-id'         => $pst['branchid'],
						'terminal-id'       => $pst['terminalid'],
						'number-of-fees'    => $pst['number-of-fees'],
						'customer-name'     => '',
						'first-name'        => '',
						'middle-name'       => '',
						'last-name'         => '',
						'partner-reference' => $pst['partner-reference'],
						'cashier-id'        => $pst['cashierid'],
						'service-fee'       => $pst['servicefee'],
						'inquire-token'     => $pst['inquire-token']
					),
					'request-id' => $pst['transid']
				);
				break;
			
			case 'callback':
				$dat = array (
					'status'          => $pst['status'],
					'transId'         => $pst['request-id'],
					'type'            => 'BPJS',
					'responseId'      => $pst['response-id'],
					'paymentMethod'   => 'Home Credit Pay',
					'transactionTime' => $pst['date'],
					'customerId'      => $pst['payment-data']['response-data']['customer-id'] ?? null,
					'customerName'    => $pst['payment-data']['response-data']['participant-name'] ?? null,
					'totalPayMonths'  => $pst['payment-data']['response-data']['number-of-months'] ?? null,
					'amount'          => $pst['payment-data']['amount'] ?? null,
					'serviceFee'      => $pst['payment-data']['service-fee'] ?? null,
					'totalAmount'     => $pst['payment-data']['total-amount'] ?? null,
					'userId'          => $pst['user-id']
				);
				break;
		}
		return json_encode($dat, JSON_PRETTY_PRINT);
	}
	private     function payment__postpaid__pdam($map, $pst, $rsp=null) {
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		if(!is_array($rsp) && is_json($rsp))
			$rsp = json_decode($rsp, true);
		
		if($rsp)
			$pst = arr__merge_recursive($pst, $rsp);
		
		switch($map) {
			case 'kaspro':
				$pin = $pst['pin'];
				$dat = array(
					'auth' => array('password' => $pin),
					'payment-data' => array(
						'biller'            => $pst['sku'],
						'paid-amount'       => $pst['amount'],
						'bill-amount'       => $pst['amount'],
						'bill-month'        => '',
						'service-reference' => $pst['msisdn'],
						'branch-id'         => $pst['branchid'],
						'terminal-id'       => $pst['terminalid'],
						'number-of-fees'    => $pst['number-of-fees'],
						'customer-name'     => '',
						'first-name'        => '',
						'middle-name'       => '',
						'last-name'         => '',
						'partner-reference' => $pst['partner-reference'],
						'cashier-id'        => $pst['cashierid'],
						'service-fee'       => $pst['servicefee'],
						'inquire-token'     => $pst['inquire-token']
					),
					'request-id' => $pst['transid']
				);
				break;
			
			case 'callback':
				$dat = array (
					'status'          => $pst['status'],
					'transId'         => $pst['request-id'],
					'type'            => 'PDAM',
					'trxLocation'     => $pst['payment-data']['response-data']['pdam'] ?? null,
					'responseId'      => $pst['response-id'],
					'paymentMethod'   => 'Home Credit Pay',
					'transactionTime' => $pst['date'],
					'customerId'      => $pst['payment-data']['response-data']['customer-number'] ?? null,
					'amount'          => $pst['payment-data']['amount'] ?? null,
					'serviceFee'      => $pst['payment-data']['service-fee'] ?? null,
					'totalAmount'     => $pst['payment-data']['total-amount'] ?? null,
					'userId'          => $pst['user-id']
				);
				break;
		}
		return json_encode($dat, JSON_PRETTY_PRINT);
	}
	
	private     function trx_man($type, $dat) {
		$cmd = "trx_man__$type";
		$mis = $this->$cmd($dat);
		if($mis)
			$this->sys['rsp']['ack'] = array(
				'ack_code'    => 'API__146',
				'ack_message' => "Missing Mandatory Post Payloads ($mis)"
			);
		else {
			$type = strtoupper($dat['type']);
			$list = array('POSTPAID', 'POSTPAID_PLN', 'PREPAID_PLN', 'PDAM', 'BPJS');
			if(!in_array($type, $list)) {
				$mis = true;
				$this->sys['rsp']['ack'] = array(
					'ack_code'    => 'API__143',
					'ack_message' => "Invalid Request Payload Parameter ~ Payment Type ({$dat['type']})"
				);
			}
		}
		
		return $mis ? false : true;
	}
	private     function trx_man__payment($dat) {
		$man = array('fromaccount', 'amount', 'sku', 'msisdn', 'servicefee', 'transid', 'partner-reference', 'type', 'user-id', 'redirect_url');
		$xtd = array();
		$mis = array();
		foreach($man as $k=>$v)
			if(!isset($dat[$v]) || $dat[$v] == '') {
				$mis[] = $v;
				break;
			}
		if(!$mis) {
			switch(strtoupper($dat['type'])) {
				case 'POSTPAID_PLN':
				case 'PDAM':
				case 'BPJS':
					$xtd = array('number-of-fees', 'inquire-token');
					break;
			}
			foreach($xtd as $k=>$v)
				if(!isset($dat[$v]) || $dat[$v] == '') {
					$mis[] = $v;
					break;
				}
		}
		return $mis ? join(', ', $mis) : false;
	}
	private     function trx_pin($dat=array()) {
		$dir = $this->sys['req']['dir'];
		$this->CI->load->view("$dir/inputpin", $dat);
	}
	private     function trx_log($act, $pst=null, $arg=null, $xtd=null) {
		$dat = array();
		
		if(!is_array($pst) && is_json($pst))
			$pst = json_decode($pst, true);
		
		if(!is_array($arg) && is_json($arg))
			$arg = json_decode($arg, true);
		
		switch($act) {
			case 'client_put':
				$key  = $pst['transid'];
				$rst  = $this->trx_log('client_get', $key);
				if(empty($rst)) {
					$ua   = $this->CI->agent->profile();
					$env  = array(
						'url'           => $_SERVER['REQUEST_URI'],
						'ip'            => $this->CI->input->ip_address(),
						'computer_name' => gethostname(),
						'agent_name'    => $ua['name'],
						'agent_version' => $ua['version'],
						'agent_os'      => $ua['os'],
						'agent_vendor'  => $ua['vendor'],
						'agent_type'    => $ua['type']
					);
					$dat = array(
						'request_id'     => $key,
						'request_name'   => $pst['sku'],
						'client_pst'     => json_encode($pst, JSON_PRETTY_PRINT),
						'client_pst_env' => json_encode($env, JSON_PRETTY_PRINT),
						'client_pst_on'  => date('Y-m-d H:i:s', time())
					);
					if(!empty($dat['request_id']))
						$this->CI->db->insert('sys_api_log', $dat);
				}
				break;
				
			case 'client_get':
				$rsp = $this->CI->db->get_where('sys_api_log', array('request_id' => $pst));
				if($rsp)
					$dat = $rsp->row_array();
				break;
			
			case 'client_callback':
				$key = array('request_id' => $pst['transId']);
				$dat = array(
					'client_callback'        => json_encode($pst, JSON_PRETTY_PRINT),
					'client_callback_on'     => $xtd['client_callback_on'],
					'client_callback_ack'    => json_encode($arg, JSON_PRETTY_PRINT),
					'client_callback_ack_on' => $xtd['client_callback_ack_on']
				);
				$this->CI->db->update('sys_api_log', $dat, $key);
				break;
				
			case 'api_pst':
				$key = array('request_id' => $pst['request-id']);
				$dat = array(
					'api_pst'        => json_encode($pst, JSON_PRETTY_PRINT),
					'api_pst_on'     => $xtd['api_pst_on'],
					'api_pst_ack'    => json_encode($arg, JSON_PRETTY_PRINT),
					'api_pst_ack_on' => $xtd['api_pst_ack_on']
				);
				$this->CI->db->update('sys_api_log', $dat, $key);
				break;
		}
		return $dat;
	}
	private     function trx_err($err, $msg=null) {
		switch($err) {
			case 'API__147':
				$msg = 'Failed to dcrypt Payloads: ' . $msg;
				break;
		}
		
		$this->sys['rsp']['ack'] = array(
			'ack_code'    => $err,
			'ack_message' => $msg
		);
	}
}
/* End of file WEBLINK.php */
/* Location: ./application/libraries/weblink/1.0/WEBLINK.php */