<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
/* URL CONVENTION : implement RESTful
 * /schema/version/resource
 * /schema/version/resource/id_val
 * /schema/version/resource/id/id_val
 * /schema/version/resource/filter1/f1/filter2/f2
 * /schema/version/resource/filter1/f1/filter2/f2/filter3/f3
 */
class RFC extends API_Controller {
	public      function __construct() {
		parent::__construct();
    }
    
	public      function index() {
		$this->argc = func_get_args();
		$this->argn = count($this->argc);
		parent::index();
	}
}
/* End of file RFC.php */
/* Location: ./application/controllers/RFC.php */