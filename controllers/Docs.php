<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Docs extends CI_Controller {
	
    public      function __construct() {
        parent::__construct();
    }
	public      function index($cmd=null) {
    	if($cmd)
    		$this->$cmd();
    	else
            $this->load->view('welcome_message');
	}
}
