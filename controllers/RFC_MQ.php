<?php

require(APPPATH.'libraries/REST_Controller.php');

class RFC_MQ extends REST_Controller {

	var $zmq_queue_type = ZMQ::SOCKET_PUSH;
	var $zmq_queue_addr = 'tcp://127.0.0.1:31336';
	
	var $zmq_context;
	var $zmq_socket;

	function getZMQContext() {
		if(!$this->zmq_context) $this->zmq_context = new ZMQContext();
		return $this->zmq_context;
	}

	function getZMQSocket() {
		if(!$this->zmq_socket) {
			$ctx = $this->getZMQContext();
			$this->zmq_socket = $ctx->getSocket($this->zmq_queue_type);
			$this->zmq_socket->connect($this->zmq_queue_addr);
		}
		return $this->zmq_socket;
	}

	function sendZMQ($topic, $message) {
		$queue = $this->getZMQSocket();
		return $queue->sendmulti(array($topic, $message));
	}

	function sendZMQProgress($progress) {
		if($requestid = $this->get('requestid')) {
			$this->sendZMQ('progress_'.$requestid, $progress);
		}
	}
	
    public function ip_get() {
		$this->response(array(
			'ip' => $_SERVER['REMOTE_ADDR'],
		));    
    }

    public function randomafter3_get() {

		srand();

		$this->sendZMQProgress(0);

		for ($progress = 0; $progress <= 100; $progress +=20) {
		  usleep(rand(300,900)*1000);
		  $this->sendZMQProgress($progress);
		}

		$this->response(array(
			'rand' => rand(0,65535),
		));    
    }

}
