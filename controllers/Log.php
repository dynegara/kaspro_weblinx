<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
	
	public      function __construct() {
		parent::__construct();
		$this->config->load('rfc', true);
		$this->rfc = $this->config->item('rfc');
		$this->rlm = 'Press Cancel to change Username';
		$this->ncn = uniqid();
		$this->usr = '';
		$this->svr = array(
			'app' => $this->rfc['ws_svr']['weblink']['app'],
			'api' => $this->rfc['ws_svr']['weblink']['api']
		);
	}
	public      function index($cmd=null, $arg=null) {
		$this->auth();
		switch($cmd) {
			case 'in':
				$this->do_login();
				break;
				
			case 'out':
				echo "Thank's";
				echo "<br/><br/><br/><a href='/log/in' class='btn btn-small'>Login</a>";
				header('HTTP/1.1 401 Unauthorized');
				die();
				break;
				
			case 'rpt':
				$this->rpt($arg);
				break;
				
			default:
				$tgl = null;
				$this->db->select('request_id, request_name');
				$this->db->order_by('client_pst_on', 'DESC');
				if ($arg)
					$this->db->where("client_pst_on between '$arg 00:00:00' and '$arg 23:59:59'");
				else
					$this->db->limit(10);
				
				$lst = $this->db->get('sys_api_log')->result_array();
				$dat = array(
					'tgl' => $arg,
					'app' => $this->svr['app'],
					'lst' => $lst,
					'exp' => '',
					'usr' => $this->usr
				);
				
				$cer = $this->rfc['ws_svr']['kaspro']['opt'][CURLOPT_SSLCERT];
				$inf = openssl_x509_parse(file_get_contents($cer));
				$dat['exp'] = date(DATE_RFC2822, $inf['validTo_time_t']);
				
				$this->load->view('log/index', $dat);
				break;
		}
	}
	
	protected   function rpt($arg) {
		$rst = $this->db->get_where('sys_api_log', array('request_id' =>$arg));
		$out = '';
		
		if($rst) {
			$opt = array(
				'table' => array(
					'class' => 'table'
				)
			);
			$rst = $rst->row_array();
			
			$rsp['client_pst']          = json__table($rst['client_pst'], $opt);
			$rsp['api_pst']             = json__table($rst['api_pst'], $opt);
			$rsp['api_pst_ack']         = json__table($rst['api_pst_ack'], $opt);
			$rsp['client_callback']     = json__table($rst['client_callback'], $opt);
			$rsp['client_callback_ack'] = json__table($rst['client_callback_ack'], $opt);
			
			$out = "
				<div class='box-container'>
					<h5>TIMESTAMP</h5><small></small>
					<table class='table'>
						<tr><th>1. APP POST</th><td>{$rst['client_pst_on']}</td></tr>
						<tr><th>2a. KASPRO API POST</th><td>{$rst['api_pst_on']}</td></tr>
						<tr><th>2b. KASPRO API ACK</th><td>{$rst['api_pst_ack_on']}</td></tr>
						<tr><th>3a. APP CALLBACK</th><td>{$rst['client_callback_on']}</td></tr>
						<tr><th>3b. APP CALLBACK ACK</th><td>{$rst['client_callback_ack_on']}</td></tr>
					</table>
				</div>
				<div class='box-container'>
					<h5>1. APP POST</h5>
					{$rsp['client_pst']}
				</div>
				<div class='box-container'>
					<h5>2a. KASPRO API POST</h5>
					{$rsp['api_pst']}
				</div>
				
				<div class='box-container'>
					<h5>2b. KASPRO API ACK</h5>
					{$rsp['api_pst_ack']}
				</div>
				<div class='box-container'>
					<h5>3a. APP CALLBACK</h5>
					{$rsp['client_callback']}
				</div>
				<div class='box-container'>
					<h5>3b. APP CALLBACK ACK</h5>
					{$rsp['client_callback_ack']}
				</div>
			";
		}
		print_r($out);
	}
	
	private     function auth() {
		$admin  = $this->rfc['ws_admin'];
		$idx    = array('nounce', 'nc', 'cnounce', 'qop', 'username', 'uri', 'response');
		$digest = $this->get_digest();  // Get the digest from the http header

		// If there was no digest, show login
		if (is_null($digest))
			$this->do_login();
		
		preg_match_all('~(\w+)="?([^",]+)"?~', $_SERVER['PHP_AUTH_DIGEST'], $m);
		
		$data           = array_combine($m[1] + $idx, $m[2]);
		$digestParts    = $this->parse_digest($digest);
		$validUser      = $data['username']          ?? 'x';
		$validPass      = $admin[$data['username']]  ?? time();
		
		// Based on all the info we gathered we can figure out what the response should be
		$A1 = md5("{$validUser}:{$this->rlm}:{$validPass}");
		$A2 = md5("{$_SERVER['REQUEST_METHOD']}:{$digestParts['uri']}");
		
		$validResponse = md5("{$A1}:{$digestParts['nonce']}:{$digestParts['nc']}:{$digestParts['cnonce']}:{$digestParts['qop']}:{$A2}");
		
		if ($digestParts['response']!=$validResponse)
			$this->do_login();
		else
			$this->usr = $validUser;
	}
	private     function get_digest() {
		$digest = null;
		// mod_php
		
		if (isset($_SERVER['PHP_AUTH_DIGEST']))
			$digest = $_SERVER['PHP_AUTH_DIGEST'];
			// most other servers
		elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
			if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'digest')===0)
				$digest = substr($_SERVER['HTTP_AUTHORIZATION'], 7);
		}
		
		return $digest;
	}
	private     function parse_digest($digest) {
		// This function extracts the separate values from the digest string
		// protect against missing data
		$needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
		$data = array();
		
		preg_match_all('@(\w+)=(?:(?:")([^"]+)"|([^\s,$]+))@', $digest, $matches, PREG_SET_ORDER);
		
		foreach ($matches as $m) {
			$data[$m[1]] = $m[2] ? $m[2] : $m[3];
			unset($needed_parts[$m[1]]);
		}
		
		return $needed_parts ? false : $data;
	}
	private     function do_login() {
		// This function forces a login prompt
		header('WWW-Authenticate: Digest realm="' . $this->rlm . '",qop="auth",nonce="' . $this->ncn . '",opaque="' . md5($this->rlm) . '"');
		header('HTTP/1.0 401 Unauthorized');
		echo 'Hmmm, You can login with other username';
		echo '<br/><br/><br/><a href="/log" class="btn btn-small">Login</a>';
		die();
	}
}
