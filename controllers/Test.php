<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	
	public      function __construct() {
		parent::__construct();
		$this->config->load('rfc', true);
		$rfc = $this->config->item('rfc');
		
		$this->svr = array(
			'app' => $rfc['ws_svr']['weblink']['app'],
			'api' => $rfc['ws_svr']['weblink']['api']
		);

		$this->arg = null;
	}
	public      function index($cmd=null) {
		$this->arg = func_get_args();
		if($cmd)
			$this->$cmd();
		else {
			$this->db->select('request_id, request_name');
			$this->db->order_by('client_pst_on', 'DESC');
			$this->db->limit(10);
			$lst = $this->db->get('sys_api_log')->result_array();
			$dat = array(
				'app' => $this->svr['app'],
				'lst' => $lst
			);
			$this->load->view('test/index', $dat);
		}
	}
	public      function payment() {
		$this->load->library('Cipher');
		
		$key     = 'KbPeShVmYq3t6v9y$B&E)H@McQfTjWnZ';
		$data    = json_encode($_POST, true);
		$cipher  = new Cipher($key, "AES-256-CBC");
		$crypted = $cipher->encrypt($data);

		$url     = $this->svr['api'] . '/payment';
		$opt     = array();
		// $opt[CURLOPT_VERBOSE] = true;
		$rsp     = curl_post($url, $opt, $crypted);
		// $rsp     = curl_posz($url, $crypted);
		echo $rsp;
	}
	
	public      function screen() {
		$this->load->view("test/{$this->arg[1]}", $this->svr);
	}
	public      function report() {
		$rst = $this->db->get_where('sys_api_log', array('request_id' =>$this->arg[1]));
		
		if($rst) {
			$opt = array(
				'table' => array(
					'class' => 'table'
				)
			);
			$rst = $rst->row_array();
			
			$rsp['client_pst']          = json__table($rst['client_pst'], $opt);
			$rsp['api_pst']             = json__table($rst['api_pst'], $opt);
			$rsp['api_pst_ack']         = json__table($rst['api_pst_ack'], $opt);
			$rsp['client_callback']     = json__table($rst['client_callback'], $opt);
			$rsp['client_callback_ack'] = json__table($rst['client_callback_ack'], $opt);
			
			$out = "
				<div class='box-container'>
					<h5>TIMESTAMP</h5><small></small>
					<table class='table'>
						<tr><th>1. APP POST</th><td>{$rst['client_pst_on']}</td></tr>
						<tr><th>2a. KASPRO API POST</th><td>{$rst['api_pst_on']}</td></tr>
						<tr><th>2b. KASPRO API ACK</th><td>{$rst['api_pst_ack_on']}</td></tr>
						<tr><th>3a. APP CALLBACK</th><td>{$rst['client_callback_on']}</td></tr>
						<tr><th>3b. APP CALLBACK ACK</th><td>{$rst['client_callback_ack_on']}</td></tr>
					</table>
				</div>
				<div class='box-container'>
					<h5>1. APP POST</h5>
					{$rsp['client_pst']}
				</div>
				<div class='box-container'>
					<h5>2a. KASPRO API POST</h5>
					{$rsp['api_pst']}
				</div>
				
				<div class='box-container'>
					<h5>2b. KASPRO API ACK</h5>
					{$rsp['api_pst_ack']}
				</div>
				<div class='box-container'>
					<h5>3a. APP CALLBACK</h5>
					{$rsp['client_callback']}
				</div>
				<div class='box-container'>
					<h5>3b. APP CALLBACK ACK</h5>
					{$rsp['client_callback_ack']}
				</div>
			";
		}
		print_r($out);
	}
	
	public      function ssl_validate() {
		$cmd = $this->arg[1] ?? 'get';
		$dat = array('key' => 'hallo world');
		
		$url = 'https://api.kaspro.id/bIcELQIzfk/089529523329/custom/validate';
		
		$tok = 'XHR6b6MRyzVA7jMs1bggPyB49BcamTkuHMRJsXfRsVbVTXrzTSvZtiYPHYODM1LYeolCIfgTYswm9ySJGcVg/nyeE+7mwP6Pze2IXpZaaXRUdxTjjn+AVBw9OyZqnnefiMuS/nIxjwQ7nBbJWmQEul5dEBBAXVCm5OgaVATlu4iSXDfLgmnTD103jeqi6N4qml9PQ3vALuVO4K8s2fJt/HGP5SDNOuYQo3D94/aSAHCncpPmdzsUqt/TPFsfMDAj';
		$ver = curl_version();
		$uag = 'curl/' . $ver['version'];
		
		$opt = array(
			CURLOPT_VERBOSE               => false,
			CURLOPT_SSL_VERIFYPEER        => true,           // true | false
			CURLOPT_SSL_VERIFYHOST        => 2,              // 0 | 1 | 2
			CURLOPT_HTTPHEADER            => array(
				'Content-Type: application/json',
				'Token: ' . $tok
			)
		);
		
		$opt[CURLOPT_USERAGENT]     = $uag;
		$opt[CURLOPT_HTTP_VERSION]  = 'CURL_HTTP_VERSION_1_1';
		
		$opt[CURLOPT_SSLCERT]       = APPPATH . 'cert/homecredit/homecredit.pem';
		// $opt[CURLOPT_SSLCERTPASSWD] = 'H0m3Cred!t19';
		$opt[CURLOPT_SSLKEY]        = APPPATH . 'cert/homecredit/homecredit.pem';
		// $opt[CURLOPT_SSLKEYPASSWD]  = 'H0m3Cred!t19';
		
		$rsp = array();
		switch($cmd) {
			case 'post':
				$rsp = curl_post($url, $dat, $opt);
				break;
			
			case 'get':
				$rsp = curl_get($url, $opt);
				break;
				
			case 'postman';
				$rsp = $this->ssl_postman();
				break;
		}
		if(is_json($rsp))
			$rsp = json_decode($rsp);
		
		$out = array(
			'url' => $url,
			'rsp' => $rsp
		);
		return $this->output->set_content_type('application/json')
			->set_status_header(200)
			->set_output(json_encode($out,JSON_PRETTY_PRINT |  JSON_NUMERIC_CHECK));
	}
	private     function ssl_postman() {
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.kaspro.id/bIcELQIzfk/089529523329/custom/validate",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			// 'CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Token: XHR6b6MRyzVA7jMs1bggPyB49BcamTkuHMRJsXfRsVbVTXrzTSvZtiYPHYODM1LYeolCIfgTYswm9ySJGcVg/nyeE+7mwP6Pze2IXpZaaXRUdxTjjn+AVBw9OyZqnnefiMuS/nIxjwQ7nBbJWmQEul5dEBBAXVCm5OgaVATlu4iSXDfLgmnTD103jeqi6N4qml9PQ3vALuVO4K8s2fJt/HGP5SDNOuYQo3D94/aSAHCncpPmdzsUqt/TPFsfMDAj"
			),
			CURLOPT_CAINFO    => '/var/www/cert/homecredit/homecredit.pem'
		));
		
		$rsp = curl_exec($curl);
		curl_close($curl);
		return $rsp;
	}
}
