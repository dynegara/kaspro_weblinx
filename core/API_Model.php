<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

// ABBRV DICTIONARY

class API_Model extends CI_Model {
	// sys-process-type (Stand Alone, Extended)
	protected $prc_hide = array('str'=>'18', 'arr'=>array(18));
	protected $acm_idx  = array('ins','upd','del','imp','exp','upl');
	protected $alfabet  = array('a','b','c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
	protected $reg_dat  = '/(dat|rsp|sys_req_pst|rsp_rsp_dat|sys_(req|bpm|prc|usr|cfg)|sys_bpm_(aux|ext)__*/',
		$reg_opr  = '/<|>|!|=|\(|\b(not|in|like|between)\b/';
	protected $counter  = 0;
	protected $sql      = array();
	protected $sys;
	protected $DBX, $DBS, $DBO, $DBP;   // S: MsSQL, O:Oracle, P:Postgres
	protected $iop_db, $tim_db;
	
	public      function __construct() {
		parent::__construct();

        $cfg = $this->config->item('ws_dbs');
        if(isset($cfg[APP_DEF])) {
            $this->iop_db   = $cfg[APP_DEF]['sys'];
            $this->tim_db   = $cfg[APP_DEF]['tim'];
        }
		$this->util_db();
	}
	private     function aaa_authz($type='init') {
		switch($type) {
			case 'init':
				$sql = "select 	group_concat(a.id) sys_app_id
						from 	sys_app a
						join 	aaa_acm__sys_app b on a.id = b.sys_app_id
						and     a.sys_data_status_id = 1
						and 	aaa_acm_id in ({$this->sys['usr']['aaa_acm_id']})";
				$this->sys['aaa']['app']['privilege'] = $this->db->query($sql)->row()->sys_app_id;
				break;
		}
		if($type=='init') {
			
		}
	}
	private     function aaa_uta() {
		// uta: user target audience
		
		// audience = by-account
		$act = $this->sys['usr']['aaa_account'];
		$this->sys['aaa']['uta']['act'] = " and (audience_aaa_account_ixd is null or audience_aaa_account_ixd regexp '[[:<:]]{$act}[[:>:]]')";
		
		// audience = by-role
		$role   = $this->sys['usr']['aaa_acm_id'];
		$tmp    = array();
		if ($role) {
			if (is_array($role))
				foreach ($role as $k => $v)
					$tmp[] = "audience_aaa_acm_ixd regexp '[[:<:]]{$v}[[:>:]]'";
		} else
			$tmp[] = $role;
		if ($tmp) {
			$tmp = implode(' or ', $tmp);
			$this->sys['aaa']['uta']['role'] = " and (audience_aaa_acm_ixd is null or $tmp)";
		}
		
		// audience = by-organization
		$dir = isset($this->sys['usr']['hrm_orgz_directorat_id'])   ? $this->sys['usr']['hrm_orgz_directorat_id']   : null;
		$div = isset($this->sys['usr']['hrm_orgz_division_id'])     ? $this->sys['usr']['hrm_orgz_division_id']     : null;
		$org = isset($this->sys['usr']['hrm_orgz_id'])              ? $this->sys['usr']['hrm_orgz_id']              : null;
		
		$this->sys['aaa']['uta']['orgz'] = " and (audience_hrm_orgz_ixd is null or audience_hrm_orgz_ixd regexp '[[:<:]]{$this->sys['cfg']['base_orgz']}[[:>:]]'";
		if ($dir) $this->sys['aaa']['uta']['orgz'] .= " or audience_hrm_orgz_ixd regexp '[[:<:]]{$dir}[[:>:]]'";
		if ($div) $this->sys['aaa']['uta']['orgz'] .= " or audience_hrm_orgz_ixd regexp '[[:<:]]{$div}[[:>:]]'";
		if ($org) $this->sys['aaa']['uta']['orgz'] .= " or audience_hrm_orgz_ixd regexp '[[:<:]]{$org}[[:>:]]'";
		$this->sys['aaa']['uta']['orgz'] .= ")";
		
		if(isset($this->sys['usr']['crm_company_id']) && $this->sys['usr']['crm_company_id'] != $this->sys['app']['base_corp']) {
			$this->sys['aaa']['uta']['comp'] = " crm_company_id = {$this->sys['usr']['crm_company_id']}";
		}
		
		// todo: audience = geography region
	}
	public      function aaa_user($usr) {
		$rst = $this->db->select('id, aaa_account, aaa_class_id')->get_where('aaa_account', array('id_code' => $usr))->row_array();
		if($rst && $rst['aaa_class_id']) {
			$sql = "select group_concat(aaa_acm_id) aaa_acm_id from aaa_account__aaa_acm where aaa_account_id = {$rst['id']}";
			$acm = $this->util_db_sql($sql, 'cel');
			$rst['aaa_acm_id'] = $acm ? $acm : 0;
			
			switch ($rst['aaa_class_id']) {
				case 14:
					$sql = "select  hrm_employee_id, crm_office_id
							from    aaa_account__hrm_employee a
							join    hrm_employee              b on b.id = a.hrm_employee_id and aaa_account_id = {$rst['id']}";
					$hrm = $this->util_db_sql($sql, 'row');
					if($hrm) {
						$rst['hrm_employee_id']    = $hrm['hrm_employee_id'];
						$rst['crm_office_id']      = $hrm['crm_office_id'];
						
						$sql = "select 	group_concat(ctms_hpl_id) ctms_hpl_ixd
								from 	hrm_employee			a
								join 	ctms_hpl__hrm_employee	b on a.id = hrm_employee_id  and hrm_employee_id = {$hrm['hrm_employee_id']}
								group by hrm_employee_id";
						$hpl = $this->util_db_sql($sql, 'cel');
						if($hpl)
							$rst['ctms_hpl_xid'] = $hpl;
					}
					break;
					
				case 30:
					$sql = "select  crm_officer_id, crm_company_id, crm_region_id
							from    aaa_account__crm_officer a
							join    crm_officer              b on b.id = a.crm_officer_id and aaa_account_id = {$rst['id']}";
					$tmp = $this->util_db_sql($sql, 'row');
					$rst['crm_officer_id']      = $tmp['crm_officer_id'];
					$rst['crm_company_id']      = $tmp['crm_company_id'];
					if($tmp['crm_region_id'])
						$rst['crm_region_id']   = $tmp['crm_region_id'];
					break;
			}
		}
		unset($rst['aaa_class_id']);
		return $rst;
	}
	public      function sync(&$sys, $idx='in', $flx='pull') {
		switch($idx) {
			case 'in':
				$this->sys =& $sys;
				break;
			
			case 'out':
				$sys = $this->sys;
				break;
			
			case 'ctrl-in':
				$this->sys = array_merge($this->sys, $sys);
				$sys = $this->sys;
				break;
			
			case 'ctrl-out':
				$sys = array_merge($this->sys, $sys);
				$this->sys =& $sys;
				break;
			
			default:
				if(is_string($idx))
					$idx = array($idx);
				
				foreach($idx as $elm)
					if($flx == 'pull') {
						$sys[$elm] = $this->sys[$elm];
					}
					else
						$this->sys[$elm] = $sys[$elm];
				break;
		}
	}
	public      function proxy() {
		$arg = func_get_args();
		$fnc = $arg[0];
		
		if(isset($arg[8]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3], $arg[4], $arg[5], $arg[6], $arg[7], $arg[8]);
		elseif(isset($arg[7]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3], $arg[4], $arg[5], $arg[6], $arg[7]);
		elseif(isset($arg[6]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3], $arg[4], $arg[5], $arg[6]);
		elseif(isset($arg[5]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3], $arg[4], $arg[5]);
		elseif(isset($arg[4]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3], $arg[4]);
		elseif(isset($arg[3]))
			$out = $this->$fnc($arg[1], $arg[2], $arg[3]);
		elseif(isset($arg[2]))
			$out = $this->$fnc($arg[1], $arg[2]);
		elseif(isset($arg[1]))
			$out = $this->$fnc($arg[1]);
		else
			$out = $this->$fnc();
		
		return $out;
	}
	
	public      function sys_var($var, $arg_1 = null, $arg_2 = null) {
		$out = true;
		switch($var) {
			case 'tbl_pky' :
				// $arg_1 : tbl
				// $arg_2 : dat
				if($arg_1 == $this->sys['prc']['sys_tbl']['id'])
					$pky = $this->sys['prc']['sys_tbl']['attr']['pkey'];
				else
					$pky = $this->db->get_where('____mysql_table_pkey', array('sys_table_id' => $arg_1))->row()->pkey;
				
				$out = array();
				$pkf = array_flip(explode(',', $pky));
				
				if($arg_2 && is_array($arg_2))
					$out = array_intersect_key($arg_2, $pkf);
				else
					foreach($pkf as $k=>$v)
						$out[$k] = null;
				break;
			case 'col_type' :
				$dty = array('tinyint', 'smallint', 'int', 'bigint', 'float', 'double', 'numeric');
				$sql = "select 	data_type pky
                        from	information_schema.columns
                        where   table_schema like '{$this->sys['app']['db_prefix']}_%'
                        and     table_name  = '$arg_1'
                        and     column_name = '$arg_2'
                        order by ordinal_position";
				$tmp = $this->db->query($sql)->row()->pky;
				$out = in_array($tmp, $dty) ? 1 : 0;
				break;
			
			case 'administrative_tree' :
				$out = array();
				$out['nodes'] = array();
                $sdb = $this->util_tbl_sdb('geo_administrative');
				$whr = $arg_1 ? "pid = $arg_1" : "geo_tree_id = 12";
				$sql = "select * from $sdb.geo_administrative where flag = 'BPS' and $whr order by geo_code";
				if($rst = $this->db->query($sql)->result_array()) {
					foreach($rst as $k => $v)
						$out['nodes'][] = array('id' => $v['id'], 'parent' => $v['pid'], 'name' => $v['geo_administrative'], 'level' => $v['geo_administrative_tree_id'] - 11, 'type' => 'folder');
				}
				break;
			
			case 'quote':
			case 'quote_random':
				$flt = $arg_1 ? "and (author like '%$arg_1%' or cms_quote_type_id = '$arg_1')" : '';
				$sdb = $this->util_tbl_sdb('cms_quote');
				$sql = "select  *
						from    {$sdb}.cms_quote
                        where   sys_data_status_id = 1 $flt
                        and     rand() < (select((1/count(*))*10) from {$sdb}.cms_quote where 1=1 $flt)
                        order by rand() limit 1";
				$out = $this->db->query($sql)->row_array();
				break;
			
			case 'hlp' : // help
				$sql = "select  id, sys_process_id,   field_name from help where sys_process_id = $arg_1    and field_name is not null union
                        select  id, sys_process_id,   field_name from help where sys_process_id is null     and field_name is not null
                        and     field_name not in(select field_name from help where sys_process_id = $arg_1 and field_name is not null)
                        order by sys_process_id";
				$out = arr__id($this->db->query($sql)->result_array(), 'field_name');
				break;
			case 'helper' :
				$sql = "select help help_h, help_content help_b, help_pdf help_f, width from sys_help where id = $arg_1";
				$out = $this->db->query($sql)->row_array();
				break;
			
			case 'sys_response_code' :
                $arg_1 ? $arg_1 : $this->sys['rsp']['ack']['ack_code'];
				$sql = "select sys_response_code msg from sys_response_code where id = '$arg_1'";
				$tmp = $this->db->query($sql)->row();
				$out = $tmp ? $tmp->msg : "Unknown Error #$arg_1";
				break;
			
			case 'svc_upl';
				$svc = isset($this->sys['req']['pst']['svc_code']) ? $this->sys['req']['pst']['svc_code'] : 'default';
				$sql = "select import_xls from sys_process__import where sys_process_id = {$this->sys['req']['rid']} and svc_code = '{$svc}'";
				if($rst = $this->db->query($sql)->row()->import_xls)
					$this->sys['svc']['upl']['cfg'] = json_decode($rst, true);
				break;
		}
		
		return $out;
	}
	public      function sys_cfg($cfg, $app) {
		switch($cfg) {
			case 'app' :
				$this->sys_cfg_app($app);
				break;
			default :
				$sql = "select {$this->tim_db}.sys_config from sys_config where id = '$cfg' and sys_data_status_id = 1";
				return $this->db->query($sql)->row()->sys_config;
				break;
		}
	}
	private     function sys_cfg_app($app) {
		$sql = "select  	a.id, a.pid, a.id_code, sys_app, hrm_orgz_id, jsd_run
                from    	{$this->tim_db}.sys_app	    a
                join        sys_app_jsd                 b on a.id = b.sys_app_id
                where   	a.sys_data_status_id 	= 1
                and     	a.id                  	= ?";

		$out = $this->db->query($sql, array($app))->row_array();
		
		$this->sys['app']['id']             = $out['id'];
		$this->sys['app']['pid']            = $out['pid'];
		$this->sys['app']['id_code']        = $out['id_code'];
		$this->sys['app']['sys_app']        = $out['sys_app'];
		$this->sys['app']['hrm_orgz_id']    = $out['hrm_orgz_id'];

		if($out['jsd_run']) {
			$this->util_parser_jsd('jsd_run', $out);
			foreach($out['cfg'] as $x => $y)
				$this->sys['cfg'][$x]   = $y;
			$this->sys['app']   = $out['app'];
			$this->sys['tbl']   = $out['tbl'];
		}
		else {
			// $cfg = $this->config->item('iop');
			// if(isset($cfg[$out['id_code']]))
			//	$this->sys['app'] = array_merge($this->sys['app'], $cfg[$out['id_code']]);
			
			$this->sys_cfg_jsd($app);
		}

		// sys-config
		$this->sys_cfg_cfg($app);

		// set controller
		$path = $this->router->directory ? $this->router->directory : '';
		if($app <= 10)
			$path   = '';

		$ctrl = $this->router->class;

        if(preg_match('/^sys/', $ctrl))
			$ctrl = strtolower(str_replace('sys', '', $ctrl));
   
		if(in_array($path.$ctrl, array_keys($this->sys['app']['ctrl']))) {
			$this->sys['app']['path']   = $path;
			$this->sys['app']['url']    = $path.$ctrl;
            $this->sys['app']['model']  = $this->sys['app']['ctrl'][$path.$ctrl];
            $this->sys['app']['ctrl']   = $ctrl;
		}
		else {
            $this->sys['rsp']['ack']['ack_code'] = 'HTTP__404';
            $this->sys['ack']['msg'] = 'Controller not defined';
        }
		
		// special PCM Holder
		if(isset($out['app']['pcm'])) {
			$this->sys['uix']['pcm']    = $out['app']['pcm'];
			unset($out['app']['pcm']);
		}
		$this->sys['uix']['elm']        = $this->sys['app']['index'] . '/elm';
		
		$out = null;
	}
	private     function sys_cfg_tbl($app) {
		$sql = "SET SESSION group_concat_max_len = 1000000";
		$this->db->query($sql);
		
		// SET GLOBAL group_concat_max_len = 1000000;
		$sql = "select 	group_concat(sys_table_id separator \"','\") sys_table_id
				from	(
					select 	id sys_table_id
					from 	{$this->tim_db}.sys_table
					where   id like 'event%'
					
					union
				    select 	distinct sys_table_id
					from 	{$this->tim_db}.sys_process
					where   sys_app_id = $app
					and     sys_table_id is not null
					
					union
					select 	distinct biz_table_id
					from 	{$this->tim_db}.sys_process
					where   sys_app_id = $app
					and     biz_table_id is not null
					
					union
					select 	distinct sys_table_id
					from 	____sys_pick_ref
					where 	sys_app_id = $app
					and 	sys_table_id is not null
				  
				    union
				    select  sys_table_id
				    from    sys_app__sys_table
				    where   sys_app_id = $app
				) x";
		$this->util_db();
		$tbl = $this->db->query($sql)->row()->sys_table_id;
		
		$sql = "select id, case when sys_cache_id = 1 then '{$this->tim_db}' else schema_db end sdb from {$this->tim_db}.sys_table where id in ('{$tbl}')";
		$rst = $this->util_db_sql($sql);
		if($rst) {
            foreach ($rst as $k => $v)
                $this->sys['tbl'][$v['id']] = "{$v['sdb']}.{$v['id']}";
        }
        else {
            $this->sys['rsp']['ack']['ack_code'] = 'APP_3';
            $this->sys['acc']['msg'] = $this->sys_var(['sys_response_code']);
            dump($this->sys['ack'], $sql, '#');
        }
	}
	protected   function sys_cfg_cfg($app) {
		if(!isset($this->sys['cfg']['iop_version'])) {
			$sql = "select  id, case when sys_config != '' then sys_config else 0 end value, sys_data_type_id, sys_app_id
					from    {$this->tim_db}.sys_config
					where   sys_data_status_id 	= 1
					and     sys_app_id 			in (1, $app)";
			$out = $this->db->query($sql)->result_array();
			if($out)
				foreach ($out as $k => $v) {
					$p = explode('.', $v['id']);
					if (isset($p[1])) {
						if ($v['sys_app_id'] == 1)
							$this->sys['cfg'][$p[0]][$p[1]] = $v['value'];
						else
							$this->sys['app'][$p[0]][$p[1]] = $v['value'];
					}
					else {
						if ($v['sys_app_id'] == 1)
							$this->sys['cfg'][$v['id']] = $v['value'];
						else
							$this->sys['app'][$v['id']] = $v['value'];
					}
				}
		}
		else {
			$this->sys['cfg']['xhr'] = $this->input->is_ajax_request();
			$this->sys['cfg']['online'] = 0;
			$this->sys['cfg']['fs']  = 0;
			$this->sys['cfg']['now'] = date('Y-m-d', TS_START);
			$this->sys['cfg']['dts'] = date('Y-m-d H:i:s', TS_START);
			$this->sys['cfg']['ts']  = TS_START;
			$this->sys['cfg']['env'] = ENVIRONMENT;
		}
	}
	private     function sys_cfg_jsd($app) {
		$this->sys_cfg_cfg($app);
		
		$sql = "select  id, sys_app, sys_app_type_id, jsd_app
	            from    {$this->tim_db}.sys_app     a
	            join    sys_app_jsd                 b on a.id = sys_app_id
	            where   sys_data_status_id          = 1
				and    (sys_app_type_id             = 1 or
	                    sys_app_id                  = $app)
				order by sys_app_type_id asc";
		$rst  = $this->db->query($sql)->result_array();
		
		if($rst) {
			$this->sys['cfg']['iop_platform'] = $rst[0]['sys_app'];
            // setup app-jsd
			foreach ($rst as $k => $v) {
				if ($v['jsd_app']) {
					$jsd = json_decode($v['jsd_app'], true);
					if(!$jsd)
					    $jsd = $this->util_app_jsd(($v['id']));
					
					switch ($v['sys_app_type_id']) {
						case 1:
							foreach ($jsd['cfg'] as $x => $y)
								$this->sys['cfg'][$x] = $y;
							
							if (isset($jsd['app']))
								foreach ($jsd['app'] as $x => $y)
									$this->sys['app'][$x] = $y;
							break;
						
						default:
							foreach ($jsd['app'] as $x => $y)
								$this->sys['app'][$x] = $y;
							break;
					}
					$jsd = null;
				}
			}
			
			$this->sys_cfg_tbl($app);
			$dat = array('cfg' => $this->sys['cfg'], 'app' => array(), 'tbl' => $this->sys['tbl']);
			unset($dat['cfg']['dbg'], $dat['cfg']['csrf']);
			
			// setup app-controller
			$sql = "select  ctrl, ctrl_model
	                from    {$this->tim_db}.sys_app_ctrl
	                where   sys_app_id             = $app
	                and     sys_data_status_id     = 1";
			if ($tmp = $this->db->query($sql)->result_array()) {
				$this->sys['app']['ctrl'] = array();
				foreach ($tmp as $k => $v) {
					$this->sys['app']['ctrl'][$v['ctrl']] = $v['ctrl_model'];
				}
			}
			$dat['app'] = $this->sys['app'];
			
			$jsd = array();
			$jsd['jsd_run'] = json_encode($dat, JSON_PRETTY_PRINT);
			$this->db->update("sys_app_jsd", $jsd, array('sys_app_id' => $app));
			return $dat;
		}
		else {
            $this->sys['rsp']['ack']['ack_code'] = 'APP_1';
            $this->sys['acc']['msg'] = $this->sys_var(['sys_response_code']);
            dump($this->sys['ack'], $sql, '#');
		}
	}
	public      function sys_load($item, $idx=null, $idy=null) {
		$out = null;
		switch($item) {
			case 'prc_check':
				$rid = $this->sys['req']['rid'];
				$sql = "select  *
						from    {$this->tim_db}.sys_process  a
						join    sys_process_jsd              b on a.id = b.sys_process_id
						where   id = $rid or id_code = '$rid'";
				$out = $this->db->query($sql)->row_array();
				if($out)
					$out = $this->util_parser_jsd('sys_jsd', $out);
				break;
			
			case 'rpc_loader':
				$app = $this->sys['app']['id'];
				$rst = $this->db->select('rpc_jsd')->get_where('aaa_account', array('id' => $this->sys['usr']['id']))->row_array();
				if ($rst) {
					$rpc = json_decode($rst['rpc_jsd'], true);
					unset($rst);
					
					if ($rpc && isset($rpc[$app])) {
						$out = true;
						$this->sys['rpc'] = $rpc[$app]['rpc'];
						$this->sys['rsp']['menu'] = $rpc[$app]['menu'];
					}
				}
				else {
					$this->sys_cmd__get_menu();
				}
				break;
			
			case 'logs' :
				switch($idx) {
					case 'inquiry' :
                        $sdb = $this->util_tbl_sdb('event');
						$sql = "select `out` from {$sdb}.event where id = '$idy'";
						$out = $this->db->query($sql)->row()->out;
						break;
					case 'insert' :
					case 'update' :
					case 'delete' :
                        $sdb = $this->util_tbl_sdb('event');
						$sql = "select * from qry_sys_event_logs a
								left join {$sdb}.event_crud b on a.event_id = b.event_id
								where a.event_id = '$idy'";
						$out = $this->db->query($sql)->result_array();
						break;
				}
				break;
		}
		return $out;
	}
	
	// section  prc-bpm
	public      function sys_prc_loader() {
		$app = $this->sys['app']['id'];
		$rid = $this->sys['req']['rid'];
		$prx = null;
		$out = array();
		if($rid) {
			if(isset($this->sys['rpc'][$app]))
				$prx = $this->sys['rpc'][$app]['rpc'];
			
			if(!$prx) {
				$this->sys_cmd__get_menu();
				$prx = $this->sys['rpc'][$app]['rpc'];
			}
			
			if (isset($prx[$rid]))
				$out = $prx[$rid];
			else {
				// meaning $rid is string
				foreach ($prx as $k => $j)
					if ($j['id_code'] == $rid) {
						$out = $prx[$k];
						break;
					}
			}
			
			$prx = null;
			if($out) {
				$this->sys['req']['rid']              = $out['id'];
				$this->sys['event']['sys_process_id'] = $out['id'];
				$this->sys['event']['sys_table_id']   = $out['sys_table_id'];
				$this->sys['event']['event']          = $out['sys_process'];
				// $this->sys['event']['sys_sdlc_id']    = $out['sys_sdlc_id'];
				
				$this->sys['bpm']['tbl'] = $out['sys_table_id'];
				$this->sys['bpm']['biz'] = $out['biz_table_id'];
				$this->sys['bpm']['bpm'] = $out['biz_table_id'] ? $out['biz_table_id'] : $out['sys_table_id'];
				$this->sys['bpm']['prc'] = $out['id'];
				if($out['sys_table_id'])
                    $this->sys['bpm']['prc'] .= '-' . $out['sys_table_id'];

				unset($out['id']);
				$this->sys['prc'] = $out;
			}
		}
		else {
			if(isset($this->sys['rpc'][$app]))
				$this->sys['rsp']['menu'] = $this->sys['rpc'][$app]['menu'];
			else
				$this->sys_cmd__get_menu();
		}
		unset($this->sys['rpc']);
	}
	
	// section  cmd-inquiry
	public      function sys_cmd__get($act=true) {
		$this->util_db_drv($this->sys['bpm']['tbl']);
		$this->sys['req']['frm'] ? $this->sys_cmd__get_req_frm() : $this->sys_cmd__get_req_lst();
		if($act)
			$this->sys['req']['frm'] ? $this->sys_cmd__get_act_frm() : $this->sys_cmd__get_act_lst();
	}
	private     function sys_cmd__get_req() {
		/*
         * init query parameter
         */
		isset($this->sys['bpm']['has']['col_status']) && $bpm['arr']['sys_data_status_id'] = 1;
		
		if($this->sys['req']['arg']) {
			foreach ($this->sys['req']['arg'] as $k=>$v)
				$this->sys['bpm']['arr'][$k] = $v;
		}
		if ($this->sys['bpm']['arr'])
			$this->util_parser_var_sys($this->sys['bpm']['arr'], false);
		
		if($this->sys['bpm']['arr']) {
			foreach ($this->sys['bpm']['arr'] as $k=>$v) {
				if(preg_match('/^(today|yesterday|last|next|since)/', $v))
					$this->sys['bpm']['arr'][$k] = date_range($v);
				elseif(preg_match('/~/', $v)) {
					$x = explode('~', $v);
					$this->sys['bpm']['arr'][$k] = "between '{$x[0]} 00:00:00' and '{$x[1]} 23:59:59'";
				}
			}
		}
		
		if($this->sys['bpm']['arr'])
			$this->sys['bpm']['str'] .= parser_whr($this->sys['bpm']['arr'], $this->sys['req']);
	}
	private     function sys_cmd__get_req_frm() {
		if(method_exists($this, 'sys_cmd__get_req_frm_pre'))
			$this->sys_cmd__get_req_frm_pre();
		
		$this->sys_cmd__get_req();
		
		if(method_exists($this, 'sys_cmd__get_req_frm_xtd'))
			$this->sys_cmd__get_req_frm_xtd();
	}
	private     function sys_cmd__get_req_lst() {
		if(method_exists($this, 'sys_cmd__get_req_lst_pre'))
			$this->sys_cmd__get_req_lst_pre();
		
		$bpm = $this->sys['bpm'];
		
		/*
		 * Filter data
		 */
		// implement free-search
		
		if($this->sys['bpm']['tbl'] && isset($this->sys['req']['arg']['q'])) {
			$stm = 'std';
			if(isset($this->sys['bpm']['search']['col']))
				$stm = 'spc';
			if(isset($this->sys['bpm']['search']['stm']))
				$stm = $this->sys['bpm']['search']['stm'];
			elseif(isset($this->sys['bpm']['search']['col']) && empty($this->sys['bpm']['search']['stm']))
				$stm = 'spc';
			
			$stf = $this->sys['req']['arg']['q'];
			
			unset($this->sys['req']['arg']['q']);
			switch ($stm) {
				case 'std':     // standard
				case 'spc':     // special column
					$s = array();
					// method 1
					if($stm == 'std') {
						$tbl = $this->sys['bpm']['tbl'];
						$exc = array('event_id', 'sys_table_id', 'sys_table_pk');
						foreach ($this->util_tbl_col($tbl) as $f) {
							if((preg_match('/(pid|rid|nid|_id)$/', $f['name']) && preg_match('/int/', $f['type'])) ||
								preg_match('/(date|time)/', $f['type']) ||
								preg_match('/(event_|jsd)/', $f['name']))
								$exc[] = $f['name'];
							
							if(!in_array($f['name'], $exc)) {
								if (is_numeric($stf)) {
									if (preg_match('/(phone|mobile|msisdn|celular|selular|telepon|telp|tlp|fax|ext)/', $f['name'])) {
										$s[] = "{$f['name']} = $stf";
										$s[] = "{$f['name']} = 62" . substr($stf, 1);
									}
									elseif (preg_match('/int/', $f['type']))
										$s[] = "{$f['name']} = $stf";
								}
								else {
									if (preg_match('/char/', $f['type']))
										$s[] = "{$f['name']} like '%$stf%'";
								}
							}
						}
					}
					// method 2
					else {
						foreach (explode(',', $this->sys['bpm']['search']['col']) as $k => $v)
							$s[] = "$v like '%{$stf}%'";
					}
					
					if($s) {
						$s                       = implode(' or ', $s);
						$this->sys['bpm']['str'] .= " and ($s)";
					}
					break;
				
				case 'fti':   // full-text-index
					break;
			}
		}
		
		/*
		 * override paging limit and offset
		 */
		$bpm['lim'] && ($this->sys['nav']['l'] = $bpm['lim']);
		if(!in_array($this->sys['req']['out'], array('Batch', 'doc', 'pdf', 'csv')))
			$this->sys['bpm']['lim'] =  $this->util_db_sql_limit('nav', 'nav');
		
		/*
		 * override sorting order
		 */
		if($this->sys['nav']['s'] && $this->sys['nav']['f']) {
			$ord = $this->sys['nav']['f'];
			if($this->sql) {
				isset($this->sql['ord'][$ord]) and $ord = $this->sql['ord'][$ord];
				$this->sys['bpm']['ord'] = "order by {$ord} {$this->sys['nav']['s']}";
			}
			else
				$this->sys['bpm']['ord'] = "order by {$ord} {$this->sys['nav']['s']}";
		}
		else
			isset($bpm['ord']) && ($this->sys['bpm']['ord'] = $bpm['ord']);
		
		if(preg_match('/pxd_order/', $this->sys['bpm']['ord']))
			$this->sys['bpm']['ord'] = str_replace('pxd_order', 'concat(lpad(ifnull(pid, id),8,"0"), lpad(id ,8,"0"))', $this->sys['bpm']['ord']);
		
		if (!preg_match('/order/', $this->sys['bpm']['ord'])) {
			$pky = isset($this->sys['bpm']['has']['col_id']) ? $this->sys['bpm']['has']['col_id'] : 'id';
			$this->sys['bpm']['ord'] = $this->sys['bpm']['ord'] ? 'order by ' . $this->sys['bpm']['ord'] : 'order by ' . $pky;
		}
		
		$this->sys_cmd__get_req();
		
		if(method_exists($this, 'sys_cmd__get_req_lst_xtd'))
			$this->sys_cmd__get_req_lst_xtd();
	}
	private     function sys_cmd__get_act_frm() {
		$whr = '';
		$pky = array();
		$tbl = $this->sys['bpm']['tbl'];
		
		if (method_exists($this, 'sys_cmd__get_act_frm_pre'))
			$this->sys_cmd__get_act_frm_pre();
		
		if(in_array($this->sys['req']['arg'], array('new', 'n3w'))) {
			if(preg_match('/^inq/', $tbl))
				$tbl = $this->sys['bpm']['tbl'];

			if(in_array($this->sys['prc']['sys_tbl']['sys_table_type_id'], range(90, 93)))
				$sql = "select * from {$this->sys['tbl'][$tbl]} where 1 = 2";
			else
				$sql = $this->util_db_sql_magic($tbl) . ' and 1 = 2';
			$this->sys['rsp']['data'] = $this->util_db_sql($sql, 'row', 'void');
			
			foreach($this->sys['rsp']['data'] as $k=>$f) {
				if(isset($this->sys['bpm']['arr'][$k]))
					$this->sys['rsp']['data'][$k] = $this->sys['bpm']['arr'][$k];
			}
			
			if(isset($this->sys['rsp']['data']['sys_data_status_id']))
				$this->sys['rsp']['data']['sys_data_status_id'] = 1;
			
			// get cfg ready for mms- upload
			if(isset($this->sys['svc']['mms']['cms_catalog_id'])) {
				$cat = false;
				if(isset($this->sys['rsp']['data']['cms_catalog_id']) && $this->sys['rsp']['data']['cms_catalog_id'] != $this->sys['svc']['mms']['cms_catalog_id'])
					$cat = $this->sys['rsp']['data']['cms_catalog_id'];
				else
					$cat = $this->sys['svc']['mms']['cms_catalog_id'];
				if($cat)
					$this->sys['svc']['mms']['util_img'] = json_decode($this->util_db_qry('dms_catalog', $cat), true);
			}
		}
		else {
			if ($this->sys['bpm']['cfg']['tbm'] && !$this->sys['rsp']['data']) {
				$tbt = $this->sys['bpm']['tbt'];
				
				switch ($tbt) {
					case 'sql_plain':    // sql-statement
						$tmp = $this->util_tbl_pky($tbl);
						foreach ($tmp as $k => $v) {
							$whr   .= $tbt == 81 ?
								" and " . $this->sql['col'][$k] . " = ?" :
								" and $k = ?";
							$pky[] = $v;
						}
						
						$sql                     = "select {$this->sql['sel']} from {$this->sql['frm']} where 1=1 $whr";
						$this->sys['rsp']['data'] = $this->util_db_sql($sql, $pky, 'row');
						break;
						
					default:
						if ($this->sys['bpm']['is_magic']) {
							$cfg = array('frm' => 1, 'sys_data_status_id' => 1);
							$sql = $this->util_db_sql_magic($tbl, $cfg, $join = true);
						}
						else
							$sql = "select * from {$this->sys['bpm']['tbl']} where 1=1 {$this->sys['bpm']['str']}";
						
						$this->sys['rsp']['data'] = $this->util_db_sql($sql, 'row', 'void');
						break;
				}
			}
			
		}
		
		if(method_exists($this, 'sys_cmd__get_act_frm_xtd'))
			$this->sys_cmd__get_act_frm_xtd();
	}
	private     function sys_cmd__get_act_lst() {
		$tbl = $this->sys['bpm']['tbl'];
		$bpm = $this->sys['bpm'];

		if (method_exists($this, 'sys_cmd__get_act_lst_pre'))
			$this->sys_cmd__get_act_lst_pre();

		$inq = $this->sys['req']['res'] . '__get_act_lst';
		if (method_exists($this, $inq))
			$this->$inq();
		
		if($bpm['cfg']['tbm'] && !$this->sys['rsp']['data']) {
			switch ($bpm['tbt']) {
				case 'sql_plain':    // sql-plain
					$this->util_tbl__sys_qry();
					
					$sql = "select  {$this->sql['tot']}
                            from    {$this->sql['frm']}
                            where   1=1
                            {$this->sys['bpm']['str']}";
					
					$this->sys['rsp']['meta']['payload']['data']['rows'] = $this->util_db_sql($sql, 'cel') | 0;
                    $this->sys['bpm']['lim'] = $this->util_db_sql_limit('nav', 'nav');
					$sql = "select  {$this->sql['sel']}
                            from    {$this->sql['frm']}
                            where   1=1
                            {$this->sys['bpm']['str']}
                            {$this->sys['bpm']['ord']}
                            {$this->sys['bpm']['lim']}";
					$this->sys['rsp']['data'] = $this->util_db_sql($sql);
					break;
				
				case 'view':
					// $pky = implode(',', $this->sys['bpm']['pky']);
					$sql = "select  count(distinct ctms_hpl_id) total
                            from    {$this->sys['bpm']['tbl']}
                            where   1=1
						    {$this->sys['bpm']['str']}";
					$this->sys['rsp']['meta']['payload']['data']['rows'] = $this->util_db_sql($sql, 'cel') | 0;

					$this->sys['bpm']['lim'] = $this->util_db_sql_limit('nav', 'nav');
					$sql = "select  *
                            from    {$this->sys['bpm']['tbl']}
                            where   1=1
                            {$this->sys['bpm']['str']}
                            {$this->sys['bpm']['ord']}
                            {$this->sys['bpm']['lim']}";

					$this->sys['rsp']['data'] = $this->util_db_sql($sql);
					break;
				
				case 'reff':
					$col = isset($this->sys['bpm']['col'])      ? $this->sys['bpm']['col'] : str_replace('vwa_', '', $tbl) . ' name';
					$xtd = isset($this->sys['bpm']['col_xtd'])  ? $this->sys['bpm']['col_xtd'] : '';
					$etc = isset($this->sys['bpm']['col_etc'])  ? $this->sys['bpm']['col_etc'] : '';
					$whr = $this->sys['req']['arg'] ? parser_whr($this->sys['req']['arg']) : 'and 1=1';
					if($this->sys['bpm']['str'])
						$whr .= ' ' . $this->sys['bpm']['str'];
					$off = $this->sys['nav']['p'] ? $this->util_db_sql_limit() : '';
					$sql = "select  id, $col $xtd $etc
							from    $tbl
							where   id > 0
							and     sys_data_status_id = 1
							$whr
							$off";
					
					$this->sys['rsp']['data'] = $this->util_db_sql($sql);
					break;
				case 'reff_grp':
					$col = isset($this->sys['bpm']['col'])      ? $this->sys['bpm']['col'] : str_replace('vwa_', '', $tbl) . ' name';
					$xtd = isset($this->sys['bpm']['col_xtd'])  ? $this->sys['bpm']['col_xtd'] : '';
					$etc = isset($this->sys['bpm']['col_etc'])  ? $this->sys['bpm']['col_etc'] : '';
					$whr = $this->sys['req']['arg'] ? parser_whr($this->sys['req']['arg']) : 'and pid is null';
					$off = $this->sys['nav']['p']   ? $this->util_db_sql_limit() : '';
					$ord = $this->sys['bpm']['ord'];
					$sql = "select  id, pid, $col $xtd $etc
							from    $tbl
							where   id > 0
							and     sys_data_status_id = 1
							$whr
						    $ord
							$off";
					$this->sys['rsp']['data'] = $this->util_db_sql($sql);
					break;
				default:
					if($bpm['is_magic']) {
						$cfg['tot']  = 1;
						$cfg['arr']  = 1;
						if(isset($this->sys['bpm']['sp_arr']['sys_data_status_id']))
							$cfg['sys_data_status_id'] = $this->sys['bpm']['sp_arr']['sys_data_status_id'];
						$sql = $this->util_db_sql_magic($tbl, $cfg);
						
						$this->sys['rsp']['meta']['payload']['data']['rows'] = $this->util_db_sql($sql[0], 'cel') | 0;
						$this->sys['rsp']['data'] = $this->util_db_sql($sql[1]);
					}
					else {
						$whr = $bpm['has']['col_status'] ? "sys_data_status_id = 1" : "1=1";
						$sql = "select  count(*)
                                from    {$this->sys['bpm']['tbl']}
                                where   $whr
                                {$this->sys['bpm']['str']}";
						$this->sys['rsp']['meta']['payload']['data']['rows'] = $this->util_db_sql($sql, 'cel') | 0;
						$this->sys['bpm']['lim'] = $this->util_db_sql_limit();
						
						$sql = "select  *
                                from    {$this->sys['bpm']['tbl']}
                                where   $whr
                                {$this->sys['bpm']['str']}
                                {$this->sys['bpm']['ord']}
                                {$this->sys['bpm']['lim']}";
						$this->sys['rsp']['data'] = $this->util_db_sql($sql);
					}
					break;
			}
		}
		
		if (method_exists($this, 'sys_cmd__get_act_lst_xtd'))
			$this->sys_cmd__get_act_lst_xtd();
		
		if(isset($this->sys['rsp']['xtd']) && $this->sys['rsp']['xtd']) {
			$this->sys['rsp']['data'] =	array_merge($this->sys['rsp']['xtd'], $this->sys['rsp']['data']);
			unset($this->sys['rsp']['xtd']);
		}
		
		isset($this->sys['rsp']['meta']['payload']['data']['rows']) && $this->sys_cmd__get_act_lst_nav();
		
	}
	protected   function sys_cmd__get_act_lst_nav() {
		//FixMe: jika pgC = 2 dan total = 10, pgC seharusnya bernilai 1;
		
		$nav = array('pgC' => 1, 'pgP' => 0, 'pgN' => 0, 'pgL' => 1, 'pgT' => 1, 'pgS' => array());
		$nav['row'] = array_unique (array($this->sys['nav']['l'], 10, 25, 50, 100, 200));
		ksort($nav['row']);
		$tot = $this->sys['rsp']['meta']['payload']['data']['rows'];
		if($tot) {
			$l = $this->sys['nav']['l'];
			$c = $this->sys['nav']['p'];
			// $c = 11;
			// $tot=200;
			if($tot > $l) {
				$t = ceil($tot / $l);
				$nav['pgT'] = $t ? $t : 1;
				$nav['pgC'] = $c > $t ? $t : $c;
				$nav['pgP'] = $c == 1 ? 0  : $c - 1;
				$nav['pgN'] = $c + 1 <= $t ? $c + 1 : 0;
				$nav['pgL'] = $c == $t ? 0 : $t;
				$d = 5;
				$a = $c - $d < 0 ? 1 : $c - $d;
				$z = $c + $d > $t ? $t : $c + $d;
				for($i = $a; $i <= $z; $i ++)
					$nav['pgS'][$i] = $i;
				$x = 0;
				// $z = 10000;
				while($z > 1000) {
					if(floor($t / $z) > 1 && ceil($t / $z) <= 8) {
						$x = $z;
						break;
					}
					$z -= 1000;
				}
				if(! $x)
					while($z > 100) {
						if(floor($t / $z) > 2 && ceil($t / $z) <= 8) {
							$x = $z;
							break;
						}
						$z = floor($z / 2);
					}
				if(! $x) {
					$z = 100;
					while($z > 10) {
						if(floor($t / $z) > 2 && ceil($t / $z) <= 8) {
							$x = $z;
							break;
						}
						$z = floor($z / 2);
					}
				}
				if(! $x && ceil($t / 10) <= 8)
					$x = 10;
				if($x) {
					$i = 0;
					while($i < $t) {
						$nav['pgS'][$i] = $i;
						$i += $x;
					}
					unset($nav['pgS'][0]);
				}
				ksort($nav['pgS']);
			}
			elseif(empty($tot)) {
				// $nav = array('pgC' => 1, 'pgP' => 0, 'pgN' => 0, 'pgL' => 0, 'pgT' => 1, 'pgS' => array());
			}
		}
		
		$this->sys['nav'] = array_merge($this->sys['nav'], $nav);
	}
	
	// section  cmd-inquiry-ref
    public      function sys_cmd__get_ref($opt = true) {
		$plc = $this->sys['req']['pid'] ? "'frm','both'" : "'lst','lst_only','both'";
		$whr = '';
		if(method_exists($this, 'sys_cmd__get_ref_pre')) {
			$whr = " and sys_pick_ref_id in($this->sys_cmd__get_ref_pre('config'))";
		}
		
		$sql = "select    *
                from      ____sys_pick_ref
                where     sys_process_id = {$this->sys['req']['rid']}
                and       elm_placement in ($plc)
                $whr
                order by  sys_seq";
		if($rst = $this->db->query($sql)->result_array()) {
		    // $this->util_db_drv($rst[0]['sys_table_id']);

			foreach($rst as $k => $cfg) {
				$ref                            = $cfg['sys_pick_ref'];
				$this->sys['rsp']['ref'][$ref]  = $cfg;
				$this->sys['cfg']['fs']         = 1;
				
				$this->util_parser_jsd('jsd_ref', $cfg);
				$this->util_parser_jsd('jsd_alt', $cfg);
				
				switch($cfg['bpm']['init']) {
					case 0: // don't init
						$this->sys['rsp']['ref'][$ref] = $cfg;
						break;
					
					case 1: // direct
					case 2: // after-dat
						/*
                         * 1: init
                         */
						if(isset($cfg['bpm']['arr']['exc'])) {
							$cfg['exc'] = $cfg['bpm']['arr']['exc'];
							unset($cfg['bpm']['arr']['exc']);
						}
						
						$cfg['elm']['placeholder'] = $cfg['elm']['placeholder'] ?: ($cfg['elm']['title'] ?: $cfg['elm']['label']);
						$cfg['elm']['title']       = $cfg['elm']['title']       ?: $cfg['elm']['label'];
						
						/*
                         * 2: unset unneeded var
                         */
						unset($cfg['sys_process_id'], $cfg['sys_pick_ref_id'], $cfg['event_id']);
						// if(isset($this->sys['bpm']['sp_arr'][$cfg['col']['fid']]))
						//	unset($this->sys['bpm']['sp_arr'][$cfg['col']['fid']]);
						
						/*
                         * 3: forced consistency
                         */
						if ($cfg['col']['par'] && $cfg['elm_type'] == 'select')
							$cfg['elm_type'] = 'select-group';
						
						if (!$cfg['col']['par'] && $cfg['elm_type'] == 'select-group')
							$cfg['col']['par'] = 'pid';
						/*
                         * 4.1: normalise sp_arr
                         */
						if ($cfg['bpm']['arr']) {
							$parse_jsn = false;
							foreach ($cfg['bpm']['arr'] as $x => $y) {
								if(in_array($this->sys['req']['pid'], array('new', 'n3w'))) {
									if(preg_match('/dat__/', $y))
										unset($cfg['bpm']['arr'][$x]);
									elseif (preg_match('/__/', $y))
										$parse_jsn = true;
								}
								else {
									// skip index rsp__dat__xxx -> will prep later after get data (exec from sys_cmd__get_ref_dat)
									if ($y && $cfg['bpm']['init'] == 1)
										switch ($y) {
											case '#first':
											case '#last':
												break;
											
											default:
												if (preg_match('/__/', $y))
													$parse_jsn = true;
												break;
										}
								}
							}
							if($parse_jsn)
								$this->util_parser_var_sys($cfg['bpm']['arr'], false);
							
							foreach ($cfg['bpm']['arr'] as $col => $val) {
								if ($val && $cfg['bpm']['init'] == 1)
									switch ($val) {
										case '#first':
										case '#last':
											// take first-row or last row
											$asc    = $val == '#first' ? 'asc' : 'desc';
											$tbf    = $this->sys['tbl'][$cfg['sys_table_id']];
                                            $lim    = $this->util_db_sql_limit(1);
											$sql    = "select $col from {$tbf} where sys_data_status_id = 1 order by id $asc $lim";
											$cfg['bpm']['arr'][$col] = $this->util_db_sql($sql, 'cel');
											break;
									}
							}
						}
						
						/*
                         * 4.2: normalise sp_str
                         */
						
						if ($cfg['bpm']['str'] && preg_match('/req__(rid|pid|fid|xid|yid|zid)/', $cfg['bpm']['str'])) {
							$idx      = array('req__rid', 'req__pid', 'req__fid', 'req__xid', 'req__yid', 'req__zid');
							$val      = array($this->sys['req']['rid'], $this->sys['req']['pid'], $this->sys['req']['fid'], $this->sys['req']['xid'], $this->sys['req']['yid'], $this->sys['req']['zid']);
							$cfg['bpm']['str'] = str_replace($idx, $val, $cfg['bpm']['str']);
						}
						
						/*
                         * 4.3: normalise default-value
                         */
						if (isset($cfg['bpm']['def'])) {
							switch ($def= $cfg['bpm']['def']) {
								case '#first':
								case '#last':
									$asc = $cfg['bpm']['def'] == '#first' ? 'asc' : 'desc';
									$tbf    = $this->sys['tbl'][$cfg['sys_table_id']];
                                    $lim    = $this->util_db_sql_limit(1);
									$sql = "select id from {$tbf} where sys_data_status_id = 1 order by id $asc $lim";
									$cfg['bpm']['def'] = $this->util_db_sql($sql, 'cel');
									break;
							}
							
							switch ($cfg['elm_type']) {
								case 'date':
								case 'date-occur':
									$cfg['bpm']['def'] = date_iso8601(date_of($cfg['bpm']['def']));
									$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = date_between($cfg['bpm']['def']);
									break;
								case 'date-range':
									$range = explode(',', $cfg['bpm']['def']);
									$cfg['bpm']['def']['start'] = date_of($range[0]);
									$cfg['bpm']['def']['end']   = date_of($range[0]);
									$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = array('sp_def' => array('start' => date_iso8601($range[0]), 'end' => date_iso8601($range[1])));
									break;
								case 'select-multiple':
									$cfg['bpm']['def'] = explode(',', $cfg['bpm']['def']);
									$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = $cfg['bpm']['def'];
									break;
								case 'select-group-multi':
									$cfg['bpm']['def'] = explode(',', $cfg[$ref]);
									$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = $cfg['bpm']['def'];
									break;
								case 'select-tree':
									$pid = str_replace('_id', '_pid', $cfg['col']['fid']);
									$this->sys['bpm']['sp_arr'][$pid]               = $cfg['bpm']['def'];
									$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = $cfg['bpm']['def'];
									break;
								case 'hidden':
									$this->sys['bpm']['arr'][$cfg['col']['fid']] = $cfg['bpm']['def'];
									break;
								default:
									$def = json_decode($cfg['bpm']['def'], true);
									if (is_array($def)) {
										$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = parser_whr($def);
										$cfg['bpm']['def'] = null;
									}
									else
										$this->sys['bpm']['sp_arr'][$cfg['col']['fid']] = $cfg['bpm']['def'];
									break;
							}
						}
						
						
						/*
                         * 5.1 set sp_def
                         */
						if (isset($cfg['bpm']['def'])) {
							
						}
						
						/*
                         * 5.2: reset sp_def when value come from form
                         *
                         */
						if($this->sys['req']['rid'] && !$this->sys['req']['pid']) {
							if ($this->sys['bpm']['sp_arr']) {
								foreach ($this->sys['bpm']['sp_arr'] as $col => $val) {
									if ($val) {
										if (isset($this->sys['rsp']['ref'][$col]))
											$this->sys['rsp']['ref'][$col]['sp_def'] = $val;
										else {
											$col = str_replace('_id', '', $col);
											if (isset($this->sys['rsp']['ref'][$col]))
												$this->sys['rsp']['ref'][$col]['sp_def'] = $val;
										}
									}
								}
							}
						}

						/*
                         * 6.0 exec direct ref (init_flg: 0:no, 1:direct, 2:after_rsp_dat, 3:custom)
                         */

						/* implementation with localstorage capability
                         *
						 * if($cfg['bpm']['init'] == 1 && $this->sys['req']['out'] == 'html')
						 *   $opt = false;
                        */
						if ($opt && $cfg['bpm']['init'] == 1 && !in_array($cfg['elm_type'], array('hidden', 'free-search', 'date', 'date-range'))) {
							switch($cfg['elm_type']) {
								case 'select-group-multi':
									if($opts = $this->sys_cmd__get_ref_option($ref, $cfg)) {
										foreach($opts as $g => $o)
											$opt[$o['pid']][$o['id']] = $o['nm'];
									}
									$cfg['option'] = $opt;
									break;
								default:
									$cfg['option'] = $this->sys_cmd__get_ref_option($ref, $cfg);
									break;
							}
						}
						
						$this->sys['rsp']['ref'][$ref] = $cfg;
						break;

                    case 3: // custom-1
                    case 4: // custom-2
                        break;
				}
			}
			if(method_exists($this, 'sys_cmd__get_ref_xtd'))
				$this->sys_cmd__get_ref_xtd();
		}
		if ($this->sys['usr']['is_admin'])
			$this->sys['cfg']['fs'] = 1;
	}
	private     function sys_cmd__get_ref_option($ref, $cfg) {
		$arg = null;
		$out = array();
		$dat = array();
		$sql = false;
		// $cfg = $this->sys['rsp']['ref'][$ref];
		
		$xtd_col = '';
		if(is_array($cfg['elm']['attr'])) {
			foreach($cfg['elm']['attr'] as $k=>$v) {
				$xtd_col .= ", $v $k";
			}
		}
		
		if(isset($cfg['sys_table_id'])) {
			$tbl = $cfg['sys_table_id'];
            $this->util_db_drv($tbl);
			$sdb = $this->util_tbl_sdb($tbl);
			$tbf = "{$sdb}.{$cfg['sys_table_id']}";
			$col = str_replace(array('vwa_'),'', $tbl);
			if(preg_match('/pxd_order/', $cfg['bpm']['ord']))
				$cfg['bpm']['ord'] = str_replace('pxd_order', 'concat(lpad(ifnull(pid, id),8,"0"), lpad(id ,8,"0"))', $cfg['bpm']['ord']);
			$ord = $cfg['bpm']['ord'] ? $cfg['bpm']['ord'] : 'id';
			
			$whr = '';
			if(!isset($cfg['sp_arr']['sys_data_status_id'])) {
				switch($cfg['elm_type']) {
					case 'select-group':
					case 'select-tree':
						$whr = 'a.sys_data_status_id = 1 ';
						break;
					
					default:
						$whr = 'sys_data_status_id = 1 ';
						break;
				}
			}
			
			if($cfg['bpm']['arr'])
				$whr .= $this->util_parser_var_sys($cfg['bpm']['arr']);
			
			if($cfg['bpm']['str'] && $cfg['bpm']['str'] != '#all')
				$whr .= $cfg['bpm']['str'];
		}
		else {
			$tbl = $col = $ref;
			$sdb = $this->util_tbl_sdb($tbl);
			$tbf = "{$sdb}.{$tbl}";
			$ord = 'id';
			$whr = 'sys_data_status_id = 1 ';
			$cfg['col']['sid'] = $cfg['bpm']['arr'] = $cfg['bpm']['str'] = null;
			if(in_array($cfg['elm_type'], array('select-group', 'select-group-multi')))
				$cfg['col']['par'] = isset($cfg['col']['par']) ? $cfg['col']['par'] : 'pid';
		}
		
		switch($ref) {
			case 'is_active' :
				$dat = array(
					array('id' => 1,'nm' => 'Active'),
					array('id' => 0,'nm' => 'Disabled'));
				break;
			case 'is_visible' :
			case 'is_mandatory' :
			case 'is_required' :
				$dat = array(
					array('id' => 1,'nm' => 'Yes'),
					array('id' => 0,'nm' => 'No'));
				break;
			case 'is_internal' :
				$dat = array(
					array('id' => 1,'nm' => 'Internal'),
					array('id' => 0,'nm' => 'External'));
				break;
			case 'sex' :
				$dat = array(
					array('id' => 1,'nm' => 'Male'),
					array('id' => 0,'nm' => 'Female'));
				break;
			case 'slider_vendor' :
				/*  $dat = array(
                    array('id' => 'owl-carousel','nm' => 'OWL Carousel'),
                    array('id' => 'layer-slider','nm' => 'Layer Slider'),
                    array('id' => 'revolution-slider', 'nm' => 'Revolution Slider'),
                    array('id' => 'nivo-slider','nm' => 'Nivo Slider'));
                */
				$dat = array(array('id' => 'owl-carousel','nm' => 'OWL Carousel'));
				break;
			case 'elm_type' :
				$dat = array(
					array('id' => 'hidden',         'nm' => 'Hidden'),
					array('id' => 'list',           'nm' => 'List'),
					array('id' => 'checkbox',       'nm' => 'Checkbox'),
					array('id' => 'free-search',    'nm' => 'Free Search'),
					array('id' => 'date',           'nm' => 'Date'),
					array('id' => 'date-occur',     'nm' => 'Date Occurrence'),
					array('id' => 'date-range',     'nm' => 'Duration'),
					array('id' => 'select',         'nm' => 'Select'),
					array('id' => 'select-self',    'nm' => 'Select-Self'),
					array('id' => 'select-group',   'nm' => 'Select-Group'),
					array('id' => 'select-tree',    'nm' => 'Select-Tree'),
					array('id' => 'select-multiple','nm' => 'Select-Multiple')
				);
				break;
			case 'elm-placement' :
				$dat = array(
					array('id' => 'lst',    'nm' => 'List'),
					array('id' => 'frm',    'nm' => 'Form'),
					array('id' => 'both',   'nm' => 'All'));
				break;
			case 'init-flg' :
				$dat = array(
					array('id' => 0, 'nm' => 'No'),
					array('id' => 1, 'nm' => 'Direct'),
					array('id' => 2, 'nm' => 'After SYS.RSP.DAT'),
					array('id' => 3, 'nm' => 'Custom'));
				break;
			case 'link-type' :
				$dat = array(
					array('id' => 'Internal','nm' => 'Internal'),
					array('id' => 'External','nm' => 'Eksternal'));
				break;
			case 'menu-location':
				$dat = array(
					array('id' => 'main',   'nm' => 'Main'),
					array('id' => 'usr',    'nm' => 'User'));
				break;
			case 'menu-type' :
				$dat = array(
					array('id' => 'mega',       'nm' => 'Mega-Menu'),
					array('id' => 'dropdown',   'nm' => 'Dropdown-Menu'),
					array('id' => 'header',     'nm' => 'Group-Menu'),
					array('id' => 'link',       'nm' => 'Menu Item'));
				break;
			
			case 'year' :
				$a = $this->sys['cfg']['iop_app_year'];
				$z = date('Y') + 1;
				for($i = $a; $i <= $z; $i ++) {
					$out[] = array('id' => $i, 'nm' => $i);
				};
				break;
			case 'date-occur':
				$dat = array(
					array('cd' =>'td',      'id' => 'today',      'nm' => 'Today'),
					array('cd' =>'-1d',     'id' => '-1 day',     'nm' => 'Yesterday'),
					array('cd' =>'+1d',     'id' => '+1 day',     'nm' => 'Tomorrow'),
					array('cd' =>'tw',      'id' => 'this week',  'nm' => 'This Week'),
					array('cd' =>'tm',      'id' => 'this month', 'nm' => 'This Month'),
					array('cd' =>'ty',      'id' => 'this year',  'nm' => 'This Year'),
					array('cd' =>'-1w',     'id' => '-1 week',    'nm' => 'Last Week'),
					array('cd' =>'-30d',    'id' => '-30 day',    'nm' => 'Last 30 Days'),
					array('cd' =>'-1m',     'id' => '-1 month',   'nm' => 'Last Month'),
					array('cd' =>'-3m',     'id' => '-3 month',   'nm' => 'Last 3 Months'),
					array('cd' =>'-6m',     'id' => '-6 month',   'nm' => 'Last 6 Months'),
					array('cd' =>'-1y',     'id' => '-1 year',    'nm' => 'Last Year'),
					array('cd' =>'-2y',     'id' => '-2 year',    'nm' => 'Last 2 Years'),
					array('cd' =>'-3y',     'id' => '-3 year',    'nm' => 'Last 3 Years'),
					array('cd' =>'-5y',     'id' => '-5 year',    'nm' => 'Last 5 Years'),
					array('cd' =>'+1w',     'id' => '+1 week',    'nm' => 'Next Week'),
					array('cd' =>'+1m',     'id' => '+1 month',   'nm' => 'Next Month'),
					array('cd' =>'+1y',     'id' => '+1 year',    'nm' => 'Next Year')
				);
				break;
			
			case 'geo-origin' :
			case 'geo-year' :
				$sdb = $this->util_tbl_sdb('geo_land_bps');
				$sql = "select distinct $ref id, $ref nm from {$sdb}.geo_land_bps where $whr order by id";
				break;
			
			case 'orgz-tree' :
                $sdb = $this->util_tbl_sdb('hrm_orgz');
				$sql = "select id, org nm, pid from {$sdb}.hrm_orgz where $whr order by id";
				$tmp = flat2tree($this->util_db_sql($sql));
				$out = $tmp[$this->sys['app']['base_orgz']]['sub'];
				break;
		}
		
		if(!$out) {
			if($dat) {
				switch($cfg['elm_type']) {
					// case 'select' :
					case 'select-group' :
					case 'select-multiple' :
					case 'select-multi-group' :
						$out = rst2ref($dat);
						break;
					default :
						$out = $dat;
						break;
				}
			}
			elseif($sql)
				$out = rst2ref($this->util_db_sql($sql));
			else {
				if($cfg['col']['sid'])
					$col = $cfg['col']['sid'];

				/*
				if(in_array($this->sys['prc']['sys_tbl']['sys_table_type_id'], range(90, 93))) {
                    if (preg_match('/^(vap|vws)/', $tbf))
                        $sql = "select id, $col nm $xtd_col from {$tbf} where $whr order by $ord";
                    elseif ($cfg['bpm']['is_magic'])
                        $qry = $this->util_db_sql_magic($tbl, array('sp_arr' => $cfg['bpm']['arr'], 'sp_str' => $cfg['bpm']['str'], 'mgc' => $cfg['bpm']['is_magic']));
                }
				*/
				switch($cfg['elm_type']) {
					case 'select':
						$id = $cfg['col']['fid'] == $cfg['col']['sid'] ? $cfg['col']['fid'] : 'id';
						
						if($cfg['bpm']['is_magic']) {
							$qry = $this->util_db_sql_magic($tbl, array('sp_arr' => $cfg['bpm']['arr'], 'sp_str' => $cfg['bpm']['str'], 'mgc' => $cfg['bpm']['is_magic']));
							$sql = "select distinct $id id, $col nm{$xtd_col} from ($qry) xx order by $ord";
						}
						else {
							$sql = "select $id id, $col nm{$xtd_col} from {$tbf} where $whr order by $ord";
						}
						$out = $this->util_db_sql($sql);
						break;
					
					case 'select-multiple':
						$idx = $cfg['col']['rid'] ? $cfg['col']['rid'] : 'id';
						$sql = "select $idx id, $col nm $xtd_col from {$tbf} where $whr order by $ord";
						break;
					
					case 'select-self':
						$col = $cfg['col']['fid'];
						$sql = "select $col id, $col nm $xtd_col from {$tbf} where $whr order by $ord";
						break;
					
					case 'select-limited':
						$col = $tbl . '_id';
						if(isset($this->sys['rsp']['data'][$col])) {
							$idx = $this->util_db_qry($tbl, array('id' => $this->sys['rsp']['data'][$col]), $cfg['col']['sid']);
							$sql = "select id, $col nm $xtd_col from {$tbf} where $whr and id in ($idx) order by $ord";
						}
						break;
					
					case 'select-group':
					case 'select-group-multi':
						if ($cfg['col']['par'] == 'pid') {
							$ord = $ord =='id' ? 'a.id' : $ord;
							$sql = " select      a.id, a.$col nm, b.$col pid $xtd_col
                                     from       {$tbf} a
                                     left join  {$tbf} b on b.id = a.pid and a.sys_data_status_id = 1 and b.sys_data_status_id = 1
                                     where      $whr
                                     order by   $ord";
						}
						else {
							$qry = $this->util_db_sql_magic($tbl, array('sp_arr'=>$cfg['bpm']['arr'], 'sp_str'=>$cfg['bpm']['str'], 'mgc'=>$cfg['bpm']['is_magic']));
							$sql = "select id, $col nm, {$cfg['col']['par']} pid $xtd_col from ($qry) xy order by $ord";
						}
						
						break;
					
					case 'select-tree':
						if($cfg['col']['par'] == 'parent-only') {
							$pid = "a.$col pid";
							$sql = "select      distinct a.id, a.$col nm, if(b.pid, 'bold', 'child') cls, concat('i', a.tree) ind, $pid $xtd_col
                                    from	    {$tbf} a
                                    left join   {$tbf} b on a.id = b.pid
                                    where       $whr
                                    and  (a.pid is null or a.id in (select distinct pid from {$tbf} where pid is not null))
                                    order by $ord";
						}
						else {
							$pid = $cfg['col']['par'] ? $cfg['col']['par'] : '';
							$ord = $ord == 'id' ? 'id' : $ord;
							$sql = "select      id, $col nm, if(pid, 'bold', 'child') cls, concat('i', tree) ind, $pid $xtd_col
                                    from	    {$tbf}
                                    where       $whr
                                    order by $ord";
						}
						break;
					
					case 'list':
						$qry = $this->util_db_sql_magic($tbl, array('sp_arr'=>$cfg['bpm']['arr'], 'sp_str'=>$cfg['bpm']['str'], 'mgc'=>$cfg['bpm']['is_magic']));
						$sql = "select id, $col nm $xtd_col from ($qry) xz order by $ord";
						break;
				}
				
				if(!$out && $sql && $tmp = $this->util_db_sql($sql)) {
					switch($cfg['elm_type']) {
						case 'select-group':
							foreach($tmp as $k => $v)
								$out[$v['pid']][] = $v;
							break;
						
						case 'list':
						case 'select':
						case 'select-self':
						case 'select-tree':
						case 'select-limited':
						case 'select-multiple':
						case 'select-group-multi':
							$out = $tmp;
							break;
					}
				}
			}
		}
		
		if ($cfg['bpm']['def']) {
			switch ($cfg['bpm']['def']) {
				case '#first':
					$this->sys['rsp']['ref'][$ref]['bpm']['def'] = $out[0]['id'];
					break;
				case '#last':
					$max = count($out);
					$this->sys['rsp']['ref'][$ref]['bpm']['def'] = $out[$max-1]['id'];
					break;
			}
		}
		return $out;
	}
	private     function sys_cmd__get_ref_dat() {
		if($this->sys['rsp']['ref'])
			foreach($this->sys['rsp']['ref'] as $ref => $rfc) {
				// setup sp_def
				switch($rfc['elm_type']) {
					case 'select-multiple':
						if(!empty($this->sys['rsp']['data'][$rfc['col']['fid']]))
							$this->sys['rsp']['data'][$rfc['col']['fid']] = explode(',', $this->sys['rsp']['data'][$rfc['col']['fid']]);
						break;
					case 'select-group-multi':
						if($this->sys['rsp']['data'][$rfc['col']['fid']])
							foreach($this->sys['rsp']['data'][$rfc['col']['fid']] as $x=>$y)
								$rfc['bpm']['def'][$y] = $y;
						break;
					default:
					    if(isset($this->sys['rsp']['data'][$rfc['col']['fid']]) && $this->sys['rsp']['data'][$rfc['col']['fid']])
						    $rfc['bpm']['def'] =  $this->sys['rsp']['data'][$rfc['col']['fid']];
						break;
				}
				
				// setup option
				// init option
				switch ($rfc['bpm']['init']) {
					case 2:     // after rsp_dat
						if($this->sys['req']['pid'] && ! in_array($this->sys['req']['pid'], array('new', 'n3w'))) {
							$this->sys['cfg']['fs'] = 1;
							$this->util_parser_sys_rsp_dat($rfc['bpm']['arr']);
							$rfc['option'] = $this->sys_cmd__get_ref_option($ref, $rfc);
						}
						break;
					
					case 3: // custom-1
						$this->sys['cfg']['fs'] = 1;
						$rfc['bpm']['arr'] = $this->util_parser_var_dat($rfc['bpm']['arr']);
						$rfc['option'] = $this->sys_cmd__get_ref_option($ref, $rfc);
						break;
					
					case 4: //custom-2
						$this->sys['cfg']['fs'] = 1;
						if(method_exists($this, 'sys_cmd__get_ref_xtd'))
							$this->sys_cmd__get_ref_xtd($ref);
						$rfc['option'] = $this->sys_cmd__get_ref_option($ref, $rfc);
						break;
				}
				
				// xtd init option
				$col_xtd = json_decode($rfc['col']['xtd'], true);
				if(is_string($col_xtd)) {
					switch($col_xtd) {
						case 'unset_prev':
							if(!empty($this->sys['rsp']['data'][$rfc['col']['fid']])) {
								$val = $this->sys['rsp']['data'][$rfc['col']['fid']];
								foreach($rfc['option'] as $r => $i)
									if($i['id'] < $val)
										unset($rfc['option'][$r]);
							}
							break;
					}
				}
				else {
					if(isset($col_xtd['unset_prev'])) {
						$cond = $col_xtd['unset_prev']['cond'];
						$keys = array_keys($cond);
						$valc = $cond[$keys[0]];
					}
				}
				
				// omit item by criteria
				if(isset($rfc['omit'])) {
					switch ($rfc['elm_type']) {
						case 'list':
						case 'select':
						case 'select-self':
						case 'select-tree':
						case 'select-limited':
						case 'select-multiple':
						case 'select-group-multi':
							foreach($rfc['option'] as $i=>$o) {
								if(in_array($rfc['id'], $rfc['omit']))
									unset($rfc['option'][$i]);
							}
							break;
					}
				}
				
				unset(
					$rfc['sys_seq'],            $rfc['col']['sid'],   $rfc['sys_table_id'], $rfc['col']['xtd'],  $rfc['col']['par'],
					$rfc['elm']['placement'],   $rfc['bpm']['arr'],   $rfc['bpm']['str'],   $rfc['sp_ord'],      $rfc['bpm']['live'],
					$rfc['bpm']['is_magic'],    $rfc['bpm']['is_public'],$rfc['bpm']['arr']);
				
				$this->sys['rsp']['ref'][$ref] = $rfc;
			}
	}
	private     function sys_cmd__get_ref_man($ref, $cfg) {
		$ref = array_merge($this->util_db_qry('sys_process__ref', array('sys_pick_ref_id' => $ref)), $cfg);
	}
	
	// section  cmd-inquiry-menu
	protected   function sys_cmd__get_menu() {
		switch($this->sys['app']['menu']) {
			case 'proc' :   // via sys-process tree
				$this->sys_cmd__get_menu_proc();
				break;
			case 'menu':    // via sys_menu
                $this->sys_cmd__get_menu_menu();
				break;
		}
	}
	private     function sys_cmd__get_menu_proc() {
		$app  = $this->sys['app']['id'];
		$rid  = $this->sys['req']['rid'];
		$menu = array(
			'rpc'   => null,
			'menu'  => array(
				'tree'  => null
			));
		$tree = array();
		$rpc  = array();
		
		if(empty($this->sys['rpc'][$app])) {
			if ($m_prc = $this->sys_cmd__get_menu_proc_list()) {
			    $r_key = array_keys($m_prc)[0];
				foreach ($m_prc as $k => $v) {
					$this->util_parser_jsd('jsd_process', $v);
					if ($v['sys_process_type_id']) {
						if($v['sys_url']) {
                            if(preg_match('/#/', $v['sys_url']))
                                $v['sys_url'] = str_replace('#', $v['id'], $v['sys_url']);
						}
						else {
                            if($this->sys['app']['url'] && preg_match('/http/', $this->sys['app']['url']))
                                $v['sys_url'] = $this->sys['app']['url'];
                            else {
                                if($v['sys_prc']['is_ajax']) {
                                    if ($this->sys['app']['ctrl'] == 'iop')
                                        $v['sys_url'] = "/iop/{$v['id']}";
                                    else
                                        $v['sys_url'] = "/{$this->sys['app']['id_code']}/{$v['id']}";
                                }
                            }
						}
						if ($v['tbl_id']) {
							$v['sys_tbl']['id']         = $v['tbl_id'];
							// $v['sys_tbl']['pid']        = $v['tbl_pid'];
							// $v['sys_tbl']['tbl_code']   = $v['tbl_code'];
							// $v['sys_tbl']['schema_db']  = $v['schema_db'];
							$v['sys_tbl']['sys_table']  = $v['sys_table'];
							$v['sys_tbl']['sys_table_type_id']      = $v['sys_table_type_id'];
							$v['sys_tbl']['sys_table_pk_type_id']   = $v['sys_table_pk_type_id'];
							$v['sys_tbl']['sys_cache_id']           = $v['sys_cache_id'];
						}
                        if (!$v['jsd_table'] && $v['tbl_id'])
                            $v['jsd_table'] = $this->util_tbl_jsd($v['tbl_id']);

						if ($v['jsd_table']) {
							$jsd = json_decode($v['jsd_table'], true);
							
							if(json_last_error() == JSON_ERROR_NONE)
								$v['sys_tbl'] = array_merge($v['sys_tbl'], $jsd);
							unset($v['jsd_table']);
						}
						if ($this->sys['cfg']['dbg'] == 3 && $this->sys['usr']['is_super']) {
							$v['sys_aaa']['right'] = array('inq' => 1, 'ins' => 1, 'upd' => 1, 'del' => 1, 'bat' => 1, 'doc' => 1, 'exp' => 1);
						}
						else {
						    // $allow = $v['allow'];
							if ($v['sys_process_type_id'] == 11) {
								$pid = $v['pid'];
								if ($pid && isset($m_prc[$pid])) {
									$v['sys_aaa']['right']['inq'] = $m_prc[$pid]['inq'];
									$v['sys_aaa']['right']['ins'] = $m_prc[$pid]['ins'];
									$v['sys_aaa']['right']['upd'] = $m_prc[$pid]['upd'];
									$v['sys_aaa']['right']['del'] = $m_prc[$pid]['del'];
									$v['sys_aaa']['right']['bat'] = $m_prc[$pid]['bat'];
									$v['sys_aaa']['right']['doc'] = $m_prc[$pid]['doc'];
									$v['sys_aaa']['right']['exp'] = $m_prc[$pid]['exp'];
								}
							}
							else {
								$v['sys_aaa']['right']['inq'] = $m_prc[$v['id']]['inq'];
								$v['sys_aaa']['right']['ins'] = $m_prc[$v['id']]['ins'];
								$v['sys_aaa']['right']['upd'] = $m_prc[$v['id']]['upd'];
								$v['sys_aaa']['right']['del'] = $m_prc[$v['id']]['del'];
								$v['sys_aaa']['right']['bat'] = $m_prc[$v['id']]['bat'];
								$v['sys_aaa']['right']['doc'] = $m_prc[$v['id']]['doc'];
								$v['sys_aaa']['right']['exp'] = $m_prc[$v['id']]['exp'];
							}
						}
						$v['sys_aaa']['acm'] = isset($v['sys_aaa']['acm']) ? $v['sys_aaa']['acm'] : 0;
						if ($this->sys['usr']['is_admin']) {
							$v['sys_aaa']['meta'] = array_combine($this->acm_idx, str_split(str_pad(decbin($v['sys_aaa']['acm']), 6, '0', STR_PAD_LEFT)));
						}
						
						// isset($v['sys_tbl']['allow']) && $v['sys_aaa']['allow'] = $v['sys_tbl']['allow'];
						isset($v['sys_tbl']['audit']) && $v['sys_aaa']['audit'] = $v['sys_tbl']['audit'];
					}
					else {
						$v['sys_url'] = '#';
						$v['sys_aaa']['right']['inq'] = $m_prc[$v['id']]['inq'];
					}

					if(!$v['sys_aaa']['right']['inq']) {
                        unset($m_prc[$k]);
                    }
					else {
                        $v['sys_prc']['header'] = $v['sys_prc']['header'] ? $v['sys_prc']['header'] : $v['sys_process'];
                        // exclude: public standalone, public ext-process, admin ext-process
                        if (!in_array($v['sys_process_type_id'], $this->prc_hide['arr']))
                            $tree[$v['id']] = array(
                                'id'            => $v['id'],
                                'pid'           => $v['pid'],
                                'sys_url'       => $v['sys_url'],
                                'sys_process'   => $v['sys_process'],
                                'sys_process_type_id' => $v['sys_process_type_id'],
                                'sys_header'    => $v['sys_prc']['header'] ? $v['sys_prc']['header'] : $v['sys_process'],
                                'sys_header_sub' => $v['sys_prc']['header_sub'],
                                'sys_font'      => $v['sys_prc']['font'],
                                'sys_icon'      => $v['sys_prc']['icon'],
                                'sys_class'     => $v['sys_prc']['class'],
                                'is_ajax'       => $v['sys_prc']['is_ajax'],
                                'right'         => $v['sys_aaa']['right']
                            );

                        unset($v['rid'], $v['id_hash'], $v['sys_process_desc'], $v['sys_category_id'], $v['sys_sdlc_id'], $v['seq'], $v['sys_data_status_id']);
                        unset($v['sys_app_id'], $v['sys_app_ixd'], $v['sys_biz_logic_id'], $v['sys_process_id']);

                        unset($v['inq'], $v['ins'], $v['upd'], $v['del'], $v['bat'], $v['doc'], $v['exp']);
                        unset(
                            $v['is_allow_inq'], $v['is_allow_ins'], $v['is_allow_upd'], $v['is_cascade_del'],
                            $v['is_allow_del'], $v['is_allow_bat'], $v['is_allow_doc'], $v['is_allow_exp'],
                            $v['sys_tbl']['allow'], $v['sys_tbl']['audit']
                        );
                        unset(
                            $v['schema_db'], $v['tbl_id'], $v['tbl_pid'], $v['tbl_code'], $v['sys_table'],
                            $v['sys_table_type_id'], $v['sys_table_pk_type_id'],
                            $v['sys_table_pk'], $v['has'], $v['attr'], $v['sys_cache_id']);

                        if($v['sys_process_type_id'] || ($v['pid'] && isset($m_prc[$v['pid']])) && $m_prc[$v['pid']]['sys_data_status_id'])
                            $rpc[$v['id']] = $v;
                    }
				}
				
				$menu['menu']['tree'] = flat2tree($tree, $m_prc[$r_key]['pid']);
				foreach ($menu['menu']['tree'] as $k => $v) {
					if ((!$v['sys_process_type_id'] && !$v['sub']) || !$v['right']['inq'])
                        unset($menu['tree'][$k]);

                    // layer-1
                    if(is_array($v['sub'])) {
                        foreach($v['sub'] as $a => $b) {
                            if ((!$b['sys_process_type_id'] && !$b['sub']) || !$b['right']['inq'])
                                unset($menu['tree'][$k]['sub'][$b['id']]);

                            // layer-2
                            if(is_array($b['sub'])) {
                                foreach($b['sub'] as $c => $d) {
                                    if ((!$d['sys_process_type_id'] && !$d['sub']) || !$d['right']['inq'])
                                        unset($menu['tree'][$k]['sub'][$b['id']]['sub'][$d['id']]);

                                    // layer-3
                                    if(is_array($d['sub'])) {
                                        foreach($d['sub'] as $e => $f) {
                                            if ((!$f['sys_process_type_id'] && !$f['sub']) || !$f['right']['inq'])
                                                unset($menu['tree'][$k]['sub'][$b['id']]['sub'][$d['id']]['sub'][$f['id']]);

                                            // layer-4
                                            if(is_array($f['sub'])) {
                                                foreach($f['sub'] as $g => $h) {
                                                    if ((!$h['sys_process_type_id'] && !$h['sub']) || !$h['right']['inq'])
                                                        unset($menu['tree'][$k]['sub'][$b['id']]['sub'][$d['id']]['sub'][$f['id']]['sub'][$h['id']]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $menu['rpc']            = $rpc;
				$this->sys['rpc'][$app] = $menu;
				unset($m_prc, $tree, $menu);
				
				$rpc = json_encode($this->sys['rpc'], JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
				$this->db->update('aaa_account_jsd', array('jsd_process' => $rpc), array('aaa_account_id' => $this->sys['usr']['id']));
			}
			
			if ($rid) {
				$prx = $this->sys['rpc'][$app]['rpc'];
				if (isset($prx[$rid]))
					$this->sys['prc'] = $prx[$rid];
				else {
					// meaning $rid is string
					foreach ($prx as $k => $j)
						if ($j['id_code'] == $rid) {
							$this->sys['prc']        = $prx[$k];
							$this->sys['req']['rid'] = $k;
							break;
						}
				}
				$prx = null;
			}
		}
		
		// breadcrumb
		if($this->sys['req']['rid']) {
			// user target audience
			$this->aaa_uta();
			
			$bc  = array();
			$app = $this->sys['app']['id'];
			$rid = $this->sys['req']['rid'];
			$tmp = $this->sys['rpc'][$app]['rpc'][$rid];
			$pid = $tmp['pid'];
			$max = $tmp['tree'] - 1;
			$bc[$tmp['tree']] = $tmp['sys_process'];
			for($i=$max; $i > 0; $i--) {
				if($pid) {
					$tmp = $this->sys['rpc'][$app]['rpc'][$pid];
					$pid = $tmp['pid'];
					$bc[$i] = $tmp['sys_process'];
				}
			}
			ksort($bc);
			
			$this->sys['rsp']['bc'] = $bc;
			unset($bc, $tmp);
		}
		else {
			if(isset($this->sys['rpc'][$app]))
				$this->sys['rsp']['menu'] = $this->sys['rpc'][$app]['menu'];
		}
	}
	private     function sys_cmd__get_menu_proc_list() {
	/*
     * Only Load Current App / Module Menu
     */
		$rst = array();
		$whr = "a.sys_app_id = {$this->sys['app']['id']}";
		
		if (isset($this->sys['usr']['is_admin']) && $this->sys['usr']['is_admin'])
			$whr .= " or (sys_app_ixd = 'all' or sys_app_ixd like '%{$this->sys['app']['id_code']}%')";
		
		$acm = 5; // guest
		if(isset($this->sys['usr']['aaa_acm_id']))
			$acm = implode($this->sys['usr']['aaa_acm_id'], ',');
		
		$uni = "select a.id sys_process_id,
					max(ifnull(b.is_allow_inq, 0)) inq,
					max(ifnull(b.is_allow_ins, 0)) ins,
					max(ifnull(b.is_allow_upd, 0)) upd,
					max(ifnull(b.is_allow_del, 0)) del,
					max(ifnull(b.is_allow_bat, 0)) bat,
					max(ifnull(b.is_allow_doc, 0)) doc,
					max(ifnull(b.is_allow_exp, 0)) exp
			from   sys_process          a
			join   aaa_acm__sys_process b on a.id = b.sys_process_id and aaa_acm_id in ($acm)
			where  a.sys_data_status_id = 1
			and    is_allow_inq    		= 1
			and    ##
			group by a.id";
		
		if(isset($this->sys['usrx']['id'])) {
			if ($uni)
				$uni .= "\nunion\n";
			$uni
				.= "select  a.id sys_process_id,
					max(ifnull(b.is_allow_inq, 0)) inq,
					max(ifnull(b.is_allow_ins, 0)) ins,
					max(ifnull(b.is_allow_upd, 0)) upd,
					max(ifnull(b.is_allow_del, 0)) del,
					max(ifnull(b.is_allow_bat, 0)) bat,
					max(ifnull(b.is_allow_doc, 0)) doc,
					max(ifnull(b.is_allow_exp, 0)) exp
			from   sys_process a
			join   aaa_acm_ibac__sys_process b on a.id = b.sys_process_id and aaa_account_id = {$this->sys['usr']['id']}
			where   a.sys_data_status_id     = 1
			and     is_allow_inq    = 1
			and     ##
			group by a.id";
		}
		
		if(isset($this->sys['usrx']['aaa_acm_obac_crm_id'])) {
			$acm = implode($this->sys['usr']['aaa_acm_obac_crm_id'], ',');
			if($uni)
				$uni .= "\nunion\n";
			$uni .= "select  a.id sys_process_id,
						max(ifnull(b.is_allow_inq, 0)) inq,
						max(ifnull(b.is_allow_ins, 0)) ins,
						max(ifnull(b.is_allow_upd, 0)) upd,
						max(ifnull(b.is_allow_del, 0)) del,
						max(ifnull(b.is_allow_bat, 0)) bat,
						max(ifnull(b.is_allow_doc, 0)) doc,
						max(ifnull(b.is_allow_exp, 0)) exp
				from   sys_process a
				join   aaa_acm_obac_crm__sys_process b on a.id = b.sys_process_id and aaa_acm_obac_crm_id in ($acm)
				where   a.sys_data_status_id     = 1
				and     is_allow_inq    = 1
				and     ##
				group by a.id";
		}
		
		if(isset($this->sys['usrx']['aaa_acm_obac_hrm_id'])) {
			$acm = implode($this->sys['usr']['aaa_acm_obac_hrm_id'], ',');
			if($uni)
				$uni .= "\nunion\n";
			$uni .= "select  a.id sys_process_id,
						max(ifnull(b.is_allow_inq, 0)) inq,
						max(ifnull(b.is_allow_ins, 0)) ins,
						max(ifnull(b.is_allow_upd, 0)) upd,
						max(ifnull(b.is_allow_del, 0)) del,
						max(ifnull(b.is_allow_bat, 0)) bat,
						max(ifnull(b.is_allow_doc, 0)) doc,
						max(ifnull(b.is_allow_exp, 0)) exp
				from   sys_process a
				join   aaa_acm_obac_hrm__sys_process b on a.id = b.sys_process_id and aaa_acm_obac_hrm_id in ($acm)
				where   a.sys_data_status_id     = 1
				and     is_allow_inq    = 1
				and     ##
				group by a.id";
		}
		
		if(isset($this->sys['usrx']['aaa_acm_obac_ppo_id'])) {
			$acm = implode($this->sys['usr']['aaa_acm_obac_ppo_id'], ',');
			if($uni)
				$uni .= "\nunion\n";
			$uni .= "select  a.id sys_process_id,
						max(ifnull(b.is_allow_inq, 0)) inq,
						max(ifnull(b.is_allow_ins, 0)) ins,
						max(ifnull(b.is_allow_upd, 0)) upd,
						max(ifnull(b.is_allow_del, 0)) del,
						max(ifnull(b.is_allow_bat, 0)) bat,
						max(ifnull(b.is_allow_doc, 0)) doc,
						max(ifnull(b.is_allow_exp, 0)) exp
				from   sys_process a
				join   aaa_acm_obac_ppo__sys_process b on a.id = b.sys_process_id and aaa_acm_obac_ppo_id in ($acm)
				where   a.sys_data_status_id     = 1
				and     is_allow_inq    = 1
				and     ##
				group by a.id";
		}
		
		$uni = str_replace('##', $whr, $uni);
		if($uni) {
			$sql = "select  distinct x.*, y.*
                    from    ____sys_process  x
                    join    (   $uni )       y on x.id = y.sys_process_id
                    where   x.sys_app_id          = {$this->sys['app']['id']}
                    and     x.sys_data_status_id  = 1
                    order by x.seq, x.id";
			$rst = arr__id($this->util_db_sql($sql));
		}
		return $rst;
	}

	// section cmd-inquiry-uix
    private     function sys_cmd__get_uix() {
		$this->sys['uix']['elm'] = $this->sys['app']['index'] . '/elm';
        $this->sys['uix']['idx'] = $this->sys['app']['index'] . '/' . $this->sys['app']['home'];
		if($this->sys['req']['rid'] && isset($this->sys['prc']['sys_uix'])) {
			if(is_string($this->sys['prc']['sys_uix']))
				$uix = json_decode($this->sys['prc']['sys_uix'], true);
			else
				$uix = $this->sys['prc']['sys_uix'];
			unset($this->sys['prc']['sys_uix']);
			$this->sys['uix']['tab'] = isset($uix['tab']) ? $uix['tab'] : false;

			$this->sys['req']['pid'] ?
				$this->sys_cmd__get_uix_frm($uix) :
				$this->sys_cmd__get_uix_lst($uix);
			
			if(!$this->sys['prc']['pid'])
				$this->session->unset_userdata('uix_fwd');
			
			// if($this->input->post('act') && $this->input->post('act') == 'inq')
			//	$this->sys['uix']['hst'] = null;
			
			if($this->sys['nav']['n'])
				$this->sys['uix']['hst'] = null;
		}
		else {
			$this->sys['uix']['hst'] = null;
			$this->sys['uix']['dir'] = $this->sys['app']['index'];
			// $this->sys['uix']['gui'] = "index.tpl";
		}
	}
	private     function sys_cmd__get_uix_lst($uix) {
		//1. define default top + hst + gui + uxt + g2x + asd
		$this->sys['uix']['top'] = isset($uix['uib']['top']) ? $uix['uib']['top'] : true;
		if(isset($uix['uib']['dir'])) {
			$this->sys['uix']['dir'] = $uix['uib']['dir'];
		}
		else {
			if($this->sys['app']['path'])
				$this->sys['uix']['dir'] = "__{$this->sys['app']['path']}";
			else
				$this->sys['uix']['dir'] = "__{$this->sys['app']['id_code']}";
		};
		$this->sys['uix']['hst'] = isset($uix['uib']['hst']) ? $uix['uib']['hst'] : 'lst';
		$this->sys['uix']['nav'] = isset($uix['uib']['nav']) ? $uix['uib']['nav'] : null;
		$this->sys['uix']['seq'] = isset($uix['uib']['seq']) ? $uix['uib']['seq'] : null;
		$this->sys['uix']['gui'] = isset($uix['uib']['gui']) ? $uix['uib']['gui'] : 'portlet-lst_uxt.tpl';  // override when tab-mode
		$this->sys['uix']['uxt'] = isset($uix['uib']['uxt']) ? $uix['uib']['uxt'] : 'foo';
		$this->sys['uix']['cbx'] = isset($uix['uib']['cbx']) ? $uix['uib']['cbx'] : false;
		$this->sys['uix']['asd'] = isset($uix['uib']['asd']) ? $uix['uib']['asd'] : array('pos' => null, 'gui' => null);
		$this->sys['uix']['pcd'] = isset($uix['uib']['pcd']) ? $uix['uib']['pcd'] : $this->sys['uix']['pcd'];
		$this->sys['uix']['pct'] = isset($uix['uib']['pct']) ? $uix['uib']['pct'] : $this->sys['uix']['pct'];
		
		// 2. define target
		if(isset($uix['uib']['pcx'])) {
			// 2a. define target (random)
			if($uix['uib']['pcx'] == 'session' || $this->input->post())
				$pcx = $this->session->userdata('pcx');
			else {
				$rdm = txt_random();
				$pcx = array('pcm' => $rdm, 'pcd' => $rdm.'D', 'pct' => $rdm.'T');
				$this->session->set_userdata('pcx', $pcx);
			}
			$this->sys['uix']['pcd'] = $pcx['pcd'];
			$this->sys['uix']['pct'] = $pcx['pct'];
		}
		
		// 3. set search option
		$this->sys['uix']['fsHide']      = isset($uix['uib']['fsH']) ? $uix['uib']['fsH']: true; // fsHide
		$this->sys['uix']['fsRow']       = isset($uix['uib']['fsR']) ? $uix['uib']['fsR']: true; // fsRow
		$this->sys['uix']['fsDefault']   = isset($uix['uib']['fsD']) ? $uix['uib']['fsD']: true; // fsDefault
		
		// 4. define host with tab
		if ($this->sys['uix']['tab']) {
			$tab = $uix['tab'];
			if(in_array($tab, array('tabA', 'tabK', 'tab10', 'tab20', 'tab30', 'tab40', 'tab50', 'tab100', 'tab200', 'tab300', 'tab400', 'tab500'))) {
				if(isset($uix['uid']['nav'])) {}
				else {
					$this->sys['uix']['pcd'] = isset($uix['uib']['pcd']) ? $uix['uib']['pcd'] : $tab . 'D';
					$this->sys['uix']['pct'] = isset($uix['uib']['pct']) ? $uix['uib']['pct'] : $tab . 'T';
					$this->sys['uix']['pcm'] = isset($uix['uid']['pcm']) ? $uix['uid']['pcm'] : $tab;
				}
			}
			$this->sys['uix']['gui'] = isset($uix['uib']['gui']) ? $uix['uib']['gui'] : 'tbl_uxt.tpl';  // override: tab-mode
		}
	}
	private     function sys_cmd__get_uix_frm($uix) {
		//1. define default top + hst + gui + uxt + g2x + asd
		$this->sys['uix']['top'] = isset($uix['uid']['top']) ? $uix['uid']['top'] : true;
		if(isset($uix['uid']['dir'])) {
			$this->sys['uix']['dir'] = $uix['uid']['dir'];
		}
		else {
			if($this->sys['app']['path'])
				$this->sys['uix']['dir'] = "__{$this->sys['app']['path']}";
			else
				$this->sys['uix']['dir'] = "__{$this->sys['app']['id_code']}";
		};
		
		$this->sys['uix']['g2x'] = isset($uix['g2x']) ? $uix['g2x'] : 'lst';
		
		$this->sys['uix']['hst'] = isset($uix['uid']['hst']) ? $uix['uid']['hst'] : 'frm';
		$this->sys['uix']['nav'] = isset($uix['uid']['nav']) ? $uix['uid']['nav'] : null;
		$this->sys['uix']['gui'] = isset($uix['uid']['gui']) ? $uix['uid']['gui'] : 'portlet-frm_pcd.tpl';
		$this->sys['uix']['uxt'] = isset($uix['uid']['uxt']) ? $uix['uid']['uxt'] : null;
		$this->sys['uix']['asd'] = isset($uix['uid']['asd']) ? $uix['uid']['asd'] : array('pos' => null, 'gui' => null);
		
		$this->sys['uix']['pcm'] = isset($uix['uid']['pcm']) ? $uix['uid']['pcm'] : $this->sys['uix']['pcm'];
		$this->sys['uix']['pcd'] = isset($uix['uid']['pcd']) ? $uix['uid']['pcd'] : $this->sys['uix']['pcd'];
		$this->sys['uix']['pct'] = isset($uix['uid']['pct']) ? $uix['uid']['pct'] : $this->sys['uix']['pct'];
		$this->sys['uix']['hdr'] = isset($uix['uid']['hdr']) ? $uix['uid']['hdr'] : false;
		$this->sys['uix']['fnm'] = 'Form ' . $this->sys['prc']['sys_prc']['header'];
		if($this->sys['req']['pid'] == 'n3w')
			$this->sys['uix']['hdr'] = true;
		
		//2. define target
		if(isset($uix['uid']['pcx'])) {
			// 2a. define target (random)
			if($uix['uid']['pcx'] == 'session' || $this->input->post())
				$pcx = $this->session->userdata('pcx');
			else {
				$rdm = txt_random();
				$pcx = array('pcm' => $rdm, 'pcd' => $rdm.'D', 'pct' => $rdm.'T');
				$this->session->set_userdata('pcx', $pcx);
			}
			$this->sys['uix']['pcm'] = $pcx['pcm'];
		}
		
		// 3. define hst with tab
		if($this->sys['uix']['tab']) {
			$tab = $uix['tab'];
			// $this->sys['uix']['gui'] = isset($uix['uid']['gui']) ? $uix['uid']['gui'] : 'tbl_uxt.tpl';  // override: tab-mode
			$this->sys['uix']['pcm'] = isset($uix['uid']['pcm']) ? $uix['uid']['pcm'] : $tab;
			$this->sys['uix']['pcd'] = isset($uix['uid']['pcd']) ? $uix['uid']['pcd'] : $tab . 'D';
			$this->sys['uix']['pct'] = isset($uix['uid']['pct']) ? $uix['uid']['pct'] : $tab . 'T';
			
			switch ($tab) {
				case 'tabA':
					if(isset($uix['uid']['nav'])) {
						// uid -> has tab
						// $this->sys['uix']['hst'] = 'frm_nav';
						$this->sys['uix']['nav'] = $uix['uid']['nav'];
					}
					elseif(isset($uix['uib']['nav'])) {
						$this->sys['uix']['hst'] = null;
					}
					if (!empty($uix['uid']['pcm']))
						$this->sys['uix']['pcm'] = $uix['uid']['pcm'];
					break;
				
				default:
					// $this->sys['uix']['hst'] = 'frm_nav';
					$this->sys['uix']['pcm'] = isset($uix['uid']['pcm']) ? $uix['uid']['pcm'] : $tab;
					break;
			}
		}
		if(isset($uix['uid']['btnUpd']))
			$this->sys['uix']['btnUpd'] = $uix['uid']['btnUpd'];
		if(isset($uix['uid']['btnDel']))
			$this->sys['uix']['btnDel'] = $uix['uid']['btnDel'];
		if(isset($uix['uid']['btnCls']))
			$this->sys['uix']['btnCls'] = $uix['uid']['btnCls'];
		
		$this->sys['uix']['reload'] = isset($uix['reload']) ? $uix['reload'] : 0;
		if($this->sys['uix']['reload']) {
			if ($this->sys['uix']['reload'] == 'referer')
				$this->sys['uix']['reload'] = $this->session->userdata('referer');
			else {
				$this->sys['uix']['reload'] = $this->sys['req']['rid'];
				if ($this->sys['req']['pid'])
					$this->sys['uix']['reload'] .= '/0/' . $this->sys['uix']['pid'];
				foreach (array('fid', 'xid', 'yid', 'zid') as $k => $v)
					if ($this->sys['req'][$v])
						$this->sys['uix']['reload'] .= '/' . $this->sys['uix'][$v];
			}
		}
		$this->sys['uix']['callback']   = isset($uix['callback']) ? $uix['callback'] : 0;
	}
	
	// section cmd-svc
	public      function sys_cmd_unq() {
		// url: rid/0/tbl/<column>/<value>

        $tbl = $this->sys['req']['fid'] ? $this->sys['req']['fid'] : $this->sys['bpm']['tbl'];
        $sdb = $this->util_tbl_sdb($tbl);
        $col = $this->sys['req']['xid'];
        $val = $this->sys['req']['yid'];
        $whr = $this->input->post('filter') ? $this->input->post('filter') : '1=1';

        if(in_array($col, array('mobile', 'mobile_1', 'mobile_2', 'mobile_3')))
            $val = msisdn_prefix($val, '62');

        $sql = "select $col 'col' from {$sdb}.{$tbl} where $col = ? and $whr";
        $this->sys['rsp']['data'] = $this->util_db_sql($sql, array($val), 'cel');

		return $this->sys['rsp']['data'] ? 'n' : 'y';
	}
	public      function sys_cmd_inf() {
        // url: rid.inf/0/<tbl>/<arg_1>/<arg_2>
		if(method_exists($this, 'bpm_inf'))
			$this->bpm_inf();
		
		if(!$this->sys['rsp']['ack']['ack_code'] && empty($this->sys['rsp']['data'])) {
			$sql = null;
			switch($this->sys['req']['fid']) {
				case 'audiences':
					parse_str($_SERVER['QUERY_STRING'], $_GET);
					$a1  = $this->input->get('term');
					$out = json_encode($this->sys_var($this->sys), JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
					break;
				
				case 'logs':
					$rsp['dat'] = '';
					$rst        = $this->sys_var($this->sys);
					switch($this->sys['req']['pid']) {
						case 'inquiry':
							$tmp = json_decode($rst, true);
							if(is_array($tmp))
								foreach($tmp['dat'] as $k => $v) {
									if(is_array($k))
										foreach($k as $x => $y)
											if($x != 'event_id')
												$rsp['dat'] .= "<tr><th>$x</th><td>$y</td></tr>";
											else
												$rsp['dat'] .= "<tr><th>$k</th><td>$v</td></tr>";
								}
							else {
								
							}
							break;
						case 'insert':
						case 'delete':
						case 'update':
							if(isset($rst[0]['dat']) && $rst[0]['dat']) {
								foreach($rst as $k => $v) {
									$rsp['dat'] .= "<table><thead>
						<tr><th>TABLE</th><td>$v[tbl]</td></tr>";
									if($v['pky']) {
										$pky = json_decode($v['pky'], true);
										$rsp['dat'] .= "<tr><th>PKEY</th><td>";
										if(is_array($pky))
											foreach($pky as $px => $py)
												$rsp['dat'] .= "$px : $py<br />";
										else
											$rsp['dat'] .= $pky;
										$rsp['dat'] .= "</td></tr>";
									}
									$rsp['dat'] .= "</thead></table>";
									$rsp['dat'] .= array2Html(json_decode($v['dat'], true));
								}
							}
							// elseif(isset($rst['dat']))
							//	$rsp['dat'] .= array2Html(json_decode($rst['dat'], true));
							// else
							//	$rsp['dat'] .= array2Html($rst);
							
							$rsp['dat'] = str_replace('<table>', '<table class="table">', $rsp['dat']);
							break;
					}
					break;
			}
		}
	}
	public      function sys_cmd_opt() {
        // url: rid.opt/0/<tbl>/<column>/<value>
		if($this->sys['req']['out'] == 'grp') {
			if(method_exists($this, 'bpm_opt_grp'))
				$this->bpm_opt_grp();
			else
				$this->sys_cmd_opt_grp();
		}
		elseif(method_exists($this, 'bpm_opt')) {
			$this->bpm_opt();
		}
		
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['cfg']['tbm'] && !$this->sys['rsp']['data']) {
			$this->sys['req']['out'] = 'echo';
			$sql = null;
			$whr = 'and 1=1';
			if($this->sys['req']['arg']) {
				$whr = parser_whr($this->sys['req']['arg']);
				$col = array_keys($this->sys['req']['arg'])[0];
				$val = $this->sys['req']['arg'][$col];
			}
            switch($tbl = $this->sys['req']['res']) {
                case 'nik' :
                    $lim = $this->util_db_sql_limit(10);
                    $sql = "select  id, hrm_employee, path, hrm_orgz_short
                            from    vwa_hrm_employee
                            where   id like '%$val%' or hrm_employee like '%$val%' $lim";
                    break;

                case 'wfs_team':
                    $sql = "select * from $tbl where id = $val";
                    $tmp = $this->util_db_sql($sql, 'row');
                    if(isset($tmp['cfg_rule'])) {
                        $sql = "select 	a.id, aaa_account name, icon
                                from 	aaa_account a
                                join	aaa_acm__aaa_account    b on a.id = b.aaa_account_id and b.aaa_acm_id in ({$tmp['cfg_pic']})
                                join	aaa_acm                 c on c.id = b.aaa_acm_id
                                join	aaa_acm_clearance       d on d.id = c.aaa_acm_clearance_id
                                where   a.id > 99";
                    }
                    break;

                case 'aaa_acm_by_sys_app':
                    $lim = $this->util_db_sql_limit(10);
                    $sql = "select  id, aaa_acm opt
                                from    aaa_acm
                                where   sys_data_status_id = 1
                                and     (sys_app_id is null or sys_app_id = $val)
                                $lim";
                    break;

                default:
                    $tbl = str_replace('vwa_', '', $tbl);
                    $sdb = $this->util_tbl_sdb($tbl);
                    $sql = "select  id, $tbl name
                            from    {$sdb}.{$tbl}
                            where   id > 0
                            and     sys_data_status_id = 1
                            $whr";

                    break;
            }
			
			if($sql)
				$this->sys['rsp']['data'] = $this->util_db_sql($sql);
		}
	}
	public      function sys_cmd_opt_grp() {
		$sql = null;
		if(method_exists($this, 'bpm_opt_grp'))
			$this->bpm_opt_grp();
		
		if(!$this->sys['rsp']['ack']['ack_code'] && !$this->sys['rsp']['data']) {
			$tbl                      = $this->sys['req']['res'];
			$whr                      = $this->sys['req']['arg'] ? parser_whr($this->sys['req']['arg']) : 'and pid is null';
			$sql                      = "select  a.id, $tbl opt
										from    $tbl
										where   id > 0
										and     sys_data_status_id = 1
										$whr";
			$this->sys['rsp']['data'] = $this->util_db_sql($sql);
		}
	}
	public      function sys_cmd_acs() {
        // url: rid.acs/0/<tbl>?q=<value>
		if(method_exists($this, 'bpm_acs'))
            $this->bpm_acs();

		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['cfg']['tbm']) {
			$obj = $this->sys['req']['fid'];
			$q   = $this->sys['req']['q'];
			$hst = $this->sys['cfg']['host'];
			$sql = '';
			
			switch($obj) {
				case 'hrm_superior' :
				case 'hrm_supervisor' :
					$sdb = $this->util_tbl_sdb('vwa_hrm_employee');
                    $lim = $this->util_db_sql_limit(10);
					$sql = "select  id, hrm_employee nm, id_code, email, concat('$hst', media_avatar) img, hrm_orgz_position hrm_position, hrm_orgz_short hrm_orgz
                            from    $sdb.vwa_hrm_employee
                            where   sys_data_status_id = 1
                            and     hrm_employee like '%$q%'
                            $lim";
					break;
				
				case 'hrm_employee' :
					$sdb = $this->util_tbl_sdb('vwa_hrm_employee');
                    $lim = $this->util_db_sql_limit(10);
					$sql = "select  id, hrm_employee nm, id_code, email, concat('$hst', media_avatar) img, hrm_orgz_position hrm_position, hrm_orgz_division hrm_orgz
                            from    $sdb.vwa_hrm_employee
                            where   sys_data_status_id = 1
                            and     (hrm_employee like '%$q%' or hrm_orgz_division LIKE '%$q%')
                            $lim";
					break;
				
				case 'geo_administrative' :
					$sdb = $this->util_tbl_sdb('geo_administrative');
                    $lim = $this->util_db_sql_limit(10);
					$sql = "select  id, geo_administrative nm, concat(location_xtd, ' (', data_origin, ':', data_year, ')') xtd
                            from    $sdb.geo_administrative
                            where   sys_data_status_id = 1
                            and     (geo_administrative like '%$q%' or location_xtd like '%$q%')
                            order by id
                            $lim ";
					break;
				
				case 'postal_code' :
					$sdb = $this->util_tbl_sdb('geo_postal_code');
                    $lim = $this->util_db_sql_limit(10);
					$sql = "select  geo_postal_code id, concat(geo_county, ', ', geo_district, ', ', geo_regency, ', ', geo_province) nm
                            from    $sdb.geo_postal_code
                            where   sys_data_status_id = 1 and (geo_county like '%$q%' or geo_district like '%$q%' or geo_regency like '%$q%' or geo_province like '%$q%')
                            $lim";
					break;
				
				case 'ldap_users' :
					if($rst = $this->ldap_users($q))
						$rsp = array('ack' => true, 'msg' => '', 'dat' => $rst);
					break;
				
				case 'sys_search' :
					$sdb = $this->util_tbl_sdb('cms_index');
                    $lim = $this->util_db_sql_limit(10);
					$sql = "select id, cms_content nm from $sdb.cms_index where cms_index like '%$q%' $lim";
					break;
				
				default :
					$col = str_replace('vwa_', '', $obj);
					$sdb = $this->util_tbl_sdb($col);
					$sql = "select id, $col nm from {$sdb}.{$obj} where $col like '%$q%'";
					break;
			}
			if($sql && !$this->sys['rsp']['data']) {
				if($rst = $this->util_db_sql($sql)) {
					$this->sys['rsp']['data'] = $rst;
				}
				else
					$this->sys['rsp']['ack']['ack_code'] = true;
			}
		}
	}
	public      function sys_cmd_upl() {
		if(method_exists($this, 'bpm_upl'))
			$this->bpm_upl();
		
		$this->wfs->inq_task();
		
		if(!$this->sys['rsp']['ack']['ack_code']) {
			switch($this->sys['bpm']['aux']['cmd']) {
				case 'tpl':
					$row = 0;
					$sql = '';
					$tbl = $this->sys['bpm']['aux']['tbl'];
					
					switch($tbl) {
						case 'dms_doc_path':
							if($this->sys['req']['pid'] == $this->sys['req']['fid']) {
								$row = 1;
								$whr = "a.id = {$this->sys['req']['pid']}";
							}
							else
								$whr = "dms_doc_id = {$this->sys['req']['pid']}";
							
							$this->sys['rsp']['map'] = array();
							$dat                     = $this->util_db_qry($this->sys['bpm']['tbl'], $this->sys['req']['pid']);
							
							$sql = "select distinct dir_map_ixd ixd from sys_dir_map";
							foreach(rst2Array($sql) as $k => $v) {
								foreach(explode(',', $v['ixd']) as $x => $y) {
									$map[$y] = null;
								}
							}
							foreach($map as $x => $y) {
								if(isset($dat[$x]))
									$this->sys['rsp']['map'][$x] = $dat[$x];
							}
							$sdb = $this->util_tbl_sdb($tbl);
							$sql = "select  a.*, dms_doc, '' filename, icon
								from    {$sdb}.{$tbl} a
								join    dms_media_type   b on b.id = a.dms_media_type_id and b.sys_data_status_id = 1
								join    dms_doc          c on c.id = a.dms_doc_id and c.sys_data_status_id = 1
								where   $whr";
							break;
					}
					
					if($this->sys['rsp']['data'] = $this->util_db_sql($sql))
						foreach($this->sys['rsp']['data'] as $k => $v) {
							$tmp                                     = pathinfo($v['path']);
							$this->sys['rsp']['data'][$k]['filename'] = str_replace(array('_', '.'), ' ', $tmp['filename']);
						}
					
					// special treatment
					if($row)
						$this->sys['rsp']['data'] = $this->sys['rsp']['data'][0];
					break;
				
				case 'path':
					$sql = "select  dms_doc, a.path
                            from    {$this->sys['bpm']['aux']['tbl']} a
                             join   dms_doc b on b.id = a.dms_doc_id
                            where   a.id = {$this->sys['req']['pid']}";
					$rst = $this->util_db_sql($sql, 'row');
					$this->sys['bpm']['aux']['dms_doc']    = $rst['dms_doc'];
					$this->sys['bpm']['aux']['path']       = $rst['path'];
					$this->sys['bpm']['aux']['viewer_id']  = $this->util_upl_inf('dms_media_viewer', strtolower(pathinfo($this->sys['bpm']['aux']['path'], PATHINFO_EXTENSION)));
					$this->sys['bpm']['aux']['portlet']    = true;
					break;
				
				case 'del_paths':
				case 'del_path':
					$tbl  = $this->sys['bpm']['aux']['tbl'];
					$file = $this->util_db_qry($tbl, $this->sys['req']['pid'], 'path');
					
					if($this->sys['bpm']['aux']['cmd'] == 'del_paths')
						$this->sys_cmd__aud_exec('delete', $tbl, array('id' => $this->sys['req']['pid']));
					else
						$this->sys_cmd__aud_exec('update', $tbl, array('path' => null), array('id' => $this->sys['req']['pid']));
					
					$this->sys['rsp']['data'] = $this->db->affected_rows() ? $file : 'error';
					break;
				
				case 'upd_paths':
				case 'upd_path':
					$tbl = $this->sys['req']['pst']['tbl'];
					if($tbl == 'dms_doc_path') {
						$dat                           = array('dms_doc_id' => $this->sys['req']['pid'], 'path' => $this->sys['rsp']['data']['path'], 'dms_media_type_id' => $this->sys['rsp']['data']['dms_media_type_id']);
						$this->sys['rsp']['data']['id'] = $this->sys_cmd__aud_exec('insert', $tbl, $dat);
					}
					else {
						$this->sys['rsp']['ack']['ack_code'] = $this->sys_cmd__aud_exec('update', $tbl, array('path' => $this->sys['rsp']['data']['path']), array('id' => $this->sys['req']['pid']));
					}
					break;
			}
		}
		
		// check if this process should update task_stage
		if(!empty($this->sys['svc']['wfs_task_id']))
			$this->wfs->upd_task_stage_prog();
		
		// check this process should send notification
		if(!empty($this->sys['svc']['ntf_catalog_id']))  {
			$ntf = $this->sys['svc']['ntf_catalog_id'] == 1 ? 'sys' : $this->sys['svc']['ntf_catalog_id'];
			$this->util_ntf_init($ntf);
		}
	}
	
	// section cmd-crud
	public      function sys_cmd__aud() {
		// audit-trail for error or non-crud
		if ($this->sys['rsp']['ack']['ack_code'] || in_array($this->sys['req']['res'], array('get','login', 'logout', 'lockin', 'lockout'))) {
			$this->sys_cmd__aud_exec('event');
		}
		else {
			$map = array('post' => 'ins', 'patch' => 'upd', 'delete' => 'del');
			$cmd = $this->sys['req']['act'];
			$alw = $map[$cmd];
			$this->sys['event']['aaa_audit_trail_id'] = $this->sys['req']['act'];
			
			if ($this->sys['bpm']['aaa']['allow'][$alw]) {
				$tbl = $this->sys['bpm']['sys_table_id'];
				$this->sys['event']['event'] = $tbl;
				
				// if ($this->sys['rsp']['ack']['ack_code'] || in_array($this->sys['req']['act'], array('get')))
				//	$this->sys_cmd__aud_exec('event');
				// else {∂du
					$this->db->trans_begin();
					$cmd = 'sys_cmd__' . $this->sys['req']['act'];
					$this->$cmd();
					if ($this->sys['rsp']['ack']['ack_code'] || $this->db->trans_status() === false) {
						$this->db->trans_rollback();
						if ($cmd == 'delete') {
							$lnk = is_array($this->sys['ack']['msg']) ? implode(', ', $this->sys['ack']['msg']) : $this->sys['ack']['msg'];
							$this->sys['ack']['msg'] = "<br />This data already been used in the following table(s): <b>$lnk</b><br /><br />Please Manually delete the data in table(s), and try again";
						}
					}
					else {
						$this->db->trans_commit();
						
						// if (isset($tbl['sys_cache_id']) && $tbl['sys_cache_id'])
						//	$this->sys_tbl($tbl);
					}
					if (!$this->sys['rsp']['ack']['ack_code']) {
						$x = 1;
						// check if this process should update task_stage
						// if (!empty($this->sys['svc']['wfs_task_id']))
						//	$this->wfs->upd_task_stage_prog();
						
						// check this process should send notification
						//if (!empty($this->sys['svc']['ntf_catalog_id'])) {
						//	$ntf = $this->sys['svc']['ntf_catalog_id'] == 1 ? 'sys' : $this->sys['svc']['ntf_catalog_id'];
						//	$this->util_ntf_init($ntf);
						//}
					}
				//}
			}
			else {
				switch ($this->sys['req']['act']) {
					case 'post':
						$this->sys['rsp']['ack']['ack_code']    = 'AAA__102';
						$this->sys['rsp']['ack']['ack_message'] = 'Insufficient Privilege to Post New Data';
						break;
					case 'put':
					case 'patch':
						$this->sys['rsp']['ack']['ack_code']    = 'AAA__103';
						$this->sys['rsp']['ack']['ack_message'] = 'Insufficient Privilege to Modify Data';
						break;
					case 'delete':
						$this->sys['rsp']['ack']['ack_code']    = 'AAA__104';
						$this->sys['rsp']['ack']['ack_message'] = 'Insufficient Privilege to Delete Data';
						break;
				}
			}
		}
	}
	private     function sys_cmd__post() {
		$tbl = $this->sys['bpm']['sys_table_id'];
		if(is_array($this->sys['req']['pst']))
			foreach($this->sys['req']['pst'] as $k=>$v)
				if($v=='')
					unset($this->sys['req']['pst'][$k]);
		
		// check if file is uploaded
		/*
		if(isset($this->sys['req']['pst']['cms_catalog_id']))
			if((isset($this->sys['req']['pst']['path']) && $this->sys['req']['pst']['path']) || (isset($this->sys['req']['pst']['img_eom']) && $this->sys['req']['pst']['img_eom']))
				$this->util_upl_eom();
			else
				unset($this->sys['req']['pst']['path']);
		*/
		
		if (method_exists($this, 'sys_cmd__post_pre'))
			$this->sys_cmd__post_pre();
		
		$add = $tbl.'__post';
		if (method_exists($this, $add))
			$this->$add();
		
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['cfg']['tbm']) {
			$dat = $this->sys_cmd__aud_parser($tbl);
			$this->sys_cmd__aud_exec('post', $tbl, $dat);
		}
		
		if(!$this->sys['rsp']['ack']['ack_code']) {
			if (method_exists($this, 'sys_cmd__post_xtd'))
				$this->sys_cmd__post_xtd();
			
			$add = 'aud__' . str_replace('-', '__', $this->sys['bpm']['prc']) . '__post';
			if (method_exists($this, $add))
				$this->$add();
			
			// check if this process should create task
			// if(isset($this->sys['req']['pst']['new_task']) && !isset($this->sys['req']['pst']['wfs_task_id']))
			//	$this->wfs->add_task();
		}
	}
	private     function sys_cmd__patch() {
		$tbl = $this->sys['bpm']['sys_table_id'];
		
		// check if file is uploaded
		if(isset($this->sys['req']['pst']['cms_catalog_id'])) {
			if (isset($this->sys['req']['pst']['path']) && $this->sys['req']['pst']['path'])
				$this->util_upl_eom();
			else
				unset($this->sys['req']['pst']['path']);
		}
		
		if (method_exists($this, 'sys_cmd_upd_pre'))
			$this->sys_cmd_upd_pre();
		
		$upd = $tbl.'__patch';
		if (method_exists($this, $upd)) $this->$upd();
		
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['cfg']['tbm']) {
			$key = $this->sys_var('tbl_pky', $tbl, $this->sys['req']['pst']);
			$xyz = $this->sys_cmd__aud_parser($tbl);
			
			foreach($key as $k => $v)
				unset($xyz[$k]);
			
			if($xyz)
				$this->sys_cmd__aud_exec('patch', $tbl, $xyz, $key);
		}
		
		if(!$this->sys['rsp']['ack']['ack_code']) {
			if (method_exists($this, 'sys_cmd_upd_xtd'))
				$this->sys_cmd_upd_xtd();
			
			$upd = 'aud__' . str_replace('-', '__', $this->sys['bpm']['prc']) . '__upd';
			if (method_exists($this, $upd))
				$this->$upd();
		}
	}
	private     function sys_cmd__delete() {
		$tbl = $this->sys['bpm']['sys_table_id'];
		
		$this->sys['bpm']['cfg']['ccd'] = $this->util_db_qry('sys_table', $tbl, 'is_cascade_del');
		
		if (method_exists($this, 'sys_cmd_del_pre'))
			$this->sys_cmd_del_pre();
		
		$del = $tbl.'__del';
		if (method_exists($this, $del))
			$this->$del();
		
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['cfg']['tbm']) {
			// if($lnk = $this->util_db_check_dependency($tbl, $this->sys['req']['pid'])) {
			if($lnk = $this->util_db_check_delete($tbl, $this->sys['req']['pid'])) {
				// cascade delete
				if($this->sys['bpm']['cfg']['ccd']) {
					foreach($lnk as $k => $v) {
						try {
							$this->sys['rsp']['ack']['ack_code'] = $this->sys_cmd__aud_exec('delete', $v['tbl'], array($v['col'] => $this->sys['req']['pst']['id']));
						}
						catch(Exception $e) {
							$this->sys['rsp']['ack']['ack_code'] = false;
							$this->sys['ack']['msg'] = implode(', ', array_column($lnk, 'tbl'));
						}
					}
				}
				if(!$this->sys['rsp']['ack']['ack_code'] || !$this->sys['bpm']['cfg']['ccd']) {
					// exception if linked tables only matrix-table
					$ida = implode("','", array_column($lnk, 'tbl'));
					$sql = "select group_concat(sys_table separator ', ') tbl from sys_table where id in ('$ida') and sys_table_type_id not between 20 and 29";
					$rst = rst2Array($sql, 'cel');
					if($rst) {
						$this->sys['rsp']['ack']['ack_code'] = false;
						$this->sys['ack']['msg'] = $rst;
					}
					else {
						foreach($lnk as $k => $t)
							$this->sys['rsp']['ack']['ack_code'] = $this->sys_cmd__aud_exec('delete', $t['tbl'], $this->sys_var('tbl_pky', $t['tbl'], $this->sys['req']['pst']));
					}
					$lnk = null;
				}
				else
					$lnk = null;
			}
			
			if(!$this->sys['rsp']['ack']['ack_code'] && !$lnk)
				$this->sys['rsp']['ack']['ack_code'] = $this->sys_cmd__aud_exec('delete', $tbl, $this->sys_var('tbl_pky', $tbl, $this->sys['req']['pst']));
		}
		if(!$this->sys['rsp']['ack']['ack_code']) {
			if (method_exists($this, 'sys_cmd_del_xtd'))
				$this->sys_cmd_del_xtd();
			
			$del = 'aud__' . str_replace('-', '__', $this->sys['bpm']['prc']) . '__del';
			if (method_exists($this, $del))
				$this->$del();
		}
	}
    protected   function sys_cmd__replace($tbl, $dat) {
        $pky = $this->sys_var('tbl_pky', $tbl);

        $pid = $this->util_escape($this->sys['req']['pid']);
        $dat = is_array($dat) ? $dat : array($dat);
        $rlx = implode(',', $dat);
        $tbf = $tbl;
        $sql = "select $pky[1] from $tbf where $pky[0] = $pid and $pky[1] not in ($rlx)";

        if($del = $this->util_db_sql($sql)) {
            foreach ($del as $k => $v) {
                foreach ($dat as $x => $y)
                    if ($y == $v[$pky[1]])
                        unset($dat[$x]);
                if (!$this->sys['rsp']['ack']['ack_code'])
                    $this->sys_cmd__aud_exec('delete', $tbl, array($pky[0] => $this->sys['req']['pid'], $pky[1] => $v[$pky[1]]));
            }
        }

        if(!$this->sys['rsp']['ack']['ack_code'] && $dat) {
            $rlx = implode(',', $dat);
            $sql = "select $pky[1] from $tbf where $pky[0] = $pid and $pky[1] in ($rlx)";
            if($old = $this->util_db_sql($sql))
                foreach($old as $k => $v) {
                    foreach($dat as $x => $y)
                        if($y == $v[$pky[1]])
                            unset($dat[$x]);
                }
        }
        if(!$this->sys['rsp']['ack']['ack_code'] && $dat)
            foreach($dat as $k => $v)
                $this->sys_cmd__aud_exec('insert', $tbl, array($pky[0] => $pid, $pky[1] => $v));
    }
	protected   function sys_cmd__aud_parser($tbl, $dat = null) {
		$xyz = array();
		$pst = $dat ? $dat : $this->sys['req']['pst'];
		$prf = $this->util_tbl_col($tbl);
		
		foreach($prf as $f => $p) {
			$is_num  = preg_match('/int$/',  $p['type']) ? 1 : 0;
			
			switch($f) {
				case 'tree':
					$this->sys['bpm']['cfg']['tree'] = 1;
					break;
				case 'event_id':
					$xyz[$f] = $this->sys['event']['id'];
					break;
			}
			
			if(isset($pst[$f])) {
				if(is_array($pst[$f])) {
					foreach($pst[$f] as $k => $v) {
						switch ($f) {
							case 'sex':
							case preg_match('/^(is_|has_)/', $f) == true:
								$pst[$f][$k] = $v ? 1 : 0;
								break;
							
							case preg_match('/date$/', $p['type']) == true:
								$pst[$f][$k] = $v ? date_iso($v) : null;
								break;
							
							case preg_match('/(email)/', $f) == true:
								$pst[$f][$k] = strtolower($v);
								break;
							
							case preg_match('/(mobile|cellular)/', $f) == true:
								$pst[$f][$k] = msisdn_prefix($v);
								break;
							case preg_match('/(pub)/', $v) == true:
								$pst[$f][$k] = $v;
								break;
							default:
								$pst[$f][$k] = $is_num ? var_cast($v) : strtoupper($v);
								break;
						}
					}
					$xyz[$f] = implode(',', $pst[$f]);
				}
				else {
					switch ($f) {
						case 'sex':
						case preg_match('/^(is_|has_)/', $f) == true:
							$xyz[$f] = $pst[$f] ? 1 : 0;
							break;
						
						case preg_match('/(email)/', $f) == true:
							$xyz[$f] = strtolower($pst[$f]);
							break;
						
						case preg_match('/(mobile|cellular)/', $f) == true:
							$xyz[$f] = msisdn_prefix($pst[$f]);
							break;
						
						case preg_match('/^(date).$/', $p['type']) == true:
							$xyz[$f] = $pst[$f] ? date_iso($pst[$f]) : null;
							break;
						
						//case preg_match('/(date|datetime)/', $p['type']) == true:
						//	$xyz[$f] = $pst[$f] ? $pst[$f] : null;
						//	break;
						
						case preg_match('/(pub)/', $pst[$f]) == true:
							$xyz[$f] = $pst[$f];
							break;
						
						default:
							if($pst[$f])
								$xyz[$f] = $is_num ? var_cast($pst[$f]) : strtoupper($pst[$f]);
							break;
					}
				}
			}
		}
		
		return $xyz;
	}
    protected   function sys_cmd__aud_exec($aud = 'event', $tbl = null, $dat = array(), $key = array()) {
        $dbg        = 0;
        $log        = 0;
        $cud        = false;
        $prc        = $this->sys['bpm'];
        $err_no     = null;
        $err_msg    = null;
        $err_cls    = 'Database Error';

        if(empty($this->sys['event']['aaa_audit_trail_id']) && in_array($aud, array('post', 'put', 'patch', 'delete')))
            $this->sys['event']['aaa_audit_trail_id'] = $aud;

        if(!$this->sys['event']['is_log'] && $this->sys['event']['aaa_audit_trail_id'] && $aud != 'event') {
            $this->sys_cmd__aud_exec('event');
        }

        if($tbl)
            $sdb = $this->util_tbl_sdb($tbl);
        $ldb = $this->util_tbl_sdb('event');
        switch($aud) {
            case 'event':
                $sql = "select 	is_aud, sys_data_status_id
                        from 	aaa_audit_trail
                        where 	id = '{$this->sys['event']['aaa_audit_trail_id']}'";
                $row = $this->util_db_sql($sql, 'row', 'void');

                if($row['is_aud'] || $row['sys_data_status_id']) {
                    $dtx                            = $this->sys_cmd__aud_parser("event", $this->sys['event']);
                    $this->sys['event']['is_log']   = 1;
                    $ack = $this->db->insert("{$ldb}.event", $dtx);

                    $dtx                    = $this->sys_cmd__aud_parser("event_ext", $this->sys['event']);
                    $dtx['event_id']        = $this->sys['event']['id'];
                    $dtx['event_ts_out']    = time();
                    $ack = $this->db->insert("{$ldb}.event_ext", $dtx);
                }
                break;

            case 'post' :
            case 'post_batch' :
	            $prc['aaa']['audit']['ins'] = 1;
                $dbg = 0;
                $log = $prc['aaa']['audit']['ins'];
                
	            $dtx = $this->sys_cmd__aud_parser($tbl, $dat);
	        
                $cud = array(
                	'aud'   => 'ins', 'sys_table_id' => $tbl,
                    'dat'   => json_encode($dtx, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT),
                    'rows'  => 0, 'event_id' => $this->sys['event']['id']);
                
                if(isset($dtx[0]) || $aud == 'post_batch') {
                    $ack = $this->db->insert_batch("{$sdb}.{$tbl}", $dtx);
                    if(!$ack)
                        $this->sys_cmd__aud_error();
                    else
                        $cud['rows'] = $this->db->affected_rows();
                }
                else {
                    switch($prc['pkt']) {
                        case 1: // auto_increment
                            unset($dtx['id']);
                            break;

                        case  2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
                        case 10:case 11:case 12:case 13:case 14:case 15:case 16: // semi_increment
	                        $inc = 1;
	                        $whr = '';
	                        if(!empty($dtx['pid']))
	                            $whr = "where pid = {$dtx['pid']}";
	                        else
	                            $inc = str_replace('Semi Serial Inc ', '', $prc['pkt']) + 0;
	
	                        $sql = "select max(id) + $inc new_id from {$sdb}.{$tbl} $whr";
	                        $dtx['id'] = rst2Array($sql, 'cel');
                        break;
                    }

                    $ack = $this->db->insert("{$sdb}.{$tbl}", $dtx);
                    if(!$ack)
                        $this->sys_cmd__aud_error();

                    if(!$this->sys['rsp']['ack']['ack_code'] && $prc['pkt']) {
	                    $col = $tbl . '_id';
                        if($prc['pkt'] == 1) {
	                        $dtx['id'] = $this->db->insert_id();
	                        $this->sys['req']['pst']['id'] = $dtx['id'];
	                        $this->sys['req']['pst'][$col] = $dtx['id'];
	                        $this->sys['bpm']['aux'][$col] = $dtx['id'];
                        }
                        $pky = $this->util_tbl_pky($tbl, $dtx);
                        $cud['pky'] = json_encode($pky, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
                        switch ($tbl) {
                            case 'dms_doc':
                                $this->sys['bpm']['aux']['lcs_doc_id'] = $dtx['id'];
                                break;

                            case 'wfs_task':
                                $this->sys['svc']['wfs']['wfs_task_id'] = $dtx['id'];
                                break;
                        }
                    }

                    if(isset($this->sys['bpm']['cfg']['tree']))
                        $this->util_db_upd_tree($tbl, $dtx['id'], $dtx['pid']);

                    $cud['rows'] = 1;
                }
                break;

            case 'put' :
	        case 'patch' :
		        $dbg = 0;
		        $log = $prc['aaa']['audit']['upd'];
		        $tmp = array();
		        $arc = array();
		        $pev = null;
		        $old = $this->db->get_where("{$sdb}.{$tbl}", $key)->result_array();
		        $dtx = $this->sys_cmd__aud_parser($tbl, $dat);

		        if(isset($old[0]))
			        $tmp = array_diff_assoc($dtx, $old[0]);
		
		        if($tmp) {
			        $pev = $old[0]['event_id'];
			        foreach($tmp as $k => $v)
				        $arc[$k] = array('old' => $old[0][$k], 'new' => $v);
		        }
		
		        $cud = array('event_id' => $dtx['event_id'], 'event_pid' => $pev, 'aud' => $aud, 'sys_table_id' => $tbl,
		                     'pky' => json_encode($key, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT),
		                     'dat' => json_encode($arc, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT), 'rows' => 0);
		        $ack = $this->db->update("{$sdb}.{$tbl}", $dtx, $key);
				file_log($this->db->last_query());
		        if(!$ack)
			        $this->sys_cmd__aud_error();
		
		        if(isset($this->sys['bpm']['cfg']['tree']))
			        $this->util_db_upd_tree($tbl, $dtx['id'], $dtx['pid']);
		
		        $cud['rows'] = $this->db->affected_rows();
		        break;
		        
            case 'patch_matrix':
                $dbg = 0;
                $prv = $this->db->get_where("{$sdb}.{$tbl}", $key);

                $ttt = array_keys($key);
                $pky = $ttt[0];
                $ttt = array_values(array_diff($prv->list_fields(), array($pky, 'event_id')));
                $clx = $ttt[0];
                if($prv->num_rows()) {
                    foreach($prv->result_array() as $k => $v) {
                        unset($v[$pky], $v['event_id']);
                        $old[] = $v[$clx];
                    }
                    if($del = array_diff($old, $dat)) {
                        foreach ($del as $k => $v) {
                            $ttt = array_values($key);
                            $this->sys_cmd__aud_exec('delete', $tbl, array($pky => $ttt[0], $clx => $v));
                        }
                    }

                    if($ins = array_diff($dat, $old)) {
                        foreach ($ins as $k => $v) {
                            $ttt = array_values($key);
                            $this->sys_cmd__aud_exec('post', $tbl, array($pky => $ttt[0], $clx => $v));
                        }
                    }
                }
                else {
                    foreach($dat as $k => $v) {
                        $ttt = array_values($key);
                        $dat[$k] = array($pky => $ttt[0], $clx => $v, 'event_id' => $this->sys['event']['id']);
                    }

                    $this->sys_cmd__aud_exec('post_batch', $tbl, $dat);
                }
                break;

            case 'replace' :
            	// insert or update
				// key should be compound pkey
                $dbg = 0;
                $old = $this->db->get_where("{$sdb}.{$tbl}", $key);
				$old = $old ? $old->row_array() : array();
	            $cud = array(
                	'aud' => $aud, 'sys_table_id' => $tbl,
                    'pky' => json_encode($key, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT),
                    'dat' => json_encode($old, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT), 'rows' => count($old), 'event_id' => $this->sys['event']['id']);
	            
                if($old)
                    $this->sys_cmd__aud_exec('patch', $tbl, $dat, $key);
                else
                    $this->sys_cmd__aud_exec('post', $tbl, array_merge($dat, $key));
                break;
                
	        case 'replace_matrix':
		        // dat are multi-rows
	        	$dbg = 0;
		        $old = $this->db->get_where("{$sdb}.{$tbl}", $key)->result_array();

		        if($old) {
		        	// check m-key exists
		        	foreach($old as $k=>$v) {
		        		$del    = true;
		        	    $o_key  = $this->util_tbl_pky($tbl, $v);
		        	    foreach($dat as $i=>$r) {
		        	    	$m_key = $this->util_tbl_pky($tbl, $r);
		        	    	if($o_key === $m_key)
		        	    		$del = false;
			            }
		        	    
		        	    if($del) {
		        	    	unset($old[$k]);
				            $this->sys_cmd__aud_exec('delete', $tbl, $o_key);
			            }
			        }
		        }
		        
		        // replace with current
		        foreach ($dat as $k => $v) {
		        	$dat_x = array_merge($v, $key);
			        $r_key = $this->util_tbl_pky($tbl, $dat_x);

			        if($old)
				        foreach($old as $i=>$r) {
					        $o_key = $this->util_tbl_pky($tbl, $r);
					        if($r_key === $o_key)
					            $this->sys_cmd__aud_exec('patch', $tbl, $v, $r_key);
				        }
			        else {
				        $this->sys_cmd__aud_exec('post', $tbl, $dat_x);
			        }
		        }
		        break;
          
	        case 'delete' :
                $dbg = 0;
                $log = $prc['aaa']['audit']['del'];
                $key = $dat;
                $idx = array_keys($key);
                if(count($key) == 2 && is_array($key[$idx[1]])) {
                    $this->db->where($idx[0], $key[$idx[0]]);
                    $this->db->where_in($idx[1], $key[$idx[1]]);
                    $rst = $this->db->get("{$sdb}.{$tbl}");
                }
                else
                    $rst = $this->db->get_where("{$sdb}.{$tbl}", $key);

                if($rst->num_rows()) {
                    try {
                        $dtx = $rst->result_array();
                        $cud = array('aud' => $aud, 'sys_table_id' => $tbl,
                            'pky' => json_encode($key, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT),
                            'dat' => json_encode($dtx, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT), 'rows' => 0, 'event_id' => $this->sys['event']['id']);
                        if(count($key) == 2 && is_array($key[$idx[1]])) {
                            $this->db->where($idx[0], $key[$idx[0]]);
                            $this->db->where_in($idx[1], $key[$idx[1]]);
                            $ack = $this->db->delete("{$sdb}.{$tbl}");
                        }
                        else
                            $ack = $this->db->delete("{$sdb}.{$tbl}", $key);

                        if(!$ack)
                            $this->sys_cmd__aud_error();
                        else
                            $cud['rows'] = $this->db->affected_rows();

                        foreach($dtx as $k=>$v)
                            if(isset($v['path']) && !preg_match('/^http/', $v['path']))
                                @unlink($v['path']);

                    }
                    catch(Exception $e) {
                        $err_no  = $this->db->_error_number();
                        $err_msg = $this->db->_error_message();
                    }
                }
                break;

            case 'error' :
                $log = $this->db->get_where('aaa_audit_trail', array('id' => 'error'))->row()->sys_data_status_id ? 1 : 0;
                $dtx = array('sys_response_code_id' => $tbl);
                if($log)
                    $this->db->update("{$ldb}.event", $dtx, array('id' => $this->sys['event']['id']));
                break;
        }
        if($dbg)
            echo $this->db->last_query() . '<br /><br />';

        if($this->sys['rsp']['ack']['ack_code']) {
            if(!$this->sys['event']['is_log'])
                $this->sys_cmd__aud_exec('event');
            else {
            	/*
                $this->db->update("{$ldb}.event", array('sys_response_code_id' => $this->sys['rsp']['ack']['ack)=_code']), array('id' => $this->sys['event']['id']));
                if ($err_no)
                    $err_cls = 'HTTP Error';
                $this->db->insert("{$ldb}.event_error", array(
                	'error_class'   => $err_cls,
	                'error_no'      => $err_no,
	                'message'       => $err_msg,
	                'event_id'      => $this->sys['event']['id']));
            	*/
            }
        }
        else {
            if($log && $cud && in_array($aud, array('post','put','patch','delete'))) {
                $this->db->insert("{$ldb}.event_crud", $cud);
            }
        }
    }
    private     function sys_cmd__aud_error() {
        $error = $this->db->error();
        $this->sys['rsp']['ack']['ack_code']    = 'DB_CRUD';
        $this->sys['rsp']['ack']['ack_message'] = $error['message'];
	    $this->sys['err']['error_code']         = $error['code'];
        $this->sys['err']['error_msg'][]        = $error['message'];
        $this->sys['err']['error_msg'][]        = $this->db->last_query();
        $dbg_trace                              = debug_backtrace(0, 3);
        unset($dbg_trace[0]);
        foreach($dbg_trace as $k => $v) {
            $dbg_trace[$k]['file'] .= ", line {$v['line']}";
            unset($dbg_trace[$k]['line'], $dbg_trace[$k]['object'], $dbg_trace[$k]['type']);
        }
        $this->sys['err']['error_msg'][]        = $dbg_trace;
        // if(ENVIRONMENT != 'production')
        //    dump($this->sys['err'], '#');
    }
	protected   function xxx_cmd__post_parser($tbl, $dat = null) {
		$xyz = array();
		$pst = $dat ? $dat : $this->sys['req']['pst'];
		$prf = $this->util_tbl_col($tbl);
		
		foreach($prf as $f => $p) {
			$is_num  = preg_match('/int$/',  $p['type']) ? 1 : 0;
			
			switch($f) {
				case 'tree':
					$this->sys['bpm']['cfg']['tree'] = 1;
					break;
				case 'event_id':
					$xyz[$f] = $this->sys['event']['id'];
					break;
			}
			
			if(isset($pst[$f])) {
				if (in_array($f, array('sex')) || preg_match('/^(is_|has_)/', $f))
					$pst[$f] = $pst[$f] ? 1 : 0;
				elseif (preg_match('/date$/', $p['type']))
					$pst[$f] = $pst[$f] ? date_iso8601($pst[$f]) : null;
				
				switch($f) {
					case 'mobile':
						$pst[$f] = msisdn_prefix($pst[$f]);
						break;
				}
				
				if(is_array($pst[$f])) {
					foreach($pst[$f] as $k => $v) {
						if(preg_match('/(email)/', $f))
							$pst[$f][$k] = $v;
						else
							$pst[$f][$k] = $is_num ? var_cast($v) : strtoupper($v);
						if(!preg_match('/(email)/', $f))
							$pst[$f][$k] = $is_num ? var_cast($v) : strtoupper($v);
					}
					$xyz[$f] = implode(',', $pst[$f]);
				}
				else {
					if($pst[$f] === '' || is_null($pst[$f]))
						$xyz[$f] = null;
					else {
						if(preg_match('/(email)/', $f))
							$xyz[$f] = $pst[$f];
						else
							$xyz[$f] = $is_num ? var_cast($pst[$f]) : strtoupper($pst[$f]);
					}
				}
			}
		}
		return $xyz;
	}
	protected   function xxx_cmd__put_parser($tbl, $dat=null) {
		$xyz = array();
		$pst = $dat ? $dat : $this->sys['req']['pst'];
		$prf = $this->util_tbl_col($tbl);
		
		foreach($prf as $f => $p) {
			$is_num = preg_match('/int$/', $p['type']) ? 1 : 0;
			if(in_array($f, array('sex')) || preg_match('/^(is_|has_)/', $f))
				$pst[$f] = isset($pst[$f]) && $pst[$f] ? 1 : 0;
			elseif (preg_match('/date$/', $p['type']))
				$pst[$f] = isset($pst[$f]) && $pst[$f] ? date_iso8601($pst[$f]) : null;
			switch($f) {
				case 'mobile':
					$pst[$f] = format_phone_id($pst[$f]);
					break;
				
				case 'tree':
					$this->sys['bpm']['cfg']['tree'] = 1;
					break;
				
				case 'event_id':
					$pst[$f] = $this->sys['event']['id'];
					break;
			}
			
			if(isset($pst[$f])) {
				if(is_array($pst[$f])) {
					foreach($pst[$f] as $k => $v) {
						if(!preg_match('/(email)/', $f))
							$pst[$f][$k] = $is_num ? var_cast($v) : strtoupper($v);
					}
					$xyz[$f] = implode(',', $pst[$f]);
				}
				else {
					if($pst[$f] === '' || is_null($pst[$f]))
						$xyz[$f] = null;
					else {
						if(preg_match('/(email)/', $f))
							$xyz[$f] = $pst[$f];
						else
							$xyz[$f] = $is_num ? var_cast($pst[$f]) : strtoupper($pst[$f]);
					}
				}
			}
		}
		
		return $xyz;
	}
	
    // section util-app
    protected   function util_app_jsd($app = null) {
	    $whr = $app ? "id = $app" : "1=1";
        $sql = "select  a.*, jsd_app
				from    sys_app         a
				join    sys_app_jsd     b on a.id = b.sys_app_id
				where   sys_app_type_id > 1
                and     $whr";

        $rst = $this->db->query($sql)->result_array();
        foreach($rst  as $k => $dat) {
            if($dat['jsd_app']) {
                $jsx = json_decode($dat['jsd_app'], true);
                if($dat['sys_app_type_id'] == 10 || $dat['sys_app_type_id'] == 11) {   // app-group | sub-group
                    $jsd = array(
                        'cfg' => array(),
                        'app' => array(
                            'short' => isset($jsx['app']['short'])  ? $jsx['app']['short']   : null,
                            'font'  => isset($jsx['app']['font'])   ? $jsx['app']['font']    : null,
                            'icon'  => isset($jsx['app']['icon']))  ? $jsx['app']['icon']    : null
                    );
                }
                else {
                    $jsd =
                        array(
                            'cfg' => array(),
                            'app' => array(
                                'short' => isset($jsx['app']['short'])  ? $jsx['app']['short']  : null,
                                'ver'   => isset($jsx['app']['ver'])    ? $jsx['app']['ver']    : '1.0',
                                'year'  => isset($jsx['app']['year'])   ? $jsx['app']['year']   : date('Y'),
                                'url'   => isset($jsx['app']['url'])    ? $jsx['app']['url']    : null,
                                'path'  => isset($jsx['app']['path'])   ? $jsx['app']['path']   : null,
                                'ctrl'  => isset($jsx['app']['ctrl'])   ? $jsx['app']['ctrl']   : array(),
                                'model' => isset($jsx['app']['model'])  ? $jsx['app']['model']  : null,
                                'asset' => isset($jsx['app']['asset'])  ? $jsx['app']['asset']  : $this->sys['app']['asset'],
                                'index' => isset($jsx['app']['index'])  ? $jsx['app']['index']  : $this->sys['app']['index'],
                                'home'  => isset($jsx['app']['home'])   ? $jsx['app']['home']   : 'index',
                                'menu'  => isset($jsx['app']['menu'])   ? $jsx['app']['menu']   : 'proc',
                                'menu_top_my_app'   => isset($jsx['app']['menu_top_my_app'])    ? $jsx['app']['menu_top_my_app']    : true,
                                'menu_top_shortcut' => isset($jsx['app']['menu_top_shortcut'])  ? $jsx['app']['menu_top_shortcut']  : false,
                                'menu_top_report'   => isset($jsx['app']['menu_top_report'])    ? $jsx['app']['menu_top_report']    : false,
                                'menu_top_action'   => isset($jsx['app']['menu_top_action'])    ? $jsx['app']['menu_top_action']    : false,
                                'menu_top_search'   => isset($jsx['app']['menu_top_search'])    ? $jsx['app']['menu_top_search']    : false,
                                'menu_top_activity' => isset($jsx['app']['menu_top_activity'])  ? $jsx['app']['menu_top_activity']  : false,
                                'menu_top_notif'    => isset($jsx['app']['menu_top_notif'])     ? $jsx['app']['menu_top_notif']     : false,
                                'pcm'       => isset($jsx['app']['pcm'])    ? $jsx['app']['pcm']    : 'content',
                                'splash'    => isset($jsx['app']['splash']) ? $jsx['app']['splash'] : 'splash',
                                'font'      => isset($jsx['app']['font'])   ? $jsx['app']['font']   : null,
                                'icon'      => isset($jsx['app']['icon'])   ? $jsx['app']['icon']   : null,
                            )
                        );

                    /*  if(preg_match('/\//', $dat['id_code'])) {
                            $jsd['app']['path'] = substr($dat['id_code'], 0, strrpos($dat['id_code'], '/'));
                            $jsd['app']['ctrl'] = substr(strrchr($dat['id_code'], "/"), 1);
                        }
                    */
                }
                $this->db->update('sys_app_jsd', array('jsd_app' => json_encode($jsd, JSON_PRETTY_PRINT)), array('sys_app_id' => $dat['id']));
            }
        }
        return $jsd;
    }

    // section util-tbl
	protected   function util_tbl_pky($tbl, $dat=null) {
		$tpk = $this->db->get_where('sys_table', array('id' => $tbl))->row()->sys_table_pk;
		$pky = explode(',', trim($tpk));
		$pkf = array_flip($pky);
		$out = array();

		// first, find in supplied data
		if($dat && is_array($dat)) {
			if($out = array_intersect_key($dat, $pkf))
				return $out;
		}
		
		// second, find in rsp-dat
		if ($this->sys['rsp']['data']) {
			$out = array_intersect_key($this->sys['rsp']['data'], $pkf);
			if($out)
				return $out;
		}
		
		// third. find in arr
		if ($this->sys['bpm']['arr'] && is_array($this->sys['bpm']['arr'])) {
			$out = array_intersect_key($this->sys['bpm']['arr'], $pkf);
			if($out)
				return $out;
		}
		
		// last, via url-string
		switch (count($pky)) {
			case 1:
				$out = array($this->sys['req']['pid']);
				break;
			case 2:
				$out = array($this->sys['req']['pid'], $this->sys['req']['fid']);
				break;
			case 3:
				$out = array($this->sys['req']['pid'], $this->sys['req']['fid'], $this->sys['req']['xid']);
				break;
			case 4:
				$out = array($this->sys['req']['pid'], $this->sys['req']['fid'], $this->sys['req']['xid'], $this->sys['req']['yid']);
				break;
			case 5:
				$out = array($this->sys['req']['pid'], $this->sys['req']['fid'], $this->sys['req']['xid'], $this->sys['req']['yid'], $this->sys['req']['zid']);
				break;
		}
		foreach($pky as $k=>$v)
			$pkf[$v] = $out[$k];
		return array_filter($pkf);
	}
	protected   function util_tbl_sdb($tbl) {
		if(isset($this->sys['tbl'][$tbl])) {
		    $sdb = explode('.', $this->sys['tbl'][$tbl]);
		    array_pop($sdb);
		    return implode('.', $sdb);
        }
		else {
			$sql = "select  case  when sys_cache_id = 1 then '{$this->tim_db}' 
                                  else schema_db 
                            end sdb
                    from    {$this->tim_db}.sys_table
                    where   id = ?";
			//$sdb = $this->db->query($sql, array($tbl))->row()->sdb;
            $sdb = $this->util_db_sql($sql, array($tbl), 'cel');
			$this->sys['tbl'][$tbl] = "{$sdb}.{$tbl}";

            $sql        = "select jsd_run from sys_app_jsd where sys_app_id = {$this->sys['app']['id']}";
			$rst        = $this->util_db_sql($sql, 'cel');
            $rst        = json_decode($rst, true);
            $rst['tbl'] = $this->sys['tbl'];
            $jsd        = json_encode($rst, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
            $this->db->update('sys_app_jsd', array('jsd_run' => $jsd), array('sys_app_id' => $this->sys['app']['id']));

			return $sdb;
		}
	}
    protected   function util_tbl_jsd($tbl) {
	    $whr = $tbl ? "a.id = '$tbl'" : "1=1";
        $sql = "select  a.*, sys_table_pid, jsd_table
                from    sys_table           a
                left join sys_table_jsd     b on a.id = b.sys_table_id
                join    ____mysql_table     c on a.id = c.sys_table_id
				where   a.sys_data_status_id  = 1
                and     $whr
				order by sys_table_type_id";

        $rst = arr__id($this->db->query($sql)->result_array(), 'id', true);

        foreach($rst  as $t => $dat) {
            $jsd = null;
            $tbf = "{$dat['schema_db']}.{$dat['id']}";
            $tbl = $dat['id'];
            $col = array_flip($this->db->list_fields($tbf));
            $met = object__array($this->db->field_data($tbf));
            $pky = array();
            foreach($met as $c => $a) {
                if($a['primary_key'])
                    $pky[] = $a['name'];
            }
            $jsn = array(
                'has'=>array(
                    'col_id'    => false, 'col_pid'     => false, 'col_code'    => false, 'col_hash' => false, 'col_self' => false, 'col_label' => false,
                    'col_desc'  => false, 'col_status'  => false, 'col_event'   => false, 'fat_rows' => 0
                ),
                'attr'=>array(),
                'allow'=>array(    // allow-action
                    "ins" => 0,
                    "upd" => 0,
                    "del" => 0,
                    "bat" => 0,
                    "doc" => 0,
                    "exp" => 0,
                    "cas" => 0
                ),
                'audit'=>array(     // audit-action
                    "inq" => 0,
                    "ins" => 0,
                    "upd" => 0,
                    "del" => 0,
                    "bat" => 0,
                    "doc" => 0,
                    "exp" => 0
                ),
                'search' => array(
                    'free' => null,
                    'full' => null
                ));
            switch ($dat['sys_table_type_id']) {
                case 90:    // View Based
                case 91:    // View System
                case 92:    // View Virtual Table
                case 93:    // View Data
                    if(!$dat['jsd_table'] && $rst[$dat['sys_table_pid']]['jsd_table'])
                        $dat['jsd_table'] = $rst[$dat['sys_table_pid']]['jsd_table'];
                    break;
            }
            if($dat['jsd_table']) {
                $jsx = json_decode($dat['jsd_table'], true);
                if(isset($jsx['action'])) {
                    $jsx['allow'] = $jsx['action'];
                    unset($jsx['action']);
                }
                $jsn = arr__merge_recursive($jsn, $jsx);
                $jsn['attr'] = array(
                    'pkey' => $pky ? implode(',', $pky) : 'id',
                    'max_tree' => isset($jsx['max_tree']) ? $jsx['max_tree'] : 0
                );
                $jsx = null;
            }

            $jsn['has']['col_id']     = isset($col['id'])           ? 'id'      : false;
            $jsn['has']['col_pid']    = isset($col['pid'])          ? 'pid'     : false;
            $jsn['has']['col_code']   = isset($col['id_code'])      ? 'id_code' : false;
            $jsn['has']['col_hash']   = isset($col['id_hash'])      ? 'id_hash' : false;
            $jsn['has']['col_self']   = isset($col[$tbl])           ? $tbl      : false;
            $jsn['has']['col_label']  = isset($jsn['has']['col_label']) ? $jsn['has']['col_label'] : $tbl;
            $jsn['has']['col_desc']   = isset($col[$tbl . '_desc']) ? $tbl . '_desc' : false;
            $jsn['has']['col_status'] = isset($col['sys_data_status_id']) ? 'sys_data_status_id' : false;
            $jsn['has']['col_event']  = isset($col['event_id'])     ? 'event_id' : false;
            $jsn['attr']['max_tree']  = $jsn['has']['col_pid']      ? 2 : 0;

            switch ($dat['sys_table_type_id']) {
                case 92:
                    if (preg_match('/^(wcrm|upl)/', $dat['id']))
                        $jsn['has']['col_code'] = 0;
                    break;

                case 90:
                case 91:
                case 93:
                    $xid                    = $rst[$tbl]['sys_table_pid'];
                    $did                    = $tbl . '_desc';
                    $jsx['has']['col_self'] = isset($rst[$xid]['id']) ? $rst[$xid]['id'] : false;
                    $jsx['has']['col_desc'] = isset($rst[$xid][$did]) ? $rst[$xid][$did] : false;
                    break;
            }

            $jsd = json_encode($jsn, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
            $this->db->update('sys_table_jsd', array('jsd_table' => $jsd), array('sys_table_id' => $dat['id']));
        }
        $this->db->update('aaa_account', array('jsd_process' => null));
    }
	protected   function util_tbl_jsd_2() {
		$sql = "select  a.id, schema_db, sys_table tbn, sys_cache_id,
			        case
			            when sys_cache_id = 1 then concat('{$this->tim_db}', '.', a.id)
			            else concat(schema_db, '.', a.id)
			        end tim,
			        concat(schema_db, '.', a.id) iop,
			        sys_table_pk, sys_table_pk_type_id, sys_table_type_id,
			        jsd_table
	        from    ____sys_table           a
	        where   a.sys_data_status_id    = 1
	        and     a.sys_table_type_id not in (90)
	        and     a.schema_db             is not null";
		
		$tbl = arr__id($this->db->query($sql)->result_array());
		$tbf = array();

		foreach ($tbl as $k => $v) {
			if($v['jsd_table']) {
				$t = $v['jsd_table'];
				unset($v['jsd_table']);
				$tbl[$k] = array_merge($v, json_decode($t, true));
			}
			$tbs[$v['id']] = array('iop' => "{$v['schema_db']}.{$v['id']}", 'tim' => "{$this->tim_db}.{$v['id']}");
			if(!$v['sys_cache_id'])
				$tbs[$v['id']]['tim'] = $tbs[$v['id']]['iop'];
			$tbf[$v['id']] = $v['sys_cache_id'] ? "{$this->tim_db}.{$v['id']}" : "{$v['schema_db']}.{$v['id']}";
		}
		
		$tbl = json_encode($tbl, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
		$this->db->update('sys_jsd', array('sys_jsd' => $tbl), array('id' => 'tbl'));
		
		// $tbs = json_encode($tbs, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
		// $this->db->update('sys_jsd', array('sys_jsd' => $tbs), array('id' => 'tbs'));
		
		// $tbf = json_encode($tbf, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
		// $this->db->update('sys_jsd', array('sys_jsd' => $tbf), array('id' => 'tbf'));
	}
	protected   function util_tbl_col($tbl, $attr=true) {
		if($attr)
			return arr__id(object__array($this->db->field_data($tbl)), 'name');
		else
			return $this->db->list_fields($tbl);
	}
	protected   function util_tbl_col2id($tbl, $col, $val=null) {
		$tbl = is_numeric($tbl) ? $this->sys['bpm']['tbl'] : $tbl;
		if(is_array($col))
			$whr = $this->util_parser_var_sys($col);
		else {
			if($col && !$val) {
				$val = $col;
				$col = $tbl;
			}
			$whr = "and $col = $val";
		}
		$sql = "select id from {$this->sys['tbl'][$tbl]} where 1=1 $whr";
		return $this->db->query($sql)->row()->id;
	}
	protected   function util_tbl_id2col($tbl, $col, $fxc=null) {
		switch($tbl) {
			case 'aaa_acm':
			case 'aaa_account':
				$fxc = $fxc ? $fxc : $tbl;
				$sql = "select  group_concat($fxc) $tbl
                        from    {$this->sys['tbl'][$tbl]}
                        where   id in ($col)";
				break;
		}
		// return $this->util_db_sql($sql, 'cel');
        return $this->db->query($sql)->row()->$tbl;
	}
	
	private     function util_tbl__sys_qry() {
		$sql = "select jsd_sql_arg, jsd_sql_frm from sys_table_jsd where sys_table_id = ?";
		$rst = $this->db->query($sql, array($this->sys['bpm']['bpm']))->row_array();
		if($rst['jsd_sql_arg']) {
			$this->sql = json_decode($rst['jsd_sql_arg'], true);
			$this->sql['frm'] = $rst['jsd_sql_frm'];
			$this->sql['sel'] = implode(',', $this->sql['col']);
		}
	}
	
	// section util-db
    protected   function util_db($db=null) {
	    if(is_string($db)) {
		    switch($db) {
			    case 'DBS':
				    $db = $this->DBS = $this->load->database('crm_dev', true);
				    break;
		    }
	    }
	    if(is_object($db)) {
            if ($this->DBX != $db)
                $this->DBX = $db;
        }
	    else
            $this->DBX = $this->db;
    }
    protected   function util_db_drv($tbl) {
	    if($tbl) {
            $drv = $this->db->get_where("{$this->tim_db}.sys_table", array('id' => $tbl))->row();
            if($drv)
            	$drv = $drv->driver_db;
            switch ($drv) {
                case 'sqlsrv':
                    if(!$this->DBS)
                        $this->DBS  = $this->load->database('crm_dev', true);
                    $this->util_db($this->DBS);
                    break;

                case 'postgre':
                    $this->util_db($this->DBP);
                    break;

                case 'oci8':
                    $this->util_db($this->DBO);
                    break;

                case 'mysqli':
                default:
                    $this->util_db();
                    break;
            }
        }
    }
	protected   function util_db_qry($tbl, $wid=null, $col='*', $def=false) {
		$whr = '';
		$arr = array();
		if($wid)
			if(is_array($wid)) {
				$this->util_parser_var_sys($wid);
				foreach($wid as $k=>$v) {
					$whr .= " and $k = ?";
					$arr[] = $v;
				}
			}
			else {
				$whr = "and id = ?";
				$arr = array($wid);
			}
		
		$sdb = $this->util_tbl_sdb($tbl);
		$sql = "select $col from {$sdb}.{$tbl} where sys_data_status_id = 1 $whr";
		
		if(!$wid || $col == '*' || preg_match('/,/', $col))
			$scp = 'all';
		else
			$scp = 'cel';
		if($arr)
			$rst = $this->util_db_sql($sql, $arr, $scp, $def);
		else
			$rst = $this->util_db_sql($sql, $scp, $def);
		return isset($rst[1]) ? $rst : $rst[0];
	}
	public      function util_db_sql($sql) {
		$err = array();
		$out = null;
		$whr = null;
		$scp = 'all';
		$opt = 'none';
		$prm = func_get_args();
		
		if(isset($prm[3])) {
			$opt = $prm[3];
			$scp = $prm[2];
			$whr = $prm[1];
		}
		else {
			if(isset($prm[2])) {
				if(is_array($prm[1])) {
					$whr = $prm[1];
					$scp = $prm[2];
				}
				else {
					$scp = $prm[1];
					$opt = $prm[2];
				}
			}
			else {
				if(isset($prm[1]))
					if(is_array($prm[1]))
						$whr = $prm[1];
					else
						$scp = $prm[1];
			}
		}

		if(preg_match('/req__(pid|fid|xid|yid|zid)/', $sql))
			dump($sql, '#');
		else {
			if($whr) {
				try {
					$rst = $this->DBX->query($sql, $whr);
					if ($rst === false) {
					    $err = $this->DBX->error();
                        throw new Exception($err['message']);
                        return true;
					}
				}
				catch(Exception $e) {
					dump($e);
				}
			}
			else {
				try {
					$rst = $this->DBX->query($sql);
					if ($rst === false) {
                        $err = $this->DBX->error();
                        throw new Exception($err['message']);
                        return true;
					}
				}
				catch(Exception $e) {
					dump($e);
				}
			}
			if($err) {
				if (isset($err['code']) && $err['code']) {
					if (ENVIRONMENT != 'production')
						dump($sql, $err, 'trace');
				}
				else
					dump($sql, $err, 'trace');
			}
			else {
				
				if($rst) {
					$tot = $rst->num_rows();
					$dat = $rst->result_array();

					if ($tot > 0) {
						switch ($scp) {
							case "cel":
								$key = array_keys($dat[0]);
								$out = $dat[0][$key[0]];
								break;
							case "row":
								$out = $dat[0];
								break;
							case "all":
								$out = $dat;
								break;
							default:
								$out = $dat[0][$scp];
								break;
						}
						unset($dat);
					}
					
					switch ($opt) {
						case "all":
							foreach ($this->DBX->query($sql)->list_fields() as $f)
								$fields[$f] = null;
							foreach ($this->DBX->query($sql)->field_data() as $m)
								$meta[$m] = null;
							$out = array('dat' => $out, 'tot' => $tot, 'fields' => $fields, 'meta' => $meta);
							break;
						case 'sql':
							$out = $sql;
							break;
						case "#":
							dump($out);
							break;
						
						case 'null':
						case 'void':
						case $opt === 0:
						case is_numeric($opt):
							if (!$tot) {
								if($whr) {
									foreach ($this->DBX->query($sql, $whr)->list_fields() as $f) {
										if ($opt == 'void' || $opt == 'null')
											$out[$f] = $opt == 'null' ? null : '';
										elseif (is_numeric($opt))
											$out[$f] = $opt;
									}
								}
								else {
									foreach ($this->DBX->query($sql)->list_fields() as $f) {
										if ($opt == 'void' || $opt == 'null')
											$out[$f] = $opt == 'null' ? null : '';
										elseif (is_numeric($opt))
											$out[$f] = $opt;
									}
								}
							}
							break;
						
					}
				}
			}
			$rst->free_result();
		}
		return $out;
	}
	protected   function util_db_sql_limit($limit='nav', $offset='nav') {
        if($limit == 'nav')
            $limit = $this->sys['nav']['l'];

        if($offset == 'nav') {
            $offset = ($this->sys['nav']['p'] - 1) * $limit;
            $offset = $offset < 0 ? 0 : $offset;
        }
        
        switch($this->DBX->dbdriver) {
            case 'sqlsrv':
                $cfg = "offset $offset rows fetch next $limit rows only";
                break;
            default:
                $cfg = "limit $limit offset $offset";
                break;
        }
        return $cfg;
    }
	protected   function util_db_sql_orderby($p=null) {
		// $p : prefix
		$order = $p ? "concat(lpad(ifnull($p.pid, $p.id), 10, '0'), lpad($p.id, 10, '0'))" : "concat(lpad(ifnull($p.pid, $p.id), 10, '0'), lpad($p.id, 10, '0'))";
		return $order;
	}
	protected   function util_db_sql_magic($tbl, $cfg = null, $join = true) {
	    $this->util_db_drv($tbl);

		$all = 0; // include active & disabled
		$A   = array(
			'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
			'cc', 'cd', 'ce', 'cf', 'cg', 'ch', 'ci', 'cj', 'ck', 'cl', 'cm', 'cn', 'co', 'cp', 'cq', 'cr', 'cs', 'ct', 'cu', 'cv', 'cw', 'cx', 'cy', 'cz',
		);
		$i   = 0;
		$J   = '';
		$F   = '';
		$W   = '';
		$C   = array();
		$L   = array();
		$G   = array('str' => '', 'ord' => '', 'off' => '');
		
		$cfg['sys_data_status_id']  = isset($cfg['sys_data_status_id']) ? $cfg['sys_data_status_id'] : 1;
		$cfg['bpm']['is_magic']     = isset($cfg['bpm']['is_magic']) ? $cfg['bpm']['is_magic'] : 0;
		
		if(!empty($cfg['frm'])) {
			if($this->sys['req']['frm'])
				$F = parser_whr($this->util_tbl_pky($tbl));
		}
		elseif(isset($cfg['esa'])) {
			$F = parser_whr($cfg['esa']);
		}
		else {
			$W = 'where 1=1';
			
			if (isset($cfg['arr'])) {
				if ($cfg['arr'] == 1) {
					// from: sys_cmd__get_std (lst)
					$G['str'] = $this->sys['bpm']['str'];
					$G['ord'] = $this->sys['bpm']['ord'];
					$G['off'] = $this->util_db_sql_limit('nav', 'nav');
					
					if (! in_array($this->sys['req']['arg'], array('new', 'n3w'))) {
						$Z = $this->util_escape($this->sys['req']['arg']);
						$t = $this->util_tbl_pky($tbl);
						$F = $this->sys['req']['arg'] ? "and id = $Z" : '';
					}
				}
				else {
					$G['str'] = $this->util_parser_var_sys($cfg['arr']);
					if (isset($cfg['ord']))
						$G['ord'] = $cfg['ord'];
					if (isset($cfg['off']))
						$G['off'] = $cfg['off'];
				}
			}
			
			if (isset($cfg['bpm']['arr']) || isset($cfg['bpm']['str'])) {
				if (!empty($cfg['bpm']['arr'])) {
					$G['str'] .= $this->util_parser_var_sys($cfg['bpm']['arr']);
				}
				
				if (isset($cfg['bpm']['str']) && $cfg['bpm']['str']) {
					if ($cfg['bpm']['str'] == '#all') {
						$cfg['bpm']['str'] = '';
						$all = 1;
					}
					$G['str'] .= $cfg['bpm']['str'];
				}
				
				if (isset($cfg['ord']))
					$G['ord'] = $cfg['ord'];
				
				if (isset($cfg['off']))
					$G['off'] = $cfg['off'];
			}
		}
		
		if(!$G['ord'] && isset($cfg['ord']))
			$G['ord'] = $cfg['ord'];
		
		if($G['ord'] == 'order by id')
			$G['ord'] = 'order by 1';
		
		if($G['ord'] && !preg_match('/order/', $G['ord']))
			$G['ord'] = "order by {$G['ord']}";
		
		if($cfg['bpm']['is_magic'])
			$join = true;
		
		$col = '';
		$dbs = $this->util_tbl_sdb($tbl);
		$FK  = $this->util_tbl_col($tbl, false);
		
		if(in_array('id', $FK)) {
			$prop = $this->util_load_jsd('tbl_prop', $tbl);
			if(in_array($prop['sys_table_type_id'], range(90, 93))) {
				$col = substr($tbl, 4);
				$FK[] = $col.'_id';
			}
			else {
				$col = $tbl;
				$FK[] = $col.'_id';
			}
		}
		
		foreach($FK as $fo) {
			if($fo != $col.'_id' && $fo != 'event_id' && substr($fo, -3) == '_id' && preg_match('/(_id$)/', $fo)) {
				$al = $A[$i++];
				$tn = $cn = substr($fo, 0, strlen($fo) - 3);
				$db = $this->util_tbl_sdb($tn);
				$bi = false;
				if($join) {
					foreach($this->util_tbl_col($tn, false) as $fx) {
						if($fx == 'sys_data_status_id')
							$bi = true;
						
						if($fx == 'pid') {
							$C[] = "$al.pid {$tn}_pid";
							$C[] = "$al.{$tn} {$tn}_parent";
						}
						elseif($fx == 'id_code')
							$C[] = "$al.id_code {$tn}_code";
						elseif(!in_array($fx, array('id', $tbl.'_id', 'sys_data_status_id', 'event_id')) && !in_array($fx, $FK) && !in_array($fx, $L)) {
							$L[] = $fx;
							$C[] = "$al.$fx";
						}
					}
				}
				else {
					$C[] = $cn;
				}
				
				$J .= " left join $db.$tn $al on $al.id = a.$fo";
				if($bi)
					$J .= " and $al.sys_data_status_id > 0";
			}
		}
		
		// note: clause 2=2 can be used as backdoor in post sql builder
		if($J) {
			$C = implode(', ', $C);
			if(!$all && isset($K['sys_data_status_id']))
				$W .= " and a.sys_data_status_id = {$cfg['sys_data_status_id']}";
			
			if(isset($cfg['tot'])) {
				$sql[0] = "select count(*) from ( select a.*, $C from $dbs.$tbl a $J $W) xz where 1=1 $F {$G['str']} and 2=2";
				$sql[1] = "select * from ( select a.*, $C from $dbs.$tbl a $J $W) xz where 1=1 $F {$G['str']} and 2=2 {$G['ord']} {$G['off']}";
			}
			else
				$sql = "select * from ( select a.*, $C from $dbs.$tbl a $J $W) xw where 1=1 $F {$G['str']} and 2=2 {$G['ord']} {$G['off']}";
			
		}
		else {
			if($all)
				$W = 'where 1=1';
			else
				$W = $cfg['sys_data_status_id'] ? 'where sys_data_status_id > 0' : 'where 1=1';
			if(isset($cfg['tot'])) {
				$sql[0] = "select count(*) from $dbs.$tbl $W $F {$G['str']} and 2=2";
				$sql[1] = "select * from $dbs.$tbl $W $F {$G['str']} and 2=2 {$G['ord']} {$G['off']}";
			}
			else
				$sql = "select * from $dbs.$tbl $W $F {$G['str']} and 2=2 {$G['ord']} {$G['off']}";
		}
		
		return $sql;
	}
	
	private     function util_db_upd_tree($tbl, $id, $pid) {
		if($pid) {
			$sdb  = $this->util_tbl_sdb($tbl);
			$rid  = null;
			$tree = 1;
			$sql  = "select id, pid, rid, tree + 1 tree from {$sdb}.{$tbl} where id = $pid and sys_data_status_id = 1";
			if($rst = $this->util_db_sql($sql, 'row')) {
				$tree = $rst['tree'];
				$rid  = $rst['rid'] ? $rst['rid'] : $pid;
			}
			
			$this->db->update("{$sdb}.{$tbl}", array('rid' => $rid, 'tree' => $tree), array('id' => $id));
		}
	}
	protected   function util_db_matrix_flatted($tbl, $col, $idc, $idv) {
		$out = array();
        $sdb  = $this->util_tbl_sdb($tbl);
		$sql = "select $col from {$sdb}.{$tbl} where $idc = ?";
		
		if($tmp = $this->util_db_sql($sql, array($idv))) {
			foreach ($tmp as $k => $v)
				$out[$k] = $v[$col];
		}
		return $out;
	}
	
	private     function util_db_check_dependency($tbl, $idx=0) {
		$out = array();
		$idx = $idx ? $idx : $this->sys['req']['pid'];
		if(is_string($idx))
			$idx = addslashes($idx);
		$sdb = 'wyeth_db';
		
		$this->db->simple_query('set session group_concat_max_len=10000');
		
		$sql = "select  group_concat(concat('select \"', b.table_name, '\" tbl, \"', a.for_col_name, '\" col from ', b.constraint_schema, '.', b.table_name, ' where ', a.for_col_name, ' = $idx') separator ' union ') str
                from 	information_schema.innodb_sys_foreign_cols a,
                        information_schema.referential_constraints b
                where	b.referenced_table_name     = '$tbl'
                and     b.unique_constraint_schema  = '$sdb'
                and     a.id = concat(constraint_schema, '/', b.constraint_name)";
		
		if($sql  = $this->util_db_sql($sql, 'cel'))
			$out = $this->util_db_sql($sql);
		
		return $out;
	}
	private     function util_db_check_delete($tbl, $idx=0) {
		$out = array();
		$idx = $this->util_escape($idx);
		$sql = "select  concat(sys_table_for_sc, '.', sys_table_for_id) tbf, sys_table_for_id tbl, sys_table_for_pk pky
                from    sys_table__foreign
                where   sys_table_par_id = '$tbl'";
		if($rst = $this->util_db_sql($sql)) {
			foreach($rst as $k=>$v)
				$str[] = "select '{$v['tbl']}' tbl, '{$v['pky']}' col from {$v['tbf']} where {$v['pky']} = $idx";
			
			$out = $this->util_db_sql(implode(' union ', $str));
		}
		return $out;
	}
	
	private     function util_db__iop_tim_load($tbl = null, $dat = null) {
		if($tbl) {
			if($dat) {
				foreach($this->db->list_fields($this->sys['tbl'][$tbl]) as $k => $f)
					if(isset($dat[$f]))
						$p[$f] = $dat[$f];
				$this->db->replace($this->sys['tbl'][$tbl], $p);
			}
			else {
				$this->db->simple_query("truncate table {$this->tim_db}.{$this->sys['tbl'][$tbl]}");
				$c = implode('`,`', array_diff($this->db->list_fields($this->sys['tbl'][$tbl]), ['event_id']));
				$s = "insert into {$this->sys['tbl'][$tbl]} (`$c`) select `$c` from {$this->sys['tbs'][$tbl]['iop']}";
				$this->db->simple_query($s);
			}
		}
		else {
			// reset table-mem
			$e = array();
			
			// truncate table-mem
			$s = "	select  table_name tbl from information_schema.tables
		        where   table_schema = '{$this->tim_db}'
		        and     engine       = 'MEMORY'";
			
			foreach ($this->util_db_sql($s) as $i => $t)
				$this->db->simple_query("truncate table {$this->tim_db}.{$t['tbl']}");
			
			// init table-mem
			$s = "	select  concat(schema_db, '.', id) id, id t, concat('{$this->tim_db}.', id) mm
		        from    sys_table
		        where   sys_cache_id        = 1
		        and     sys_data_status_id  = 1
		        order by id";
			
			$this->db->trans_begin();
			foreach ($this->util_db_sql($s) as $k => $v) {
				$t = $v['t'];
				$o = array_diff($this->db->list_fields($v['id']), ['event_id']);
				$m = $this->db->list_fields($v['mm']);
				
				if ($m != $o)
					$e[] = "$t is not match";
				else {
					/*
				$c = implode('`,`', $m);
				$q = "insert into {$this->tim_db}.{$t} (`$c`) select `$c` from {$v['id']}";
				$a = $this->db->simple_query($q);
				if (!$a)
					$e[] = $q;
				*/
				}
			}
			if ($e) {
				$this->db->trans_rollback();
				dump($e, '#');
			}
			else {
				$this->db->trans_commit();
			}
		}
	}
	private     function util_db__iop_tim_ddl($tbl) {
		$this->iop_tim  = $this->load->database($this->tim_db, true);
		$spx = "call sp_iop_tim_dll(?)";
		$this->iop_tim->query($spx, array($tbl));
	}
	
	// section util-svc
	protected   function util_svc__load ($svc) {
		if(!isset($this->sys['svc'][$svc])) {
			$this->load->model('iop/MSVC_'.strtoupper($svc), $svc);
			$this->sys['svc'][$svc] = $this->sys['prc']['sys_svc'][$svc];
			unset($this->sys['prc']['sys_svc'][$svc]);
			$this->$svc->sync($this->sys);
		}
	}
	protected   function util_svc__wfs ($act) {
		if(isset($this->sys['svc']['wfs']['wfs_catalog_id'])) {
			if(!isset($this->wfs)) {
				$this->load->model('iop/MSVC_WFS', 'wfs');
				$this->wfs->sync($this->sys);
			}
			return $this->wfs->bpm_wfs_task($act);
		}
	}
    public      function util_svc__exim($type) {
	    switch ($type) {
            case 'imp_xls':
                $sql = "select import_xls from sys_process__exim where sys_process_id = {$this->sys['req']['rid']}";
                $tmp = $this->util_db_sql($sql, 'cel');
                if($tmp)
                    $this->sys['svc']['imp'] = json_decode($tmp, true);
                break;

            case 'exp_doc':
            case 'exp_pdf':
                $sql = "select export_doc from sys_process__exim where sys_process_id = {$this->sys['req']['rid']}";
                $tmp = $this->util_db_sql($sql, 'cel');
                if($tmp)
                    $this->sys['svc']['exp'] = json_decode($tmp, true);
                break;
        }
    }
	protected   function util_svc__find_file($prm) {
		$dir = $this->config->item('ws_dir')['app'];
		$cfg = $this->sys['svc']['exp'];
		$out = null;
		
		foreach($prm as $k=>$v)
			$cfg['out'] = str_replace("#{$k}#", $v, $cfg['out']);
		
		$this->sys['svc']['exp']['out'] = $cfg['out'];
		
		if(file_exists("{$dir}/{$cfg['dir']}{$cfg['out']}"))
			$out = "{$cfg['dir']}{$cfg['out']}";
		return $out;
	}
    // section util
	protected   function util_escape($v, $escape = true) {
		if($escape) {
			return is_numeric($v) ? $v : $this->db->escape($v);
		}
		else
			return str_replace("'", '', $v);
	}
	protected   function util_file_ext($filename, $case = null) {
		$ext = array_search_path(mime_content_type($filename), get_mimes());
		switch ($case) {
			case 'ucfirst':
				$ext = ucfirst($ext);
				break;
			
			case 'upper':
				$ext = strtoupper($ext);
				break;
			
			case 'lower':
			default:
				$ext =  strtolower($ext);
				break;
		}
		return $ext;
	}
	
	// section util-parser

	protected   function util_parser_var_sys(&$arg, $whr = true) {
		$__mapper = function ($v, &$sys) {
			$s = preg_split('/ /', $v);
			foreach($s as $i=>$j) {
				if(preg_match('/__/', $j)) {
					$p = explode('__', $j);
					if ($j != $p[0]) {
						$r = '';
						switch (count($p)) {
							case 2:
								if (isset($sys[$p[0]][$p[1]]))
									$r = $sys[$p[0]][$p[1]];
								break;
							
							case 3:
								if (isset($sys[$p[0]][$p[1]][$p[2]]))
									$r = $sys[$p[0]][$p[1]][$p[2]];
								break;
							
							case 4:
								if (isset($sys[$p[0]][$p[1]][$p[2]][$p[3]]))
									$r = $sys[$p[0]][$p[1]][$p[2]][$p[3]];
								break;
							
							case 5:
								if (isset($sys[$p[0]][$p[1]][$p[2]][$p[3]][$p[4]]))
									$r = $sys[$p[0]][$p[1]][$p[2]][$p[3]][$p[4]];
								break;
						}
						$v = str_replace($j, $r, $v);
					}
				}
			}
			return $v;
		};
		
		if(is_string($arg))
			$arg = json_decode($arg, true);
		
		if(is_array($arg)) {
			foreach ($arg as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $x => $y) {
						if(preg_match('/__/', $y))
							$arg[$k][$x] = $__mapper($y,$this->sys);
					}
				}
				else {
					if(preg_match('/__/', $v)) {
						$arg[$k] = $__mapper($v, $this->sys);
					}
				}
			}
			
			if ($whr)
				return parser_whr($arg);
		}
	}
	protected   function util_parser_var_dat($cfg, &$dat=null) {
		// json string or assosiate array
		if(is_string($cfg))
			$cfg = json_decode($cfg, true);
		
		if($cfg && is_array($cfg)) {
			foreach($cfg as $k=>$v) {
				if(is_array($v)) {
					foreach($v as $x => $y) {
						if(isset($dat[$x]) && $dat[$x])
							$cfg[$k][$x] = $dat[$x];
						elseif(preg_match($this->reg_dat, $y, $m)) {
							$var = null;
							$opr = '';
							$idx = trim(str_replace($m[0], '', $y));
							
							$this->util_parser_map_dat($m[1], $var, $dat);
							if(preg_match($this->reg_opr, $idx)) {
								$opr = explode(' ', $idx);
								$idx = end($opr);
								$opr = implode(' ', array_pop($opr));
							}
							
							$val = $this->util_parser_map_val($idx, $var);
							$cfg[$k][$x] = "$opr $val";
							unset($var);
						}
						elseif (preg_match($this->reg_opr, $y)) {
							$opr = explode(' ', trim($y));
							$idx = end($opr);
							$opr = implode(' ', array_pop($opr));
							$val = $this->util_parser_map_val($idx);
							$cfg[$k][$x] = "$opr $val";
						}
						else {
							$val = $this->util_parser_map_val($y);
							$cfg[$k][$x] = $val;
						}
					}
				}
				else {
					if(!is_numeric($v) && !is_bool($v)) {
						if(isset($dat[$k]) && $dat[$k] != $v)
							$cfg[$k] = $dat[$k];
						elseif(preg_match($this->reg_dat, $v, $m)) {
							$var = null;
							$opr = '';
							$idx = trim(str_replace($m[0], '', $v));
							
							$this->util_parser_map_dat($m[1], $var, $dat);
							if(preg_match($this->reg_opr, $idx)) {
								$opr = explode(' ', $idx);
								$idx = end($opr);
								$opr = implode(' ', array_pop($opr));
							}
							
							$val     = $this->util_parser_map_val($idx, $var);
							$cfg[$k] = "$opr $val";
							unset($var);
						}
						elseif(preg_match($this->reg_opr, $v)) {
							$opr     = explode(' ', trim($v));
							$idx     = end($opr);
							$opr     = implode(' ', array_pop($opr));
							$val     = $this->util_parser_map_val($idx);
							$cfg[$k] = "$opr $val";
						}
						else {
							$val     = $this->util_parser_map_val($k);
							$cfg[$k] = $k == $val ? $v : $val;
						}
					}
				}
			}
		}
		
		return $cfg;
	}
	protected   function util_parser_sys_rsp_dat(&$arr) {
		if(is_array($arr)) {
			foreach($arr as $k=>$v) {
				if(preg_match('/rsp__dat/', $v)) {
					$idx = str_replace('/rsp__dat__/', '', $v);
					if(isset($this->sys['rsp']['data'][$k]))
						$arr[$k] = $this->sys['rsp']['data'][$idx];
				}
			}
		}
	}
	protected   function util_parser_cfg_key_str($cfg, &$dat=null) {
		// comma delimeter string or assosiate array
		if(is_string($cfg))
			$cfg = array_fill_keys(explode(',', trim($cfg)), null);
		return $this->util_parser_var_dat($cfg, $dat);
	}
	protected   function util_parser_cfg_key_whr($arg, &$dat=null) {
		// json string or assosiate array
		if(is_string($arg))
			$arg = json_decode($arg, true);
		
		$whr = '';
		if($arg && is_array($arg)) {
			foreach($arg as $k=>$v) {
				if(is_array($v)) {
					foreach($v as $x => $y) {
						$p = strlen($x) > 3 ? preg_replace('/_id$/', '_pid', $x) : $x;
						if (isset($dat[$k]) && isset($dat[$p]))
							$arg[$k][$x] = "($x = $y or $p = $y)";
					}
				}
				else {
					$p = strlen($k) > 3 ? preg_replace('/_id$/', '_pid', $k) : $k;
					if (isset($dat[$k]) && isset($dat[$p]))
						$arg[$p] = $v;
				}
			}
			
			$arg = $this->util_parser_var_dat($arg, $dat);
			
			if($arg)
				foreach($arg as $k=>$v) {
					if(is_array($v))
						foreach($v as $f=>$x)
							$whr .= parser_whr_xtd($f, $x);
					else
						$whr .= parser_whr_xtd($k, $v);
				}
		}
		return $whr;
	}
	
	protected   function util_parser_map_val($val, &$dat=null) {
		if($val && !is_numeric($val) && !is_bool($val)) {
			if(isset($dat[$val]) && $dat[$val])
				$val = $dat[$val];
			elseif(isset($this->sys['rsp']['data'][$val]) && $this->sys['rsp']['data'][$val])
				$val = $this->sys['rsp']['data'][$val];
			else {
				foreach($this->sys as $sub => $var)
					if(isset($var[$val])) {
						$val = $var[$val];
						break;
					}
			}
		}
		return $val;
	}
	private     function util_parser_map_dat($map, &$var, &$dat) {
		if($map == 'dat')
			$var = $dat;
		elseif(substr($map, 0, 3) == 'ref') {
			$ref = str_replace('ref_', '', $map);
			$var = $this->sys['rsp']['ref'][$ref];
		}
		elseif($map == 'rsp' || $map=='rsp__dat')
			$var = $this->sys['rsp']['data'];
		else {
			$map = str_replace('sys_', '', $map);
			$var = $this->sys[$map];
		}
	}
	
	protected   function util_parser_jsd($jsd, &$dat) {
		if(isset($dat[$jsd]) && $dat[$jsd]) {
			$tmp = json_decode($dat[$jsd], true);
			if(json_last_error() == JSON_ERROR_NONE)
				$dat = arr__merge_recursive($dat, $tmp);
			else {
			    // error happened
                dump($jsd, $dat);
            }
		}
		unset($dat[$jsd]);
	}
	protected   function util_load_jsd($jsd, $arg=null) {
		$out = array();
		switch ($jsd) {
			case 'tbl_pky':
				$out = $this->db->get_where("{$this->tim_db}.sys_table", array('id' => $arg))->row()->sys_table_pk;
				break;
			case 'tbl_prop':
				$out = $this->db->get_where("____sys_table", array('id' => $arg))->row_array();
				$out = array_merge($out, json_decode($out['jsd_table'], true));
				if($out['sys_table_type_id'] == 81) {
					$this->sql = json_decode($out['jsd_sql_arg'], true);
					$this->sql['frm'] = $out['jsd_sql_frm'];
					$this->sql['sel'] = implode(',', $this->sql['col']);
				}
				unset($out['jsd_table'], $out['jsd_sql_arg'], $out['event_id'], $out['sys_data_status_id']);
				break;
			case 'tbl_sys_jsd':
				$out = json_decode($this->db->select('sys_jsd')->get_where('sys_jsd', array('id' => 'tbl'))->row()->sys_jsd, true);
				break;
			case 'tbf_sys_jsd':
				$out = json_decode($this->db->select('sys_jsd')->get_where('sys_jsd', array('id' => 'tbf'))->row()->sys_jsd, true);
				break;
			case 'tbs_sys_jsd':
				$out = json_decode($this->db->select('sys_jsd')->get_where('sys_jsd', array('id' => 'tbs'))->row()->sys_jsd, true);
				break;
			case 'tbx_sys_jsd':
				$sql = "select * from sys_jsd where id in ('tbf', 'tbs')";
				$rst = $this->db->query($sql)->result_array();
				foreach($rst as $k=>$v)
					$this->sys[$v['id']] = json_decode($v['sys_jsd'], true);
				break;
			
			case 'rpc_jsd':
				$out = json_decode($this->db->select('rpc_jsd')->get_where('aaa_account', array('id' => $this->sys['usr']['id']))->row()->rpc_jsd, true);
				break;
			case 'usr_jsd':
				$out = json_decode($this->db->select('usr_jsd')->get_where('aaa_account', array('id' => $this->sys['usr']['id']))->row()->usr_jsd, true);
				break;
		}
		return $out;
	}
	
	protected   function dump() {
		if($this->sys['usr']['is_super'] && $this->sys['usr']['is_debug']) {
			foreach(func_get_args() as $arg) {
				switch($arg) {
					case 'sys':
						dump($this->sys);
						break;
					case 'cfg':
					case 'usr':
					case 'req':
					case 'rsp':
					case 'bpm':
					case 'prc':
					case 'rpc':
					case 'svc':
					case 'nav':
					case 'uix':
					case 'aaa':
					case 'ack':
					case 'err':
						dump($this->sys[$arg]);
						break;
					case 'app':
						dump($this->sys['app']);
						break;
					case 'dat':
					case 'ref':
					case 'aux':
						dump($this->sys['rsp'][$arg]);
						break;
					case '#':
						die();
					default:
						dump($arg);
						break;
				}
			}
		}
	}
	
	public      function sys_validate_res() {
		$api = "{$this->sys['req']['sch']}__{$this->sys['req']['res']}__{$this->sys['req']['ver']}";
		$sql = "select  sys_process_id, sys_process, sys_table_id, biz_table_id, ifnull(biz_table_id, sys_table_id) tbl, api_cfg
				from    sys_process         a
				join    sys_process_api     b on a.id = b.sys_process_id
				where   id_code             = '$api'
				and     api_act             like '%{$this->sys['req']['act']}%'
				and     sys_data_status_id  = 1";

		if($rst = $this->util_db_sql($sql, 'row')) {
			$this->sys['req'] ['rid']   = $rid = $rst['sys_process_id'];
			$this->sys['bpm']           = array_merge($this->sys['bpm'], $rst);
			
			if($this->sys['bpm']['api_cfg']) {
				$cfg = json_decode($rst['api_cfg'], true);
				$this->sys['bpm']['cfg'] = arr__merge_recursive($this->sys['bpm']['cfg'], $cfg);
			}
			
			if(isset($this->sys['bpm']['cfg']['bpm']))
				$this->sys['bpm'] = arr__merge_recursive($this->sys['bpm'], $this->sys['bpm']['cfg']['bpm']);
			unset($this->sys['bpm']['api_cfg'], $this->sys['bpm']['cfg']['bpm']);
			
			
			// default value : model
			if(!isset($this->sys['bpm']['cls']))
				$this->sys['bpm']['cls']    =  'model';
			
			// default value : data-status
			if(!isset($this->sys['bpm']['has']['col_status']))
				$this->sys['bpm']['has']['col_status'] = 'sys_data_status_id';
			/*
			// default value : auth-t
			if($this->sys['bpm']['aaa']['auth_a'] && !$this->sys['bpm']['aaa']['auth_t'])
				$this->sys['bpm']['aaa']['auth_t'] = 'oath2_jwt';
			
			// privileges
			
			if(in_array($this->sys['req']['res'], array('login', 'cpwd', 'fpwd', 'rpwd'))) {
				$this->sys['bpm']['aaa']['allow']['ins'] = 1;
				$this->sys['bpm']['aaa']['allow']['upd'] = 1;
				$this->sys['bpm']['aaa']['allow']['del'] = 1;
			}
			else {
				$acm = $this->sys['usr']['aaa_acm_id'];
				$sql = "select  max(ifnull(is_allow_get, 0)) inq,
								max(ifnull(is_allow_ins, 0)) ins,
								max(ifnull(is_allow_upd, 0)) upd,
								max(ifnull(is_allow_del, 0)) del,
								max(ifnull(is_allow_imp, 0)) imp,
								max(ifnull(is_allow_exp, 0)) exp,
								max(ifnull(is_allow_upl, 0)) upl
					from   aaa_acm__sys_process
					where  sys_process_id       = {$rst['sys_process_id']}
					and    aaa_acm_id           in ($acm)
					and    is_allow_get    		= 1
					and    sys_data_status_id   = 1
					group by sys_process_id";
				
				$rst = $this->util_db_sql($sql, 'row');
				
				if ($rst) {
					$this->sys['bpm']['aaa']['allow']['get'] = $rst['inq'];
					$this->sys['bpm']['aaa']['allow']['ins'] = $rst['ins'];
					$this->sys['bpm']['aaa']['allow']['upd'] = $rst['upd'];
					$this->sys['bpm']['aaa']['allow']['del'] = $rst['del'];
					$this->sys['bpm']['aaa']['allow']['imp'] = $rst['imp'];
					$this->sys['bpm']['aaa']['allow']['exp'] = $rst['exp'];
					$this->sys['bpm']['aaa']['allow']['upl'] = $rst['upl'];
					
					if (!$this->sys['bpm']['aaa']['allow']['get'])
						$this->sys['rsp']['ack']['ack_code'] = 'AAA__101';
				}
				else
					$this->sys['rsp']['ack']['ack_code'] = 'AAA__100';
				
				if (!$this->sys['rsp']['ack']['ack_code']) {
					$sql  = "select jsd_table from sys_table_jsd where sys_table_id = '{$this->sys['bpm']['tbl']}'";
					$jsd  = json_decode($this->util_db_sql($sql, 'cel'), true);
					$this->sys['bpm']['aaa']['audit'] = $jsd['audit'];
					if(isset($jsd['allow']))
						foreach ($jsd['allow'] as $k => $v) {
							if ($v == 0)
								$this->sys['bpm']['aaa']['allow'][$k] = $v;
						}
				}
			}
			
			if(empty($this->sys['bpm']['col_man']) || $this->sys['bpm']['col_man'] == 'rebuild') {
				$sql = "select group_concat(column_name) col_man from ____mysql_table_column_mandatory where sys_table_id = '{$this->sys['bpm']['sys_table_id']}'";
				$this->sys['bpm']['col_man'] = $this->util_db_sql($sql, 'cel');
				$cfg['bpm']['col_man']       = $this->sys['bpm']['col_man'];
				
				$this->db->update('sys_process_api',
				                    array('api_cfg' => json_encode($cfg, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT)),
									array('sys_process_id' => $rid));
			}
			*/
		}
		else
			$this->sys['bpm'] = null;
	}
	public      function sys_validate_key() {
		// validate user, find who is own the API-TOKEN
		// aaa_token_type_id = 20 = IOP-TOKEN
		
		$sql = "select a.id, aaa_account, token_private, dmz_ip
                from  aaa_account       a
                join  aaa_token         b on  a.id = b.aaa_account_id
                                              and b.sys_app_id      = {$this->sys['app']['id']}
                                              and aaa_token_type_id = 20
                                              and aaa_token         = ?
                                              and (token_expire is null or token_expire >= '{$this->sys['req']['nts']}')
                                              and a.sys_data_status_id       = 1
                join  aaa_dmz           c on a.id = c.aaa_account_id
                                              and c.sys_app_id      = {$this->sys['app']['id']}
                                              and a.sys_data_status_id       = 1";
		$rst = $this->util_db_sql($sql, array($this->sys['req']['aaa']['authentication']), 'row');
		
		if($rst) {
			$this->sys['usr']['id']                     = $rst['id'];
			$this->sys['usr']['aaa_account']            = $rst['aaa_account'];
			// $this->sys['usr']['ppo_agent_id']           = $rst['ppo_agent_id'];
			$this->sys['usr']['aaa']['token_private']   = $rst['token_private'];
			$this->sys['usr']['aaa']['dmz_id']          = $rst['dmz_ip'] ? json_decode($rst['dmz_ip'], true) : null;
		}
		// else
		//    $this->sys_error(401, 'Invalid API-TOKEN');
		
		// validate ip-whitelist
		$ipc = $this->input->ip_address();
		if(!$this->sys['rsp']['ack']['ack_code']) {
			if(is_ip_localhost($ipc) || ENVIRONMENT == 'development') {
				// bypass
			}
			else {
				$dmz['dmz_ip'] = array_keys(arr__id($this->sys['usr']['aaa']['dmz_ip'], 'ip'));
				if (!ip_allow($ipc, $dmz['dmz_ip']))
					$this->sys_error(401, 'Unregistered IP Address');
				else
					$this->sys_error(401, 'Unregistered IP Address');
			}
		}
		
		// validate sign-key
		if(in_array($this->sys['req']['act'], array('post', 'put'))) {
			if (!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['aaa']['auth_s']) {
				// generate a-key
				$pld    = $this->sys['req']['pst'];
				$pld[]  = $ipc;
				$pld[]  = $this->sys['usr']['aaa']['auth'];
				$pld    = join('', $pld);
				
				$c_key = hash_hmac('sha512', $rst['token_private'], $this->sys['usr']['aaa']['auth']);
				$s_key = hash_hmac('sha512', $pld, $c_key);
				
				if (hash_equals($s_key, $this->sys['usr']['aaa']['sign']))
					$this->sys_error(401, 'Invalid Payload Checksum');
			}
		}
	}
	
	public      function sys_error($err) {
		$sql = "select  sys_response_code ack_message, replace(ifnull(err_http, 'HTTP__400'), 'HTTP__', '') http_code
                from    sys_response_code
                where   id                  = '$err'
                and     sys_data_status_id  = 1";
		return $this->util_db_sql($sql, 'row');
	}
	
}
