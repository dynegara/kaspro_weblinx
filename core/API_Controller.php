<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
use \Firebase\JWT\JWT;
class API_Controller extends CI_Controller {
	protected   $argc, $argn,
				$http_hdr,
				$http_code = 200;
	protected   $sys = array(
		'usr'   => array(
			'id'            => null,
			'aaa_account'   => null,
			'aaa_type_id'   => null,
			'aaa'           => array('auth' => null, 'dmz' => null)
		),
		'app'   => array(),
		'cfg'   => array(),
		'req'   => array(
			'act' => null,
			'svr' => APP_DEF, // server
			'sch' => null, // schema
			'rid' => null, // resource-id
			'res' => null, // resource
			'ver' => 1.0,  // version
			'dir' => '',   // dir
			'aaa' => array('authentication' => null, 'signature' => null, 'sdlc' => false, 'dump' => false),
			'arg' => array(), // argument
			'pst' => array(), // payload
			'out' => 'json',
			'dbg' => 0,     // debug 0: no-debug, 1:full-debug
			'trc' => null,  // trace-id
			'frm' => false,
			'uix' => null,
			'now' => null,
			'nts' => null
		),
		'rsp'   => array(
			'ack'   => array(
				'ack_code'    => 0,
				'ack_message' => 'Success'
			),
			'meta' => array(
				'privilege' => array('post' => 0, 'patch' => 0, 'delete' => 0),
				'payload'   => null)
			,
			'data' => array()
		),
		'nav'   => array(
			'n' => false,
			'p' => 0,
			'l' => 10,
			's' => null,          // nav-sorting
            'f' => null,          // nav-field-sorting
            't' => 0,             // nav-total-rows
		),
		'bpm'   => array(
			'cls' => 'model',
			'prc' => null,
			'arr' => array(),       'str'   => '',          // qp : query-parameter
			'lim' => '',            'ord'   => null,
			'is_magic'  => false,
			'tbl' => null,
			'tbt' => null,
			'pky' => 'id',
			'pkt' => 1,
			'col' => null,
			'col_xtd' => null,
			'col_etc' => null,
			'col_man' => null,
			'has' => array(),
			'cfg' => array(
				'tbm' => true,
				'req' => array(),
				'rsp' => array()
			),
			'aaa' => array(
				'auth_a' => false,  // authentication action : 0 > none, 1 > get, 2 > crud
				'auth_t' => false,  // authentication type : basic | digest | password | oauth | oauth2_jwt | bearer | HOBA
				'auth_n' => false,  // authentication provider
				'auth_z' => false,  // authorization
				'auth_s' => false,   // signature
				'allow'  => array(),
				'audit'  => array())
		),
		'aux'   => array()
	);
	/*
	 * TODO
	 * 1. show menu after login
	 */
	public      function __construct() {
		parent::__construct();
		$this->benchmark->mark('code_start');
		$this->config->load('rfc', true);
		$this->config->load('jwt', true);
		
		$this->sys['cfg'] = $this->config->item('rfc');
		$this->jwt        = $this->config->item('jwt');
		$agent            = $this->agent->profile();
		
		$this->sys['req']['act'] = $this->input->method();
		$this->sys['req']['now'] = date('Y-m-d');
		$this->sys['req']['nts'] = time();
		$this->sys['event']      = array(
			'id'                => uuid(),
			'event_ts_in'       => time(),
			'sys_app_id'        => 1,
			'sys_ack_code_id'   => 0,
			'response_type'     => 'API',
			'aaa_account_id'    => 1,           // visitor
			'aaa_audit_trail_id' => $this->sys['req']['act'],
			'is_log'        => false,
			'url'           => $_SERVER['REQUEST_URI'],    'ip' => $this->input->ip_address(), 'computer_name' => gethostname(),
			'agent_name'    => $agent['name'],
			'agent_version' => $agent['version'],
			'agent_os'      => $agent['os'],    'agent_vendor'  => $agent['vendor'],
			'agent_type'    => $agent['type']
		);
		$this->__construct_cors();
	}
	private     function __construct_cors() {
		// Convert the config items into strings
		$allowed_headers = implode(', ', $this->sys['cfg']['ws_cors_headers']);
		$allowed_methods = implode(', ', $this->sys['cfg']['ws_cors_methods']);
		
		// If we want to allow any domain to access the API
		if ($this->sys['cfg']['ws_cors_domain'] === true) {
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Headers: '.$allowed_headers);
			header('Access-Control-Allow-Methods: '.$allowed_methods);
		}
		else {
			// We're going to allow only certain domains access
			// Store the HTTP Origin header
			$origin = $this->input->server('HTTP_ORIGIN');
			if ($origin === NULL) {
				$origin = '';
			}
			
			// If the origin domain is in the allowed_cors_origins list, then add the Access Control headers
			if (in_array($origin, $this->sys['cfg']['ws_cors_origins'])) {
				header('Access-Control-Allow-Origin: '.$origin);
				header('Access-Control-Allow-Headers: '.$allowed_headers);
				header('Access-Control-Allow-Methods: '.$allowed_methods);
			}
		}
		
		// If there are headers that should be forced in the CORS check, add them now
		if (is_array($this->sys['cfg']['ws_cors_headers'])) {
			foreach ($this->sys['cfg']['ws_cors_headers'] as $header => $value) {
				header($header . ': ' . $value);
			}
		}
		
		// If the request HTTP method is 'OPTIONS', kill the response and send it to the client
		if ($this->input->method() === 'options') {
			// Load DB if needed for logging
			if (!isset($this->rest->db) && $this->config->item('rest_enable_logging')) {
				$this->rest->db = $this->load->database($this->config->item('rest_database_group'), true);
			}
			exit;
		}
	}
	public      function __destruct() {
		unset($this->sys);
	}
	
	public      function index() {
		$this->rfc_req();
		$this->rfc_act();
		$this->rfc_rsp();
	}
	public      function indey() {
		if($this->argn < 2) {
			if ($this->agent->is_browser() || $this->agent->is_mobile() || $this->agent->is_robot()) {
				header("location: /docs");
			}
			else {
				$this->rfc_error('API__144'); // Incomplete url
				$this->rfc_rsp();
			}
		}
		else {
			$this->rfc_req();
			$this->rfc_act();
			$this->rfc_rsp();
		}
	}
	
	protected   function rfc_req() {
		// parse pre
		$this->rfc_req__pre();
		
		// parse api payload
		$this->rfc_req__parse_arg();
		
		// parse api-method
		$this->rfc_req__parse_act();
		
		// parse api authentication
		$this->rfc_req__parse_auth();
		
		// validate xtd
		$this->rfc_req__validate_xtd();
		
		// if format-output == html
		if($this->sys['req']['act'] == 'get') {
			switch($out = $this->sys['req']['out']) {
				case 'html':
					$tpl = strtolower($this->sys['req']['sch']) . '/' . $this->sys['req']['ver'] .'/' . $this->sys['req']['res'];
					$this->sys['req']['uix'] = "api/{$tpl}_get.tpl";
					break;
				
				case 'xls':
				case 'pdf':
					$cfg = $this->db->get_where('sys_process__svc_exim', array('sys_process_id' => $this->sys['req']['rid']))->row_array();
					if($cfg)
						$this->sys['svc']['exp'] = json_decode($cfg['export_doc'], true);
					break;
			}
		}
	}
	private     function rfc_req__pre() {
		$this->sys['req']['sch'] = 'weblink';
		$this->sys['req']['ver'] = '1.0';
		$this->sys['req']['res'] = $this->argc[0] ?? null;
		$this->sys['req']['dir'] = "/{$this->sys['req']['sch']}/{$this->sys['req']['ver']}";
		
		$reg = implode('|', $this->sys['cfg']['ws_out']);
		$ext = explode(',', '.' . implode(',.', $this->sys['cfg']['ws_out']));
		for ($i = 0; $i < $this->argn; $i++) {
			if (preg_match("/({$reg})$/", $this->argc[$i], $m)) {
				$this->sys['req']['out'] = $m[0];
				$this->argc[$i] = str_replace($ext, '', $this->argc[$i]);
				break;
			}
		}
		
		$lib = $this->sys['req']['dir'].DIRECTORY_SEPARATOR.strtoupper($this->sys['req']['sch']);
		$mod = $this->sys['req']['dir'].DIRECTORY_SEPARATOR.'M'.strtoupper($this->sys['req']['sch']);
		
		$this->load->library($lib, null, 'RFL');
		
		$this->load->unload('RFM');
		$this->load->model($mod, 'RFM');
		
		$this->RFL->sync($this->sys, $this->RFM);
		$this->RFM->sync($this->sys);
	}
	private     function rfc_req__prx() {
		// parse api schema
		$this->sys['req']['sch'] = $this->argc[0];
		
		// parse api version
		if(!is_numeric($this->argc[1])) {
			array_splice($this->argc, 1, 0, '1.0');
			$this->argn = count($this->argc);
		}
		$this->sys['req']['ver'] = $this->argc[1];
		
		// parse api resource
		$this->sys['req']['res'] = $this->argc[2];
		$this->sys['req']['dir'] = APP_DEF . "/{$this->sys['req']['sch']}/{$this->sys['req']['ver']}";
		
		// parse api output
		$reg = implode('|', $this->sys['cfg']['ws_out']);
		$ext = explode(',', '.' . implode(',.', $this->sys['cfg']['ws_out']));
		for ($i = 0; $i < $this->argn; $i++) {
			if (preg_match("/({$reg})$/", $this->argc[$i], $m)) {
				$this->sys['req']['out'] = $m[0];
				$this->argc[$i] = str_replace($ext, '', $this->argc[$i]);
				break;
			}
		}
		
		$dir = $this->rfc_dir_lib();
		$sch = $this->sys['req']['sch'];
		$ver = $this->sys['req']['ver'];
		
		if($dir) {
			if(!file_exists(APPPATH.'libraries'.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$ver)) {
				$this->rfc_error('API__120', '-> ' . $ver);    // Invalid API Version
			}
			elseif(!file_exists(APPPATH.'libraries'.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$ver.DIRECTORY_SEPARATOR.strtoupper($sch).'.php')) {
				$this->rfc_error('API__110', '-> ' . $sch);    // Invalid API Schema
			}
			else {
				$lib = $dir.DIRECTORY_SEPARATOR.$ver.DIRECTORY_SEPARATOR.strtoupper($sch);
				$mod = $dir.DIRECTORY_SEPARATOR.$ver.DIRECTORY_SEPARATOR.'M'.strtoupper($sch);
				
				$this->load->library($lib, null, 'RFL');

				$this->load->unload('RFM');
				$this->load->model($mod, 'RFM');
				
				$this->RFL->sync($this->sys, $this->RFM);
				$this->RFM->sync($this->sys);
			}
		}
		else
			$this->rfc_error('API__110', '-> '. $sch);    // Invalid API Schema -> registered in config RFC
	}
	private     function rfc_req__parse_arg() {
		// parse query-string and validate
		parse_str($_SERVER['QUERY_STRING'], $qs);
		if($qs) {
			foreach($qs as $k => $v) {
				// qs_key : q, p, l, s, d, f -- Reserve query-string: Question, Page, Limit, Sort, Debug, Flag
				switch($k) {
					case 'p':
					case 'l':
					case 's':
					case 'f':
						// Page, Limit, Field, Sort
						$this->sys['nav'][$k] = $v;
						break;
					case 'a':
						$this->sys['req']['aaa']['authentication'] = $v;
						break;
					default:
						// Question, Flag and others
						// prevent override qs to form-body
						if(!isset( $this->sys['req']['arg'][$k]))
							$this->sys['req']['arg'][$k] = $v;
						break;
				}
			}
			// re-map-arg
			if(isset($this->sys['req']['arg']['query'])) {
				$this->sys['req']['arg']['q'] = $this->sys['req']['arg']['query'];
				unset($this->sys['req']['arg']['query']);
			}
			
			// sanitasi qs-arg
			if($this->sys['req']['arg']) {
				foreach ($this->sys['req']['arg'] as $k=>$v) {
					if((string)$k == $v)
						unset($this->sys['req']['arg'][$k]);
				}
			}
		}
	}
	private     function rfc_req__parse_act() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			if (!in_array($this->sys['req']['act'], $this->sys['cfg']['ws_act'])) {
				$msg = strtoupper($this->sys['req']['act']) . ' not Implemented';
				$this->rfc_error('API__100', $msg);    // Invalid API Action
			}
		}
		
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$parser = 'rfc_req__parse_act_' . $this->sys['req']['act'];
			$this->$parser();
		}
	}
	private     function rfc_req__parse_act_get() {
		// parse query-string and validate
	}
	private     function rfc_req__parse_act_post() {
		$this->sys['req']['pst'] = $this->input->post(null, true);
		if(!$this->sys['req']['pst'])
			$this->sys['req']['pst'] = $_POST;
		
		if(!$this->sys['req']['pst']) {
			$jsn = null;
			if($pst = file_get_contents('php://input'))
				$jsn = json_decode($pst, true);

			$this->sys['req']['pst'] = $jsn ?: $pst;
		}
		
		if($_FILES) {
			foreach($_FILES as $f => $file)
				if(is_array($f)) {
					foreach($f as $k=>$v)
						$this->sys['req']['pst'][$f][] = $v['name'];
				}
				else
					$this->sys['req']['pst'][$f] = $file['name'];
		}
	}
	private     function rfc_req__parse_act_put() {
		$this->rfc_req__parse_patch();
	}
	private     function rfc_req__parse_act_patch() {
		$this->sys['req']['pst'] = $this->input->post(null, true);
		
		if(!$this->sys['req']['pst'])
			parse_str(file_get_contents('php://input'), $this->sys['req']['pst']);
		
		if(!is_array($this->sys['req']['pst']))
			$this->sys['req']['pst'] = $this->input->input_stream(null, true);
	}
	private     function rfc_req__parse_act_delete() {
		$this->sys['req']['pst'] = $this->input->input_stream(null, true);
	}
	
	private     function rfc_req__parse_auth() {
		$sch = $this->sys['req']['sch'];
		$aaa = $this->sys['cfg']['ws_aaa'];
		
		$a_token  = isset($aaa[$sch]) ? $aaa[$sch]['auth_token']  : $aaa['auth_token'];
		$s_token  = isset($aaa[$sch]) ? $aaa[$sch]['sign_token']  : $aaa['sign_token'];
		$a_method = isset($aaa[$sch]) ? $aaa[$sch]['auth_method'] : $aaa['auth_method'];
		
		if($a_token && empty($a_method))
			$a_method = 'oath2_jwt';
		
		foreach($acb = $this->input->request_headers() as $k => $v) {
			if(in_array($k, array($a_token, $s_token, 'dump', 'sdlc'))) {
				if($k == $a_token)
					$this->sys['req']['aaa']['authentication'] = $v;
				elseif($k == $s_token)
					$this->sys['req']['aaa']['signature'] = $v;
				else
					$this->sys['req']['aaa'][$k] = $v;
			}
		}
		
		switch($this->sys['req']['res']) {
			case 'login':
				break;
				
			default:
				switch($a_method) {
					case 'oath2_jwt':
						$this->rfc_req__parse_auth_oath2_jwt();
						break;
				}
				break;
		}
	}
	private     function rfc_req__parse_auth_oath2_jwt() {
		try {
			$aaa = $this->sys['cfg']['ws_aaa'];
			$sch = $this->sys['req']['sch'];
			$arg = isset($aaa[$sch]) ? $aaa[$sch]['auth_token'] : $aaa['auth_token'];
			$jwt = JWT::decode($this->sys['req']['aaa'][$arg], $this->jwt['key'], array($this->jwt['alg']));
			if ($jwt) {
				if ($usr = $this->RFM->aaa_user($jwt->usr)) {
					$this->sys['usr'] = $usr;
					if ($jwt->exp < time())
						$this->rfc_error('AAA__901'); // Token Expire
				}
				else
					$this->rfc_error('AAA__901');
			}
			else
				$this->rfc_error('AAA__901');
		}
		catch (\Exception $e) {
			$this->rfc_error('AAA__901');
		}
	}
	
	private     function rfc_req__validate_xtd() {
		// validate IP
		$this->rfc_req__validate_ip();
		
		// validate https
		$this->rfc_req__validate_https();
		
		// validate api-resource
		$this->rfc_req__validate_resource();
		
		// validate api-req-arg
		$this->rfc_req__validate_arg();
		$this->rfc_req__validate_pst();
		
		// validate todo
		// # validate limit
		// # validate HTTP_ACCEPT
		
		// validate mandatory header: authentication
		$this->rfc_req__validate_auth();
		
		// validate mandatory header: signature
		$this->rfc_req__validate_sign();
		
		// if (!$this->sys['rsp']['ack']['ack_code'])
		//	$this->RFM->sys_validate_key();
	}
	private     function rfc_req__validate_ip() {
		$whitelist = explode(',', $this->config->item('rest_ip_whitelist'));
		array_push($whitelist, '127.0.0.1', '0.0.0.0');
		
		foreach ($whitelist as &$ip) {
			// As $ip is a reference, trim leading and trailing whitespace, then store the new value
			// using the reference
			$ip = trim($ip);
		}
		
		if (in_array($this->input->ip_address(), $whitelist) === FALSE) {
			// $this->rfc_error('API__401');
		}
	}
	private     function rfc_req__validate_https() {
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['cfg']['ws_https']) {
			if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") || (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || is_https()) {
				// looking good, just continue
			}
			else {
				if (ENVIRONMENT != 'development')
					$this->rfc_error('API__011');    // Please use https for access this api
			}
		}
	}
	private     function rfc_req__validate_auth() {
		$auth = isset($this->sys['bpm']['aaa']['auth_a']) ? $this->sys['bpm']['aaa']['auth_a'] : 0;
		
		if(!$this->sys['rsp']['ack']['ack_code'] && $auth) {
			if($auth && !$this->sys['req']['aaa']['sdlc']) {
				if (empty($this->sys['req']['aaa']['authentication'])) {
					$this->rfc_error('AAA__910');           // Header must contain Authentication
				}
			}
		}
	}
	private     function rfc_req__validate_resource() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$res = $this->sys['req']['res'];
			if (empty($res))
				$this->rfc_error('API__130', 'Mandatory');    // Invalid API Resource, must present
			else {
				$this->RFM->sys_validate_res();
				
				if (empty($this->sys['bpm']))
					$this->rfc_error('API__131', '-> ' . $this->sys['req']['res']);    // Invalid API Resource
				elseif ($this->sys['rsp']['ack']['ack_code'])
					$this->rfc_error($this->sys['rsp']['ack']['ack_code']);
				else {
					if (in_array($this->sys['req']['act'], array('post', 'put', 'patch', 'delete'))) {
						$map = array('post' => 'ins', 'put' => 'upd', 'patch' => 'upd', 'delete' => 'del');
						$cmd = $this->sys['req']['act'];
						$alw = $map[$cmd];
						$this->sys['bpm']['aaa']['allow'][$alw] = 1;
						if (!$this->sys['bpm']['aaa']['allow'][$alw]) {
							switch ($this->sys['req']['act']) {
								case 'post':
									$this->rfc_error('AAA__102');
									break;
								case 'put':
								case 'patch':
									$this->rfc_error('AAA__103');
									break;
								case 'delete':
									$this->rfc_error('AAA__104');
									break;
							}
						}
					}
				}
			}
		}
	}
	private     function rfc_req__validate_arg() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			// validate arg for method get, put, patch dan delete
			if (in_array($this->sys['req']['act'], array('get', 'put', 'patch', 'delete'))) {
				$pk_key = isset($this->sys['bpm']['cfg']['pky']) ? $this->sys['bpm']['cfg']['pky'] : array('id');
				$pk_num = count($pk_key);
				
				if ($this->argn == 4) {
					array_splice($this->argc, 3, 0, $pk_key);
					$this->argn = count($this->argc);
				}
				
				if (in_array($this->sys['req']['act'], array('put', 'patch', 'delete'))) {
					if ($this->argn <= 3 || $this->argn != $pk_num + 4)
						$this->rfc_error('API__140');    // Invalid Primary-Key Argument for this Resource
				}
				
				// get api additional arg from url
				if (!$this->sys['rsp']['ack']['ack_code']) {
					for ($i = 3; $i < $this->argn; $i += 2) {
						if (!isset($this->argc[$i + 1]))
							$this->rfc_error('API__141');    // Bad URL Arguments Pair
						else {
							// set arg from url, but don't override
							if (!isset($this->sys['req']['arg'][$this->argc[$i]])) {
								$this->sys['req']['arg'][$this->argc[$i]] = $this->argc[$i + 1];
							}
						}
					}
				}
				
				if (!$this->sys['rsp']['ack']['ack_code']) {
					$cfg = isset($this->sys['bpm']['cfg']['req']) ? $this->sys['bpm']['cfg']['req'] : array();
					$arg = $this->sys['req']['arg'];
					unset($arg['callback'], $arg['_']);
					
					// check parameter input : unregister
					$not = array();
					foreach ($arg as $p => $v) {
						if(in_array($p, array('p', 'l', 's', 'f')))
							$this->sys['nav'][$p] = $v;
						elseif (!in_array($p, array('id', 'q')) && !in_array($p, array_keys($cfg)))
							$not[] = $p;
					}
					
					if ($not) {
						$msg = implode(', ', $not) . '. Check your documentation';
						$this->rfc_error('API__142', ': ' . $msg);   // Unregistered
					}
					
					// check parameter input : mandatory
					if (!$this->sys['rsp']['ack']['ack_code'] && $cfg) {
						$must = array();
						foreach ($cfg as $k => $v) {
							if (isset($v['mandatory']) && $v['mandatory'])
								$must[] = $k;

							// map parameter input
							if (isset($v['map']) && $v['map']) {
								if (isset($this->sys['req']['arg'][$k])) {
									if (isset($v['opr']) && $v['opr']) {
										switch ($v['opr']) {
											case 'like':
												$this->sys['req']['arg'][$v['map']] = "like '{$this->sys['req']['arg'][$k]}%'";
												break;
											case 'like_all':
												$this->sys['req']['arg'][$v['map']] = "like '%{$this->sys['req']['arg'][$k]}%'";
												break;
											default:
												$this->sys['req']['arg'][$v['map']] = "{$v['opr']} {$this->sys['req']['arg'][$k]}";
												break;
										}
									}
									else
										$this->sys['req']['arg'][$v['map']] = $this->sys['req']['arg'][$k];
									unset($this->sys['req']['arg'][$k]);
								}
							}
							else {
								if (isset($v['opr']) && $v['opr']) {
									switch ($v['opr']) {
										case 'like':
											$this->sys['req']['arg'][$k] = "like '{$this->sys['req']['arg'][$k]}%'";
											break;
										case 'like_all':
											$this->sys['req']['arg'][$k] = "like '%{$this->sys['req']['arg'][$k]}%'";
											break;
										default:
											$this->sys['req']['arg'][$k] = "{$v['opr']} {$this->sys['req']['arg'][$k]}";
											break;
									}
								}
							}
						}
						
						$fair = array_diff($must, array_keys($arg));
						if (!empty($fair)) {
							$msg = implode(', ', $fair) . ' must present';
							$this->rfc_error('API__143', ':' . $msg);
						}
						
						// map parameter input
						if (!$this->sys['rsp']['ack']['ack_code']) {
							foreach ($this->sys['bpm']['cfg']['req'] as $k => $v) {
							
							}
							
							// check pk_key exists
							if ($this->sys['req']['act'] == 'get' && $pk_key == array_keys($this->sys['req']['arg']))
								$this->sys['req']['frm'] = true;
						}
					}
				}
			}
		}
	}
	private     function rfc_req__validate_pst() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
	      // validate arg for method post, put, patch
			if (in_array($this->sys['req']['act'], array('post', 'put', 'patch')) && !$this->sys['req']['pst'])
				$this->rfc_error('API__145');   // Empty Payload
		}
		/*
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$man = explode(',', $this->sys['bpm']['col_man']);
			
			if ($man && in_array($this->sys['req']['act'], array('post', 'patch'))) {
				foreach($man as $k => $m) {
					if($ack = arr__isset_key($m, $this->sys['req']['pst'])) {
						unset($man[$k]);
					}
				}
				
				if($man) {
					$msg = implode(', ', $man) . '. Check your documentation';
					$this->rfc_error('API__146', ': ' . $msg);   // Mandatory Payload
				}
				
			}
		}
		*/
	}
	
	private     function rfc_req__validate_sign() {
		if(!$this->sys['rsp']['ack']['ack_code'] && $this->sys['bpm']['aaa']['auth_s']) {
			if (empty($this->sys['req']['aaa']['signature'])) {
				$this->rfc_error('AAA__911');    // Header must contain Signature
				unset($this->sys['usr']['aaa']['sign']);
			}
			else {
				/*
				 * implement signature
				 */
			}
		}
	}
	
	protected   function rfc_act() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$act = $this->sys['req']['act'];
			$prc = $this->sys['bpm']['prc'];
			
			switch ($this->sys['bpm']['cls']) {
				case 'controller':
					if($prc) {
						if (method_exists($this, $prc))
							$this->$prc();
						elseif (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ', unregistered');
					}
					else {
						$prc = $this->sys['req']['res'] . "__{$act}";
						if (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					break;
				
				case 'model':
				default:
					if($prc) {
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					else {
						$prc = $this->sys['req']['res'] . "__{$act}";
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else {
							$cmd = $act == 'get' ? 'sys_cmd__get': 'sys_cmd__aud';
							$this->RFM->$cmd();
						}
					}
					break;
			}
		}
	}
	private     function rfc_act__login() {
		$u = isset($this->sys['req']['pst']['user']) ? $this->sys['req']['pst']['user'] : '';
		$p = isset($this->sys['req']['pst']['pswd']) ? $this->sys['req']['pst']['pswd'] : '';
		if($u && $p) {
			$this->RFM->login($u, $p);
			if($this->sys['rsp']['data'] && !$this->sys['rsp']['ack']['ack_code']) {
				$jwt['usr'] = $this->sys['rsp']['data']['id_code'];
				$jwt['iat'] = $this->sys['req']['nts'];
				$jwt['exp'] = $this->sys['req']['nts'] + $this->jwt['tto'];
				$rsp = $this->sys['rsp']['data'];
				$this->sys['rsp']['data'] = array(
					'user'              => $rsp['id_code'],
					'name'              => $rsp['aaa_account'],
					"hrm_employee_id"   => null,
					"crm_office_id"     => null,
					"crm_officer_id"    => null,
					"crm_region_id"     => null,
					'token'             => JWT::encode($jwt, $this->jwt['key'])
				);
				$this->RFM->login_xtd();
				if(isset($rsp['crm_officer_id'])) {
					$this->sys['rsp']['data']['crm_officer_id'] = $rsp['crm_officer_id'];
					$this->sys['rsp']['data']['crm_region_id']  = $rsp['crm_region_id'];
				}
				if(isset($rsp['hrm_employee_id'])) {
					$this->sys['rsp']['data']['hrm_employee_id'] = $rsp['hrm_employee_id'];
					$this->sys['rsp']['data']['crm_office_id']   = $rsp['crm_office_id'];
				}
			}
		}
		else {
			$m = array('Are you kidding me ?',
			           'Are you a hacker ?',
			           'Oh, come on ...',
			           'Hey, You ! ... Get me some coffee first',
			           'Come on, ... earn your money',
			           'Hey kid, don\'t play with daddy\'s computer'
			);
			$this->rfc_error('AAA__1', '. '. $m[array_rand($m)]);
		}
	}
	private     function rfc_act__cpwd() {}
	private     function rfc_act__fpwd() {}
	private     function rfc_act__rpwd() {}
	
	// -- tobe delete
	protected   function xxx_act__get() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$act = $this->sys['req']['act'];
			$prc = $this->sys['bpm']['prc'];
			switch ($this->sys['bpm']['cls']) {
				case 'controller':
					if($prc) {
						if (method_exists($this, $prc))
							$this->$prc();
						elseif (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ', unregistered');
					}
					else {
						$prc = $this->sys['req']['res'] . '__get';
						if (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					break;
				
				case 'model':
				default:
					if($prc) {
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					else {
						$prc = $this->sys['req']['res'] . '__get';
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else
							$this->RFM->sys_cmd__get();
					}
					break;
			}
		}
	}
	protected   function xxx_act__aud() {
		if(!$this->sys['rsp']['ack']['ack_code']) {
			$act = $this->sys['req']['act'];
			$prc = $this->sys['bpm']['prc'];
			
			switch ($this->sys['bpm']['cls']) {
				case 'controller':
					if($prc) {
						if (method_exists($this, $prc))
							$this->$prc();
						elseif (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ', unregistered');
					}
					else {
						$prc = $this->sys['req']['res'] . "__{$act}";
						if (method_exists($this->RFL, $prc))
							$this->RFL->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					break;
				
				case 'model':
				default:
					if($prc) {
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else
							$this->rfc_error('API__132', ", $prc is Unregistered");
					}
					else {
						$prc = $this->sys['req']['res'] . "__{$act}";
						if (method_exists($this->RFM, $prc))
							$this->RFM->$prc();
						else {
							$this->RFM->sys_cmd__aud();
						}
					}
					break;
			}
		}
	}
	
	protected   function rfc_rsp() {
		if($dump = $this->sys['req']['aaa']['dump']) {
			$j    = $dump == 'req' ?
					json_encode($this->sys['req']['pst'], JSON_PRETTY_PRINT || JSON_NUMERIC_CHECK) :
					json_encode($this->sys['rsp']['data'], JSON_PRETTY_PRINT || JSON_NUMERIC_CHECK);
			return $this->output->set_content_type('application/json')
			                    ->set_status_header($this->http_code)
			                    ->set_output($j);
			die();
		}
		
		$this->benchmark->mark('code_end');
		$this->sys['rsp']['meta']['elapsed_time'] = $this->benchmark->elapsed_time('code_start', 'code_end', 4) . ' ms';
		$this->sys['rsp']['meta']['memory_usage'] = sys_mem_usage();
		
		$this->output->parse_exec_vars = false;
		
		if($this->sys['rsp']['ack']['ack_code']) {
			unset($this->sys['rsp']['meta'], $this->sys['rsp']['data']);
			return $this->output->set_content_type('application/json')
			                    ->set_status_header($this->http_code)
			                    ->set_output(json_encode($this->sys['rsp']['ack'],JSON_PRETTY_PRINT |  JSON_NUMERIC_CHECK));
		}
		else {
			$ack_ok = array('aux' => 200, 'get' => 200, 'post' => 201, 'patch' => 201, 'delete' => 204);
			$this->http_code = $ack_ok[$this->sys['req']['act']];

			switch ($this->sys['req']['act']) {
				case 'post':
				case 'put':
				case 'patch':
				case 'delete':
					if(empty($this->sys['rsp']['data']))
						unset($this->sys['rsp']['data']);
					break;
				
				case 'get':
					if (empty($this->sys['rsp']['data']) && empty($this->sys['rsp']['meta']['payload'])) {
						$this->sys['rsp']['ack']['ack_message'] = 'Data not Found';
					}
					else
						$this->rfc_rsp__get();
					break;
			}
			$this->rfc_rsp__out();
		}
		
		if($this->sys['req']['dbg'])
			$this->rfc_log($this->sys);
	}
	private     function rfc_rsp__out() {
		// dispatch response
		switch($this->sys['req']['out']) {
			case 'json':
			case 'grp':
				if($this->sys['req']['sch']  == 'sys')
					unset($this->sys['rsp']['meta']['privilege']);
				else {
					$this->sys['rsp']['meta']['privilege']['post']   = 1;
					$this->sys['rsp']['meta']['privilege']['patch']  = 1;
					$this->sys['rsp']['meta']['privilege']['delete'] = 1;
					/*
					$this->sys['rsp']['meta']['privilege']['post']   = $this->sys['bpm']['aaa']['allow']['ins'];
					$this->sys['rsp']['meta']['privilege']['patch']  = $this->sys['bpm']['aaa']['allow']['upd'];
					$this->sys['rsp']['meta']['privilege']['delete'] = $this->sys['bpm']['aaa']['allow']['del'];
					*/
				}
				
				if(empty($this->sys['rsp']['meta']['payload'])) {
					if(isset($this->sys['bpm']['cfg']['meta']))
						$this->sys['rsp']['meta']['payload'] = $this->sys['bpm']['cfg']['meta'];
					else
						if($this->sys['req']['act'] == 'get') {
							if ($this->sys['req']['frm']) {
								$this->sys['rsp']['meta']['payload'] = array(
									'data' => array(
										'type'    => 'form',
										'title'   => $this->sys['bpm']['sys_process'],
										'element' => count($this->sys['rsp']['data'])
									)
								);
							}
							else {
								$this->sys['rsp']['meta']['payload'] = array(
									'data' => array(
										'type'  => 'table',
										'title' => $this->sys['bpm']['sys_process'],
										'rows'  => count($this->sys['rsp']['data']),
										'page'  => $this->sys['nav']['p'] ? $this->sys['nav']['p'] : 1
									)
								);
							}
						}
						else {
							if(isset($this->sys['rsp']['data'])) {
								if (!isset($this->sys['rsp']['meta']['payload']['data']['title']))
									$this->sys['rsp']['meta']['payload']['data']['title'] = $this->sys['bpm']['sys_process'];
								if (!isset($this->sys['rsp']['meta']['payload']['data']['element']))
									$this->sys['rsp']['meta']['payload']['data']['element'] = count($this->sys['rsp']['data']);
							}
						}
				}
				return $this->output->set_content_type('application/json')
				                    ->set_status_header($this->http_code)
				                    ->set_output(json_encode($this->sys['rsp'],JSON_PRETTY_PRINT |  JSON_NUMERIC_CHECK));
				break;
			
			case 'xml':
				$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><response></response>');
				arr2xml($this->sys['rsp'], $xml);
				return $this->output->set_content_type('text/xml', 'utf-8')
				                    ->set_status_header($this->http_code)
				                    ->set_output($xml->asXML());
				break;
			
			case 'csv':
				if(isset($this->sys['rsp']['data']))
					echo arr2csv($this->sys['rsp']['data']);
				else
					echo 'Data not Found';
				break;
			
			case 'opt':
			case 'opt.tree':
				unset($this->sys['rsp']['ack'], $this->sys['rsp']['ack']);
				$rsp = '<option value=""></option>';
				if(!empty($this->sys['rsp']['data']))
					foreach($this->sys['rsp']['data'] as $k => $v) {
						$disabled   = !empty($v['disabled']) ? ' disabled'   : false;
						$class      = !empty($v['class'])    ? $v['class']   : '';
						$indent     = !empty($v['indent'])   ? 'tree-'.$v['indent']  : '';
						$icon       = !empty($v['icon'])     ? $v['icon']    : false;
						$title      = !empty($v['title'])    ? $v['title']   : false;
						$subtext    = !empty($v['subtext'])  ? $v['subtext'] : false;
						$content    = !empty($v['content'])  ? $v['content'] : false;
						$rsp .= "<option value='{$v['id']}'";
						$rsp .= $title   ? " title='$title'" : '';
						if($indent || $class)
							$rsp .= " class='$indent $class'";
						$rsp .= $icon    ? " data-icon='$icon'" : '';
						$rsp .= $subtext ? " data-subtext='$subtext'" : '';
						$rsp .= $content ? " data-content='$content'" : '';
						$rsp .= $disabled;
						$rsp .= ">{$v['name']}</option>";
					}
				echo $rsp;
				break;
			
			case 'opt.grp':
				$rsp = '<option value=""></option>';
				foreach($this->sys['rsp']['data'] as $grp => $opt){
					$rsp .= "<optgroup label='{$grp}'>";
					foreach($opt as $k => $v){
						$disabled   = !empty($v['disabled']) ? ' disabled'   : false;
						$class      = !empty($v['class'])    ? $v['class']   : '';
						$icon       = !empty($v['icon'])     ? $v['icon']    : false;
						$title      = !empty($v['title'])    ? $v['title']   : false;
						$subtext    = !empty($v['subtext'])  ? $v['subtext'] : false;
						$content    = !empty($v['content'])  ? $v['content'] : false;
						$rsp .= "<option value='{$v['id']}'";
						$rsp .= $title   ? " title='$title'" : '';
						if($class)
							$rsp .= " class='$class'";
						$rsp .= $icon    ? " data-icon='$icon'" : '';
						$rsp .= $subtext ? " data-subtext='$subtext'" : '';
						$rsp .= $content ? " data-content='$content'" : '';
						$rsp .= $disabled;
						$rsp .= ">{$v['name']}</option>";
					}
					$rsp .=  "</optgroup>";
				}
				echo $rsp;
				break;
			
			case 'html':
				$this->smarty->template_dir = "__view";
				$this->smarty->compile_dir  = "__tmp";
				$this->smarty->assign('response', $this->sys['rsp']['data']);
				$this->smarty->display($this->sys['req']['uix']);
				break;
			
			case 'xls':
				$this->rfc_rsp__xls();
				break;
			
			case 'pdf':
				$this->rfc_rsp__pdf();
				break;
				
			case 'custom':
				return $this->output->set_content_type('application/json')
					->set_status_header($this->http_code)
					->set_output(json_encode($this->sys['cst'],JSON_PRETTY_PRINT |  JSON_NUMERIC_CHECK));
				break;
				
			case 'echo':
				break;
		}
	}
	private     function rfc_rsp__get() {
		$new = array();
		
		if($this->sys['req']['frm']) {
			if(isset($this->sys['rsp']['data']) && is_array($this->sys['rsp']['data']) && isset($this->sys['rsp']['data'][0])) {
				$dat = $this->sys['rsp']['data'][0];
				$rsp = isset($this->sys['bpm']['cfg']['rsp']) ? $this->sys['bpm']['cfg']['rsp'] : array();
				if ($dat && $rsp) {
					foreach ($dat as $k => $r) {
						foreach ($rsp as $i => $f) {
							if (isset($dat[$f]))
								$new[$i] = $dat[$f];
						}
					}
				}
				$this->sys['rsp']['data'] = $new ? $new : $dat;
			}
		}
		else {
			if(isset($this->sys['bpm']['cfg']['rsp']) && $this->sys['bpm']['cfg']['rsp']) {
				if(is_array($this->sys['rsp']['data']))
					foreach($this->sys['rsp']['data'] as $k=>$v) {
						foreach ($this->sys['bpm']['cfg']['rsp'] as $i => $n) {
							if (isset($v[$n]))
								$new[$k][$i] = $v[$n];
						}
					}
			}
			if($new)
				$this->sys['rsp']['data'] = $new;
		}
	}
	private     function rfc_rsp__xls() {}
	private     function rfc_rsp__pdf() {
		if($this->sys['svc']['exp']['lib_name']) {
			$dir = $this->sys['svc']['exp']['lib_path'];
			$lib = $this->sys['svc']['exp']['lib_name'];
			$fnc = $this->sys['svc']['exp']['lib_func'];
			
			/*
			$this->smarty = new Smarty();
			$this->smarty->assign('sys', $this->sys);
			$this->smarty->display($this->sys['svc']['exp']['tpl']);
			*/
			
			$this->load->library($dir.$lib, $this->sys, 'pdf');
			$file = $this->pdf->create();
			
			switch($this->sys['svc']['exp']['rsp']) {
				case 'pdf':
					// force download
					break;
				case 'link':
					$this->sys['rsp']['data']   = array(
						'file_pdf' => $this->sys['cfg']['ws_svr']['pdf']['svr'] . '/' . $file
					);
					$inc = array('ack', 'meta', 'data');
					unset($this->sys['rsp']['meta']['payload']);
					foreach($this->sys['rsp'] as $k => $v) {
						if(!in_array($k, $inc))
							unset($this->sys['rsp'][$k]);
					}
					// $this->rfc_rsp__out();
					
					return $this->output->set_content_type('application/json')
					                    ->set_status_header($this->http_code)
					                    ->set_output(json_encode($this->sys['rsp'],JSON_PRETTY_PRINT |  JSON_NUMERIC_CHECK));
					break;
			}
		}
	}
	private     function rfc_rsp__tcpdf() {
		$pdf = new SVC_EXP__TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set header and footer fonts
		
		// set font
		$pdf->SetFont('helvetica', 'B', 20);
		
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('helvetica', '', 8);
		$pdf->writeHTML('halo', true, false, false, false, '');
		
		$path = $this->sys['svc']['exp']['dir'];
		
		if (!is_dir($path) && !@mkdir($path, 0777, true))
			die('failed to create sub-directory: ' . $path);
		
		$file = "/{$path}{$this->sys['svc']['exp']['out']}";
		$file = FCPATH ."tmp/{$this->sys['svc']['exp']['out']}";
		
		$pdf->Output($file, 'I');
	}
	private     function rfc_rsp__mpdf() {
		$mpdf   = new \Mpdf\Mpdf();
		$header = '';
		$letter = '';
		
		// Write some HTML code:
		$mpdf->WriteHTML($header);
		$mpdf->WriteHTML($letter);
		
		// Output a PDF file directly to the browser
		//$mpdf->Output();
		$mpdf->Output('pub/filename.pdf', \Mpdf\Output\Destination::FILE);
	}
	protected   function rfc_error($err, $msg='', $http_code=null) {
		$ack = $this->RFM->sys_error($err);
		$this->sys['rsp']['ack']['ack_code']    = $err;
		$this->sys['rsp']['ack']['ack_message'] = $msg ? $ack['ack_message'] . '. ' . $msg : $ack['ack_message'];
		$this->http_code                        = $http_code ? $http_code : $ack['http_code'];
	}
	
	private     function rfc_dir_lib() {
		return arr__search_path(strtolower($this->sys['req']['sch']), $this->sys['cfg']['ws_sch']);
	}
	private     function rfc_log($var, $f='w') {
		$sch = $this->sys['req']['sch'];
		$dir = $this->sys['cfg']['ws_dir'][$sch]['app'];
		$fp  = fopen($dir .'/logs/log-var.txt', $f);
		if($fp) {
			fwrite($fp, date('Y-m-d H:i:s'));
			fwrite($fp, PHP_EOL);
			if (is_array($var))
				fwrite($fp, json_encode($var, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK));
			else
				fwrite($fp, $var);
			fwrite($fp, PHP_EOL);
			fwrite($fp, PHP_EOL);
			fclose($fp);
		}
	}
	
	protected   function token_request_aaa() {
		$time = time();
		$user = $this->db->get_where('aaa_account', array('id_code' => $this->post['cpid']))->row_array();
		
		$auth = array('user' => false, 'authorization' => false);
		if($user && isset($user['id_code']) && $user['id_code'] == $this->post['cpid'])  {
			$auth['user'] = true;
			if (password_verify($this->post['cpassword'], $user['credential'])) {
				$this->rest->user_id = $user['id'];
				$payload['usr']      = $this->post['cpid'];
				$payload['iat']      = $time;
				$payload['exp']      = $time + $this->jwt['tto'];
				$auth['authorization']	= JWT::encode($payload, $this->jwt['key']);
				
				$this->token_updated($user['id'], $auth['authorization']);
			}
		}
		return $auth;
	}
	private     function token_updated($user, $key) {
		$this->db->update('sys_process_api_keys', $key, array('aaa_account_id' => $user));
	}
	
	protected   function cmd_ref_opt() {}
	
	private     function _parsePut() {
		global $_PUT;
		
		/* PUT data comes in on the stdin stream */
		$putdata = fopen("php://input", "r");
		
		/* Open a file for writing */
		// $fp = fopen("myputfile.ext", "w");
		
		$raw_data = '';
		
		/* Read the data 1 KB at a time
		   and write to the file */
		while ($chunk = fread($putdata, 1024))
			$raw_data .= $chunk;
		
		/* Close the streams */
		fclose($putdata);
		
		// Fetch content and determine boundary
		$boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));
		
		if(empty($boundary)){
			parse_str($raw_data,$data);
			$GLOBALS[ '_PUT' ] = $data;
			return;
		}
		
		// Fetch each part
		$parts = array_slice(explode($boundary, $raw_data), 1);
		$data = array();
		
		foreach ($parts as $part) {
			// If this is the last part, break
			if ($part == "--\r\n") break;
			
			// Separate content from headers
			$part = ltrim($part, "\r\n");
			list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);
			
			// Parse the headers list
			$raw_headers = explode("\r\n", $raw_headers);
			$headers = array();
			foreach ($raw_headers as $header) {
				list($name, $value) = explode(':', $header);
				$headers[strtolower($name)] = ltrim($value, ' ');
			}
			
			// Parse the Content-Disposition to get the field name, etc.
			if (isset($headers['content-disposition'])) {
				$filename = null;
				$tmp_name = null;
				preg_match(
					'/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
					$headers['content-disposition'],
					$matches
				);
				list(, $type, $name) = $matches;
				
				//Parse File
				if( isset($matches[4]) )
				{
					//if labeled the same as previous, skip
					if( isset( $_FILES[ $matches[ 2 ] ] ) )
					{
						continue;
					}
					
					//get filename
					$filename = $matches[4];
					
					//get tmp name
					$filename_parts = pathinfo( $filename );
					$tmp_name = tempnam( ini_get('upload_tmp_dir'), $filename_parts['filename']);
					
					//populate $_FILES with information, size may be off in multibyte situation
					$_FILES[ $matches[ 2 ] ] = array(
						'error'=>0,
						'name'=>$filename,
						'tmp_name'=>$tmp_name,
						'size'=>strlen( $body ),
						'type'=>$value
					);
					
					//place in temporary directory
					file_put_contents($tmp_name, $body);
				}
				//Parse Field
				else
				{
					$data[$name] = substr($body, 0, strlen($body) - 2);
				}
			}
			
		}
		$GLOBALS[ '_PUT' ] = $data;
		return $data;
	}
}