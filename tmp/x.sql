-- ----------------------------
-- view structure for qry_ppo_agent
-- ----------------------------
drop view if exists qry_ppo_agent;
create view qry_ppo_agent
select    a.*, b.ppo_agent_class, c.ppo_rebat, d.ppo_agent as ppo_agent_upline
from      ppo_agent as a
join      ppo_agent_class	b on b.id = a.ppo_agent_class_id  and a.is_active = 1
left join ppo_rebat		    c on c.id = a.ppo_rebat_id        and c.is_active = 1
left join ppo_agent		    d on d.id = a.pid                 and d.is_active = 1
order by a.id asc;
