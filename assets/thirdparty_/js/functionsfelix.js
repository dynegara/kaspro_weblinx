$(function(){
	$(".settingsButton,.uibutton").button();
	$('#dialog').dialog({
		autoOpen: false,
		width: 700,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}
		},
		draggable: false,
		resizable: false
	});
	
	$('.ui-button').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);
	
	$("#pDOB, #pIDExpiry, #regExpiryDate, #DofIncorporation").datepicker({
		changeMonth:true, changeYear:true,
		maxDate: '',
		yearRange: '-100:+20',
		dateFormat: 'yy-mm-dd'
	});
	// Register Account
	$("#regDob").datepicker({
		//showOn: 'button',
		//buttonImage: 'assets/thirdparty/images/calendar.gif',
		//buttonImageOnly: true, 
		changeMonth:true, changeYear:true,
		maxDate: new Date(),
		yearRange: '-100:+20',
		dateFormat: 'yy-mm-dd',
		onSelect: function (date) {
			 var dob = new Date(date);
			 var today = new Date();
			 var age = today.getFullYear() - dob.getFullYear();
			 (age>22) ? $('#tblKin').hide() : $('#tblKin').show();
		}
	});
	// END Register Account
	
	// Statement
	$("#historyFrom" ).datepicker({
		changeMonth: true, changeYear:true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#historyTO" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$("#historyTO" ).datepicker({
		changeMonth: true, changeYear:true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#historyFrom" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	// END Statement
	
	// Personal Details
	$('#btnEditPInfo').click(function (){
		//alert('Btn Edit info');	
		$('.personalInfo :input ').attr('disabled', false);
		$('#btnUpdatePInfo').show();
		$('#btnCancelPInfo').show();
		$(this).hide();
	});
	$('#btnUpdatePInfo').click(function (){
		$('.personalInfo :input ').attr('disabled', true);
		$('#btnEditPInfo').show();
		$('#btnCancelPInfo').hide();
		$(this).hide();
	});
	$('#btnCancelPInfo').click(function (){
		$('.personalInfo :input ').attr('disabled', true);
		$('#btnEditPInfo').show();
		$('#btnUpdatePInfo').hide();
		$(this).hide();
	});
	// END Personal Details
	
});
