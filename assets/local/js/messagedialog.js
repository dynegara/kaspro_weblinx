
function messageDialog(title,msg)
{
title = title==null ? '' :title;


$("#msg-dialog-container").append('<div id="dialog-message1" title="'+title+'" style="display:none"><p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0px 7px 0px 0;"></span>'+msg+'</p></div>');


$( "#dialog-message1" ).dialog({
      resizable: false,
      modal: true,
      width: 300,
      height: 'auto',
       show: {
        effect: "fade",
        duration: 1000
      },
      open:function(event,ui){

         setTimeout(function(){
          $("#dialog-message1").parent().find('.ui-dialog-buttonpane').find('button').eq(0).focus();
        }, 1);  
      },
   
      buttons: {
        Ok: function() {
          $(this).dialog("close");
          $("#dialog-message1").dialog('destroy');
          $("#msg-dialog-container").empty();
        }
      }
  });
$('.ui-dialog-titlebar-close').html('X')
  
   
      
}

function messageDialogInvalid(title,msg,invalidId)
{
title = title==null ? '' :title;
invalidId = invalidId==null ? '':invalidId;

$("#msg-dialog-container").append('<div id="dialog-message1" title="'+title+'" style="display:none"><p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 0px 0;"></span>'+msg+'</p></div>');


$( "#dialog-message1" ).dialog({
      resizable: false,
      modal: true,
      width: 300,
      height: 'auto',
       show: {
        effect: "fade",
        duration: 1000
      },
      open:function(event,ui){
         setTimeout(function(){
          $("#dialog-message1").parent().find('.ui-dialog-buttonpane').find('button').eq(0).focus();
        }, 1);  
      },
   
      buttons: {
        Ok: function() {
          $(this).dialog("close");
         
          $("#"+invalidId).focus();
          $("#"+invalidId).addClass('ui-state-error');

          setTimeout(function () {

              $("#"+invalidId).removeClass('ui-state-error');
               $("#dialog-message1").dialog('destroy');
              $("#msg-dialog-container").empty();

          }, 3000);
          
        }
      }
  });
  
   
      
}
function messageDialogModal(title,msg)
{
   $('#messagealertmodal').html(msg);
   $('#messagedialogmodal').modal('show');
}

function messageDialogModal(title,msg,invalidId)
{
   $('#messagealertmodal').html(msg);
   $('#messagedialogmodal').modal('show');

   $('#btnmessagedialogmodal').click(function(){
          $("#"+invalidId).focus();
          $("#"+invalidId).addClass('ui-state-error');

          setTimeout(function () {

              $("#"+invalidId).removeClass('ui-state-error');
               $("#dialog-message1").dialog('destroy');
              $("#msg-dialog-container").empty();

          }, 3000);
   });
}
function bayadpocLoading(loadingEvent)
{

   if(loadingEvent=="show"){

   $('#bayadpocloadingmodal').modal('show');

 }else{

   $('#bayadpocloadingmodal').modal('hide');
 }
}

     


