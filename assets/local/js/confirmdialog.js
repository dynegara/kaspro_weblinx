


function confirmDialog(obj){
/*
obj.msg  = the message to appear in dialog
obj.title = the title of confirmation dialog
obj.url = the url to be passed to ajax
obj.transID = the id to be passed to database
obj.callback = the callback to call after ajax success
obj.btnName = the callback to call after ajax success
*/

if(isOBJempty(obj)[0]){

		 console.log('Confirmation dialog Error Missing Parameter '+isOBJempty(obj)[1]+'');
		 emptyOBJ();
	return false;
}
	else{

 

	$('body').append('<div id="confirmation-dialog" title="'+obj.title+'" style="display:none"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+obj.msg+'</p></div>')

var btn =obj.btnName;

 $( "#confirmation-dialog").dialog({
      resizable: false,
      modal: true,
      width: 300,
      height: 'auto',
      autoOpen:false,
      
      buttons: [{
                                id:"dialog-btn",
                                text: obj.btnName,
                                click: function() {
                                        $(this).dialog("close");
                                         $("#confirmation-dialog").remove();
                                        ajaxCall(obj.url,obj.callback,obj.transID);


                                }
                        },{
                                id:"dialog-btn-cancel",
                                text: "Cancel",
                                click: function() {
                                        $(this).dialog("close");
                                        $("#confirmation-dialog").remove();
                                        
                                }
                        }]
    });





$("#confirmation-dialog").dialog('open');  
}

} 


function ajaxCall(url,callback,id){
	  emptyOBJ();

   $.ajax({
      url: url,
      type: 'post',
      data: {'ID':id},
      dataType: 'json',
      success: function (json) {

        if(json.Code.toString()=='00')
        {

           window.hatch.forms[callback](json);
           emptyOBJ();

        $("#confirmation-dialog").remove();
            
        }
        else
        {
        	
          messageDialog('Server Message',json.Message);
           $("#confirmation-dialog").remove();
           emptyOBJ();

        }
	},
       error: function(e) {
        console.log(e);
        
       }

    });



}

function isOBJempty(obj){
	var empty = false;
	var emptyval = '';
    Object.keys(obj).forEach(function(key) {
     if(obj[key]==undefined  || obj[key]==null || obj[key]=='')
       {
       	empty=true; 
       	emptyval=key;
       	return false;
       }    
    });
  // return empty;

   return [empty,emptyval];

}


function emptyOBJ(){


window.confirmDialogObj.callback = '';
window.confirmDialogObj.msg = '';
window.confirmDialogObj.title = '';
window.confirmDialogObj.url='';
window.confirmDialogObj.transID = '';//ID FOR EVERY TRANSACTION
window.confirmDialogObj.btnName = '';  

}