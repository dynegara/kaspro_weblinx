window.previousDate = undefined;
window.sessionInterval = 5;


function dateDiff(date1, date2, interval) {
	var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
	var timediff = date2 - date1;
	if (isNaN(timediff)) return NaN;
	switch (interval) {
		case "years":
			return date2.getFullYear() - date1.getFullYear();
		case "months":
			return (
				(date2.getFullYear() * 12 + date2.getMonth())
				-
				(date1.getFullYear() * 12 + date1.getMonth())
			);
		case "weeks"  :
			return Math.floor(timediff / week);
		case "days"   :
			return Math.floor(timediff / day);
		case "hours"  :
			return Math.floor(timediff / hour);
		case "minutes":
			return Math.floor(timediff / minute);
		case "seconds":
			return Math.floor(timediff / second);
		default:
			return undefined;
	}
};
$(document).ready(function () {
	$(document).bind('mousemove', function () {
		if (window.previousDate != undefined) {
			var idle = dateDiff(window.previousDate, new Date(), 'seconds');
			if (idle > window.sessionInterval) {

				$.ajax({
					url: window.checkSessionUrl,
					method: 'POST',
					dataType: 'json',
					success: window.sessionCallback,
					error: function (error) {
						console.log(error);
						//alert("You have been logged out");
						//location.reload();
					}
				});

			}
		}
		window.previousDate = new Date();
	});
});
