$(function () {

	$.is_string = function (s) {
		return typeof (s) === 'string' || s instanceof String;
	}
	$.hmac_signature = function (data, key) {
		var keys = [];
		var tosign = 'empty|';
		var ret = '';
		if (data instanceof FormData) {
			for (k of data.keys()) {
				keys.push(k);
			}
		} else {
			for (k in data) {
				if (data.hasOwnProperty(k)) {
					keys.push(k);
				}
			}
		}

		keys.sort();
		for (i = 0; i < keys.length; i++) {
			k = keys[i];
			var d = data instanceof FormData ? data.get(k) : data[k];
			if ($.is_string(d))
				tosign += d + "|";
		}
		ret = CryptoJS.HmacSHA1(tosign, key) + '';
		return ret;
	}
	//IF HTML5 storage is not supported, please ask user to upgrade
	var oajax = $.ajax;
	var sess = window.sessionStorage;
	if (typeof (sess) == 'undefined') {
		alert('Browser not supported. Please update to latest version');
	}
	$.ajax = function (params) {

		if (!params.hasOwnProperty('data')) {
			params.data = {};
		}

		var hashkey = $.md5(params.url);
		var nonce = sess.getItem(hashkey) != null ? sess.getItem(hashkey) : sess.getItem("__nonce__");
		var sign = '';
		sign = $.hmac_signature(params.data, nonce);
		if (params.data instanceof FormData) {
			params.data.append('hash', sign);
		} else {
			params.data.hash = sign;
		}

		ret = oajax.apply($, Array.prototype.slice.apply(arguments));
		ret.success(function (json) {
			oajax({
				url: window.baseurl + 'myaccount/form/session/nonce',
				type: 'post',
				data: {
					hashkey: $.md5(params.url)
				},
				dataType: 'json',
				success: function (json) {
					if (json.Code == '00') {
						sess.setItem($.md5(params.url), json.Message);
					}
				}
			});
		});
		ret.complete(function () {
			return ret;
		});
		return ret;
	}
});