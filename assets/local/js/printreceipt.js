    var img = new Image();
    var img2 = new Image();
    var base = $("#imagebase").val();
    img.src = base+'/assets/thirdparty/images/payureceipt.png';
    img2.src = base+'/assets/thirdparty/images/payureceipt.png';
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $(document).on('click', '#savepdfcbp', function () {
        var doc = new jsPDF();
        var d1 = moment().format("YYYY-MM-DD");
        var message = $('#messagecontainercbp').html();
        var str = message.replace("\t","");
        var res = str.split("\n");
        var yLine = 20;
        var xLine = 20;
        console.log(res.length);
        doc.addImage(img, 'png', 20, 5, 20,10);
        doc.addFont('monospace', 'normal');
        doc.setFont('monospace');
        doc.setFontSize(10);
        for(var a=0;a<res.length;a++){
            yLine += 5;
            if(res[a] == res[0]){
                doc.setFontType("bold");
                doc.text(xLine,yLine,res[a]);
            } else if (res[a].indexOf('<cbci-logo>') >= 0){
                res[a] = res[a].replace('<cbci-logo>', '');
            console.log("cbci");
            doc.addImage(img2, 'png', 180, 5, 9, 11);
            } else {
                doc.setFontType("normal");
                doc.text(xLine,yLine,res[a]);
            }
        }
        doc.save('BillsPayment'+d1+'.pdf');
    });
    $(document).on('click', '#printcbp', function () {
        $('#messagecontainercbp').print();
    });