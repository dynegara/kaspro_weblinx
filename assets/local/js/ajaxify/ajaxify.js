$(function(){
 

        $('*[data-ajax=true]  *.ajax-form-button').click(function(e){
          

                $(this).focus();
                var  btn = $(this);

                loadingid = btn.attr("loadingid");

                e.preventDefault();
              
                showLoading(loadingid);

                btn.attr('disabled','disabled');

                var arr = {};
                var items ={};
                var itm_obj = new Object();

                var cls = $(this).attr('form-data').toString();
                var method = $('form[data-ajax=true]').attr('method');
                 var callback_val = $(this).attr('callback');
                 callback_val = callback_val.replace(/-/g,'_');               

                $('form[data-ajax=true]  *.'+cls+'').each(function(){

                          var name = ($(this).attr('name')!=null) ? $(this).attr('name') : '';
                          var value = ($(this).val() !=null) ? $(this).val() : '';


                          if($(this).is('input[type=button]'))
                          {
                            // do nothing with the button
                            return false;
                          }

                          else if($(this).is('input[type=radio]:checked'))
                          {
                            arr[name] = value;
                            return true;
                          } 

                          else if($(this).is('select'))
                          {                       
                            arr[name] = value;
                            return true;
                          }

                          else if($(this).is('input[type=checkbox]'))
                          {
                              if($(this).is(':checked'))
                              {
                                items.push($(this).val());
                              }
                          }

                          else if ($(this).is('input[type=text]'))
                          {   
                              arr[name] = value;
                              return true;
                          }

                          else if($(this).is(':hidden'))
                          {
                              arr[name] = value;
                              return true;
                          }

                          else if ($(this).is('textarea'))
                          {
                              arr[name] = value;
                              return true;
                          }

                          else if ($(this).is('input[type=password]'))
                          {
                              arr[name] = value;
                              return true;
                          }
                          else{}
                });                 

                //arr["items"] = items;
                //alert(JSON.stringify(arr)); //temporary lng i2


                btn.attr('disabled','disabled');
                var _url = $(this).attr('url');

                $.ajax({
                        url: _url,
                        type: method,
                        data: arr,
                        dataType: 'json',
                        success: function (json) {

                              btn.removeAttr('disabled');
                                if(json.Code.toString()=='00')
                                { 
                                   hideLoading(loadingid);
                                   window.hatch.forms[callback_val](json);
                                }
                                else
                                {

                                hideLoading(loadingid);
                                messageDialogModal("Server Message",json.Message,false);


                                }
                        

                        }, error: function(e) {
                              console.log(e); 
                              btn.removeAttr('disabled');
                        }
                });
      });
});

// Edited: 6/2/15 for loading icons 
function showLoading(id){

  $("#"+id).show();
  $("."+id).show();

}


function hideLoading(id){
  $("#"+id).hide();
  $("."+id).hide();
}




