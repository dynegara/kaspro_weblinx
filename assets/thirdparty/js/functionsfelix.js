$(function(){
	$(".settingsButton,.uibutton").button();
	$('#dialog').dialog({
		autoOpen: false,
		width: 700,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}
		},
		draggable: false,
		resizable: false
	});
	
	$('.ui-button').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);
	
	$("#pDOB, #pIDExpiry, #regExpiryDate, #DofIncorporation").datepicker({
		changeMonth:true, changeYear:true,
		maxDate: '',
		yearRange: '-100:+20',
		dateFormat: 'yy-mm-dd'
	});
	// Register Account
	$("#regDob").datepicker({
		//showOn: 'button',
		//buttonImage: 'assets/thirdparty/images/calendar.gif',
		//buttonImageOnly: true, 
		changeMonth:true, changeYear:true,
		maxDate: new Date(),
		yearRange: '-100:+20',
		dateFormat: 'yy-mm-dd',
		onSelect: function (date) {
			 var dob = new Date(date);
			 var today = new Date();
			 var age = today.getFullYear() - dob.getFullYear();
			 (age>22) ? $('#tblKin').hide() : $('#tblKin').show();
		}
	});
	// END Register Account
	
	// Statement
	$("#historyFrom" ).datepicker({
		changeMonth: true, changeYear:true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#historyTO" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$("#historyFrom").datepicker( "option", "maxDate", new Date());
	$("#historyTO" ).datepicker({
		changeMonth: true, changeYear:true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#historyFrom" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	$("#historyTO").datepicker( "option", "maxDate", new Date());
	// END Statement
	//Transaction history
	 $("#transhistoryfrom" ).datepicker({
            changeMonth: true, changeYear:true,
            dateFormat: 'yy-mm-dd',
            onSelect: function( selectedDate ) {
                  $( "#transhistoryto" ).datepicker( "option", "minDate", selectedDate );
            }
      });
	 $("#transhistoryfrom").datepicker( "option", "maxDate", new Date());
      $("#transhistoryto" ).datepicker({
            changeMonth: true, changeYear:true,
            dateFormat: 'yy-mm-dd',
            onSelect: function( selectedDate ) {
			$( "#transhistoryfrom" ).datepicker( "option", "maxDate", selectedDate );
		}
      });
      $("#transhistoryto").datepicker( "option", "maxDate", new Date());
     //end transaction history

     //TRANSACTION REPORTS
     $("#transreportsfrom" ).datepicker({
            changeMonth: true, changeYear:true,
            dateFormat: 'yy-mm-dd',
            onSelect: function( selectedDate ) {
                   var datefrom = new Date(selectedDate);
                    var max = new Date();
                  $( "#transreportsto" ).datepicker( "option", "minDate", selectedDate );
                  $("#transreportsto").datepicker( "option", "maxDate", max );
            }
      });
     $("#transreportsfrom").datepicker( "option", "maxDate", new Date());
      $("#transreportsto" ).datepicker({
            changeMonth: true, changeYear:true,
            dateFormat: 'yy-mm-dd',
            onSelect: function( selectedDate ) {
			$( "#transreportsfrom" ).datepicker( "option", "maxDate", selectedDate );
		}
      });
      $("#transreportsto").datepicker( "option", "maxDate", new Date());
      //END TRANSACTION REPORTS
	
	// Personal Details
	$('#btnEditPInfo').click(function (){
		//alert('Btn Edit info');	
		$('.personalInfo :input ').attr('disabled', false);
		$('#btnUpdatePInfo').show();
		$('#btnCancelPInfo').show();
		$(this).hide();
	});
	
	$('#btnCancelPInfo').click(function (){
		$('.personalInfo :input ').attr('disabled', true);
		$('#btnEditPInfo').show();
		$('#btnUpdatePInfo').hide();
		$(this).hide();
	});
	// END Personal Details
	
});
