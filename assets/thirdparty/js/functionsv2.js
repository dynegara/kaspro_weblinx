$(function(){
	$(".settingsButton,.uibutton").button();
	
	$("#regDob").datepicker({
		//duration:''
		// window.imgPath + 
		showOn: 'button', buttonImage: 'assets/thirdparty/images/calendar.gif', buttonImageOnly: true, 
		changeMonth:true, changeYear:true,
		maxDate: new Date(),
		yearRange: '-100:+20',
		dateFormat: 'yy-mm-dd'
	});
	
		$('#dialog').dialog({
		autoOpen: false,
		width: 700,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}
		},
		draggable: false,
		resizable: false
	});
	
	$('.ui-button').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);

	
});