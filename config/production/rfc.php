<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// list of dir-tree
$config['ws_dir'] = array(
	'weblink' => array(
		'app' => '/var/www/weblink/public_html'
	)
);

$httpt = 'WLu28cXFYvrdtQ7KFNxDUI3hpufmj+EbNknAEL9i7pfdjx69s/lnu3YSScaxUv+7Iere9Or5f1AvNC3rO8l+U3gkcU87vUrlHu6llGJeZiolpM2mD1ZePTlPyjVrArkmlK5Ui8vnGmu55anh2jq2Y4KD9HIj2FI8ENzfFqPX3/vmVH2e8ImkxsDuK1Ot+oH6BVxUKThhqcVPFfv3Qe52AA==';
$token = 'XHR6b6MRyzVA7jMs1bggPyB49BcamTkuHMRJsXfRsVbVTXrzTSvZtiYPHYODM1LYeolCIfgTYswm9ySJGcVg/nyeE+7mwP6Pze2IXpZaaXRUdxTjjn+AVBw9OyZqnnefiMuS/nIxjwQ7nBbJWmQEul5dEBBAXVCm5OgaVATlu4iSXDfLgmnTD103jeqi6N4qml9PQ3vALuVO4K8s2fJt/HGP5SDNOuYQo3D94/aSAHCncpPmdzsUqt/TPFsfMDAj';

$config['ws_svr'] = array(
	'weblink' => array(
		'app' => 'https://weblink.kaspro.id',
		'api' => 'https://weblink.kaspro.id',
		'key' => 'KbPeShVmYq3t6v9y$B&E)H@McQfTjWnZ'
	),
	'kaspro' => array(
		'api'   => 'https://api.kaspro.id/bIcELQIzfk',
		'opt'   => array(
			CURLOPT_HTTPHEADER  => array(
				'Content-Type: application/json',
				'token: ' . $token
			),
			CURLOPT_SSLCERT     => APPPATH . 'cert/homecredit/homecredit2.pem',
			CURLOPT_SSLKEY      => APPPATH . 'cert/homecredit/homecredit2.pem'
		)
	),
	'client' => array(
		'svr'   => 'https://portal.homecredit.co.id/ewallet-service/v1/pin/callback',
		'usr'   => 'HCI_KASPRO',
		'pwd'   => 'HCI_KASPRO'
	)
);

if(!preg_match('/https/', $config['ws_svr']['kaspro']['api'])) {
	$config['ws_svr']['kaspro']['opt'] = array(
		CURLOPT_HTTPHEADER => array(
			'token: ' . $httpt
		)
	);
}

$config['ws_sch'] = array(
    'weblink'
);

$config['ws_admin'] = array(
	'qc.officer'    => 'kaspro@w3b.L1nx',
	'qc.admin'      => 'kaspro!w3b.Pr0d'
);

$config['ws_ver'] = array('1.0');
$config['ws_act'] = array('get', 'post', 'put', 'patch', 'delete');
$config['ws_out'] = array('json', 'csv', 'xml', 'opt.grp', 'opt.tree', 'opt', 'grp', 'xlsx', 'xls', 'pdf', 'html');
$config['ws_aaa'] = array(
	'auth_token'    => true, 'sign_token' => true, 'auth_method' => null,
	'weblink'       => array('auth_token' => false, 'sign_token' => false, 'auth_method' => null),
);
$config['ws_dbs'] = array(
	'weblink'   => array('sys' => 'iop_sys',        'tim' => 'iop_sys'),
);
$config['ws_prc'] = array(
	'weblink'   => array('sys' => 'iop_sys',        'tim' => 'iop_sys'),
);
/*
 * auth-method : basic | digest access | jwt | bearer
 */
/*
|--------------------------------------------------------------------------
| Security
|--------------------------------------------------------------------------
*/
$config['ws_https']     = false;
$config['ws_cors']      = false;
$config['ws_cors_headers'] = array(
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Access-Control-Request-Method'
);
$config['ws_cors_methods'] = array(
    'GET',
    'POST',
    'OPTIONS',
    'PUT',
    'PATCH',
    'DELETE'
);

/*
|--------------------------------------------------------------------------
| CORS Allow Any Domain
|--------------------------------------------------------------------------
|
| Set to TRUE to enable Cross-Origin Resource Sharing (CORS) from any
| source domain
|
*/
$config['ws_cors_domain'] = false;

/*
|--------------------------------------------------------------------------
| CORS Allowable Domains
|--------------------------------------------------------------------------
|
| Used if $config['check_cors'] is set to TRUE and $config['allow_any_cors_domain']
| is set to FALSE. Set all the allowable domains within the array
|
| e.g. $config['allowed_origins'] = ['http://www.example.com', 'https://spa.example.com']
|
*/
$config['ws_cors_origins'] = [];

/*
|--------------------------------------------------------------------------
| CORS Forced Headers
|--------------------------------------------------------------------------
|
| If using CORS checks, always include the headers and values specified here
| in the OPTIONS client preflight.
| Example:
| $config['forced_cors_headers'] = [
|   'Access-Control-Allow-Credentials' => 'true'
| ];
|
| Added because of how Sencha Ext JS framework requires the header
| Access-Control-Allow-Credentials to be set to true to allow the use of
| credentials in the REST Proxy.
| See documentation here:
| http://docs.sencha.com/extjs/6.5.2/classic/Ext.data.proxy.Rest.html#cfg-withCredentials
|
*/
$config['ws_cors_headers'] = [];
